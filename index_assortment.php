<h2>Ассортимент</h2>

<div class="grid page-subsection">

	<a class="range-item grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12" href="/catalog/sukhie-stroitelnye-smesi/shtukaturka/">
		<picture>
			<source srcset="/images/plaster.webp, /images/plaster@2x.webp 2x" type="image/webp"/>
			<source srcset="/images/plaster.jpg, /images/plaster@2x.jpg 2x"/>
			<img class="range-item__image" src="/images/plaster.jpg" loading="lazy" decoding="async" alt="Штукатурки" title="Штукатурки" lazyload="true"/>
		</picture>
		<div class="range-item__name">Штукатурки</div>
	</a>

	<a class="range-item grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12" href="/catalog/sukhie-stroitelnye-smesi/klei/">
		<picture>
			<source srcset="/images/glue.webp, /images/glue@2x.webp 2x" type="image/webp"/>
			<source srcset="/images/glue.jpg, /images/glue@2x.jpg 2x"/>
			<img class="range-item__image" src="/images/glue.jpg" loading="lazy" decoding="async" alt="Клеи" title="Клеи" lazyload="true"/>
		</picture>
		<div class="range-item__name">Клеи</div>
	</a>

	<a class="range-item grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12" href="/catalog/sukhie-stroitelnye-smesi/kladochnaya-smes/">
		<picture>
			<source srcset="/images/brickwork.webp, /images/brickwork@2x.webp 2x" type="image/webp"/>
			<source srcset="/images/brickwork.jpg, /images/brickwork@2x.jpg 2x"/>
			<img class="range-item__image" src="/images/brickwork.jpg" loading="lazy" decoding="async" alt="Кладочная смесь" title="Кладочная смесь" lazyload="true"/>
		</picture>
		<div class="range-item__name">Кладочная смесь</div>
	</a>

	<a class="range-item grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12" href="/catalog/sukhie-stroitelnye-smesi/cps/">
		<picture>
			<source srcset="/images/cement.webp, /images/cement@2x.webp 2x" type="image/webp"/>
			<source srcset="/images/cement.jpg, /images/cement@2x.jpg 2x"/>
			<img class="range-item__image" src="/images/cement.jpg" loading="lazy" decoding="async" alt="ЦПС" title="ЦПС" lazyload="true"/>
		</picture>
		<div class="range-item__name">ЦПС</div>
	</a>

	<? /* <a class="range-item grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12" href="/catalog/suhoi-pesok/">
		<picture>
			<source srcset="/images/dry-sand.webp, /images/dry-sand@2x.webp 2x" type="image/webp"/>
			<source srcset="/images/dry-sand.jpg, /images/dry-sand@2x.jpg 2x"/>
			<img class="range-item__image" src="/images/dry-sand.jpg" loading="lazy" decoding="async" alt="Сухой песок" title="Сухой песок" lazyload="true"/>
		</picture>
		<div class="range-item__name">Сухой песок</div>
	</a> */?> 

</div>
