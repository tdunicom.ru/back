<section class="page-section container home-header">
	<div class="content">

		<div class="home-header__inner">


			<h1 class="home-header__title">Сухие строительные смеси оптом и&nbsp;в&nbsp;розницу по&nbsp;ценам производителя</h1>

			<p class="home-header__description">Получите бесплатную консультацию</p>

			<form class="home-header__form">

                <input type="text" name="req_name">

                <label class="form-input form-input--tel home-header__phone">
                    <input class="form-input__field" type="tel" name="phone" required placeholder="+7 (000) 000-00-00">
                </label>

                <button class="button home-header__submit" type="submit">Подобрать смесь</button>

                <input type="hidden" name="action" value="callback">

			</form>

		</div>

	</div>
</section>



