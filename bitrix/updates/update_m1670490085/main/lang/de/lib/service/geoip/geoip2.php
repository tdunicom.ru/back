<?php
$MESS["geoip_geoip2_desc"] = "Bestimmt das Land und die Stadt aufgrund von lokalen Daten GeoIP2 binary MMDB. Die Datenbanken GeoIP2 und GeoLite2 werden unterstützt.<br>Website: <a href=\"https://www.maxmind.com\">https://www.maxmind.com</a>";
$MESS["geoip_geoip2_err_reading"] = "Fehler beim Lesen der Datenbankdatei.";
$MESS["geoip_geoip2_file"] = "Absoluter Pfad zur Datenbankdatei (*.mmdb)";
$MESS["geoip_geoip2_file_not_found"] = "Datenbankdatei wurde nicht gefunden.";
$MESS["geoip_geoip2_no_file"] = "Pfad zur Datenbankdatei ist nicht angegeben.";
$MESS["geoip_geoip2_type"] = "Datenbanktyp";
