<?php
$MESS["ACTION"] = "Aktionen";
$MESS["ACTIVE"] = "Aktiv";
$MESS["ADD_LANG"] = "Neue Sprache";
$MESS["ADD_LANG_TITLE"] = "Neue Sprache";
$MESS["CHANGE"] = "Ändern";
$MESS["CHANGE_HINT"] = "Spracheinstellungen bearbeiten";
$MESS["CONFIRM_DEL"] = "Wollen Sie diese Sprache wirklich löschen?";
$MESS["COPY"] = "Kopie hinzufügen";
$MESS["COPY_HINT"] = "Spracheinstellungen kopieren";
$MESS["DEF"] = "Standard";
$MESS["DELETE"] = "Löschen";
$MESS["DELETE_ERROR"] = "Fehler beim Löschen der Sprachversion. Möglicherweise besteht Vebindung zu anderen Objekten.";
$MESS["DELETE_HINT"] = "Sprachversion löschen";
$MESS["DIR"] = "Ordner";
$MESS["LANG_EDIT_TITLE"] = "Sprache bearbeiten";
$MESS["MAIN_ADMIN_LIST_ACTIVATE"] = "aktivieren";
$MESS["MAIN_ADMIN_LIST_DEACTIVATE"] = "deaktivieren";
$MESS["NAME"] = "Überschrift";
$MESS["PAGES"] = "Sprachen";
$MESS["SAVE"] = "Änderungen speichern";
$MESS["SAVE_ERROR"] = "Fehler beim Speichern der Sprachversion";
$MESS["SORT"] = "Sort.";
$MESS["TITLE"] = "Sprachen";
$MESS["lang_admin_code"] = "ID";
