<?php
$MESS["main_user_devices_history_app_pass"] = "Passwort der Anwendung";
$MESS["main_user_devices_history_city"] = "Stadt";
$MESS["main_user_devices_history_city_id"] = "Stadtcode";
$MESS["main_user_devices_history_country"] = "Land";
$MESS["main_user_devices_history_country_id"] = "Landescode";
$MESS["main_user_devices_history_date"] = "Zuletzt eingeloggt am";
$MESS["main_user_devices_history_device_id"] = "Gerät-ID";
$MESS["main_user_devices_history_hash_pass"] = "Einmalpasswort";
$MESS["main_user_devices_history_ip"] = "IP-Adresse";
$MESS["main_user_devices_history_page"] = "Seite";
$MESS["main_user_devices_history_region"] = "Region";
$MESS["main_user_devices_history_region_id"] = "Regioncode";
$MESS["main_user_devices_history_stored_pass"] = "Gespeichertes Passwort";
$MESS["main_user_devices_history_title"] = "Login-History";
