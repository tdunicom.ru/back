<?php
$MESS["ACTIVE"] = "Aktiv:";
$MESS["ADD"] = "Neue Sprache";
$MESS["CHARSET"] = "Codierung:";
$MESS["DAY_OF_WEEK_0"] = "Sonntag";
$MESS["DAY_OF_WEEK_1"] = "Montag";
$MESS["DAY_OF_WEEK_2"] = "Dienstag";
$MESS["DAY_OF_WEEK_3"] = "Mittwoch";
$MESS["DAY_OF_WEEK_4"] = "Donnerstag";
$MESS["DAY_OF_WEEK_5"] = "Freitag";
$MESS["DAY_OF_WEEK_6"] = "Samstag";
$MESS["DEF"] = "Standard:";
$MESS["DIR"] = "Seitenverzeichnis:";
$MESS["DIRECTION"] = "Leserichtung:";
$MESS["DIRECTION_LTR"] = "Von Links nach Rechts";
$MESS["DIRECTION_RTL"] = "Von Rechts nach Links";
$MESS["EDIT_LANG_TITLE"] = "Sprachversion \"#ID#\" bearbeiten";
$MESS["ERROR_DELETE"] = "Fehler beim Löschen der Sprachversion. Möglicherweise besteht Vebindung zu anderen Objekten.";
$MESS["FORMAT_DATE"] = "Datumsformat:";
$MESS["FORMAT_DATETIME"] = "Zeitformat:";
$MESS["FORMAT_NAME"] = "Namensformat:";
$MESS["LANG_EDIT_WEEK_START"] = "Erster Wochentag:";
$MESS["LANG_EDIT_WEEK_START_DEFAULT"] = "1";
$MESS["MAIN_COPY_RECORD"] = "Kopie hinzufügen";
$MESS["MAIN_COPY_RECORD_TITLE"] = "Sprache kopieren";
$MESS["MAIN_DELETE_RECORD"] = "Sprache löschen";
$MESS["MAIN_DELETE_RECORD_CONF"] = "Wollen Sie diese Sprache wirklich löschen?";
$MESS["MAIN_DELETE_RECORD_TITLE"] = "Aktuelle Sprache löschen";
$MESS["MAIN_ERROR_SAVING"] = "Beim Speichern ist ein Fehler aufgetreten";
$MESS["MAIN_NEW_RECORD"] = "Neue Sprache";
$MESS["MAIN_NEW_RECORD_TITLE"] = "Neue Sprache";
$MESS["MAIN_PARAM"] = "Parameter";
$MESS["MAIN_PARAM_TITLE"] = "Systemsprache-Parameter";
$MESS["NAME"] = "Überschrift:";
$MESS["NEW_LANG_TITLE"] = "Neue Sprache";
$MESS["RECORD_LIST"] = "Sprachversionen";
$MESS["RECORD_LIST_TITLE"] = "Sprachen";
$MESS["RESET"] = "Zurücksetzen";
$MESS["SAVE"] = "Änderungen speichern";
$MESS["SORT"] = "Sortierung:";
$MESS["lang_edit_code"] = "Sprachcode (IETF BCP 47):";
$MESS["lang_edit_culture"] = "Regionale Einstellungen:";
$MESS["lang_edit_culture_edit"] = "Ausgewählte Optionen bearbeiten:";
