<?php

if ($updater->CanUpdateDatabase())
{
	if (!$updater->TableExists('b_user_device'))
	{
		$updater->Query("
			CREATE TABLE b_user_device
			(
				ID bigint unsigned not null auto_increment,
				USER_ID bigint unsigned not null,
				DEVICE_UID varchar(50) not null,
				DEVICE_TYPE int unsigned not null default 0,
				BROWSER varchar(100),
				PLATFORM varchar(25),
				USER_AGENT varchar(1000),
				COOKABLE char(1) not null default 'N',
				PRIMARY KEY(ID),
				INDEX ix_user_device_user(USER_ID, DEVICE_UID)
			)
		");
	}

	if (!$updater->TableExists('b_user_device_login'))
	{
		$updater->Query("
			CREATE TABLE b_user_device_login
			(
				ID bigint unsigned not null auto_increment,
				DEVICE_ID bigint unsigned not null,
				LOGIN_DATE datetime,
				IP varchar(20),
				CITY_GEOID bigint,
				REGION_GEOID bigint,
				COUNTRY_ISO_CODE varchar(10),
				APP_PASSWORD_ID bigint unsigned,
				STORED_AUTH_ID bigint unsigned,
				HIT_AUTH_ID bigint unsigned,
				PRIMARY KEY(ID),
				INDEX ix_user_device_login_device(DEVICE_ID),
				INDEX ix_user_device_login_date(LOGIN_DATE)
			);
		");
	}

	if (!$DB->Query("SELECT CODE FROM b_language WHERE 1=0", true))
	{
		$updater->Query("ALTER TABLE b_language ADD COLUMN CODE varchar(35)");
	}

	$languages = [
		'ru' => 'ru',
		'en' => 'en',
		'de' => 'de',
		'ua' => 'uk',
		'la' => 'es',
		'br' => 'pt-BR',
		'fr' => 'fr',
		'pl' => 'pl',
		'it' => 'it',
		'ja' => 'ja',
		'sc' => 'zh-CN',
		'tc' => 'zh-TW',
		'tr' => 'tr',
		'vn' => 'vi',
		'id' => 'id-ID',
		'ms' => 'ms-MY',
		'th' => 'th',
	];

	foreach ($languages as $lid => $code)
	{
		$DB->Query("update b_language set CODE = '{$code}' where LID = '{$lid}'");
	}

	if (!$updater->TableExists('b_geoname'))
	{
		$updater->Query("
			CREATE TABLE b_geoname
			(
				ID bigint unsigned not null,
				LANGUAGE_CODE varchar(35),
				NAME varchar(600),
				PRIMARY KEY(ID, LANGUAGE_CODE)
			);
		");
	}

	if (!$DB->IndexExists('b_agent', ['ACTIVE', 'IS_PERIOD', 'NEXT_EXEC'], true))
	{
		$updater->Query("alter table b_agent add index ix_agent_act_period_next_exec(ACTIVE, IS_PERIOD, NEXT_EXEC)");
	}

	$updater->Query("ALTER TABLE b_culture MODIFY COLUMN DAY_MONTH_FORMAT varchar(50) null default 'F j'");

	if (!\Bitrix\Main\Service\GeoIp\HandlerTable::getList(['filter' => ['=CLASS_NAME' => '\\Bitrix\\Main\\Service\\GeoIp\\GeoIP2']])->fetch())
	{
		\Bitrix\Main\Service\GeoIp\HandlerTable::add(array('SORT' => 80, 'ACTIVE' => 'Y', 'CLASS_NAME' => '\\Bitrix\\Main\\Service\\GeoIp\\GeoIP2'));
	}

	$languages = [];
	$langs = \CLanguage::getList();
	while($lang = $langs->fetch())
	{
		$languages[] = $lang['LID'];
	}

	$types = [
		"NEW_DEVICE_LOGIN" => [
			"EVENT_TYPE" => 'email',
			"NAME" => "MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN",
			"DESCRIPTION" => "MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN_DESC",
			"SUBJECT" => "MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN_SUBJECT",
			"MESSAGE" => "MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN",
		],
	];

	foreach($types as $eventName => $eventType)
	{
		$res = $DB->query("SELECT 'x' FROM b_event_type WHERE EVENT_NAME = '{$eventName}'");
		if (!($res->fetch()))
		{
			global $MESS;

			foreach($languages as $lid)
			{
				$defLang = \Bitrix\Main\Localization\Loc::getDefaultLang($lid);

				if (file_exists($_SERVER['DOCUMENT_ROOT'].$updater->curPath.'/lang/'.$defLang.'/install/index.php'))
					include $_SERVER['DOCUMENT_ROOT'].$updater->curPath.'/lang/'.$defLang.'/install/index.php';

				if (file_exists($_SERVER['DOCUMENT_ROOT'].$updater->curPath.'/lang/'.$lid.'/install/index.php'))
					include $_SERVER['DOCUMENT_ROOT'].$updater->curPath.'/lang/'.$lid.'/install/index.php';

				$event = array(
					'LID' => $lid,
					'EVENT_NAME' => $eventName,
					'EVENT_TYPE' => $eventType["EVENT_TYPE"],
					'NAME' => $DB->ForSQL(getMessage($eventType["NAME"])),
					'DESCRIPTION' => $DB->ForSQL(getMessage($eventType["DESCRIPTION"])),
				);

				$DB->Query("insert into b_event_type(LID, EVENT_NAME, EVENT_TYPE, NAME, DESCRIPTION) values (
					'{$event['LID']}',
					'{$event['EVENT_NAME']}',
					'{$event['EVENT_TYPE']}',
					'{$event['NAME']}',
					'{$event['DESCRIPTION']}'
				)");

				$sitesIds = [];
				$sites = \CSite::getList('', '', array('LANGUAGE_ID' => $lid));
				while($item = $sites->fetch())
				{
					$sitesIds[] = $item['LID'];
				}

				if(!empty($sitesIds))
				{
					//email template
					$emailTemplate = [
						'EVENT_NAME' => $eventName,
						'LID' => end($sitesIds),
						'LANGUAGE_ID' => $lid,
						'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
						'EMAIL_TO' => '#EMAIL#',
						'SUBJECT' => GetMessage($eventType["SUBJECT"]),
						'MESSAGE' => GetMessage($eventType["MESSAGE"]),
					];

					$result = \Bitrix\Main\Mail\Internal\EventMessageTable::add($emailTemplate);
					if($result->isSuccess())
					{
						foreach ($sitesIds as $siteId)
						{
							\Bitrix\Main\Mail\Internal\EventMessageSiteTable::add(array(
								'EVENT_MESSAGE_ID' => $result->getId(),
								'SITE_ID' => $siteId,
							));
						}
					}
				}
			}
		}
	}

	$res = $DB->Query("select ENTITY_ID from b_user_field where MULTIPLE = 'Y' group by ENTITY_ID");
	while($field = $res->Fetch())
	{
		$entity_id = strtolower($field["ENTITY_ID"]);
		if($DB->TableExists("b_utm_".$entity_id))
		{
			while (($name = $DB->GetIndexName("b_utm_".$entity_id, ["FIELD_ID", "VALUE_INT", "VALUE_ID"], true)) != '')
			{
				$DB->Query("alter table b_utm_".$entity_id." drop index {$name}", true);
			}
			if(!$DB->IndexExists("b_utm_".$entity_id, ["FIELD_ID", "VALUE_ID", "VALUE_INT"], true))
			{
				$DB->Query("alter table b_utm_".$entity_id." add index ix_utm_".$field["ENTITY_ID"]."_4(FIELD_ID, VALUE_ID, VALUE_INT)", true);
			}
		}
	}

	if ($updater->TableExists('b_user') && $updater->TableExists('b_user_index'))
	{
		\Bitrix\Main\Config\Option::delete('main', ['name' => 'main_index_user']);
		\Bitrix\Main\Config\Option::delete('main.stepper.main', ['name' => 'Bitrix\Main\Update\UserStepper']);
		\Bitrix\Main\Update\Stepper::bindClass('Bitrix\Main\Update\UserStepper', 'main');
	}

	\CAgent::RemoveAgent('Bitrix\Main\Update\UserSelectorStepper::execAgent();', 'main');
}

$updater->CopyFiles("install/admin", "admin");
$updater->CopyFiles("install/bitrix", "");
$updater->CopyFiles("install/components", "components");
$updater->CopyFiles("install/js", "js");
$updater->CopyFiles("install/css", "css");
$updater->CopyFiles("install/panel", "panel");
$updater->CopyFiles("install/themes", "themes");
$updater->CopyFiles("install/gadgets", "gadgets");
$updater->CopyFiles("install/images", "images");

if ($updater->CanUpdateKernel())
{
	$arToDelete = array(
		"modules/main/classes/mysql/main.php",
		"modules/main/lib/context/site.php",
		"modules/main/lib/service/geoip/dataresult.php",
		"modules/main/classes/mysql/quota.php",
	);
	foreach ($arToDelete as $file)
	{
		CUpdateSystem::DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"].$updater->kernelPath."/".$file);
	}
}
