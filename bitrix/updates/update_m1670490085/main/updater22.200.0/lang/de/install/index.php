<?php
$MESS["MAIN_ADMIN_GROUP_DESC"] = "Voller Zugriff zur Seitenverwaltung.";
$MESS["MAIN_ADMIN_GROUP_NAME"] = "Administratoren";
$MESS["MAIN_DEFAULT_LANGUAGE_AM_VALUE"] = "am";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_MONTH_FORMAT"] = "j. F";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_OF_WEEK_MONTH_FORMAT"] = "l, j. F";
$MESS["MAIN_DEFAULT_LANGUAGE_DAY_SHORT_MONTH_FORMAT"] = "j. M";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_CHARSET"] = "iso-8859-1";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_DATE"] = "DD.MM.YYYY";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_DATETIME"] = "DD.MM.YYYY HH:MI:SS";
$MESS["MAIN_DEFAULT_LANGUAGE_FORMAT_NAME"] = "#NAME# #LAST_NAME#";
$MESS["MAIN_DEFAULT_LANGUAGE_FULL_DATE_FORMAT"] = "l, j. F Y";
$MESS["MAIN_DEFAULT_LANGUAGE_LONG_DATE_FORMAT"] = "j. F Y";
$MESS["MAIN_DEFAULT_LANGUAGE_LONG_TIME_FORMAT"] = "H:i:s";
$MESS["MAIN_DEFAULT_LANGUAGE_MEDIUM_DATE_FORMAT"] = "j. M Y";
$MESS["MAIN_DEFAULT_LANGUAGE_NAME"] = "German";
$MESS["MAIN_DEFAULT_LANGUAGE_NUMBER_DECIMAL_SEPARATOR"] = ",";
$MESS["MAIN_DEFAULT_LANGUAGE_NUMBER_THOUSANDS_SEPARATOR"] = ".";
$MESS["MAIN_DEFAULT_LANGUAGE_PM_VALUE"] = "pm";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DATE_FORMAT"] = "d.m.Y";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DAY_OF_WEEK_MONTH_FORMAT"] = "D, j. F";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_DAY_OF_WEEK_SHORT_MONTH_FORMAT"] = "D, j. M";
$MESS["MAIN_DEFAULT_LANGUAGE_SHORT_TIME_FORMAT"] = "H:i";
$MESS["MAIN_DEFAULT_SITE_FORMAT_CHARSET"] = "iso-8859-1";
$MESS["MAIN_DEFAULT_SITE_FORMAT_DATE"] = "DD.MM.YYYY";
$MESS["MAIN_DEFAULT_SITE_FORMAT_DATETIME"] = "DD.MM.YYYY HH:MI:SS";
$MESS["MAIN_DEFAULT_SITE_FORMAT_NAME"] = "#NAME# #LAST_NAME#";
$MESS["MAIN_DEFAULT_SITE_NAME"] = "Standardseite";
$MESS["MAIN_DESKTOP_CREATEDBY_KEY"] = "Website erstellt von";
$MESS["MAIN_DESKTOP_CREATEDBY_VALUE"] = "Unternehmen &laquo;Bitrix24&raquo;.";
$MESS["MAIN_DESKTOP_EMAIL_KEY"] = "E-Mail";
$MESS["MAIN_DESKTOP_EMAIL_VALUE"] = "<a href=\"mailto:info@bitrix.de\">info@bitrix.de</a>";
$MESS["MAIN_DESKTOP_INFO_TITLE"] = "Informationen über die Website";
$MESS["MAIN_DESKTOP_PRODUCTION_KEY"] = "Website freigegeben";
$MESS["MAIN_DESKTOP_PRODUCTION_VALUE"] = "12. Dezember 2010";
$MESS["MAIN_DESKTOP_RESPONSIBLE_KEY"] = "Zuständige Person";
$MESS["MAIN_DESKTOP_RESPONSIBLE_VALUE"] = "Max Mustermann";
$MESS["MAIN_DESKTOP_RSS_TITLE"] = "Bitrix News";
$MESS["MAIN_DESKTOP_URL_KEY"] = "Website-URL";
$MESS["MAIN_DESKTOP_URL_VALUE"] = "<a href=\"http://www.bitrix.de\">www.bitrix.de</a>";
$MESS["MAIN_EVENT_MESS_NOTIFICATION"] = "Benachrichtigung des Ereignisprotokolls: #NAME#";
$MESS["MAIN_EVENT_MESS_NOTIFICATION_TEXT"] = "Gefunden wurden Protokollereignisse entsprechend den Benachrichtigungsparametern:

Ereignistyp: #AUDIT_TYPE_ID#
Objekt: #ITEM_ID#
Nutzer: #USER_ID# 
IP-Adresse: #REMOTE_ADDR#
Browser: #USER_AGENT#
URL der Seite: #REQUEST_URI# 

Anzahl der Ereignisse: #EVENT_COUNT# 

#ADDITIONAL_TEXT#

Ereignisprotokoll öffnen:
http://#SERVER_NAME#/bitrix/admin/event_log.php?set_filter=Y&find_audit_type_id=#AUDIT_TYPE_ID#";
$MESS["MAIN_EVERYONE_GROUP_DESC"] = "Alle Nutzer, einschließlich nicht autorisierte";
$MESS["MAIN_EVERYONE_GROUP_NAME"] = "Alle Nutzer, einschließlich nicht autorisierte";
$MESS["MAIN_INSTALL_DB_ERROR"] = "Verbindung zur Datenbank nicht möglich. Überprüfen Sie die angegebenen Parameter.";
$MESS["MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN"] = "Hallo #NAME#,

ein neues Gerät wurde angemeldet, das Ihren Login verwendet: #LOGIN#.
 
Gerät: #DEVICE# 
Browser: #BROWSER#
Plattform: #PLATFORM#
Standort: #LOCATION# (ungefähr)
Datum: #DATE#

Wir empfehlen Ihnen, Ihr Passwort unverzüglich zu ändern, wenn es nicht Sie waren oder Einloggen nicht in Ihrem Auftrag erfolgte.";
$MESS["MAIN_INSTALL_EVENT_MESSAGE_NEW_DEVICE_LOGIN_SUBJECT"] = "Neues Gerät angemeldet";
$MESS["MAIN_INSTALL_EVENT_MESS_USER_CODE_REQUEST"] = "#SITE_NAME#: Anfrage wegen des Verifizierungscodes";
$MESS["MAIN_INSTALL_EVENT_MESS_USER_CODE_REQUEST_MESS"] = "Nutzen Sie zum Einloggen den folgenden Code:

#CHECKWORD#

Nachdem Sie eingeloggt sind, können Sie Ihr Passwort in Ihrem Nutzerprofil ändern.

Ihre Registrierungsinformationen:

Nutzer-ID: #USER_ID#
Account-Status: #STATUS#
Login: #LOGIN#

Diese Nachricht wurde automatisch erzeugt.";
$MESS["MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN"] = "Neues Gerät angemeldet";
$MESS["MAIN_INSTALL_EVENT_TYPE_NEW_DEVICE_LOGIN_DESC"] = "#USER_ID# - Nutzer-ID
#EMAIL# - Nutzer-E-Mail:
#LOGIN# - Nutzerlogin
#NAME# - Vorname des Nutzers
#LAST_NAME# - Nachname des Nutzers
#DEVICE# - Gerät
#BROWSER# - Browser
#PLATFORM# - Plattform
#USER_AGENT# - Nutzeragent
#IP# - IP-Adresse
#DATE# - Datum
#COUNTRY# - Land
#REGION# - Region
#CITY# - Stadt
#LOCATION# - Vollständiger Standort (Stadt, Region, Land)
";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION"] = "Benachrichtigung des Ereignisprotokolls";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION_DESC"] = "#EMAIL# - E-Mail des Empfängers
#ADDITIONAL_TEXT# - Text zusätzlicher Aktion
#NAME# - Benachrichtigungsname
#AUDIT_TYPE_ID# - Ereignistyp
#ITEM_ID# - Objekt
#USER_ID# - Nutzer
#REMOTE_ADDR# - IP-Adresse
#USER_AGENT# - Browser
#REQUEST_URI# - URL der Seite
#EVENT_COUNT# - Anzahl der Ereignisse";
$MESS["MAIN_INSTALL_EVENT_TYPE_NOTIFICATION_DESC_SMS"] = "#PHONE_NUMBER# - Telefonnummer des Empfängers
#ADDITIONAL_TEXT# - Text zusätzlicher Aktion
#NAME# - Benachrichtigungsname
#AUDIT_TYPE_ID# - Ereignistyp
#ITEM_ID# - Objekt
#USER_ID# - Nutzer
#REMOTE_ADDR# - IP-Adresse
#USER_AGENT# - Browser
#REQUEST_URI# - URL der Seite
#EVENT_COUNT# - Anzahl der Ereignisse";
$MESS["MAIN_INSTALL_EVENT_TYPE_USER_CODE_REQUEST"] = "Anfrage wegen des Verifizierungscodes";
$MESS["MAIN_INSTALL_EVENT_TYPE_USER_CODE_REQUEST_DESC"] = "#USER_ID# - Nutzer-ID
#STATUS# - Loginstatus
#LOGIN# - Login
#CHECKWORD# - Verifizierungscode
#NAME# - Vorname
#LAST_NAME# - Nachname
#EMAIL# - E-Mail des Nutzers
";
$MESS["MAIN_MAIL_CONFIRM_EVENT_TYPE_DESC"] = "

#EMAIL_TO# - E-Mail-Adresse für Bestätigung
#MESSAGE_SUBJECT# - Nachrichtenbetreff
#CONFIRM_CODE# - Bestätigungscode";
$MESS["MAIN_MAIL_CONFIRM_EVENT_TYPE_NAME"] = "E-Mail-Adresse des Absenders bestätigen";
$MESS["MAIN_MODULE_DESC"] = "Produktkernel";
$MESS["MAIN_MODULE_NAME"] = "Hauptmodul";
$MESS["MAIN_NEW_USER_CONFIRM_EVENT_DESC"] = "Nachricht von #SITE_NAME#!
------------------------------------------

Hallo,

Sie haben diese Nachricht erhalten, weil Sie (oder jemand Anderes) Ihre E-Mail benutzt hat, um sich auf #SERVER_NAME# anzumelden.

Ihr Registrierungsbestätigungscode lautet: #CONFIRM_CODE#

Bitte benutzen Sie den folgenden Link, um Ihre Anmeldung zu bestätigen und zu aktivieren:
http://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#&confirm_code=#CONFIRM_CODE#

Oder Sie öffnen diesen Link in Ihrem Browser und tragen den Code manuell ein:
http://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#

Warnung! Ihr Account wird nicht aktiviert, bis Sie Ihre Anmeldung bestätigt haben.

---------------------------------------------------------------------

Dies ist eine automatisch erstellte Nachricht.";
$MESS["MAIN_NEW_USER_CONFIRM_EVENT_NAME"] = "#SITE_NAME#: Neue Registrierungsbestätigung";
$MESS["MAIN_NEW_USER_CONFIRM_TYPE_DESC"] = "#USER_ID# - Nutzer ID
#LOGIN# - Loginname
#EMAIL# - E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - Nutzer IP
#USER_HOST# - Nutzer Host
#CONFIRM_CODE# - Bestätigungscode";
$MESS["MAIN_NEW_USER_CONFIRM_TYPE_NAME"] = "Registrierungsbestätigung für neue Nutzer";
$MESS["MAIN_NEW_USER_EVENT_DESC"] = "Nachricht von #SITE_NAME#
---------------------------------------

Ein neuer Nutzer wurde auf der Seite registriert #SERVER_NAME#.

Details:
Nutzer ID: #USER_ID#

Vorname: #NAME#
Nachname: #LAST_NAME#
E-Mail: #EMAIL#

Nutzername: #LOGIN#

Dies ist eine automatisch generierte Nachricht.";
$MESS["MAIN_NEW_USER_EVENT_NAME"] = "#SITE_NAME#: Neuer Nutzer hat sich auf der Seite registriert";
$MESS["MAIN_NEW_USER_TYPE_DESC"] = "#USER_ID# - Nutzer ID
#LOGIN# - Loginname
#EMAIL# -E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - Nutzer IP
#USER_HOST# - Nutzer Host";
$MESS["MAIN_NEW_USER_TYPE_NAME"] = "Ein Neuer Nutzer hat sich registriert";
$MESS["MAIN_RATING_AUTHORITY_NAME"] = "Autorität";
$MESS["MAIN_RATING_NAME"] = "Ranking";
$MESS["MAIN_RATING_TEXT_LIKE_D"] = "Gefällt mir";
$MESS["MAIN_RATING_TEXT_LIKE_N"] = "Gefällt mir nicht mehr";
$MESS["MAIN_RATING_TEXT_LIKE_Y"] = "Gefällt mir";
$MESS["MAIN_RULE_ADD_GROUP_AUTHORITY_NAME"] = "In die Gruppe Nutzer eintragen, die für Abstimmung auf eine Autorität berechtigt sind";
$MESS["MAIN_RULE_ADD_GROUP_RATING_NAME"] = "In die Gruppe Nutzer eintragen, die für Abstimmung auf ein Ranking berechtigt sind";
$MESS["MAIN_RULE_AUTO_AUTHORITY_VOTE_NAME"] = "Automatische Abstimmung für die Nutzerautorität";
$MESS["MAIN_RULE_REM_GROUP_AUTHORITY_NAME"] = "Aus der Gruppe Nutzer entfernen, die für Abstimmung auf eine Autorität nicht berechtigt sind";
$MESS["MAIN_RULE_REM_GROUP_RATING_NAME"] = "Aus der Gruppe Nutzer entfernen, die für Abstimmung auf ein Ranking nicht berechtigt sind";
$MESS["MAIN_SMILE_DEF_SET_NAME"] = "Standardsatz";
$MESS["MAIN_USER_INFO_EVENT_DESC"] = "Nachricht von #SITE_NAME#
---------------------------------------

#NAME# #LAST_NAME#,

#MESSAGE#

Ihre Registrierungsinformation:

Nutzer ID: #USER_ID#
Kontostatus: #STATUS#
Loginname: #LOGIN#

Um Ihr Passwort zu ändern, klicken Sie bitte auf den folgenden Link:
http://#SERVER_NAME#/auth/index.php?change_password=yes&lang=de&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#URL_LOGIN#

Dies ist eine automatisch generierte Nachricht.";
$MESS["MAIN_USER_INFO_EVENT_NAME"] = "#SITE_NAME#: Registrierungsinformationen";
$MESS["MAIN_USER_INFO_TYPE_DESC"] = "#USER_ID# - Nutzer ID
#STATUS# - Accountstatus
#MESSAGE# - Nachricht an den Nutzer
#LOGIN# - Loginname
#URL_LOGIN# - Verschlüsselter Login bei der Übergabe über URL
#CHECKWORD# - Kontrollwort für die Passwortänderung
#NAME# - Vorname
#LAST_NAME# - Nachname
#USER_IP# - Nutzer IP
#USER_HOST# - Nutzer Host";
$MESS["MAIN_USER_INFO_TYPE_NAME"] = "Nutzerinformation";
$MESS["MAIN_USER_INVITE_EVENT_DESC"] = "Nachricht von der Seite #SITE_NAME#
------------------------------------------
Hallo #NAME# #LAST_NAME#!

Der Administrator hat Sie zu den registrierten Nutzer hinzugefügt. 

Wir laden Sie ein, unsere Seite zu besuchen.

Ihre Anmeldedaten:

Nutzer ID: #ID#
Loginname: #LOGIN#

Wir empfehlen Ihnen, das automatisch generierte Passwort zu ändern.

Um das Passwort zu ändern, benutzen Sie bitte den folgenden Link:
http://#SERVER_NAME#/auth.php?change_password=yes&USER_LOGIN=#URL_LOGIN#&USER_CHECKWORD=#CHECKWORD#";
$MESS["MAIN_USER_INVITE_EVENT_NAME"] = "#SITE_NAME#: Einladung zur Seite";
$MESS["MAIN_USER_INVITE_TYPE_DESC"] = "#ID# - Nutzer ID
#LOGIN# - Loginname
#URL_LOGIN# - Verschlüsselter Login bei der Übergabe über URL
#EMAIL# - E-Mail
#NAME# - Vorname
#LAST_NAME# - Nachname
#PASSWORD# - Passwort
#CHECKWORD# - Kontrollwort für die Passwortänderung
#XML_ID# - Nutzer ID, um sich mit externen Datenquellen zu verbinden";
$MESS["MAIN_USER_INVITE_TYPE_NAME"] = "Einladung eines neuen Nutzers";
$MESS["MAIN_USER_PASS_CHANGED_EVENT_DESC"] = "Nachricht von #SITE_NAME#
---------------------------------------

#NAME# #LAST_NAME#,

#MESSAGE#

Ihre Registrierungsinformation:

Nutzer ID: #USER_ID#
Kontostatus: #STATUS#
Loginname: #LOGIN#

Dies ist eine automatisch generierte Nachricht.";
$MESS["MAIN_USER_PASS_CHANGED_EVENT_NAME"] = "#SITE_NAME#: Bestätigung des Passwortwechsels";
$MESS["MAIN_USER_PASS_CHANGED_TYPE_NAME"] = "Bestätigung des Passwortwechsels";
$MESS["MAIN_USER_PASS_REQUEST_EVENT_DESC"] = "Nachricht von #SITE_NAME#
---------------------------------------

#NAME# #LAST_NAME#,

#MESSAGE#

Um Ihr Passwort zu ändern, klicken Sie bitte auf den folgenden Link:
http://#SERVER_NAME#/auth/index.php?change_password=yes&lang=de&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#URL_LOGIN#

Ihre Registrierungsinformation:

Nutzer ID: #USER_ID#
Kontostatus: #STATUS#
Loginname: #LOGIN#

Dies ist eine automatisch generierte Nachricht.";
$MESS["MAIN_USER_PASS_REQUEST_EVENT_NAME"] = "#SITE_NAME#: Anfrage zum Passwortwechsel";
$MESS["MAIN_USER_PASS_REQUEST_TYPE_NAME"] = "Anfrage zum Passwortwechsel";
$MESS["MAIN_VOTE_AUTHORITY_GROUP_DESC"] = "Nutzer werden zu dieser Gruppe automatisch hinzugefügt.";
$MESS["MAIN_VOTE_AUTHORITY_GROUP_NAME"] = "Nutzer, die zur Abstimmung auf eine Autorität berechtigt sind";
$MESS["MAIN_VOTE_RATING_GROUP_DESC"] = "Nutzer werden zu dieser Gruppe automatisch hinzugefügt.";
$MESS["MAIN_VOTE_RATING_GROUP_NAME"] = "Nutzer, die zur Abstimmung auf ein Ranking berechtigt sind";
$MESS["MF_EVENT_DESCRIPTION"] = "#AUTHOR# - Nachrichtenautor
#AUTHOR_EMAIL# - Autoradresse
#TEXT# - Nachricht
#EMAIL_FROM# - Absenderadresse
#EMAIL_TO# - Empfängeradresse";
$MESS["MF_EVENT_MESSAGE"] = "Benachrichtigung von #SITE_NAME#
------------------------------------------

Sie haben eine Nachricht erhalten. 

Gesendet von: #AUTHOR#
Absender-E-Mail: #AUTHOR_EMAIL#

Nachricht:
#TEXT#

Diese Benachrichtigung wurde automatisch erstellt.";
$MESS["MF_EVENT_NAME"] = "Nachricht über das Rückmeldeformular senden";
$MESS["MF_EVENT_SUBJECT"] = "#SITE_NAME#: Nachricht aus der Rückmeldungsform";
$MESS["main_install_sms_event_confirm_descr"] = "#USER_PHONE# - Telefonnummer
#CODE# - Bestätigungscode";
$MESS["main_install_sms_event_confirm_name"] = "Telefonnummer via SMS bestätigen";
$MESS["main_install_sms_event_restore_descr"] = "#USER_PHONE# - Telefonnummer
#CODE# - Bestätigungscode wiederherstellen";
$MESS["main_install_sms_event_restore_name"] = "Passwort via SMS wiederherstellen";
$MESS["main_install_sms_template_confirm_mess"] = "Bestätigungscode: #CODE#";
$MESS["main_install_sms_template_notification_mess"] = "#NAME#: #ADDITIONAL_TEXT# (Ereignisse: #EVENT_COUNT#)";
$MESS["main_install_sms_template_restore_mess"] = "Code zum Wiederherstellen des Passwortes: #CODE#";
