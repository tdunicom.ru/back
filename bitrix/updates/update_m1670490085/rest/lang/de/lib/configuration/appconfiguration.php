<?php
$MESS["REST_CONFIGURATION_ERROR_INSTALL_APP_CONTENT"] = "Fehler beim Installieren zusätzlicher Anwendung";
$MESS["REST_CONFIGURATION_ERROR_INSTALL_APP_CONTENT_DATA"] = "Fehler beim Installieren zusätzlicher Anwendung: #ERROR_MESSAGE# (#ERROR_CODE#)";
$MESS["REST_CONFIGURATION_ERROR_UNKNOWN_APP"] = "Beim Installieren zusätzlicher Anwendungen wurde eine unbekannte Anwendung festgestellt. Bitte wenden Sie sich an den Ersteller der App oder konfigurieren Sie die Datei.";
