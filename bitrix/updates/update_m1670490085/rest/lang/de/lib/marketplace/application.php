<?php
$MESS["RMP_ACCESS_DENIED"] = "Zugriff verweigert. Wenden Sie sich an Ihren Administrator, um die Anwendung zu installieren.";
$MESS["RMP_ERROR_ACCESS_DENIED"] = "Zugriff verweigert.";
$MESS["RMP_ERROR_SUBSCRIPTION_REQUIRED"] = "Das Abonnement für Bitrix24.Market Plus ist erforderlich, um mit der Installation der Anwendung fortsetzen zu können";
$MESS["RMP_ERROR_VERIFICATION_NEEDED"] = "Fehler beim Verifizieren der Lizenz. Bitte versuchen Sie in 10 Minuten erneut.";
$MESS["RMP_INSTALL_ERROR"] = "Fehler! Die Anwendung war nicht installiert.";
$MESS["RMP_NOT_FOUND"] = "Die Anwendung wurde nicht gefunden.";
