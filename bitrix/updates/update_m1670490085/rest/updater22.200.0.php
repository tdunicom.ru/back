<?php

if (IsModuleInstalled('rest'))
{
	$updater->CopyFiles("install/components", "components");
}

if ($updater->CanUpdateDatabase() && $updater->TableExists("b_rest_placement"))
{
	if (!$DB->Query("SELECT USER_ID FROM b_rest_placement WHERE 1=0", true))
	{
		$DB->Query("ALTER TABLE b_rest_placement ADD USER_ID INT(18) NULL DEFAULT 0");
	}
	if (!$DB->IndexExists("b_rest_placement", array("PLACEMENT", "USER_ID", ), true))
	{
		$DB->Query("CREATE INDEX ix_b_rest_placement4 ON b_rest_placement(PLACEMENT, USER_ID)");
	}
}

if (IsModuleInstalled('rest') && $updater->CanUpdateDatabase())
{
	\Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnAfterSetOption_~mp24_paid_date', 'rest', '\Bitrix\Rest\Marketplace\Client', 'onChangeSubscriptionDate');
}
