<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()

$strReturn .= '<div class="bx-breadcrumb">';

$itemSize = count($arResult);
$itemListElements = '';
for($index = 0; $index < $itemSize; $index++)
{
    $itemListElementItem = '';

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$arrow = ($index > 0? '<i class="fa fa-arrow-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			<div class="bx-breadcrumb-item">
				'.$arrow.'
				<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">
					<span>'.$title.'</span>
				</a>
			</div>';

        $itemListElementItem  = '"@id": "'.$arResult[$index]["LINK"].'",';
	}
	else
	{
		$strReturn .= '
			<div class="bx-breadcrumb-item">
				'.$arrow.'
				<span>'.$title.'</span>
			</div>';
	}

    $itemListElementItem .= '"name": "'.$title.'"';
    $itemListElement  = '"@type": "ListItem",';
    $itemListElement .= '"position": '.($index + 1).',';
    $itemListElement .= '"item": {'.$itemListElementItem.'}';
    $itemListElement  = '{'.$itemListElement.'}';

    $itemListElements .= $itemListElement.',';
}
$itemListElements = substr($itemListElements, 0, -1);

$strReturn .= '<div style="clear:both"></div></div>';




$strLdJson  = '"@context": "http://schema.org",';
$strLdJson .= '"@type": "BreadcrumbList",';
$strLdJson .= '"itemListElement": ['.$itemListElements.']';
$strLdJson  = '<script type="application/ld+json">{'.$strLdJson.'}</script>';
$strLdJson  = "\n".$strLdJson."\n";

return $strReturn.$strLdJson;



/*
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
      "@id": "https://example.com/books",
      "name": "Books",
      "image": "http://example.com/images/icon-book.png"
    }
  },{
    "@type": "ListItem",
    "position": 2,
    "item": {
      "@id": "https://example.com/books/authors",
      "name": "Authors",
      "image": "http://example.com/images/icon-author.png"
    }
  },{
    "@type": "ListItem",
    "position": 3,
    "item": {
      "@id": "https://example.com/books/authors/annleckie",
      "name": "Ann Leckie",
      "image": "http://example.com/images/author-leckie-ann.png"
    }
  },{
    "@type": "ListItem",
    "position": 4,
    "item": {
      "@id": "https://example.com/books/authors/ancillaryjustice",
      "name": "Ancillary Justice",
      "image": "http://example.com/images/cover-ancillary-justice.png"
    }
  }]
}
</script>
*/
