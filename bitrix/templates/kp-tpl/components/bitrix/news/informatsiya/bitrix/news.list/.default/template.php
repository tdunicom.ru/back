<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block-wrap block-wrap_wrap page-layout">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-layout__content page-layout__content_s-bottom">
		<ul class="info-list">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="info-list__item">
				<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		            <div class="info-list__title"><?echo $arItem["NAME"]?></div>
		        <?endif;?>

				<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
					<div class="info-list__preview"><?echo $arItem["PREVIEW_TEXT"];?></div>
				<?endif;?>

		        <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
		            <div class="info-list__read-more"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">Подробнее</a></div>
		        <?endif;?>

			</li>
		<?endforeach;?>
		</ul>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
			<br />
				<?=$arResult["NAV_STRING"]?>
		<?endif;?>

	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6  page-layout__side page-layout__side_s-top">
		<?
			global $curPage;
			if($curPage == '/informatsiya/'):
			    $APPLICATION->IncludeComponent(
			        "bitrix:main.include",
			        "",
			        Array(
			            "AREA_FILE_SHOW" => "page",
			            "AREA_FILE_SUFFIX" => "inc",
			            "COMPONENT_TEMPLATE" => ".default",
			            "EDIT_TEMPLATE" => ""
			        )
			    );
			endif;
		?>
	</div>
</div>
</div>
	</div>
