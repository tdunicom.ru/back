<div class="how_to_buy_title">
    <div class="how_to_buy_counter">02</div>
    <span>Оплата</span>
</div>
<div class="how_to_buy_text">
    <p>Выставим счет на вашу организацию, после оплаты отправляем полностью заказ.</p>
    <p>Для выставления счета пришлите реквизиты на почту: </p>
    <p><a href="mailto:zakaz@kvarc-pesok.ru" class="email">zakaz@kvarc-pesok.ru</a></p>
</div>