<div class="container menu-products">
      <div class="content">
        <input class="menu-products__switcher" type="checkbox" id="menu-products__switcher">
        <div class="menu-products__wrapper">
          <label class="menu-products__label-inner xl-hidden l-hidden m-hidden" for="menu-products__switcher">Назад</label>
          <ul class="menu-products__list">
            <li class="menu-products__item menu-products__item_parent"><a href="/kvartseviy-pesok/">Сухой песок</a>
              <ul class="menu-products__categories">
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Применение:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-peskostruynykh-rabot/">Песок для пескоструйных работ</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/formovochnyy/">Формовочный песок</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-filtrov-ochistki-vody/">Песок для фильтров воды и бассейна</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/stekolnyy/">Стекольный песок</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-sportivnykh-pokrytiy/">Для спортивных покрытий</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-pesochnits/">Для песочницы</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-sukhikh-stroitelnykh-smesey/">Для сухих строительных смесей</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/stroitelnyy/">Строительный песок</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/primenenie/dlya-pola/">Для пола</a></li>
                  </ul>
                </li>
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Фасовка:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/fasovka/v-big-begah/">В биг-бегах</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/fasovka/v-meshkakh/">В мешках</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/">Навалом</a></li>
                  </ul>
                  <div class="menu-products__sublist-name">ГОСТы:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/informatsiya/gost-na-kvartsevyy-pesok/">Основные виды ГОСТов</a></li>
                  </ul>
                  <? /* <div class="menu-products__subtitle"><a href="/informatsiya/gost-na-kvartsevyy-pesok/">ГОСТы</a></div> */ ?>
                </li>
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Фракции:</div>
                  <div class="menu-products__sublist-name">ГОСТ 8736 (строительный песок):</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-63-2-5-mm">0.63-2.5</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-2-5-mm">0-2.5</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-1-25-mm">0-1.25</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-63-1-25-mm">0.63-1.25</a></li>
                  </ul>
                  <div class="menu-products__sublist-name">ГОСТ 2138 (формовочный песок):</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-0-315-mm">0-0.315</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-315-0-63-mm">0.315-0.63</a></li>
                    <li class="menu-products__sublist-item"><a href="/kvartseviy-pesok/frakcii/0-0-63-mm">0-0.63</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <? /* <li class="menu-products__item"><a href="/tsvetnoy-kvartsevyy-pesok/">Цветной кварцевый песок</a>
              <ul class="menu-products__categories menu-products__categories_right">
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Применение:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/tsvetnoy-kvartsevyy-pesok/shtukaturka-s-kvartsevym-peskom/">Штукатурка с кварцевым песком</a></li>
                  </ul>
                </li>
              </ul>
            </li> */ ?>
            <li class="menu-products__item"><a href="/sukhie-stroitelnye-smesi-tolling/">Сухие строительные смеси (толлинг)</a></li>
            <li class="menu-products__item menu-products__item_parent"><a href="/graviy/">Гравий (отгрохотка)</a>
              <ul class="menu-products__categories menu-products__categories_right">
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Применение:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-dorozhek/">Для дорожек</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-stoyanki/">Для стоянки и парковки</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-detskoy-ploshchadki/">Для детской площадки</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-betona">Для бетона</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-landshaftnogo-dizayna">Для ландшафтного дизайна</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/primenenie/dlya-otsypki-drenazhnykh-sistem">Для отсыпки дренажных систем</a></li>
                  </ul>
                </li>
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Виды:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/graviy/vidy/melkiy/">Мелкий</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/vidy/sredniy/">Средний</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/vidy/krupnyy/">Крупный</a></li>
                  </ul>
                </li>
                <li class="menu-products__category">
                  <div class="menu-products__subtitle">Фракции:</div>
                  <ul class="menu-products__sublist">
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/2-5-5-mm/">2,5 - 5 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/5-10-mm/">5 - 10 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/20-40-mm/">20 - 40 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/5-20-mm/">5 - 20 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/3-10-mm/">3 - 10 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/10-20-mm/">10 - 20 мм</a></li>
                    <li class="menu-products__sublist-item"><a href="/graviy/frakcii/10-30-mm/">10 - 30 мм</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
