<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>









<!--test-->

<? /*$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multilevel1", Array(
	"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "top_sub",	// Тип меню для остальных уровней
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"MENU_CACHE_TYPE" => "N",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"COMPONENT_TEMPLATE" => "horizontal_multilevel"
	),
	false
); */ ?>

<!--test-->
<ul class="menutop-list">
  <li><a href="/kak-kupit/" class="root-item">Как купить</a></li>
  <li><a href="/dostavka/" class="root-item">Доставка</a></li>
  <li><a href="/tseny/" class="root-item">Цены</a></li>
  <li><a href="/aktsii/" class="root-item">Акции</a></li>
  <li><a href="/kompaniya/" class="root-item root-item--parent">О компании</a>
    <ul class="menutop-sublist">
      <li><a href="/kompaniya/klienty/">Клиенты</a></li>
      <li><a href="/kompaniya/partnyery/"> Партнёры </a></li>
      <li><a href="/kompaniya/proizvodstvo/"> Производство </a></li>
      <li><a href="/kompaniya/otzyvy/"> Отзывы </a></li>
      <li><a href="/kompaniya/proekty/"> Проекты </a></li>
    </ul>
  </li>
  <li><a href="/kontakty/" class="root-item">Контакты</a></li>
</ul>
