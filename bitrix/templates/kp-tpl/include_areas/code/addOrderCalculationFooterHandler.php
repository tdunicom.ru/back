<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$iBlockIdToSave = 2; // "Заказы расчетов (Footer)"
$emailEventName = "NEW_ORDER_FOR_CALCULATION_ROOT";

if(!empty($_POST["addData"])){

	if($_POST["addData"] == "pages_graviy"){
		$iBlockIdToSave = 6; //  "Заказы расчетов (Гравий)"
		$emailEventName = "NEW_ORDER_FOR_CALCULATION_GRAVIY";
	}

	if($_POST["addData"] == "pages_pesok"){
		$iBlockIdToSave = 9; //  "Заказы расчетов (Песок)"
		$emailEventName = "NEW_ORDER_FOR_CALCULATION_PESOK";
	}

	if($_POST["addData"] == "pages_tsementnoPeschanayaSmes"){
		$iBlockIdToSave = 10; //  "Заказы расчетов (Цементно-песчаная смесь)"
		$emailEventName = "NEW_ORDER_FOR_CALCULATION_CPS";
	}

	if($_POST["addData"] == "pages_sukhie_stroitelnye_smesi"){
		$iBlockIdToSave = 12; //  "Заказы расчетов (Сухие строительные смеси (толлинг))"
		$emailEventName = "NEW_ORDER_FOR_CALCULATION_SUKHIE_STROITELNYE_SMESI";
	}
}

$siteCode = "s1";

if(!empty($_POST))
{
	//init data
	$name     = htmlspecialcharsbx($_POST['name']);
	$email    = htmlspecialcharsbx($_POST['email']);
	$phone    = htmlspecialcharsbx($_POST['phone']);
	$comments = htmlspecialcharsbx($_POST['comment']);

    if(!$name){
         die(
            json_encode(array(
                'result' => 'error',
                'message' => 'Name is empty!')
            )
        );
    }

    if(!$email){
    	$email = "zakaz@kvarc-pesok.ru";
    }

    CModule::IncludeModule('iblock');

	$dateForUser  = date('Y/m/d H:i:s');
	$dateForCode  = ConvertTimeStamp(time(), "FULL");

	$topicInit    = $name;
    $topicForUser = $topicInit.' от '.$dateForUser;
    $topicForCode = $topicInit.'_'.$dateForCode;

	//code generation
	$arParamsCode = array(
	   "max_len" => 255,  
	   "change_case" => "L",  
	   "replace_space" => '-',  
	   "replace_other" => '-',   
	   "delete_repeat_replace" => true
	);
	 
	$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

	$element = new CIBlockElement;
	
	$elementProps = array();
	$elementProps['PHONE']    = $phone;
	$elementProps['EMAIL']    = $email;

	if (!empty($_FILES['orderFile']['size']))
	{
		$orderFile = $_FILES['orderFile'];
		$orderFile['MODULE_ID'] = 'iblock';
		$elementProps['FILE']   = $orderFile;
	}

	$arElementFields = Array(
		"IBLOCK_ID"        => $iBlockIdToSave,
		"NAME"             => $topicForUser,
		"CODE"			   => $code,
		"PREVIEW_TEXT"     => $comments,
		"ACTIVE"           => "Y",
		"DATE_ACTIVE_FROM" => $dateForCode,
        "PROPERTY_VALUES"  => $elementProps
	);

	if ($newIblockItemId = $element->Add($arElementFields))
	{
        $backendLink  = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $backendLink .= '?IBLOCK_ID='.$iBlockIdToSave.'&type=feedback&ID='.$newIblockItemId;

        $arEventFields = array(
            "NAME"     => $name,
			"PHONE"    => $phone,
			"EMAIL"    => $email,
			"COMMENTS" => $comments,
            "DATE"     => $dateForUser,
            "ID"       => $newIblockItemId,
            "BE_LINK"  => $backendLink
        );

        $returnMsg = "New Iblock ($iBlockIdToSave) Item successfully added: $newIblockItemId";

        if($GLOBALS['prod']){
            $resSend    = CEvent::Send($emailEventName, $siteCode, $arEventFields);
            $returnMsg .= "; Message send: ".$resSend;
        }

        $data = array("result" => "success", "message" => $returnMsg);
    }
	else{
		$data = array("result" => "error", "message" => $element->LAST_ERROR);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

} else {
	LocalRedirect('/');
}

?>