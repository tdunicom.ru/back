<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule("iblock");

require __DIR__."/models/ModelRequest.php";
require __DIR__."/models/ModelElements.php";
require __DIR__."/models/ModelRobokassa.php";
require __DIR__."/models/ModelHelper.php";
