<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>

<!DOCTYPE html>
<html lang="ru">
	<head>
	    <title><?$APPLICATION->ShowTitle();?></title>
		<?$APPLICATION->ShowHead();?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<link href="<?= SITE_TEMPLATE_PATH ?>/css/base.css" rel="stylesheet">
		<link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery.fancybox.css" rel="stylesheet">
		<link href="<?= SITE_TEMPLATE_PATH ?>/css/core.min.css?v=3" rel="stylesheet">
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery-2.2.0.min.js"></script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery.fancybox.pack.js"></script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery.plugin.min.js"></script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery.countdown.min.js"></script>

		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/ajax_form.js"></script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/form_validation.js"></script>

		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/sss.min.js"></script>
		<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/jquery.flexslider-min.js"></script>
	  <?
		  global $USER;
        $numbPage = ($APPLICATION->GetCurPage() == '/' || $APPLICATION->GetCurPage() == '/kvartseviy-pesok/')  ? 'first' : 'all';
        $numbPageStr = ' class="'.$numbPage.'"';

        $APPLICATION->ShowPanel(); ?>
        <?
            $reqURI = $_SERVER["REQUEST_URI"];
            if(strpos($reqURI, 'PAGEN_')){
                $lastSlash = strrpos($reqURI, '/') + 1;
                $canonicalURI = 'http://'.$_SERVER["HTTP_HOST"].substr($reqURI, 0, $lastSlash);
                $APPLICATION->AddHeadString('<link rel="canonical" href="'.$canonicalURI.'">');
            }
        ?>

        <script type='application/ld+json'>
        {
          "@context": "http://www.schema.org",
          "@type": "WebSite",
          "name": "kvarc-pesok.ru",
          "alternateName": "Кварц-Песок",
          "url": "http://www.kvarc-pesok.ru/"
        }
         </script>

    </head>

    <body<?=$numbPageStr?>>

    		<? /* <? if(!$USER->IsAdmin()): ?>
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript" >
                (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

                ym(36383700, "init", {
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/36383700" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
			<? endif ?> */ ?>

        <div class="container">

            <div class="content">
                <header class="header">
                    <div class="logo"><a href="/"><? blockInclude('header', 'logo'); ?></a></div>
                    <div class="header_contacts">
                        <div class="header_contacts_phone"><? blockInclude('header', 'contacts_phone'); ?></div>
                        <div class="header_contacts_callback"><? blockInclude('header', 'contacts_callback'); ?></div>
                    </div>
                </header><!-- .header-->
            </div>
				</div><!-- .wrapper -->


<? blockInclude('header', 'menu-products'); ?>
            	<div class="container menutop">
                <div class="content menutop_inner">
									<label for="menutop-switcher" class="menutop-label xl-hidden l-hidden m-hidden">Меню</label>
									<input type="checkbox" id="menutop-switcher" class="menutop-switcher">
									<? blockInclude('header', 'menu'); ?></div>

        <label class="menu-products__label-outer xl-hidden l-hidden m-hidden" for="menu-products__switcher">Каталог</label>
							</div>

            <main>
                <? if($numbPage == 'all'): ?>
                  <div class="container">
                    <div class="content">
                        <? blockInclude('inner', 'breadcrumbs'); ?>
                        <h1><?$APPLICATION->ShowTitle(false)?></h1>

                  </div>
                </div>







<? endif; ?>
