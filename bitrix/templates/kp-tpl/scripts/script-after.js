Date.prototype.getNextWeekDay = function (weekday) {
    if (weekday !== undefined) {
        this.setDate(this.getDate() + (weekday + 7 - this.getDay()) % 7);
        this.setHours(0, 0, 0);
        return this;
    }
}

var now = new Date();
var nextSunday = now.getNextWeekDay(0); // 0 = Sunday, 1 = Monday, ...

$('.banner-home-countdown').countdown({
  until: nextSunday,
  format: 'DHMS',
  padZeroes: true,
  layout:
    '<div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{d10}</span><span class="banner-home-countdown__digit">{d1}</span><span class="banner-home-countdown__desc">Дней</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{h10}</span><span class="banner-home-countdown__digit">{h1}</span><span class="banner-home-countdown__desc">Часов</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{m10}</span><span class="banner-home-countdown__digit">{m1}</span><span class="banner-home-countdown__desc">Минут</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{s10}</span><span class="banner-home-countdown__digit">{s1}</span><span class="banner-home-countdown__desc">Секунд</span></div>'
});

  $('.ssslider').sss({
    speed : 5000, // Slideshow speed in milliseconds.
  });