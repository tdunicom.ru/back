<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="grid grid--cols-12">
    <div class="grid__cell grid__cell--m-6 grid__cell--s-10 grid__cell--xs-12">
        <? ShowMessage($arParams["~AUTH_RESULT"]) ?>
		<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
            <p><?= GetMessage("AUTH_EMAIL_SENT") ?></p>
		<?else:?>
            <? if($arResult["USE_EMAIL_CONFIRMATION"] === "Y"): ?>
                <p><?= GetMessage("AUTH_EMAIL_WILL_BE_SENT") ?></p>
            <? endif ?>
            <? include __DIR__."/include/form.php" ?>
            <script type="text/javascript">document.bform.USER_NAME.focus();</script>
		<?endif?>
    </div>
</div>

<p><?= $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"] ?></p>
<p><span class="required-mark">*</span> Обязательные поля</p>
