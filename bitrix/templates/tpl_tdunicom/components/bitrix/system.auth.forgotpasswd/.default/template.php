<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<? ShowMessage($arParams["~AUTH_RESULT"]) ?>

<p class="text-marked"><?= GetMessage("AUTH_GET_CHECK_STRING") ?></p>

<?= GetMessage("AUTH_FORGOT_PASSWORD_1") ?>

<div class="grid grid--cols-12">
    <div class="grid__cell grid__cell--m-4 grid__cell--s-8 grid__cell--xs-12">

        <form
            class="form-register"
            name="bform"
            method="post"
            target="_top"
            action="<?= $arResult["AUTH_URL"] ?>"
        >

            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
            <? endif ?>

            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">

            <div class="form-input form-input--text form-register__field">
                <label class="form-input__label">Логин</label>
                <input
                    type="text"
                    name="USER_LOGIN"
                    maxlength="50"
                    value="<?= $arResult["LAST_LOGIN"] ?>"
                    class="form-input__field"
                />
            </div>

            <div class="form-register__field">или</div>

            <div class="form-input form-input--email form-register__field">
                <label class="form-input__label">E-Mail:</label>
                <input
                    type="email"
                    name="USER_EMAIL"
                    maxlength="255"
                    class="form-input__field"
                />
            </div>

            <input
                type="submit"
                name="send_account_info"
                value="<?= GetMessage("AUTH_SEND") ?>"
                class="btn form-register__submit"
            />

        </form>

    </div>
</div>

<p>
    <strong>
        <a href="<?= $arResult["AUTH_AUTH_URL"] ?>">Авторизация</a>
    </strong>
</p>

<script type="text/javascript">
    document.bform.USER_LOGIN.focus();
</script>