<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!empty($arResult["ORDER"]))
{
	?>
	<h1 style="margin-top: 0.9375rem"><?=GetMessage("SOA_TEMPL_ORDER_COMPLETE")?></h1/>

	<p><?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]))?></p>
	<p><?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?></p>

	<?
        /* Yandex.Metrika */
        $arProducts = array();

        $dbBasket = CSaleBasket::GetList(array("NAME" => "ASC"), array("ORDER_ID" => $arResult['ORDER_ID'], "ACTIVE" => 'Y'));
        while ($arBasket = $dbBasket->Fetch())
        {
           $arProducts[$arBasket['ID']] = array(
              'ID' => $arBasket['ID'],
              'PRODUCT_ID' => $arBasket['PRODUCT_ID'],
              'NAME' => $arBasket['NAME'],
              'PRICE' => $arBasket['PRICE'],
              'QUANTITY' => $arBasket['QUANTITY'],
              'DETAIL_PAGE_URL' => $arBasket['DETAIL_PAGE_URL'],
           );
        }
        ?>

        <script>
        window.onload = function(){
                var yaParams = {
                   order_id: "<?=$arResult['ORDER_ID']?>",
                   order_price: <?=intval($arResult["ORDER"]['PRICE'])?>,
                   currency: "RUR",
                   exchange_rate: 1,
                   goods:
                   [
                      <?foreach($arProducts as $products){?>
                      {
                         id: "<?=intval($products['PRODUCT_ID'])?>",
                         name: "<?=$products['NAME']?>",
                         price: <?=intval($products['PRICE'])?>,
                         quantity: <?=intval($products['QUANTITY'])?>
                      },
                      <?}?>
                   ]
                };

            yaCounter30658727.reachGoal('GOAL_MAKEORDER', yaParams);
        }
        </script>
	<?
	if (!empty($arResult["PAY_SYSTEM"]))
	{
		?>

		<table class="sale_order_full_table">
			<tr>
				<td class="ps_logo">
					<div class="pay_name"><?=GetMessage("SOA_TEMPL_PAY")?></div>
					<?=CFile::ShowImage($arResult["PAY_SYSTEM"]["LOGOTIP"], 100, 100, "border=0", "", false);?>
					<div class="paysystem_name"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></div><br>
				</td>
			</tr>
			<?
			if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0)
			{
				?>
				<tr>
					<td>
						<?
						if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y")
						{
							?>
							<script language="JavaScript">
								window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
							</script>
							<?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))))?>
							<?
							if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE']))
							{
								?><br />
								<?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))."&pdf=1&DOWNLOAD=Y")) ?>
								<?
							}
						}
						else
						{
							if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"])>0)
							{
								include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
							}
						}
						?>
					</td>
				</tr>
				<?
			}
			?>
		</table>
		<?
	}
}
else
{
	?>
	<b><?=GetMessage("SOA_TEMPL_ERROR_ORDER")?></b><br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"]))?>
				<?=GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>
	<?
}
