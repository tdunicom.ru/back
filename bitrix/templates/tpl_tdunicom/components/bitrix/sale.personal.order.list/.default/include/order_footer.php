<div class="orders-current-item__footer">
	<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>">Подробнее о заказе</a>
	<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>" class="orders-current-item__repeat">Повторить заказ</a>

	<? if ($order['ORDER']['CAN_CANCEL'] !== 'N'): ?>
		<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"]) ?>">Отменить заказ</a>
	<? endif ?>

</div>