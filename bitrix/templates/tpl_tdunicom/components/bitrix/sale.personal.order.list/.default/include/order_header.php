<? use Bitrix\Main\Localization\Loc; ?>
<div class="orders-current-item__header">
	<h3 class="orders-current-item__title sale-order-list-title">
		Заказ № <?= $order['ORDER']['ACCOUNT_NUMBER'] ?>
		от <?= $order['ORDER']['DATE_INSERT_FORMATED'] ?>,
		<?= count($order['BASKET_ITEMS']) ?>
		<?
		$count = count($order['BASKET_ITEMS']) % 10;
		if ($count == '1')
		{
			echo Loc::getMessage('SPOL_TPL_GOOD');
		}
		elseif ($count >= '2' && $count <= '4')
		{
			echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
		}
		else
		{
			echo Loc::getMessage('SPOL_TPL_GOODS');
		}
		?>
		на сумму <?= $order['ORDER']['FORMATED_PRICE'] ?>
	</h3>
</div>
