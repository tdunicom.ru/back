<?
foreach($arResult['ERRORS']['FATAL'] as $error)
{
	ShowError($error);
}
$component = $this->__component;
if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
{
	$APPLICATION->AuthForm('', false, false, 'N', false);
}
