<?
$paymentChangeData = array();
$orderHeaderStatus = null;

foreach ($arResult['ORDERS'] as $key => $order): ?>

	<? include __DIR__ . "/orders_header.php" ?>

    <div class="orders-current-item">
        <? include __DIR__."/order_header.php" ?>
        <div class="orders-current-item__body">
			<? include __DIR__."/order_payment.php" ?>
			<? include __DIR__."/order_shipment.php" ?>
        </div>
		<? include __DIR__."/order_footer.php" ?>
    </div>

<? endforeach ?>