<? use Bitrix\Main\Localization\Loc; ?>
<? if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS'): ?>
	<? $orderHeaderStatus = $order['ORDER']['STATUS_ID']; ?>
	<h2 class="sale-order-title">
		<?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?>&raquo;
	</h2>
<? endif ?>