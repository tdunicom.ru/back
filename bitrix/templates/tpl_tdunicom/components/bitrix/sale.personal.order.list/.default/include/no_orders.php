<?

use Bitrix\Main\Localization\Loc;

if (!count($arResult['ORDERS']))
{
	if ($_REQUEST["filter_history"] == 'Y')
	{
		if ($_REQUEST["show_canceled"] == 'Y')
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER')?></h3>
			<?
		}
		else
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST')?></h3>
			<?
		}
	}
	else
	{
		?>
		<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
		<?
	}
}
