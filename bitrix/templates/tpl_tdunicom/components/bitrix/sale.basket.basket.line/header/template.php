<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<div class="bx_cart_block main_nav_basket bx_cart_block">

    <? $frame = $this->createFrame('bx_cart_block', false)->begin() ?>
        <?require(realpath(dirname(__FILE__)).'/ajax_template.php') ?>
    <? $frame->beginStub() ?>

    <div class="main_nav_basket_inner">
        <a
            href="<?= $arParams["PATH_TO_BASKET"] ?>"
            class="header-cart main_nav_basket_inner_count"
        ><span>0 товаров</span></a>
    </div>
    <?$frame->end()?>
</div>

<? if(isInnerPage()): ?>
    <script>
        sbbl.elemBlock = document.getElementsByClassName("bx_cart_block")[0];
        sbbl.ajaxPath = '<?=$componentPath?>/ajax.php';
        sbbl.siteId = '<?=SITE_ID?>';
        sbbl.templateName = '<?=$templateName?>';
        sbbl.arParams = <?=CUtil::PhpToJSObject ($arParams)?>;
        BX.addCustomEvent(window, 'OnBasketChange', sbbl.refreshCart);
    </script>
<? endif ?>