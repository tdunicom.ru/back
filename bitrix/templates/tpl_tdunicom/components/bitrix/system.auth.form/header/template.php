<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="header-register">

  <? if($arResult["FORM_TYPE"] == "login"): ?>
      <a href="/auth/" class="header-register__item link-secondary">Вход</a>
      /
      <a href="<?= $arParams["REGISTER_URL"] ?>" class="header-register__item link-secondary">Регистрация</a>

  <? else: //if($arResult["FORM_TYPE"] == "login") ?>

    <form action="<?=$arResult["AUTH_URL"]?>" id="form-logout">

            <a
                href="<?=$arResult["PROFILE_URL"]?>"
                title="<?=GetMessage("AUTH_PROFILE")?>"
                class="header-register__item link-secondary"
            >
                <?=(strlen($arResult["USER_NAME"])) ? $arResult["USER_NAME"] : $arResult["USER_LOGIN"]?>
            </a> /


            <? foreach ($arResult["GET"] as $key => $value): ?>
                <input type="hidden" name="<?=$key?>" value="<?=$value?>" />
            <? endforeach ?>

            <input type="hidden" name="logout" value="yes" />
            <a
                onclick="document.getElementById('form-logout').submit()"
                title="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" style="cursor:pointer;"
                class="header-register__item link-secondary"
            ><?=GetMessage("AUTH_LOGOUT_BUTTON")?></a>

    </form>

  <?endif?>

</div>