<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<? ShowMessage($arParams["~AUTH_RESULT"]) ?>

<p class="text-marked">Смена пароля</p>

<div class="grid grid--cols-12">
    <div class="grid__cell grid__cell--m-4 grid__cell--s-8 grid__cell--xs-12">

        <form
            method="post"
            action="<?= $arResult["AUTH_FORM"] ?>"
            name="bform"
            class="form-register"
        >

			<? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
			<? endif ?>

            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="CHANGE_PWD">

            <div class="form-input form-input--text form-register__field">
                <label class="form-input__label">Логин <span class="form-register__required-mark">*</span></label>
                <input
                    type="text"
                    name="USER_LOGIN"
                    maxlength="50"
                    value="<?= $arResult["LAST_LOGIN"] ?>"
                    class="form-input__field"
                    required="required"
                />
            </div>

            <div class="form-input form-input--text form-register__field">
                <label class="form-input__label">Контрольная строка <span class="form-register__required-mark">*</span></label>
                <input
                    type="text"
                    name="USER_CHECKWORD"
                    maxlength="50"
                    value="<?= $arResult["USER_CHECKWORD"] ?>"
                    class="form-input__field"
                    required="required"
                />
            </div>

            <div class="form-input form-input--password form-register__field">
                <label class="form-input__label">Новый пароль <span class="form-register__required-mark">*</span></label>
                <input
                    type="password"
                    name="USER_PASSWORD"
                    maxlength="50"
                    value="<?= $arResult["USER_PASSWORD"] ?>"
                    class="form-input__field"
                    required="required"
                />
            </div>

            <div class="form-input form-input--password form-register__field">
                <label class="form-input__label">Подтверждение пароля <span class="form-register__required-mark">*</span></label>
                <input
                    type="password"
                    name="USER_CONFIRM_PASSWORD"
                    maxlength="50"
                    value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>"
                    class="form-input__field"
                    required="required"
                />
            </div>

            <input
                type="submit"
                name="change_pwd"
                value="<?= GetMessage("AUTH_CHANGE") ?>"
                class="btn form-register__submit"
            />

        </form>

    </div>
</div>

<p><?= $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"] ?></p>

<p><span class="form-register__required-mark">*</span>  — обязательные поля</p>
<p><strong><a href="<?= $arResult["AUTH_AUTH_URL"] ?>">Авторизация</a></strong></p>

<script type="text/javascript">
    document.bform.USER_LOGIN.focus();
</script>