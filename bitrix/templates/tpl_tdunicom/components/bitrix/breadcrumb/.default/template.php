<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult)){return "";}

$lastIndex = count($arResult) - 1;

$string = '';
$string .= '<div class="bx-breadcrumb">';
foreach ($arResult as $index => $item){
	if($index != $lastIndex) {
		$string .= '<div class="bx-breadcrumb-item">';
			$string .= ($index > 0) ? ' <i class="fa fa-arrow-right"></i> ' : '';
			$string .= '<a class="link-secondary" href="' . $item["LINK"] . '"><span>' . $item["TITLE"] . '</span></a>';
		$string .= '</div>';
	}else{
		$string .= '<div class="bx-breadcrumb-item">';
			$string .= ' <i class="fa fa-arrow-right"></i> ';
			$string .= '<span>'.$item["TITLE"].'</span>';
		$string .= '</div>';
	}
}
$string .= '</div>';

$itemListElements = [];
foreach ($arResult as $index => $item){
	$itemListElements[] = [
		"@type" => "ListItem",
		"position" => $index + 1,
		"item" => [
			"@id" => '"'.$item["LINK"].'"',
			"name" => '"'.$item["TITLE"].'"'
		]
	];
}
$string .= '<script type="application/ld+json">';
	$string .= '{';
	$string .= '"@context": "https://schema.org",';
	$string .= '"@type": "BreadcrumbList",';
	$string .= '"itemListElement": '.json_encode($itemListElements);
$string .= '}';
$string .= '</script>';

return $string;