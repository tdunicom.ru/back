<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="grid grid--padding-y grid--par-like">

    <? foreach ($arResult['ITEMS'] as $arItem): ?>
    <div class="grid__cell grid__cell--m-3 grid__cell--s-3 grid__cell--xs-6">
        <div class="products-list-element">

            <a
                class="products-list-element__image"
                href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
            >
                <picture>
                    <source
                        srcset="<?= WebPHelper($arItem["PREVIEW_PICTURE"]["SRC"]) ?>"
                        type="image/webp"
                    />
                    <img
                        src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                        alt="<?= $arItem["NAME"] ?>"
                    />
                </picture>
            </a>

            <a
                class="products-list-element__name"
                href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
            ><?= $arItem["NAME"] ?></a>

            <div class="products-list-element__price"><?= getOfferMinPriceOrBasePrice($arItem) ?></div>

            <div class="products-list-element__buy">
                <a
                    class="btn"
                    href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                >Купить</a>
            </div>

        </div>
    </div>
    <? endforeach ?>

</div>