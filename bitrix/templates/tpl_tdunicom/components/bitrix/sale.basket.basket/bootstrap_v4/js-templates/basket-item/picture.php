{{#DETAIL_PAGE_URL}}
<a href="{{DETAIL_PAGE_URL}}">
    {{/DETAIL_PAGE_URL}}

    <img
        alt="{{NAME}}"
        src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}"
    >

    {{#SHOW_LABEL}}
    <div class="basket-item-label-text basket-item-label-big <?=$labelPositionClass?>">
        {{#LABEL_VALUES}}
        <div{{#HIDE_MOBILE}} class="d-none d-sm-block"{{/HIDE_MOBILE}}>
            <span title="{{NAME}}">{{NAME}}</span>
        </div>
        {{/LABEL_VALUES}}
    </div>
    {{/SHOW_LABEL}}

    {{#DETAIL_PAGE_URL}}
</a>

{{/DETAIL_PAGE_URL}}
