{{#WARNINGS.length}}
<div class="basket-items-list-item-warning-container">
	<div class="alert alert-warning alert-dismissable" data-entity="basket-item-warning-node">
		<span class="close" data-entity="basket-item-warning-close">&times;</span>
		{{#WARNINGS}}
		<div data-entity="basket-item-warning-text">{{{.}}}</div>
		{{/WARNINGS}}
	</div>
</div>
{{/WARNINGS.length}}