<? use Bitrix\Main\Localization\Loc; ?>
<div class="cart-table__properties basket-item-block-properties">
	<?
	if (!empty($arParams['PRODUCT_BLOCKS_ORDER']))
	{
		foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName)
		{
			switch (trim((string)$blockName))
			{

				case 'props':
                    break;

				case 'sku':
					?>
					{{#SKU_BLOCK_LIST}}
					{{#IS_IMAGE}}
					<div class="basket-item-property basket-item-property-scu-image"
						 data-entity="basket-item-sku-block">
						<div class="basket-item-property-name">{{NAME}}</div>
						<div class="basket-item-property-value">
							<ul class="basket-item-scu-list">
								{{#SKU_VALUES_LIST}}
								<li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
                                                                            {{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
									title="{{NAME}}"
									data-entity="basket-item-sku-field"
									data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
									data-value-id="{{VALUE_ID}}"
									data-sku-name="{{NAME}}"
									data-property="{{PROP_CODE}}"
                                >
                                    <span
                                        class="basket-item-scu-item-inner"
                                        style="background-image: url({{PICT}});"
                                    ></span>
								</li>
								{{/SKU_VALUES_LIST}}
							</ul>
						</div>
					</div>
					{{/IS_IMAGE}}

					{{^IS_IMAGE}}
					<div class="basket-item-property basket-item-property-scu-text"
						 data-entity="basket-item-sku-block">
						<div class="basket-item-property-name">{{NAME}}</div>
						<div class="basket-item-property-value">
							<ul class="basket-item-scu-list">
								{{#SKU_VALUES_LIST}}
								<li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
                                                                            {{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
									title="{{NAME}}"
									data-entity="basket-item-sku-field"
									data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
									data-value-id="{{VALUE_ID}}"
									data-sku-name="{{NAME}}"
									data-property="{{PROP_CODE}}">
									<span class="basket-item-scu-item-inner">{{NAME}}</span>
								</li>
								{{/SKU_VALUES_LIST}}
							</ul>
						</div>
					</div>
					{{/IS_IMAGE}}
					{{/SKU_BLOCK_LIST}}

					{{#HAS_SIMILAR_ITEMS}}
					<div class="basket-items-list-item-double" data-entity="basket-item-sku-notification">
						<div class="alert alert-info alert-dismissable text-center">
							{{#USE_FILTER}}
							<a href="javascript:void(0)"
							   class="basket-items-list-item-double-anchor"
							   data-entity="basket-item-show-similar-link">
								{{/USE_FILTER}}
								<?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P1')?>{{#USE_FILTER}}</a>{{/USE_FILTER}}
							<?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P2')?>
							{{SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}
							<br>
							<a href="javascript:void(0)" class="basket-items-list-item-double-anchor"
							   data-entity="basket-item-merge-sku-link">
								<?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P3')?>
								{{TOTAL_SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}?
							</a>
						</div>
					</div>
					{{/HAS_SIMILAR_ITEMS}}
					<?
					break;

				case 'columns':
					?>
					{{#COLUMN_LIST}}
					{{#IS_IMAGE}}
					<div
                        class="basket-item-property-custom basket-item-property-custom-photo {{#HIDE_MOBILE}}d-none d-sm-block{{/HIDE_MOBILE}}"
                        data-entity="basket-item-property"
                    >
						<div class="basket-item-property-custom-name">{{NAME}}</div>
						<div class="basket-item-property-custom-value">
							{{#VALUE}}
							<span>
                                <img
                                    class="basket-item-custom-block-photo-item"
                                    src="{{{IMAGE_SRC}}}" data-image-index="{{INDEX}}"
                                    data-column-property-code="{{CODE}}"
                                >
                            </span>
							{{/VALUE}}
						</div>
					</div>
					{{/IS_IMAGE}}

					{{#IS_TEXT}}
					<div
                        class="basket-item-property-custom basket-item-property-custom-text {{#HIDE_MOBILE}}d-none d-sm-block{{/HIDE_MOBILE}}"
                        data-entity="basket-item-property"
                    >
						<div class="basket-item-property-custom-name">{{NAME}}</div>
						<div class="basket-item-property-custom-value"
							 data-column-property-code="{{CODE}}"
							 data-entity="basket-item-property-column-value">
							{{VALUE}}
						</div>
					</div>
					{{/IS_TEXT}}

					{{#IS_LINK}}
					<div
                        class="basket-item-property-custom basket-item-property-custom-text {{#HIDE_MOBILE}}d-none d-sm-block{{/HIDE_MOBILE}}"
                        data-entity="basket-item-property"
                    >
						<div class="basket-item-property-custom-name">{{NAME}}</div>
						<div class="basket-item-property-custom-value"
							 data-column-property-code="{{CODE}}"
							 data-entity="basket-item-property-column-value">
							{{#VALUE}}
							{{{LINK}}}{{^IS_LAST}}<br>{{/IS_LAST}}
							{{/VALUE}}
						</div>
					</div>
					{{/IS_LINK}}
					{{/COLUMN_LIST}}
					<?
					break;
			}
		}
	}
	?>
</div>
