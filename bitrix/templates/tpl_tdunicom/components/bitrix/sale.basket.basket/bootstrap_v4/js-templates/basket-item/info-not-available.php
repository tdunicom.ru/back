<? use Bitrix\Main\Localization\Loc; ?>
{{#NOT_AVAILABLE}}
<div class="basket-items-list-item-warning-container">
	<div class="alert alert-warning text-center">
		<?=Loc::getMessage('SBB_BASKET_ITEM_NOT_AVAILABLE')?>.
	</div>
</div>
{{/NOT_AVAILABLE}}