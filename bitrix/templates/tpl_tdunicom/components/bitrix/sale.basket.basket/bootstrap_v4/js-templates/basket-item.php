<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();} ?>

<? include __DIR__ . "/basket-item/init.php" ?>

<script id="basket-item-template" type="text/html">

	<div
        class="cart-table__item {{#SHOW_RESTORE}} basket-items-list-item-container-expend{{/SHOW_RESTORE}}"
		id="basket-item-{{ID}}"
        data-entity="basket-item"
        data-id="{{ID}}"
    >

        <? include __DIR__."/basket-item/show-restore.php" ?>

        <div class="cart-table__image">
            <? include __DIR__."/basket-item/picture.php" ?>
        </div>

        <div class="cart-table__body">

            <div class="cart-table__info">
                <? include __DIR__."/basket-item/info-name.php" ?>
                <? include __DIR__."/basket-item/info-not-available.php" ?>
                <? include __DIR__."/basket-item/info-delayed.php" ?>
                <? include __DIR__."/basket-item/info-warnings.php" ?>
                <? include __DIR__."/basket-item/info-show_loading.php" ?>
            </div>

            <div class="cart-table__properties">
                <? include __DIR__."/basket-item/properties-amount.php" ?>
                <? include __DIR__."/basket-item/properties-price.php" ?>
                <? include __DIR__."/basket-item/properties-sum.php" ?>
                <? include __DIR__."/basket-item/properties-action.php" ?>
            </div>

        </div>
		{{/SHOW_RESTORE}}
	</div>

</script>