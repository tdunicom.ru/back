<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die() ?>
<? use Bitrix\Main\Localization\Loc; ?>

<script id="basket-item-template" type="text/html">
    <div
            class="cart-table__item basket-items-list-item-container"
            id="basket-item-{{ID}}"
            data-entity="basket-item"
            data-id="{{ID}}"
    >
        {{#SHOW_RESTORE}}
        <div class="basket-items-list-item-notification cart-table__notification">
            <div class="basket-items-list-item-notification-inner basket-items-list-item-notification-removed" id="basket-item-height-aligner-{{ID}}">
                <div class="basket-items-list-item-removed-container">
                    <div>
						<?=Loc::getMessage('SBB_GOOD_CAP')?> <strong>{{NAME}}</strong>
                        <?=Loc::getMessage('SBB_BASKET_ITEM_DELETED')?>.
                    </div>
                </div>
            </div>
        </div>
        {{/SHOW_RESTORE}}
        {{^SHOW_RESTORE}}

        <div class="cart-table__image">
			<? include __DIR__."/include/picture.php" ?>
        </div>

        <div class="cart-table__body">

            <div class="cart-table__info">
				<? include __DIR__."/include/title.php" ?>
				<? include __DIR__."/include/properties.php" ?>
            </div>

            <div class="cart-table__properties">
				<? include __DIR__."/include/quantity.php" ?>
				<? include __DIR__."/include/price.php" ?>
				<? include __DIR__."/include/actions.php" ?>
            </div>

        </div>

        {{/SHOW_RESTORE}}

    </div>
</script>