<? $imageSrc = !empty($item["PRODUCT_PREVIEW"]["SRC"]) ? $item["PRODUCT_PREVIEW"]["SRC"] : $item["PREVIEW_PICTURE"]["SRC"]  ?>

<a
    class="product-item-image-wrapper products-list-element__image"
    href="<?=$item['DETAIL_PAGE_URL']?>"
    title="<?=$imgTitle?>"
    data-entity="image-wrapper"
>

    <picture>
        <source srcset="<?= $imageSrc ?>" type="image/webp" />
        <img src="<?= $imageSrc ?>" alt="<?= $item["NAME"] ?>" />
    </picture>

    <span
        class="product-item-image-slider-slide-container slide"
        id="<?=$itemIds['PICT_SLIDER']?>"
        <?= ($showSlider ? '' : 'style="display: none;"') ?>
        data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>"
        data-slider-wrap="true"
    ></span>

    <span class="product-item-image-original" id="<?=$itemIds['PICT']?>"></span>

	<? if ($item['SECOND_PICT']): ?>
        <span class="product-item-image-alternative" id="<?=$itemIds['SECOND_PICT']?>"></span>
	<? endif ?>

</a>