<? if ($actualItem['CAN_BUY']): ?>
	<div class="products-list-element__buy product-item-button-container" id="<?=$itemIds['BASKET_ACTIONS']?>">
		<a class="btn <?=$buttonSizeClass?>" id="<?=$itemIds['BUY_LINK']?>"
		   href="javascript:void(0)" rel="nofollow">
			<?=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
		</a>
	</div>
<? else: ?>
	<div class="products-list-element__buy product-item-button-container">

		<? if ($showSubscribe){
			$APPLICATION->IncludeComponent(
				'bitrix:catalog.product.subscribe',
				'',
				array(
					'PRODUCT_ID' => $actualItem['ID'],
					'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
					'BUTTON_CLASS' => 'btn '.$buttonSizeClass,
					'DEFAULT_DISPLAY' => true,
					'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
				),
				$component,
				array('HIDE_ICONS' => 'Y')
			);
		} ?>

		<a
			class="btn btn-link <?=$buttonSizeClass?>"
			id="<?=$itemIds['NOT_AVAILABLE_MESS']?>"
			href="javascript:void(0)"
			rel="nofollow"
		>
			<?= $arParams['MESS_NOT_AVAILABLE'] ?>
		</a>

	</div>
<? endif ?>