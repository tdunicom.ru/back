<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$isShowOffersAddToCartButton = false;
if(!empty($arParams["IS_SHOW_OFFERS_ADD_TO_CART_BUTTON"])){
    if((count($item["OFFERS"])) > 1){
         $isShowOffersAddToCartButton = true;
    }
}
?>
<div
    class="grid__cell grid__cell--l-3 grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12"
    id="<?= $areaId ?>"
    data-entity="item"
>
    <div class="products-list-element product-item <?= $isShowOffersAddToCartButton ? "products-list-element--wide" : "" ?>">
        <? include __DIR__."/image.php" ?>
        <? include __DIR__."/name.php" ?>
        <? if($isShowOffersAddToCartButton): ?>
            <? include __DIR__."/offers-add-to-cart-button.php" ?>
        <? else: ?>
            <? include __DIR__."/price.php" ?>
            <? include __DIR__."/button.php" ?>
        <? endif ?>
        <? include __DIR__."/hidden-offers.php" ?>
    </div>
</div>