<div
    class="product-item-info-container product-item-price-container"
    data-entity="price-block"
>

    <div
        class="products-list-element__price product-item-price-current"
        id="<?= $itemIds['PRICE'] ?>"
    >
    <?
    if (!empty($price))
    {
        if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
        {
            echo Loc::getMessage(
                'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                array(
                    '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                    '#VALUE#' => $measureRatio,
                    '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                )
            );
        }
        else
        {
            echo $price['PRINT_RATIO_PRICE'];
        }
    }
    ?>
    </div>
</div>