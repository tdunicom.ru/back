<div class="page-section">
    <div class="container">
        <div class="content">

            <p><?= GetMessage("SEARCH_ERROR") ?></p>
            <? ShowError($arResult["ERROR_TEXT"]) ?>
            <p><?= GetMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>
            <br /><br />
            <p><?= GetMessage("SEARCH_SINTAX")?><br /><b><?= GetMessage("SEARCH_LOGIC") ?></b></p>

            <table border="0" cellpadding="5">
                <tr>
                    <td align="center" valign="top"><?= GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
                    <td><?= GetMessage("SEARCH_DESCRIPTION")?></td>
                </tr>
                <tr>
                    <td align="center" valign="top"><?= GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
                    <td><?= GetMessage("SEARCH_AND_ALT")?></td>
                </tr>
                <tr>
                    <td align="center" valign="top"><?= GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
                    <td><?= GetMessage("SEARCH_OR_ALT") ?></td>
                </tr>
                <tr>
                    <td align="center" valign="top"><?= GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
                    <td><?= GetMessage("SEARCH_NOT_ALT") ?></td>
                </tr>
                <tr>
                    <td align="center" valign="top">( )</td>
                    <td valign="top">&nbsp;</td>
                    <td><?= GetMessage("SEARCH_BRACKETS_ALT") ?></td>
                </tr>
            </table>

        </div>
    </div>
</div>