<div class="container">
	<div class="content">
		<header class="header">

			<div class="logo">
				<a href="/"><? include __DIR__."/logo.php" ?></a>
			</div>

			<div class="header_contacts">
				<div class="header_contacts_phone"><? include __DIR__."/contacts_phone.php" ?></div>
				<div class="header_contacts_callback"><? include __DIR__."/contacts_callback.php" ?></div>
			</div>

            <?
            $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.line",
                "header", //
                Array(
                    "PATH_TO_BASKET" => SITE_DIR."personal/cart/",  // Страница корзины
                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",  // Персональный раздел
                    "SHOW_PERSONAL_LINK" => "N",  // Отображать персональный раздел
                    "SHOW_NUM_PRODUCTS" => "Y",  // Показывать количество товаров
                    "SHOW_TOTAL_PRICE" => "N",  // Показывать общую сумму по товарам
                    "SHOW_PRODUCTS" => "N",  // Показывать список товаров
                    "POSITION_FIXED" => "N",  // Отображать корзину поверх шаблона
                    "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",  // Страница оформления заказа
                    "SHOW_DELAY" => "N",  // Показывать отложенные товары
                    "SHOW_NOTAVAIL" => "N",  // Показывать товары, недоступные для покупки
                    "SHOW_SUBSCRIBE" => "N",  // Показывать товары, на которые подписан покупатель
                    "SHOW_IMAGE" => "N",  // Выводить картинку товара
                    "SHOW_PRICE" => "N",  // Выводить цену товара
                    "SHOW_SUMMARY" => "N",  // Выводить подытог по строке
                ),
                false
            );
            ?>

		</header>
	</div>
</div>