<div class="container prefooter-contacts">
	<div class="content">
		<div class="block-wrap block-wrap_wrap ">
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 prefooter-contacts__item">
				<div class="prefooter-contacts__title">
					 Отдел продаж
				</div>
				<div class="prefooter-contacts__info">
					 197022,&nbsp;Санкт-Петербург,<br>
					 пр.Медиков, д.5, лит.Б, оф.337а<br>
				</div>
				<div class="prefooter-contacts__info">
 <a href="/informatsiya/">Информация / FAQ</a>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 prefooter-contacts__item">
				<div class="prefooter-contacts__title">
					 Склад выдачи
				</div>
				<div class="prefooter-contacts__info">
					 Всеволожский район, д.Щеглово, ул.&nbsp;Инженерная, 17
				</div>
				<div class="prefooter-contacts__info">
 <a href="/kontakty/">Как нас найти?</a>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 prefooter-contacts__item">
				<div class="prefooter-contacts__phones">
					<div class="prefooter-contacts__phone">
            <p><span>+7 (812) 414-97-93</span> <small style="font-size: 0.875rem; font-weight: normal">для юр. лиц и ИП</small></p>
            <p><span>+7 (812) 414-97-82</span> <small style="font-size: 0.875rem; font-weight: normal">для физ. лиц</small></p>
					</div>

				</div>
				<div class="prefooter-contacts__info">
					 Круглосуточно, без&nbsp;выходных
				</div>
				<div class="prefooter-contacts__info">
          <a href="mailto:mainsale@tdunicom.ru">mainsale@tdunicom.ru</a>
				</div>
				<ul class="prefooter-socials">
					<li class="prefooter-socials__item prefooter-socials__item_vk"><a href="https://vk.com/kvarcpesok" target="_blank"></a></li>
					<li class="prefooter-socials__item prefooter-socials__item_fb"><a href="https://www.facebook.com/%D0%AE%D0%BD%D0%B8%D0%BA%D0%BE%D0%BC-%D0%BA%D0%B2%D0%B0%D1%80%D1%86%D0%B5%D0%B2%D1%8B%D0%B9-%D0%BF%D0%B5%D1%81%D0%BE%D0%BA-%D0%B2-%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3%D0%B5-213232975753732/" target="_blank"></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
 <br>
