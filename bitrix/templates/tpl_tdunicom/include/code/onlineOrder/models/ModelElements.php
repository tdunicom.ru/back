<?
class ModelElements
{
    const STATUS_PENDING = 1; // Ожидает оплаты
    const STATUS_PAID = 2; // Оплачен

    public function isOrderAlreadyPaid($request)
    {
      $arOrder  = [];
      $arFilter = [
        "IBLOCK_ID" => IBLOCK_ID_ORDERS,
        "PROPERTY_PHONE" => $request->phone,
        "PROPERTY_PAID_STATUS" => self::STATUS_PAID,
        "PROPERTY_PAID_DATE" => date("Y-m-d"), // Оплачен сегодня
      ];

      $arLimit  = [];
      $arSelect = [
        "IBLOCK_ID",
        "ID",
        "PROPERTY_PAID_DATE",
      ];
      $res = CIBlockElement::GetList($arOrder, $arFilter, false, $arLimit, $arSelect);

      if($res->Fetch()){
          return true;
      }

      return false;
    }

    public function createNewOrder($request)
    {
        if(
            empty($request->quantity) ||
            empty($request->cost) ||
            empty($request->name) ||
            empty($request->phone) ||
            empty($request->email)
        ){
            throw new Exception("Не все поля ввода заполнены");
        }

        $dateForUser = date('Y/m/d H:i:s');
        $dateForCode = ConvertTimeStamp(time(), "FULL");

        $topicForUser = $request->name.'_'.$dateForUser;
        $topicForCode = $request->name.'_'.$dateForCode;

        //code generation
        $arParamsCode = array(
          "max_len" => 255,
          "change_case" => "L",
          "replace_space" => '-',
          "replace_other" => '-',
          "delete_repeat_replace" => true
        );

        $code = CUtil::translit($topicForCode, "ru", $arParamsCode);

        $element = new CIBlockElement;

        $elementProps = [
          "QUANTITY" => $request->quantity,
          "NAME" => $request->name,
          "PHONE" => $request->phone,
          "EMAIL" => $request->email,
          "SUM" => ModelHelper::CurrencyFormat($request->cost),
          "PAID_STATUS" => self::STATUS_PENDING
        ];

        $arElementFields = [
          "IBLOCK_ID" => IBLOCK_ID_ORDERS,
          "NAME" => $topicForUser,
          "CODE" => $code,
          "DATE_ACTIVE_FROM" => $dateForCode,
          "PROPERTY_VALUES" => $elementProps
        ];

        if(!$newElementId = $element->Add($arElementFields)){
            throw new Exception($element->LAST_ERROR);
        }

        return $newElementId;
    }


}





