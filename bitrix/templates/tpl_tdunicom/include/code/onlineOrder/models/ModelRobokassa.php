<?
class ModelRobokassa
{
	const SAND_TYPE = "песок сухой кварцевый";

    public function getRobokassaForm($inv_id, $request)
    {
        $mrh_login = ROBOKASSA_SHOP_ID;      // your login here
        $mrh_pass1 = ROBOKASSA_PASSWORD_1;   // merchant pass1 here

		$out_summ = $request->cost;

        $receipt = $this->getReceipt($request, $out_summ);

        // order properties
        $inv_desc = "Оплата заказа песка на kvarc-pesok.ru";   // invoice desc

        // build CRC value
        $crc = md5("$mrh_login:$out_summ:$inv_id:$receipt:$mrh_pass1");

        $paramsRequired = [
            "MerchantLogin" => $mrh_login,
            "OutSum" => $out_summ,
            "InvDesc" => $inv_desc,
            "Receipt" => $receipt,
            "SignatureValue" => $crc,
        ];

        $paramsNotRequired = [
            "InvId" => $inv_id,
        ];

        if($request->email){
            $paramsNotRequired["Email"] = $request->email;
        }

        // build URL
        $action = "https://auth.robokassa.ru/Merchant/Index.aspx";
        $method = "post";

        $form = '<form action="'.$action.'" method="'.$method.'">'."\n";
        $form .= $this->getFormFields($paramsRequired, $paramsNotRequired)."\n";
        $form .= '</form>';

        return $form;
    }

    private function getFormFields($paramsRequired, $paramsNotRequired)
    {
        $paramsWKeys = $paramsRequired + $paramsNotRequired;

        $paramsWoKeys = [];
        foreach ($paramsWKeys as $name => $value) {
            $paramsWoKeys[] = "<input type='hidden' name='".$name."' value='".$value."'>";
        }

        $params = implode("\n", $paramsWoKeys);

        return $params;
    }

    private function getReceipt($request, $out_summ)
    {
        $receipt = [
            "items" => [
                [
                    "name" => self::SAND_TYPE,
                    "sum" => $out_summ,
                    "quantity" => $request->quantity,
                    "payment_method" => "full_payment",
                    "payment_object" => "payment",
                ],
            ],
        ];

        $receipt = json_encode($receipt);

        return $receipt;
    }

}