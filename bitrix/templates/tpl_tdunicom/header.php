<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?><!DOCTYPE html>
<html lang="ru">

<head>
    <?php include __DIR__ . "/include/header/head.php" ?>
</head>

<body>

<?php $APPLICATION->ShowPanel() ?>

<?php include __DIR__ . "/include/header/top.php" ?>

<?php include __DIR__ . "/include/header/menu-products.php" ?>
<?php include __DIR__ . "/include/header/menu-top.php" ?>

<main>

    <?php if (isInnerPage()): ?>
    <div class="container">
        <div class="content">
            <?php include __DIR__ . "/include/header/breadcrumbs.php" ?>
            <h1><?php $APPLICATION->ShowTitle(false) ?></h1>
            <?php endif ?>

