<?
// добавляем обработчик события при регистрации
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");

function OnBeforeUserRegisterHandler($args)
{
	if (!preg_match('/^[а-яА-ЯёЁ]+$/u', $args["NAME"])) {
		$GLOBALS["APPLICATION"]->ThrowException('В поле "Имя" должно быть одно слово на кириллице');
		return false;
	}

	return true;
}