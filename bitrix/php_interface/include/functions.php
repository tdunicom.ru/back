<?
function pluralDeclension(string $singular, int $count): string
{
	$result = "";

	if(in_array($count, [11, 12, 13, 14])){
		$result = $singular."ов";
	}

	if(!$result){
		switch($count % 10){
			case 1:
				$result = $singular; break;
			case 2:
			case 3:
			case 4:
				$result = $singular."а"; break;
			default:
				$result = $singular."ов";
		}
	}

	if($result == "мешоков"){$result = "мешков";}
	if($result == "мешока"){$result = "мешка";}

	return $result;
}

function camelize($input)
{
	return str_replace('_', '', ucwords($input, '_'));
}