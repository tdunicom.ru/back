<?
AddEventHandler("iblock", "OnAfterIBlockElementAdd", ["ImageResize", "RunOnElement"]);
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", ["ImageResize", "RunOnElement"]);

//AddEventHandler("iblock", "OnAfterIBlockSectionAdd", ["ImageResize", "RunOnSection"]);
//AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", ["ImageResize", "RunOnSection"]);

use Models\Bx\Element as BxElement;
use Models\Bx\Section as BxSection;

class ImageResize
{
	public static function RunOnElement(&$arFields)
	{
		self::initModels();
		$bx = new BxElement;

		if($arFields["IBLOCK_ID"] == IBLOCK_ID_PRODUCTS){
			self::process(Models\Image\Type\Products\Product\Preview::class, $arFields["ID"], $bx);
			self::process(Models\Image\Type\Products\Product\Detail::class, $arFields["ID"], $bx);
		}
	}

	public function RunOnSection(&$arFields)
	{
		self::initModels();
		$bx = new BxSection;

		if($arFields["IBLOCK_ID"] == IBLOCK_ID_PRODUCTS){
			self::process(Models\Image\Type\Products\Section\Preview::class, $arFields["ID"], $bx);
		}
	}

	private static function process($modelName, $elementId, $bx)
	{
		$images = $bx->getImages($modelName, $elementId);
		$resizer = new Models\Image\Resizer;

		foreach ($images as $image){
			$resizer->makeCopies($image);
		}
	}

	private static function initModels()
	{
		$modelsPath = __DIR__."/Models";
		require_once $modelsPath."/Image/Type/ImageType.php";

		$dir = new RecursiveDirectoryIterator($modelsPath);
		foreach (new RecursiveIteratorIterator($dir) as $file) {
			if (!is_dir($file)) {
				if( fnmatch('*.php', $file) ){
					require_once $file;
				}
			}
		}
	}

}