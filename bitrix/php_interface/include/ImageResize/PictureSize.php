<?
class PictureSize
{
	public $name;
	public $mediaQuery;
	public $densitiesStandard = [];
	public $densitiesWebp = [];

	public function __construct(string $name)
	{
		$this->name = $name;
	}

}