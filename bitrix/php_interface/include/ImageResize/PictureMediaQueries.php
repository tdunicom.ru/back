<?
class PictureMediaQueries
{
	const BREAKPOINT_XS = 479;
	const BREAKPOINT_S = 799;
	const BREAKPOINT_M = 999;

	public static $mediaQueries =
	[
		"XS-S" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"s" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px)"
		],
		"XS-M" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px)"
		],
		"XS-L" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px)"
		],
		"XS-S-M" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"s" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px) or (max-width: ".self::BREAKPOINT_S."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_S + 1)."px)"
		],
		"XS-S-L" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"s" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px) or (max-width: ".self::BREAKPOINT_S."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_S + 1)."px)"
		],
		"XS-M-L" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px) or (max-width: ".self::BREAKPOINT_S."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_S + 1)."px)"
		],
		"XS-S-M-L" => [
			"xs" => "(max-width: ".self::BREAKPOINT_XS."px)",
			"s" => "(min-width: ".(self::BREAKPOINT_XS + 1)."px) or (max-width: ".self::BREAKPOINT_S."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_S + 1)."px) or (max-width: ".self::BREAKPOINT_M."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_M + 1)."px)"
		],

		"S-M" => [
			"s" => "(max-width: ".self::BREAKPOINT_S."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_S + 1)."px)"
		],
		"S-L" => [
			"s" => "(max-width: ".self::BREAKPOINT_S."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_S + 1)."px)"
		],
		"S-M-L" => [
			"s" => "(max-width: ".self::BREAKPOINT_S."px)",
			"m" => "(min-width: ".(self::BREAKPOINT_S + 1)."px) or (max-width: ".self::BREAKPOINT_M."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_M + 1)."px)"
		],

		"M-L" => [
			"m" => "(max-width: ".self::BREAKPOINT_M."px)",
			"l" => "(min-width: ".(self::BREAKPOINT_M + 1)."px)"
		]
	];

}