<?
use Models\DimensionHelper;
use Models\PathHelper;

class PictureTag
{
	const DENSITIES = [1, 2];
	const SIZES = ["xs", "l"];

	public function __construct()
	{
		require_once __DIR__."/Models/PathHelper.php";
		require_once __DIR__."/Models/DimensionHelper.php";
		require_once __DIR__."/PictureSize.php";
		require_once __DIR__."/PictureMediaQueries.php";
	}

	public static function render($src, string $pictureAttributes = "", string $imageAttributes = "")
	{
		$thisObj = new self();
		return $thisObj->renderPictureTag($src, $pictureAttributes, $imageAttributes);
	}

	public static function renderWithMQ($src, string $pictureAttributes = "", string $imageAttributes = "")
	{
		$thisObj = new self();
		return $thisObj->renderPictureTag($src, $pictureAttributes, $imageAttributes, true);
	}

	private function renderPictureTag(
		$src,
		string $pictureAttributes = "",
		string $imageAttributes = "",
		bool $withMediaQueries = false
	): string
	{
		if((string)$src == ""){return "";}
		if(!PathHelper::isPictureExist($src)){return "";}

		$pictureNameArr = $this->getPictureNameFromSrc($src);

		$allSizes = $this->getExistSizes($pictureNameArr);

		if($allSizes){
			$allSizes = $this->addMediaQueries($allSizes, $withMediaQueries);
			$sources = array_merge(
				$this->getWebpSources($pictureNameArr, $allSizes),
				$this->getStandardSources($pictureNameArr, $allSizes)
			);
			$width = $this->getImgTagWidth($pictureNameArr, $allSizes);
			$height = $this->getImgTagHeight($src, $width);
		}else{
			$sources = null;
			$width = DimensionHelper::getWidth($_SERVER["DOCUMENT_ROOT"].$src);
			$height = DimensionHelper::getHeight($_SERVER["DOCUMENT_ROOT"].$src);
		}

		$imageTag = $this->getImageTag($src, $width, $height, $imageAttributes);
		$pictureTag = $this->getPictureTag($imageTag, $pictureAttributes, $sources);

		return $pictureTag;
	}

	private function getPictureNameFromSrc(string $src) : array
	{
		$file = explode("/", $src);
		$fileName = end($file);
		$basePath = str_replace($fileName, "", $src);
		$fileName = explode(".", $fileName);

		return [
			"base_path" => $basePath,
			"name" => $fileName[0],
			"extension" => $fileName[1]
		];
	}

	private function getExistSizes(array $picture) : array
	{
		$existSizes = [];
		foreach (self::SIZES as $sizeName){

			$existWebpDensities = [];
			$existStandardDensities = [];

			foreach (self::DENSITIES as $density){
				$densitySuffix = self::getDensitySuffix($density);

				$path = $picture["base_path"].$picture["name"]."_".$sizeName.$densitySuffix.".webp";
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$path)){
					$existWebpDensities[] = $density;
				}

				$path = $picture["base_path"].$picture["name"]."_".$sizeName.$densitySuffix.".".$picture["extension"];
				if(file_exists($_SERVER["DOCUMENT_ROOT"].$path)){
					$existStandardDensities[] = $density;
				}
			}

			if(!empty($existWebpDensities) OR !empty($existStandardDensities)){
				$pictureSize = new PictureSize($sizeName);
				$pictureSize->densitiesWebp = $existWebpDensities;
				$pictureSize->densitiesStandard = $existStandardDensities;
				$existSizes[] = $pictureSize;
			}

		}

		return $existSizes;
	}

	private function addMediaQueries(array $allSizes, bool $withMediaQueries): array
	{
		if(!$withMediaQueries OR count($allSizes) == 1){
			return $allSizes;
		}

		$sizesSignature = array_map(function (PictureSize $size) {
			return strtoupper($size->name);
		}, $allSizes);
		$sizesSignature = implode("-", $sizesSignature);

		$refSizesSignatureToMQ = PictureMediaQueries::$mediaQueries;

		if(!array_key_exists($sizesSignature, $refSizesSignatureToMQ)){
			return $allSizes;
		}

		$mediaQueries = $refSizesSignatureToMQ[$sizesSignature];

		$allSizes = array_map(function (PictureSize $size) use ($mediaQueries) : PictureSize {
			if(array_key_exists($size->name, $mediaQueries)){
				$size->mediaQuery = ' media="'.$mediaQueries[$size->name].'"';
			}
			return $size;
		}, $allSizes);

		return $allSizes;
	}

	private function getWebpSources(array $picture, array $allSizes) : array
	{
		$sources = [];

		/** @var PictureSize $size */
		foreach ($allSizes as $size){
			$srcsets = $this->getSrcsets($picture, $size->densitiesWebp, $size->name, "webp");
			if($srcsets){
				$sources[] = '<source srcset="'.implode(", ", $srcsets).'" type="image/webp"'.$size->mediaQuery.' />';
			}
		}

		return $sources;
	}

	private function getStandardSources(array $picture, array $allSizes) : array
	{
		$sources = [];

		/** @var PictureSize $size */
		foreach ($allSizes as $size){
			$srcsets = $this->getSrcsets($picture, $size->densitiesStandard, $size->name);
			if($srcsets){
				$sources[] = '<source srcset="'.implode(", ", $srcsets).'"'.$size->mediaQuery.' />';
			}
		}

		return $sources;
	}

	private function getSrcsets(array $picture, array $densities, string $sizeName, string $extension = "") : array
	{
		$srcsets = [];

		$extension = $extension ? $extension : $picture["extension"];

		foreach ($densities as $density){
			$path = $picture["base_path"].$picture["name"]."_".$sizeName.self::getDensitySuffix($density).".".$extension;
			$srcsets[] = $path.self::getDensityPostfix($density);
		}

		return $srcsets;
	}

	private static function getDensitySuffix(int $density) : string
	{
		return $density > 1 ? "@".$density."x" : "";
	}

	private static function getDensityPostfix(int $density) : string
	{
		return $density > 1 ? " ".$density."x" : "";
	}

	private function getImageTag(string $src, int $width, int $height, string $attributes) : string
	{
		$attribs = [
			"src" => $src,
			"width" => $width,
			"height" => $height,
			"attributes" => $attributes,
			"loading" => "lazy",
			"decoding" => "async",
			"lazyload" => "true"
		];
		$attribsStr = "";
		foreach ($attribs as $key => $value){
			if($value){
				if($key == "attributes"){
					$attribsStr .= ' '.$value;
				}else{
					$attribsStr .= ' '.$key.'="'.$value.'"';
				}
			}
		}

		$img = '<img'.$attribsStr.' />';

		return $img;
	}

	private function getImgTagWidth(array $picture, array $allSizes): int
	{
		$width = 0;

		foreach ($allSizes as $size){
			$filePath = $picture["base_path"].$picture["name"]."_".$size->name;
			foreach ($size->densitiesStandard as $density){
				if($density == 1){
					$fullPath = $filePath.self::getDensitySuffix($density).".".$picture["extension"];
					$densityWidth = DimensionHelper::getWidth($_SERVER["DOCUMENT_ROOT"].$fullPath);
					if($densityWidth > $width){
						$width = $densityWidth;
					}
				}
			}
			foreach ($size->densitiesWebp as $density){
				if($density == 1){
					$fullPath = $filePath.self::getDensitySuffix($density).".webp";
					$densityWidth = DimensionHelper::getWidth($_SERVER["DOCUMENT_ROOT"].$fullPath);
					if($densityWidth > $width){
						$width = $densityWidth;
					}
				}
			}
		}

		return (int)$width;
	}

	private function getImgTagHeight(string $src, int $width): int
	{
		$originalWidth = DimensionHelper::getWidth($_SERVER["DOCUMENT_ROOT"].$src);
		$originalHeight = DimensionHelper::getHeight($_SERVER["DOCUMENT_ROOT"].$src);

		$height = ($originalHeight * $width) / $originalWidth;

		return (int)$height;
	}

	private function getPictureTag(string $imageTag, string $attributes, $sources): string
	{
		$string = "<picture". ($attributes ? " ".$attributes : "") .">"."\n\t";
		if($sources){
			$string .= implode("\n\t", $sources)."\n\t";
		}
		$string .= $imageTag."\n";
		$string .= "</picture>";

		return $string;
	}

}