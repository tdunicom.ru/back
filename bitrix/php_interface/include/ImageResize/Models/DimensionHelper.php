<?
namespace Models;

class DimensionHelper
{
	public static function getWidth(string $src)
	{
		$imageSize = getimagesize($src);
		return $imageSize[0];
	}

	public static function getHeight(string $src)
	{
		$imageSize = getimagesize($src);
		return $imageSize[1];
	}

}