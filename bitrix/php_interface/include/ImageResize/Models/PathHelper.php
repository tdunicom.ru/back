<?
namespace Models;

class PathHelper
{
	public static function getNewPath(string $src, string $sizeName, int $density): string
	{
		$oldNameFull = explode("/", $src);
		$oldNameFull = end($oldNameFull);
		$oldNameFull = explode(".", $oldNameFull);
		$oldName = reset($oldNameFull);

		if($density == 1){
			$density = "";
		}else{
			$density = "@".$density."x";
		}

		$newName = $oldName."_".strtolower($sizeName).$density;

		return str_replace($oldName, $newName, $src);
	}

	public static function getWebpPath(string $path): string
	{
		$tempPath = explode("/", $path);
		$tempPath = end($tempPath);
		$tempPath = explode(".", $tempPath);
		$extension = end($tempPath);

		return str_replace(".".$extension, ".webp", $path);
	}

	public static function isPictureExist(string $src): bool
	{
		return file_exists($_SERVER["DOCUMENT_ROOT"].$src);
	}

}