<?
namespace Models\Image\Type\Products\Product;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Товары / Товар - Анонсовое изображение";
	const SLUG = "ProductsProductPreview";
	const IBLOCK_ID = IBLOCK_ID_PRODUCTS;
	const SOURCE = "PREVIEW_PICTURE";
	const SIZES = [
		"XS" => [162, 324],
		"L" => [270, 540],
	];
}