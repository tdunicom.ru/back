<?
namespace Models\Image\Type\Products\Product;

use Models\Image\Type\ImageType;

class Detail extends ImageType
{
	const TITLE = "Товары / Товар - Детальное изображение";
	const SLUG = "ProductsProductDetail";
	const IBLOCK_ID = IBLOCK_ID_PRODUCTS;
	const SOURCE = "DETAIL_PICTURE";
	const SIZES = [
		"XS" => [340, 680],
		"L" => [417, 834],
	];
}