<?
namespace Models\Image\Type\Products\Section;

use Models\Image\Type\ImageType;

class Preview extends ImageType
{
	const TITLE = "Товары / Раздел - Изображение";
	const SLUG = "ProductsSectionPreview";
	const IBLOCK_ID = IBLOCK_ID_PRODUCTS;
	const SIZES = [
		"XS" => [440, 880],
		"L" => [373, 746],
	];
}