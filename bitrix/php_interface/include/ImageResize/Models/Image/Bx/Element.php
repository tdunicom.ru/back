<?
namespace Models\Bx;

class Element
{
	public function __construct()
	{
		\CModule::IncludeModule("iblock");
	}

	public function getImages(string $imageClassName, int $elementId = 0): array
    {
		if(!class_exists($imageClassName)){
			throw new \Exception("Не найден класс: ".$imageClassName);
		}

		$arOrder = [];

		$arFilter = [
			"IBLOCK_ID" => $imageClassName::IBLOCK_ID,
		];

		if($elementId){
			$arFilter["ID"] = $elementId;
		}

		$arSelectFields = ["IBLOCK_ID", "ID", "NAME", $imageClassName::SOURCE];

		$res = \CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelectFields);

		$sourceCode = $imageClassName::SOURCE;
		if(strpos($sourceCode, "PROPERTY") !== false){
			$sourceCode .= "_VALUE";
		}

		$images = [];
		while($element = $res->Fetch()){
			if(!empty($element[$sourceCode])){
				$imageId = $element[$sourceCode];
				$images[] = new $imageClassName($imageId);
			}
		}

		return $images;
    }

}

