<?
namespace Models\Bx;

use Models\Image\Type;
use Models\Image\Type\Products;

class Section
{
	public function __construct()
	{
		\CModule::IncludeModule("iblock");
	}

	public function getImages(string $imageClassName, int $elementId = 0): array
    {
    	if(!class_exists($imageClassName)){
			throw new \Exception("Не найден класс: ".$imageClassName);
		}

		$arOrder = [];

		$arFilter = [
			"IBLOCK_ID" => $imageClassName::IBLOCK_ID,
		];

		if($elementId){
			$arFilter["ID"] = $elementId;
		}

		$arSelectFields = ["IBLOCK_ID", "ID", "NAME", "PICTURE"];

		$res = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelectFields);

		$images = [];
		while($section = $res->Fetch()){
			if(!empty($section["PICTURE"])){
				$imageId = $section["PICTURE"];
				$images[] = new $imageClassName($imageId);
			}
		}

		return $images;
    }

}

