<?
class ModelFinishOrder
{
    const STATUS_PAID = 2; //значения свойства "Статус оплаты"  - "Оплачен"
    const SITE_CODE = "s1";
    const EVENT_NAME = "NEW_ONLINE_ORDER";

    private $orderId;

    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    public function setStatusPaid()
    {
        CModule::IncludeModule("iblock");
        CIBlockElement::SetPropertyValueCode($this->orderId, "PAID_STATUS", self::STATUS_PAID);
        CIBlockElement::SetPropertyValueCode($this->orderId, "PAID_DATE", ConvertTimeStamp(time(), "FULL"));
    }

    public function sendMail()
    {
        $order = $this->getOrder();

        if(!$order || !$GLOBALS['prod']){
            return false;
        }

        $backendLink = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $backendLink .= '?IBLOCK_ID='.IBLOCK_ID_ORDERS.'&type=feedback&ID='.$order["ID"];

        $order["BE_LINK"] = $backendLink;

        CEvent::SendImmediate(self::EVENT_NAME, self::SITE_CODE, $order);

        return true;
    }

    private function getOrder()
    {
        $arOrder  = [];
        $arFilter = [
          "IBLOCK_ID" => IBLOCK_ID_ORDERS,
          "ID" => $this->orderId,
        ];
        $arGroupBy = false;
        $arLimit  = [];
        $arSelect = [];
        $res = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arLimit, $arSelect);

        $order = [];
        while($ar_element = $res->GetNextElement()) {

            if(!$ar_element){
                return false;
            }

            $fields = $ar_element->GetFields();
            $properties = $ar_element->GetProperties();

            $order = [
                "ID" => $fields["ID"],
                "NAME" => $properties["NAME"]["VALUE"],
                "PHONE" => $properties["PHONE"]["VALUE"],
                "EMAIL" => $properties["EMAIL"]["VALUE"],
                "DATE_CREATE" => date("d.m.Y", $fields["DATE_CREATE_UNIX"]),
                "PAID_DATE" => $properties["PAID_DATE"]["VALUE"],
                "SAND_TYPE" => $properties["SAND_TYPE"]["VALUE"],
                "FRACTION" => $properties["FRACTION"]["VALUE"],
                "PACKAGING" => $properties["PACKAGING"]["VALUE"],
                "QUANTITY" => $properties["QUANTITY"]["VALUE"],
                "DELIVERY_TYPE" => $properties["DELIVERY_TYPE"]["VALUE"],
                "DELIVERY_COST" => $properties["DELIVERY_COST"]["VALUE"],
                "SUM" => $properties["SUM"]["VALUE"],
            ];
        }

        return $order;
    }


}