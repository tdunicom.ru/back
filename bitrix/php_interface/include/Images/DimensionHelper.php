<?
class DimensionHelper
{
	public static function getWidth($src)
	{
		$imageSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$src);
		return $imageSize[0];
	}

	public static function getHeight($src)
	{
		$imageSize = getimagesize($_SERVER["DOCUMENT_ROOT"].$src);
		return $imageSize[1];
	}

	public static function isImageWider($src, $width)
	{
		return self::getWidth($src) > $width;
	}

}
