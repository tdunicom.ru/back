<?
class ResizeHelper
{
	public static function resize($path, $width)
	{
		$path = $_SERVER["DOCUMENT_ROOT"].$path;
		$gdImage = imagecreatefromwebp($path);
		$gdImageResized = imagescale($gdImage, $width, -1, IMG_BICUBIC);

		imagewebp($gdImageResized, $path);

		imagedestroy($gdImage);
		imagedestroy($gdImageResized);

		return true;
	}
}
