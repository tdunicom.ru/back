<?
function getCurPage(): string
{
	global $APPLICATION;
	$curPage = explode('?', $APPLICATION->GetCurPage());
	$curPage = reset($curPage);

	return $curPage;
}

function isHomePage(): bool
{
	$curPage = getCurPage();
	return $curPage == "/shop/";
}

function isInnerPage(): bool
{
	$curPage = getCurPage();

	if(
		isHomePage() ||
		$curPage == "/kvartseviy-pesok/" ||
		defined("ERROR_404")
	){
		return false;
	}

	return true;
}

function isCatalogPage(): bool
{
	$curPage = getCurPage();
	if(!strpos($curPage, "catalog")){
		return false;
	}

	return true;
}

function isMakeOrderPage()
{
	$curPage = getCurPage();
	if(!strpos($curPage, "personal/order")){
		return false;
	}

	return true;
}

function isShowBreadcrumbsAndTitle(): bool
{
	return isInnerPage();
}
