<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}

if($_SERVER['REMOTE_ADDR'] != "127.0.0.1") {
	$GLOBALS['prod'] = true;
}

include __DIR__."/include/functions.php";
include __DIR__."/include/ImageResize/PictureTag.php";
include __DIR__."/include/ImageResize/ImageResize.php";
include __DIR__."/include/OnBeforeUserRegisterHelper.php";
include __DIR__."/include/SpamHandler.php";
include __DIR__."/include/Images/DimensionHelper.php";
include __DIR__."/include/Images/WebPHelper.php";
include __DIR__."/include/Images/ResizeHelper.php";
include __DIR__."/include/pages.php";



if( CModule::IncludeModule('sale')){
    setcookie('pay_order_id',$_GET['ORDER_ID'],time()+3600, "/");
}
AddEventHandler("sale", "OnOrderNewSendEmail", "ModifyOrderSaleMails");
function ModifyOrderSaleMails($orderID, &$eventName, &$arFields)
{
    if(CModule::IncludeModule("sale") && CModule::IncludeModule("iblock") && !empty($_POST['ORDER_DESCRIPTION']))
    {



        //ОБЪЯВЛЯЕМ ПЕРЕМЕННУЮ ДЛЯ ПИСЬМА
        $arFields["ORDER_TABLE_ITEMS"] = $_POST['ORDER_DESCRIPTION'];
    }
}

if(!isHomePage()){
    //include __DIR__."/include/removeSystemAssets.php";

}
function custom_mail($to, $subject, $body, $headers) {

    $sFrom = 'from@TEST.ru';

    if (strpos($headers, 'Message-ID') === false) {
        $headers .= 'Message-ID : <' . time() . '-' . md5($sFrom . $to) . '@TEST.ru' . ">\r\n";
        return mail($to, $subject, $body, $headers);

    }

}
//AddEventHandler('main', 'OnBeforeEventAdd', 'includeCustomMail');
//function includeCustomMail($event, $lid, $arFields) {
//    if ($event == 'MY_TYPE') {
//        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/classes/custom_mail.php');
//    }
//}

//Массив товаров будущего заказа, может быть получен
//различными путями, например передан из формы "Заказ в один клик"




require __DIR__."/include/ModelFinishOrder.php";

$curPage = getCurPage();

define("IBLOCK_ID_ORDERS", 11);
define("IBLOCK_ID_PRODUCTS", 13);
define("IBLOCK_ID_COMMENTS", 15);
define("IBLOCK_ID_QUICK_BUY", 16);

define("ROBOKASSA_PASSWORD_1", "lllZe7IW9tk26Pe1xMAS");
define("ROBOKASSA_PASSWORD_2", "VMRaur686Xh7UvP1iYUo");
define("ROBOKASSA_SHOP_ID", "UniTech");


function blockInclude($section, $filename)
{
    global $APPLICATION;
    $includeAreasPath = SITE_TEMPLATE_PATH.'/include_areas/';

    return $APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => $includeAreasPath.$section.'/'.$filename.'.php'
        ),
        false
    );
}
AddEventHandler("main", "OnAfterUserLogin", Array("Login", "OnAfterUserLoginHandler"));
class Login
{
    // создаем обработчик события "OnAfterUserLogin"
    public static function OnAfterUserLoginHandler(&$fields)
    {
        // если логин не успешен то
        LocalRedirect("/shop/");

    }
}
AddEventHandler("main", "OnAfterUserLogout", Array("Logout", "OnAfterUserLogoutHandler"));
class Logout
{
    // создаем обработчик события "OnAfterUserLogin"
    public static function OnAfterUserLogoutHandler(&$fields)
    {
        // если логин не успешен то
        LocalRedirect("/shop/");

    }
}
AddEventHandler("main", "OnAfterUserRegister", Array("CustomRegister", "OnAfterUserRegisterHandler"));

class CustomRegister
{
    // создаем обработчик события "OnAfterUserRegister"
    public static function OnAfterUserRegisterHandler(&$arFields)
    {
        global $USER;
        if ($USER->IsAuthorized()){
            // если регистрация успешна то
            LocalRedirect("/shop/");

        }


    }
}

AddEventHandler('main',   'OnEpilog',   '_Check404Error', 1);
function _Check404Error()
{
	if (defined('ERROR_404') && ERROR_404=='Y' && !defined('ADMIN_SECTION'))
	{
		GLOBAL $APPLICATION;
		$APPLICATION->RestartBuffer();

		include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/header.php';
		include $_SERVER['DOCUMENT_ROOT'].'/404.php';
		include $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/'.SITE_TEMPLATE_ID.'/footer.php';

		$title = "Ошибка 404. Страница не найдена";
		$APPLICATION->SetTitle($title);
		$APPLICATION->SetPageProperty("title", $title);
	}
}

function getIbElement($elementId, $isOnlyFields = false)
{
	$res = \CIBlockElement::GetList([], ["ID" => $elementId]);

	if(!$bxElement = $res->GetNextElement()){
		return null;
	}

	$element = new stdClass();
	$element->fields = $bxElement->GetFields();

	if($isOnlyFields){
		return $element->fields;
	}

	$element->props = $bxElement->GetProperties();

	return $element;
}

function getIbElementFields($elementId)
{
	return getIbElement($elementId, true);
}

function getImageSrc($imageId)
{
	return !empty($imageId) ? \CFile::GetPath($imageId) : null;
}

function getSectionPropertyValue($fieldCode, $iblockId, $sectionCode)
{
	$result = false;

	$ufName = [$fieldCode];
	$ufArResult = CIBlockSection::GetList(
		["SORT" => "ASC"],
		["IBLOCK_ID" => $iblockId, "CODE" => $sectionCode],
		false,
		$ufName
	);

	if($ufValue = $ufArResult->GetNext()):
		if(!empty($ufValue[$fieldCode])):
			$result = $ufValue[$fieldCode];
		endif;
	endif;

	return $result;
}

function getCommentsForProduct(int $productId): ?array
{
	CModule::IncludeModule("iblock");
	$arOrder = [];
	$arFilter = [
		"IBLOCK_ID" => IBLOCK_ID_COMMENTS,
		"ACTIVE" => "Y",
		"PROPERTY_PRODUCT" => $productId
	];
	$arGroupBy = false;
	$arNavStartParams = [];
	$arSelectFields = ["IBLOCK_ID", "ID", "NAME", "PREVIEW_TEXT", "PROPERTY_NAME"];
	$resource = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

	$comments = [];
	while($arr = $resource->Fetch()){
		$comments[] = [
			"TITLE" => $arr["NAME"],
			"TEXT" => $arr["PREVIEW_TEXT"],
			"NAME" => $arr["PROPERTY_NAME_VALUE"],
		];
	}

	return $comments;
}

function getProductsForCalculation()
{
	$arOrder = [];
	$arFilter = [
		"IBLOCK_ID" => IBLOCK_ID_PRODUCTS,
		"ACTIVE" => "Y",
		"!PROPERTY_CONSUMPTION" => false
	];
	$arGroupBy = false;
	$arNavStartParams = [];
	$arSelectFields = ["IBLOCK_ID", "ID", "NAME", "PROPERTY_CONSUMPTION"];
	$resource = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

	$products = [];
	while($arr = $resource->Fetch()){
		$products[] = [
			"NAME" => $arr["NAME"],
			"CONSUMPTION" => $arr["PROPERTY_CONSUMPTION_VALUE"],
		];
	}

	return $products;
}

function getAllProductsInSection($sectionId = null)
{
	$arOrder = [];
	$arFilter = [
		"IBLOCK_ID" => IBLOCK_ID_PRODUCTS,
		"ACTIVE" => 'Y',
	];

	if($sectionId){
		$arFilter["SECTION_ID"] = $sectionId;
	}

	$arSelect = [
		"IBLOCK_ID",
		"ID",
		"NAME",
		"DETAIL_PAGE_URL",
	];
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, [], $arSelect);

	$products = [];
	while($obj = $res->GetNext()) {
		$products[] = $obj;
	}

	return $products;
}

AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");

//-- Собственно обработчик события

function bxModifySaleMails($orderID, &$eventName, &$arFields)
{
    $arOrder = CSaleOrder::GetByID($orderID);

    //-- получаем телефоны и адрес
    $order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
    $phone="";

    while ($arProps = $order_props->Fetch())
    {
        if ($arProps["CODE"] == "PHONE")
        {
            $phone = htmlspecialchars($arProps["VALUE"]);
        }





    }

    //-- добавляем новые поля в массив результатов
    $arFields["ORDER_DESCRIPTION"] = $arOrder["USER_DESCRIPTION"];
    $arFields["PHONE"] =  $phone;

}

