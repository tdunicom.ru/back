<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $title; ?></title>
		<link href="<?= BASIC_TEMPLATE ?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= BASIC_TEMPLATE ?>css/base.css" rel="stylesheet">

		<? if(!empty($style)): ?>
		<link href="<?= $style ?>" rel="stylesheet">
		<? endif; ?>

	</head>

	<body>

		<div class="container">

			<div class="page-header">
				<? if(!empty($appRoot)): ?>
					<h1><a href="<?= $appRoot ?>"><?= $title; ?></a></h1>
				<? else: ?>
					<h1><?= $title; ?></h1>
				<? endif; ?>
			</div>

			<? if(!empty($front)): ?>
			<div class="content">
				<?= $front; ?>
			</div>
			<? endif; ?>

			<? if(!empty($result)): ?>
			<div class="result">
				<?= $result; ?>
			</div>
			<? endif; ?>

			<? if(!empty($footer)): ?>
			<div class="bs-callout bs-callout-info">
				<?= Helper::beautifyMessage($footer); ?>
			</div>
			<? endif; ?>

		</div><!-- /.container -->


		<? if(!empty($useJquery)): ?>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<? endif; ?>

		<? if(!empty($script)): ?>
			<script src="<?= $script ?>"></script>
		<? endif; ?>

		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<!-- <script src="js/bootstrap.min.js"></script>-->
  </body>
</html>