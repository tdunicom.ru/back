<?php
class Model
{
	static private $countRetry = 5;
	static private $delay = 1;

	static private function testSingleUrl($url)
	{
		$timeTotal = 0;
		$cnt = 0;
		while($cnt < self::$countRetry)
		{
			if($cnt && self::$delay){
				sleep(self::$delay);
			}

			$begin = microtime(true);
			file_get_contents($url);
			$end  = microtime(true);
			$time = $end - $begin;
			$timeTotal += $time;
			$cnt++;
		}
		$timeAverage = round(($timeTotal / self::$countRetry), 3) * 1000;
		$link = "<a href='$url' target='_blank'>$url</a>";
		return Helper::panel("Среднее время ответа страницы: |$timeAverage| ms.", $link, "success", true);
	}

	public function test($urls)
	{
		if(empty($urls)){
		    return Helper::message("Не введен(ы) url для проверки", "danger", true);
		}

		$urls = explode(PHP_EOL, $urls);

		$result = "";
		foreach($urls as $url)
		{
			$url = trim($url);
			if(!empty($url))
			{
				if(!filter_var($url, FILTER_VALIDATE_URL)){
					$result .= Helper::message("|Невалидный URL:| ".$url, "danger", true);
					continue;
				}
				$result .= self::testSingleUrl($url);
			}
		}

		return $result;
	}

}