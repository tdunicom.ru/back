<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'helper.php';

define("ROOT", "testing");
define("BASIC_TEMPLATE", "/".ROOT."/basic_template/");

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new \Slim\App(['settings' => $config]);
$container = $app->getContainer();
$container['view'] = new \Slim\Views\PhpRenderer('basic_template');

$app->any('/{app}/[{action}]', function (Request $request, Response $response, $args) {

	$dataPost = $request->getParsedBody();
	if(!empty($dataPost)){
		$args = array_merge($args, $dataPost);
	}

	try{
		$app = false;
		$action = false;

		if(!empty($args["app"])){
			$app = $args["app"];
			$pathController = "apps/".$app."/controller.php";
			if(file_exists($pathController)){

				$pathModel = "apps/".$app."/model.php";
				if(file_exists($pathModel)){
					require_once $pathModel;
				}

				if(is_dir("apps/".$app."/models")){
					foreach (glob("apps/".$app."/models/*.php") as $filename){
						include $filename;
					}
				}

				$includes = [];
				if(is_dir("apps/".$app."/includes")){
					foreach (glob("apps/".$app."/includes/*.php") as $filename){
						$file = str_replace("apps/".$app."/includes/", "", $filename);
						$file = str_replace(".php", "", $file);
						$includes[$file] = include $filename;
					}
				}

				require_once $pathController;
				if(class_exists("Controller")){
					$controller = new Controller();

					$controller->args = $args;

					if(!empty($includes)){
						$controller->includes = $includes;
					}

				}
			}

			$pathFront = "apps/".$app."/front.php";
			if(file_exists($pathFront)){
				ob_start();
				include $pathFront;
				$front = ob_get_clean();
			}

			$pathStyle = "apps/".$app."/style.css";
			if(file_exists($pathStyle)){
				$style = "/".ROOT."/".$pathStyle;
			}

			$pathScript = "apps/".$app."/script.js";
			if(file_exists($pathScript)){
				$script = "/".ROOT."/".$pathScript;
			}
		}

		if(!empty($args["action"]) && $controller){
			$action = $args["action"];

			// Подключили ядро Битрикс
			ob_start();
			require(__DIR__.'/../bitrix/header.php');
			ob_end_clean();

			$action = str_replace("_", "", $action);
			$action = ucwords($action);
			$action = lcfirst($action);

			if(method_exists($controller, $action)){
				ob_start();
				$controller->$action();
				$controller->result = ob_get_clean();
			}else{
				throw new Exception("Не найден метод класса: ".$action);
			}

		}

		$data = [];

		if(!empty($app)){
			$appRoot = "/".ROOT."/".$app."/";
			$data["appRoot"] = $appRoot;
		}

		if(!empty($controller->title)){
			$data["title"] = $controller->title;
		}
		if(!empty($controller->result)){
			$data["result"] = $controller->result;
		}
		if(!empty($controller->useJquery)){
			$data["useJquery"] = true;
		}
		if(!empty($front)){
			$data["front"] = $front;
		}
		if(!empty($style)){
			$data["style"] = $style;
		}
		if(!empty($script)){
			$data["script"] = $script;
		}

	}catch (Exception $e){
		$data = [
			'title' => "Ошибка",
			'result' => $e->getMessage()
		];
	}

	$response = $this->view->render($response, 'index.php', $data);

    return $response;
});

$app->run();
