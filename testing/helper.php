<?
class Helper
{
	static function dump($items, $isReturn = false, $isPrintr = false)
	{
		if($isReturn){ob_start();}

		echo '<pre>';

		if($isPrintr){
			print_r($items);
		}else{
			var_dump($items);
		}

		echo '</pre>';

		if($isReturn){
			$result = ob_get_clean();
			return $result;
		}
	}

	static function dieNow()
	{
		die('<h2>Exit!</h2><b>File: </b>'.__FILE__.'<br><b> Line: </b>'.__LINE__);
	}

	/**
	* Generate message varius of type
	* @param string $text Text to display
	* @param string $type Type of message, can be 'success', 'info', 'warning', 'danger'
	* @return string Returns text of message.
	*/
	static function message($text, $type = 'success', $return = false){
		$pattern    = '<div class="alert alert-#TYPE#">#TEXT#</div>';
		$arrSearch  = array('#TYPE#', '#TEXT#');
		$arrReplace = array($type, $text);
		$result  = str_replace($arrSearch, $arrReplace, $pattern);
		$result = self::beautifyMessage($result);

		if($return){
			return $result;
		}

		echo $result;
	}

	static function msgInfo($text)   {self::message($text, "info");}
	static function msgSucces($text) {self::message($text, "success");}
	static function msgWarning($text){self::message($text, "warning");}
	static function msgDanger($text) {self::message($text, "danger");}

	static function panel($text, $title, $type = 'info', $isReturn = false){
		$pattern    = '<div class="panel panel-#TYPE#"><div class="panel-heading"><h3 class="panel-title">#TITLE#</h3></div><div class="panel-body">#TEXT#</div></div>';
		$arrSearch  = array('#TYPE#', '#TEXT#', '#TITLE#');

		if(is_array($text)){
			$text = self::dump($text, true);
		}

		$arrReplace = array($type, $text, $title);
		$arrSearch2  = array('success!', 'info!', 'primary!', 'warning!', 'danger!');
		$arrReplace2 = array('Success!', 'Information!', 'Information!', 'Warning!', 'Error!');
		$result  = str_replace($arrSearch, $arrReplace, $pattern);
		$result  = str_replace($arrSearch2, $arrReplace2, $result);

		$result = preg_replace("/<script.*?\/script>/s", '', $result);
		$result = preg_replace("/<object.*?\/object>/s", '', $result);

		$result = self::beautifyMessage($result);

		if($isReturn){
			return $result;
		}

		echo $result;
	}

	static function beautifyMessage($text)
	{
		$patternBold = '/\|(.*?)\|/';
		$replacementBold = '<b>#TEXT#</b>';
		$text	= self::beautifyMessageByType($patternBold, $replacementBold, $text);

		$patternBadge = '/\%(.*)\%/';
		$replacementBadge = '<span class="badge">#TEXT#</span>';
		$text	= self::beautifyMessageByType($patternBadge, $replacementBadge, $text);

		return $text;
	}
	static function beautifyMessageByType($pattern, $replacement, $text)
	{
		preg_match_all($pattern, $text, $matches);
		if($matches[0]){
			foreach($matches[0] as $key => $match){
				$text = str_replace($match, $replacement, $text);
				$text = str_replace('#TEXT#', $matches[1][$key], $text);
			}
		}

		return $text;
	}

	function dbConnect(){
		@require BASE_PATH."/_conf/main.cfg";

		$link = @mysql_connect($GLOBALS['server'], $GLOBALS['DBUSER'], $GLOBALS['DBPASS']);
		if (!$link) {
			return array('result' => false, 'message' => '<b>Not connected</b> : '.mysql_error());
		}

		mysql_query('SET NAMES utf8');
		$db_selected = mysql_select_db($GLOBALS['DBNAME'], $link);

		if (!$db_selected) {
			return array('result' => false, 'message' => "<b>Can't use foo</b> : ".mysql_error());
		}else{
			return array('result' => true, 'message' => '<b>Success!</b> Connected to DB');
		}

	}

	static function build_table($array, $escape = false, $return = false, $tableId = ""){

		// start table
		if($tableId){
			$html = '<table id="'.$tableId.'">';
		}else{
			$html = '<table>';
		}

		// header row
		$html .= '<tr>';
		$firstRow = reset($array);
		foreach($firstRow as $key=>$value){
			if($escape){
				$html .= '<th>' . htmlspecialchars($key) . '</th>';
			}else{
				$html .= '<th>' . $key . '</th>';
			}

		}
		$html .= '</tr>';

		// data rows
		foreach( $array as $key=>$value){
			$html .= '<tr>';
			foreach($value as $key2=>$value2){
				if($escape){
					$html .= '<td>' . htmlspecialchars($value2) . '</td>';
				}else{
					$html .= '<td>' . $value2 . '</td>';
				}
			}
			$html .= '</tr>';
		}

		// finish table and return it

		$html .= '</table>';

		if($return){
			return $html;
		}else{
			echo $html;
		}

	}

	static function renderControllers($controllers, $title = ""){
		$lis = "";
		foreach($controllers as $controller){
			$cLink = $controller[0];

			if(!empty($controller[1])){
			    $cTitle = $controller[1];
			}

			$cType = "info";
			if(!empty($controller[2])){
			   $cType = $controller[2];
			}
			if($cLink == "divider"){
				$lis .= "<br><br>\n";
			}else{
				$lis .= '<li><a href="'.$cLink.'" class="btn btn-'.$cType.'">'.$cTitle.'</a></li>'."\n";
			}
		}
		$result = '<ul class="list-inline">'.$lis.'</ul>';
		if(!empty($title)){
			$result = "<h4>$title</h4>\n".$result."\n\n";
		}
		echo $result;
	}

}

class Result{
	public $status;
	public $message;
	public $type;
	public $return;

	function __construct($status, $message = false, $type = "info"){
		$this->status = $status;
		$this->message = $message;
		$this->type = $type;
	}

	function setReturn($return){
		$this->return = $return;
		return $this;
	}

	static function show($resultObj){
		if($resultObj->status){
			if(!$resultObj->message){
				$resultObj->message = "Success!";
			}
		}else{
			if(!$resultObj->message){
				$resultObj->message = "Error!";
			}
		}
		Helper::panel($resultObj->return, $resultObj->message, $resultObj->type);
	}

}