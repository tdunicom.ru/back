
<div class="container home-section home-section_home-info">
	<div class="content">
		<div class="block-wrap  block-wrap_wrap ">
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Как выбрать кварцевый песок?</h2>
				<p>Различные виды кварцевого песка обладают своими особенностями. Для того чтобы его использование было максимально эффективны, при выборе необходимо учитывать следующие факторы.</p>
				<h3>Сфера применения</h3>
				<p>Кварцевый песок для каждого вида работ отличается своими свойствами. Например, содержание кварца. В строительном песке его около 60-70%, в формовочном и стекольном до 99%.</p>
				<h3>Фракция</h3>
				<p>Кварцевый песок при помощью виброустановок в зависимости от размеров частичек разделяют на фракции. Каждая фракция имеет свою сферу применения.</p>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Как рассчитать нужный объем?</h2>
				<h3>Для пескоструя</h3>
				<p>Средний расход для очистки 1 метра² – 60-110 кг.</p>
				<h3>Для фильтров бассейнов</h3>
				<p>для бассейна средних размеров (вместимость чаши около 50 кубических метров) требуется 50-100 кг песка</p>
				<h3>Для спортивных покрытий</h3>
				<p>Для поля с размером в 800 кв.м потребуется 12 тонн песка фракции 0,1 - 0,63 мм</p>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "callback",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>

<div class="container home-section home-section_home-how-to-buy">
	<div class="content">
		<h2>Как купить кварцевый песок?</h2>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_RECURSIVE" => "Y",
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
</div>
<div class="container home-section home-section_home-info">
	<div class="content">
		<h2>Свойства кварцевого песка</h2>
		<p>Кварцевый песок — это однородный материал природного происхождения, полученный из кварца. Основным компонентом химического состава является кремнезем, или диоксид кремния (SiO<sub>2</sub>).</p>
		<p>Среди преимуществ, которые имеет кварцевый песок — цена.</p>
		<ul class="list-marked list-marked_full">
			<li><strong>Мономинеральность</strong> – однородный и беспримесный химический состав</li>
			<li><strong>Высокая пористость</strong> – может впитывать большую часть примесей из жидкости</li>
			<li><strong>Хорошие сорбционные свойства</strong> – способность впитывать большинство примесей, содержащихся в жидкости;</li>
			<li><strong>Химическая пассивность</strong> — не взаимодействует с другими химическими материалами</li>
			<li><strong>Хорошая сыпучесть и высокая насыпная плотность</strong> – удобно использовать и перевозить такой материал</li>
			<li><strong>Высокая твердость и долговечность</strong> — частицы почти не истираются</li>
			<li><strong>Радиационная безопасность</strong> – кварцевый песок не абсорбирует излучение</li>
			<li><strong>Возможность окраски</strong> – можно легко окрасить в любой цвет</li>
		</ul>
	</div>
</div>
