<?
class CamelizeTest extends \Codeception\Test\Unit
{
	/** @var UnitTester */
	protected $tester;

	protected function _before()
	{
		require_once __DIR__."/../../bitrix/php_interface/include/functions.php";
	}

	/**
	 * @dataProvider pageProvider
	 */
	public function testFunction($raw, $expected)
	{
		$this->tester->assertEquals($expected, camelize($raw));
	}

	public function pageProvider()
	{
		return [
			["some_method", "SomeMethod"],
		];
	}

}