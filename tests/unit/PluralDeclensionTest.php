<?
class PluralDeclensionTest extends \Codeception\Test\Unit
{
	/** @var UnitTester */
	protected $tester;

	protected function _before()
	{
		require_once __DIR__."/../../bitrix/php_interface/include/functions.php";
	}

	/**
	 * @dataProvider pageProvider1
	 */
	public function testFunction1($raw, $expected)
	{
		$this->tester->assertEquals($expected, pluralDeclension("товар", $raw), "Ошибка при количестве: ".$raw);
	}

	/**
	 * @dataProvider pageProvider2
	 */
	public function testFunction2($raw, $expected)
	{
		$this->tester->assertEquals($expected, pluralDeclension("мешок", $raw), "Ошибка при количестве: ".$raw);
	}

	public function pageProvider1()
	{
		return [
			[1, "товар"],
			[2, "товара"],
			[3, "товара"],
			[4, "товара"],
			[5, "товаров"],
			[6, "товаров"],
			[7, "товаров"],
			[8, "товаров"],
			[9, "товаров"],
			[10, "товаров"],
			[11, "товаров"],
			[12, "товаров"],
			[13, "товаров"],
			[14, "товаров"],
			[15, "товаров"],
			[16, "товаров"],
			[17, "товаров"],
			[18, "товаров"],
			[19, "товаров"],
			[20, "товаров"],
			[21, "товар"],
			[22, "товара"],
			[23, "товара"],
			[24, "товара"],
			[25, "товаров"],
		];
	}

	public function pageProvider2()
	{
		return [
			[1, "мешок"],
			[2, "мешка"],
			[3, "мешка"],
			[4, "мешка"],
			[5, "мешков"],
			[6, "мешков"],
			[7, "мешков"],
			[8, "мешков"],
			[9, "мешков"],
			[10, "мешков"],
			[11, "мешков"],
			[12, "мешков"],
			[13, "мешков"],
			[14, "мешков"],
			[15, "мешков"],
			[16, "мешков"],
			[17, "мешков"],
			[18, "мешков"],
			[19, "мешков"],
			[20, "мешков"],
			[21, "мешок"],
			[22, "мешка"],
			[23, "мешка"],
			[24, "мешка"],
			[25, "мешков"],
		];
	}


}