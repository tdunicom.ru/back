<?
class PictureTagTest extends \Codeception\Test\Unit
{
    protected \UnitTester $tester;
    protected $src;
    
    protected function _before()
    {
    	require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/ImageResize/PictureTag.php";
    	$this->src = "/tests/_data/photo/photo.png";
    }

    public function testTwoSizes()
    {
    	$exceptedTag = <<<END
<picture>
	<source srcset="/tests/_data/photo/photo_xs.webp, /tests/_data/photo/photo_xs@2x.webp 2x, /tests/_data/photo/photo_xs@3x.webp 3x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_l.webp, /tests/_data/photo/photo_l@2x.webp 2x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_xs.png, /tests/_data/photo/photo_xs@2x.png 2x, /tests/_data/photo/photo_xs@3x.png 3x" />
	<source srcset="/tests/_data/photo/photo_l.png, /tests/_data/photo/photo_l@2x.png 2x" />
	<img src="/tests/_data/photo/photo.png" width="268" height="178" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::render($this->src));
    }

    public function testTwoSizesWidthPictureAttributes()
    {
    	$exceptedTag = <<<END
<picture id="xxx" class="phone">
	<source srcset="/tests/_data/photo/photo_xs.webp, /tests/_data/photo/photo_xs@2x.webp 2x, /tests/_data/photo/photo_xs@3x.webp 3x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_l.webp, /tests/_data/photo/photo_l@2x.webp 2x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_xs.png, /tests/_data/photo/photo_xs@2x.png 2x, /tests/_data/photo/photo_xs@3x.png 3x" />
	<source srcset="/tests/_data/photo/photo_l.png, /tests/_data/photo/photo_l@2x.png 2x" />
	<img src="/tests/_data/photo/photo.png" width="268" height="178" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::render($this->src, 'id="xxx" class="phone"'));
    }

    public function testTwoSizesWidthImageAttributes()
    {
    	$exceptedTag = <<<END
<picture>
	<source srcset="/tests/_data/photo/photo_xs.webp, /tests/_data/photo/photo_xs@2x.webp 2x, /tests/_data/photo/photo_xs@3x.webp 3x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_l.webp, /tests/_data/photo/photo_l@2x.webp 2x" type="image/webp" />
	<source srcset="/tests/_data/photo/photo_xs.png, /tests/_data/photo/photo_xs@2x.png 2x, /tests/_data/photo/photo_xs@3x.png 3x" />
	<source srcset="/tests/_data/photo/photo_l.png, /tests/_data/photo/photo_l@2x.png 2x" />
	<img src="/tests/_data/photo/photo.png" width="268" height="178" alt="img-alt" title="img-title" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::render($this->src, "", 'alt="img-alt" title="img-title"'));
    }

	public function testTwoSizesWithMQWidthPictureAttributesWidthImageAttributes()
	{
		$exceptedTag = <<<END
<picture id="xxx" class="phone">
	<source srcset="/tests/_data/photo/photo_xs.webp, /tests/_data/photo/photo_xs@2x.webp 2x, /tests/_data/photo/photo_xs@3x.webp 3x" type="image/webp" media="(max-width: 479px)" />
	<source srcset="/tests/_data/photo/photo_l.webp, /tests/_data/photo/photo_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)" />
	<source srcset="/tests/_data/photo/photo_xs.png, /tests/_data/photo/photo_xs@2x.png 2x, /tests/_data/photo/photo_xs@3x.png 3x" media="(max-width: 479px)" />
	<source srcset="/tests/_data/photo/photo_l.png, /tests/_data/photo/photo_l@2x.png 2x" media="(min-width: 480px)" />
	<img src="/tests/_data/photo/photo.png" width="268" height="178" alt="img-alt" title="img-title" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals(
			$exceptedTag,
			PictureTag::renderWithMQ($this->src, 'id="xxx" class="phone"', 'alt="img-alt" title="img-title"')
		);
	}

    public function testTwoSizesWithMQ()
    {
    	$exceptedTag = <<<END
<picture>
	<source srcset="/tests/_data/photo/photo_xs.webp, /tests/_data/photo/photo_xs@2x.webp 2x, /tests/_data/photo/photo_xs@3x.webp 3x" type="image/webp" media="(max-width: 479px)" />
	<source srcset="/tests/_data/photo/photo_l.webp, /tests/_data/photo/photo_l@2x.webp 2x" type="image/webp" media="(min-width: 480px)" />
	<source srcset="/tests/_data/photo/photo_xs.png, /tests/_data/photo/photo_xs@2x.png 2x, /tests/_data/photo/photo_xs@3x.png 3x" media="(max-width: 479px)" />
	<source srcset="/tests/_data/photo/photo_l.png, /tests/_data/photo/photo_l@2x.png 2x" media="(min-width: 480px)" />
	<img src="/tests/_data/photo/photo.png" width="268" height="178" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::renderWithMQ($this->src));
    }

	public function testNoSizes()
	{
		$exceptedTag = <<<END
<picture>
	<img src="/tests/_data/photo-no-sizes/photo.png" width="600" height="400" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::renderWithMQ("/tests/_data/photo-no-sizes/photo.png"));
	}

}