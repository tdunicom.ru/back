<?
class PictureTagWidthTest extends \Codeception\Test\Unit
{
    protected \UnitTester $tester;
    protected $src;
    
    protected function _before()
    {
    	require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/ImageResize/PictureTag.php";
    }

	public function test_XS_S_M_L()
	{
		$exceptedTag = <<<END
<picture>
	<source srcset="/tests/_data/photo_XS_S_M_L/photo_xs.png" media="(max-width: 479px)" />
	<source srcset="/tests/_data/photo_XS_S_M_L/photo_s.png" media="(min-width: 480px) or (max-width: 799px)" />
	<source srcset="/tests/_data/photo_XS_S_M_L/photo_m.png" media="(min-width: 800px) or (max-width: 999px)" />
	<source srcset="/tests/_data/photo_XS_S_M_L/photo_l.png" media="(min-width: 1000px)" />
	<img src="/tests/_data/photo_XS_S_M_L/photo.png" width="1000" height="666" loading="lazy" decoding="async" lazyload="true" />
</picture>
END;
		$this->tester->assertEquals($exceptedTag, PictureTag::renderWithMQ("/tests/_data/photo_XS_S_M_L/photo.png"));
	}

}