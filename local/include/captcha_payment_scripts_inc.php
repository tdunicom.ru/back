<script>
    const form = document.getElementById('custom-callback');
    let widgetId;
    const formPayment = document.getElementById('custom_form');
    let widgetIdPayment
    const containerCallback = document.getElementById('captcha-container-callback');
    const containerPayment = document.getElementById('captcha-container-custom')
    let paymentField = document.querySelector('.payment-form-opt .form-payment_field')
    let callbackField = document.querySelector('.home-call-back-form .form-callback_field')


    function callbackPayment(token) {
        if (typeof token === "string" && token.length > 0) {
            // Отправить форму на бекенд
            console.log(token);
            document.querySelector('.form-button_payment').removeAttribute('disabled');

        }
    }
    function callback(token) {
        if (typeof token === "string" && token.length > 0) {
            // Отправить форму на бекенд
            console.log(token);
            document.querySelector('.home-call-back-form .form-button').removeAttribute('disabled');

        }
    }

    function handleSubmitPayment(event) {
        if (!window.smartCaptcha) {
            return;
        }
        window.smartCaptcha.execute(widgetIdPayment);
    }
    function handleSubmit(event) {
        if (!window.smartCaptcha) {
            return;
        }
        window.smartCaptcha.execute(widgetId);
    }
    try {
        if (window.location.href == 'https://tdunicom.ru/kak-kupit/' && paymentField) {

            paymentField.addEventListener('click', function () {



                handleSubmitPayment()
            })
        }
    }catch (e) {
        console.log(`Отсутсвует класс form-payment_field ошибка ${e}`)
    }

    try {
        if (callbackField){
            callbackField.addEventListener('click',function (){

                handleSubmit()
            })
        }
    }catch (e){
        console.log(`Отсутсвует класс form-callback_field ошибка ${e}`)


    }



    function onloadFunctionCaptcha() {
        if (containerPayment){
            widgetIdPayment = window.smartCaptcha.render(containerPayment, {
                sitekey: 'ysc1_XLwZmwVhBdlQMMXtNojgD73OTEUhixOGRDbDkjDe630f5f78',
                invisible: true, // Сделать капчу невидимой
                callback: callbackPayment,
                hideShield: true


            });
        }
        if (containerCallback){
            widgetId = window.smartCaptcha.render(containerCallback, {
                sitekey: 'ysc1_ZVROda5A5xmaTvgRpl4pLlrGVyFi0kV7Cf2Dzw3Fe780524d',
                invisible: true, // Сделать капчу невидимой
                callback: callback,
                hideShield: true

            });
        }

    }

</script>
