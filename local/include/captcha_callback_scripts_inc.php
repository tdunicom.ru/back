<script>
    const form = document.getElementById('custom-callback');
    let widgetId;

    function onloadFunction() {
        if (!window.smartCaptcha) {
            return;
        }
        const container = document.getElementById('captcha-container-callback');

        widgetId = window.smartCaptcha.render(container, {
            sitekey: 'ysc1_ZVROda5A5xmaTvgRpl4pLlrGVyFi0kV7Cf2Dzw3Fe780524d',
            invisible: true, // Сделать капчу невидимой
            callback: callback,
        });
    }

    function callback(token) {
        if (typeof token === "string" && token.length > 0) {
            // Отправить форму на бекенд
            console.log(token);
            document.querySelector('.home-call-back-form .form-button').removeAttribute('disabled');

        }
    }

    function handleSubmit(event) {
        if (!window.smartCaptcha) {
            return;
        }
        window.smartCaptcha.execute(widgetId);
    }
    document.querySelector('.home-call-back-form .form-callback_field').addEventListener('click',function (){
        handleSubmit()
    })

</script>
