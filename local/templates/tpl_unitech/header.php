<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
use Bitrix\Main\Page\Asset;

?><!DOCTYPE html>
<html lang="ru">

<head>
    <? $APPLICATION->ShowHead() ?>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/local/front/template/styles/core.css">
    <? /* <link rel="preload" href="<?= SITE_TEMPLATE_PATH ?>/fonts/OpenSans-Regular.ttf" as="font" type="font/ttf" crossorigin="anonymous"> */?>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <script src="/local/front/template/scripts/script.js"></script>
    <script src="https://smartcaptcha.yandexcloud.net/captcha.js" defer>f</script>

</head>

<body>

<div id="panel"><? $APPLICATION->ShowPanel() ?></div>

<header class="header">
    <? include_once __DIR__."/include/header/index.php" ?>
</header>

<main>

    <? if(isShowBreadcrumbsAndTitle() && $APPLICATION->GetCurPage() != '/shop/' ): ?>
        <? include __DIR__."/include/body/breadcrumbs-and-title.php" ?>
    <? endif ?>

    <? if( $APPLICATION->GetCurPage() != '/shop/' && !isCatalogPage()):?>
    <section class="page-section container">
        <div class="content">
            <? endif ?>

