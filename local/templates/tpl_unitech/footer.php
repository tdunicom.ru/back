<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();} ?>
<?php use Bitrix\Main\Page\Asset;?>
        <? if(isInnerPage()): ?>
                </div>
            </section>
        <? endif ?>

        </main>

        <footer class="footer">

            <h2 class="visually-hidden">Подвал</h2>

            <div class="container footer__top">
                <div class="grid content footer__inner">
                    <? include __DIR__."/include/footer/top/index.php" ?>
                </div>
            </div>

            <div class="container footer__bottom">
                <div class="content footer__inner">
                    <? include __DIR__."/include/footer/bottom/index.php" ?>
                </div>
            </div>

        </footer>
      <? include __DIR__."/include/footer/modals/index.php" ?>

<? include __DIR__."/include/footer/styles.php" ?>
    <link href="<?= SITE_TEMPLATE_PATH ?>/styles/add.css" rel="stylesheet">
    <script src="<?php echo SITE_TEMPLATE_PATH . '/scripts/custom.js'?>"></script>



    </body>

</html>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');