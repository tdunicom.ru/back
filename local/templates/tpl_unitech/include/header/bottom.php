<div class="container header__bottom">
	<div class="content header__inner">

		<button class="header__switcher hidden-l hidden-xl hidden-hd" type="button"></button>

		<div class="header__menu">

			<button class="header__close hidden-l hidden-xl hidden-hd" type="button"></button>

            <? include __DIR__."/menu.php" ?>
            <? include __DIR__."/search.php" ?>

            <a class="header__callback-mobile hidden-l hidden-xl hidden-hd" href="tel:+79213385002"></a>

			<? include __DIR__."/register-mobile.php" ?>

			<div class="header__contacts-mobile hidden-l hidden-xl hidden-hd">
				<? include __DIR__."/phones.php" ?>
			</div>

		</div>

        <? /* <a class="header__logo-mobile hidden-l hidden-xl hidden-hd" href="/"></a> */?> 
				<div class="phone header__phone hidden-l hidden-xl hidden-hd">
					<div class="phone__number phone__number--white">+7 (921) 338-50-02</div>
				</div>
        <? include __DIR__."/cart-mobile.php" ?>

	</div>
</div>
