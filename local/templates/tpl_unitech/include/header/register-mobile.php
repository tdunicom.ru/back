<?
global $USER;
if ($USER->IsAuthorized()):
	$name = trim($USER->GetFullName());
	if (!$name){$name = trim($USER->GetLogin());}
	if (mb_strlen($name) > 15){$name = mb_substr($name, 0, 12).'...';}
	?>
    <div class="header-user header-user--signed-in header__user-mobile hidden-l hidden-xl hidden-hd">
        <a
            class="button button--empty button--icon button--icon--user button--size--s header-user__item"
            href="/shop/personal/"
        ><?= htmlspecialcharsbx($name) ?></a>

        <? if ($USER->IsAuthorized()):?>
            <a href="/?logout=yes&<?=bitrix_sessid_get()?>" class="btn_exit_account">
                Выйти
            </a>
        <?endif;?>
    </div>
<? else: ?>
    <div class="header-user header__user-mobile hidden-l hidden-xl hidden-hd">
        <a
            class="button button--empty button--icon button--icon--enter button--size--s header-user__item"
            href="/shop/login/?login=yes&backurl=%2F"
        >Войти</a>
        <a
            class="header-user__item"
            href="/shop/login/?register=yes&backurl=%2F"
        >Регистрация</a>
    </div>
<? endif ?>
