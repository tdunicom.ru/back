<? $APPLICATION->IncludeComponent(
	"bitrix:search.form", 
	".default", 
	array(
		"USE_SUGGEST" => "N",
		"PAGE" => "/shop/search/index.php",
		"COMPONENT_TEMPLATE" => ".default",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);