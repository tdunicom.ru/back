<? $APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket.line", 
	"basket-desktop", 
	array(
		"PATH_TO_BASKET" => "/shop/personal/cart/",
		"PATH_TO_PERSONAL" => SITE_DIR."personal/",
		"SHOW_PERSONAL_LINK" => "N",
		"SHOW_NUM_PRODUCTS" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",
		"SHOW_PRODUCTS" => "N",
		"POSITION_FIXED" => "N",
		"SHOW_AUTHOR" => "Y",
		"PATH_TO_REGISTER" => "/shop/login/",
		"PATH_TO_PROFILE" => "/shop/personal/",
		"COMPONENT_TEMPLATE" => "basket-desktop",
		"PATH_TO_ORDER" => "/shop/personal/order/make/",
		"SHOW_EMPTY_VALUES" => "Y",
		"PATH_TO_AUTHORIZE" => "",
		"SHOW_REGISTRATION" => "Y",
		"HIDE_ON_BASKET_PAGES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);