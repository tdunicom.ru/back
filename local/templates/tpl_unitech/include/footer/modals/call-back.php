<div class="modal call-back" id="call-back">
	<div class="modal__inner">

		<form class="call-back__form form">

			<input type="text" class="req_name" name="req_name">

			<h2>Заказать обратный звонок</h2>
			<p>Укажите свой номер телефона, и мы перезвоним вам в ближайшее время</p>

			<label class="form-input form-input--tel par">
				<input class="form-input__field" type="tel" name="phone" required placeholder="Телефон">

            </label>
            <div class="yandex-captcha">
							<div
								style="height: 100px"
								id="captcha-container"
								class="smart-captcha"
								data-sitekey="ysc1_ZVROda5A5xmaTvgRpl4pLlrGVyFi0kV7Cf2Dzw3Fe780524d"
							></div>
            </div>

			<button
				class="button button--size--s form__submit par"
				type="submit"
			>Заказать обратный звонок</button>

			<input type="hidden" name="action" value="callback">


		</form>

		<button class="modal__close" type="button"></button>

	</div>
</div>
