<div class="modal modal-quick-buy" id="modal-quick-buy">
	<div class="modal__inner">

		<h2>Заказать в 1 клик</h2>
		<p>Время работы офиса — с 9:00 до 18:00.</p>
		<form class="modal-quick-buy__form form par">

            <input type="text" name="req_name">

			<label class="form-input form-input--text par">
                <span class="form-input__label" for="quick-buy-name">Имя</span>
				<input class="form-input__field" type="text" name="name" id="quick-buy-name" required>
			</label>

			<label class="form-input form-input--tel par">
                <span class="form-input__label" for="quick-buy-phone">Телефон</span>
				<input class="form-input__field" type="tel" name="phone" id="quick-buy-phone" required placeholder="+7 (___) ___-__-__">
			</label>

			<label class="form-input form-input--checkbox par">
                <span class="form-input__label">Нажимая на кнопку, я даю свое<a href="">согласие на обработку своих персональных данных</a></span>
				<input class="form-input__field" type="checkbox" name="agreement" required>
			</label>

			<button class="button button--size--s par" type="submit">Заказать</button>

            <input type="hidden" name="product_id" value="">
            <input type="hidden" name="product_quantity" value="">

            <input type="hidden" name="action" value="quick_buy">

        </form>

		<button class="modal__close" type="button"></button>

	</div>
</div>