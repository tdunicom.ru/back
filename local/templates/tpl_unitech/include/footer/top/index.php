<div class="footer-menu grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
	<? include __DIR__."/menu-about.php" ?>
</div>

<div class="footer-menu grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
	<? include __DIR__."/menu-catalog.php" ?>
</div>

<? /* <div class="footer-menu grid__cell grid__cell--l--2 grid__cell--m--3 grid__cell--s--6 grid__cell--xs--12">
	<? include __DIR__."/menu-buyers.php" ?>
</div> */?>

<div class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--xs--12 footer__contacts">
	<? include __DIR__."/phones.php" ?>
	<? include __DIR__."/contacts.php" ?>
</div>
