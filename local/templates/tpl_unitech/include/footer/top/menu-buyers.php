<h3 class="footer-menu__title">Покупателям</h3>

<ul class="footer-menu__list par">

	<li class="footer-menu__item">
		<a href="/pokupatelyam/oplata/">Оплата</a>
	</li>

	<li class="footer-menu__item">
		<a href="/pokupatelyam/dostavka/">Доставка</a>
	</li>

	<li class="footer-menu__item">
		<a href="/pokupatelyam/rashet-materiala/">Расчет материала</a>
	</li>

</ul>
