<h3 class="footer-menu__title">Каталог</h3>

<ul class="footer-menu__list par">

	<li class="footer-menu__item">
		<a href="/shop/catalog/shtukaturka/">Штукатурка</a>
	</li>

	<li class="footer-menu__item">
		<a href="/shop/catalog/klei/">Клей</a>
	</li>

	<li class="footer-menu__item">
		<a href="/shop/catalog/kladochnaya-smes/">Кладочная смесь</a>
	</li>

	<li class="footer-menu__item">
		<a href="/shop/catalog/cps/">ЦПС</a>
	</li>

	<? /* <li class="footer-menu__item">
		<a href="/catalog/suhoi-pesok/">Сухой песок</a>
	</li> */?>

</ul>
