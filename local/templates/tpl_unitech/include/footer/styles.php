<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$styles = [];

if(isHomePage()){
	$styles[] = "home";
}else{
	$styles[] = "glightbox";
	$styles[] = "inner";
}

$styles[] = "rest";

if(isMakeOrderPage()){
	$styles[] = "bootstrap.edit";
}

$cssVersion = 5;

foreach($styles as $style): ?>
	<link href="/local/front/template/styles/<?= $style ?>.css?v=1" rel="stylesheet">
<? endforeach ?>

<link href="<?php echo SITE_TEMPLATE_PATH . '/styles/custom.css'?>" rel="stylesheet">
