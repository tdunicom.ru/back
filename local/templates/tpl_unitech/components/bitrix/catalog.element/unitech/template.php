<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
include __DIR__."/include/init.php";
?>
<section class="page-section container" id="<?= $itemIds['ID'] ?>" itemscope itemtype="http://schema.org/Product">
    <div class="content">
        <div class="grid">

            <? include __DIR__."/include/picture.php" ?>

            <div class="grid__cell grid__cell--l--7 grid__cell--m--8 grid__cell--xs--12">
                <? include __DIR__."/include/header.php" ?>
                <? include __DIR__."/include/benefits.php" ?>
                <? include __DIR__."/include/description.php" ?>
                <? include __DIR__."/include/properties.php" ?>
                <div class="product-actions">
                    <? include __DIR__."/include/price.php" ?>
                    <? include __DIR__."/include/quantity.php" ?>
                    <? include __DIR__."/include/buttons.php" ?>
                    <? include __DIR__."/include/calculator.php" ?>
                </div>
            </div>

        </div>
    </div>
</section>

<? include __DIR__."/include/tabs/index.php" ?>

<? include __DIR__."/include/meta.php" ?>
<? include __DIR__."/include/scripts.php" ?>

