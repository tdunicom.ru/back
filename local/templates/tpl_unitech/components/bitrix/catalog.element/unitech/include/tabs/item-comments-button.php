<?
global $USER;
if($USER->IsAuthorized()):
?>
    <div class="page-subsection grid">
        <a
            class="button button--size--m grid__cell grid__cell--l--3 grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12"
            href="#modal-comment"
            data-modal=""
        >Оставить отзыв</a>
    </div>
<? else: ?>
    <div class="page-subsection">
        <p style="color: red">Только авторизованные пользователи могут оставлять комментарии.</p>
        <p><a href="/shop/login/?login=yes">Войти</a></p>
    </div>
<? endif ?>
