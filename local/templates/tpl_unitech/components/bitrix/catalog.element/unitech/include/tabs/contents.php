<div class="tabs__contents">

	<div class="tabs__item">
		<? include __DIR__."/item-features.php" ?>
	</div>

	<div class="tabs__item">
		<? include __DIR__."/item-use.php" ?>
	</div>

	<div class="tabs__item">
		<? include __DIR__."/item-how-to-buy.php" ?>
	</div>

	<div class="tabs__item">
		<? include __DIR__."/item-pay.php" ?>
	</div>

	<div class="tabs__item">
		<? include __DIR__."/item-delivery.php" ?>
	</div>

	<div class="tabs__item">
		<? include __DIR__."/item-reviews.php" ?>
	</div>

</div>
