<div class="modal modal-comment" id="modal-comment">
    <div class="modal__inner">

        <form class="modal-calculate__form form" id="add_comment">

            <h2>Оставить отзыв</h2>

            <label class="form-input form-input--text par">
                <input class="form-input__field" type="text" name="name" required placeholder="Имя">
            </label>

            <label class="form-input form-input--email par">
                <input class="form-input__field" type="email" name="email" required placeholder="E-mail">
            </label>

            <label class="form-input form-input--text par">
                <input class="form-input__field" type="text" name="title" required placeholder="Тема отзыва">
            </label>

            <label class="form-input form-input--textarea par">
                <textarea class="form-input__field" name="text" required placeholder="Текст отзыва"></textarea>
            </label>

            <button
                class="button button--size--s modal-calculate__submit form__submit par"
                type="submit"
            >Добавить</button>

            <input type="hidden" name="product_id" value="<?= $arResult["ID"] ?>">
            <input type="hidden" name="action" value="add_comment">
            <input type="text" name="req_name">

        </form>

        <button class="modal__close" type="button"></button>

    </div>
</div>
