<?
if(empty($arResult["DETAIL_PICTURE"]["SRC"])){
	$arResult["DETAIL_PICTURE"]["SRC"] = $arResult["DEFAULT_PICTURE"]["SRC"];
}
?>
<div class="grid__cell grid__cell--l--5 grid__cell--m--4 grid__cell--xs--12">
    <div class="product-images" id="<?= $itemIds['BIG_SLIDER_ID'] ?>">

        <? if($arResult["DETAIL_PICTURE"]["SRC"]): ?>
        <a
            class="product-images__main"
            href="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
            data-glightbox
            data-gallery="product"
        >
            <picture>
                <source
                    srcset="<?= WebPHelper::getOrCreateWebPUrl($arResult["DETAIL_PICTURE"]["SRC"], [450, 900]) ?>"
                    type="image/webp"
                />
                <img
                    src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                    loading="lazy"
                    decoding="async"
                    lazyload="true"
                    alt="<?= $arResult["NAME"] ?>"
                    title="<?= $arResult["NAME"] ?>"
                    width="450"
                />
            </picture>
        </a>
        <? else: ?>
            <img src="/images/no-photo-417-417.png" alt="">
        <? endif ?>

        <? if (!empty($actualItem["MORE_PHOTO"]) && count($actualItem["MORE_PHOTO"]) > 1): ?>
        <ul class="product-images__list">
            <? foreach ($actualItem["MORE_PHOTO"] as $k => $photo): ?>
                <li class="product-images__list-item">
                    <a
                        href="<?= $photo["SRC"] ?>"
                        data-glightbox
                        data-gallery="product"
                    >
                        <?= PictureTag::render($photo["SRC"]) ?>
                    </a>
                </li>
            <? endforeach ?>
        </ul>
        <? endif ?>

        <div class="product-item-detail-slider-container" id="<?= $itemIds['BIG_SLIDER_ID'] ?>" style="display: none;">
            <div class="product-item-detail-slider-block" data-entity="images-slider-block">
                <div class="product-item-detail-slider-images-container" data-entity="images-container"></div>
            </div>
        </div>

    </div>
</div>
