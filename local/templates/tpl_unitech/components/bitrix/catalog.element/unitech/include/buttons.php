<div class="product-actions__buy" id="<?= $itemIds["BASKET_ACTIONS_ID"] ?>">

    <a
        class="button button--icon button--icon--cart product-actions__buy-button <?= !$arResult['CAN_BUY'] ? "button--disabled" : "" ?>"
        href="javascript:void(0);"
        id="<?= $itemIds["ADD_BASKET_LINK"] ?>"
    >В корзину</a>

    <a
        class="product-actions__fast-buy <?= !$arResult['CAN_BUY'] ? "product-actions__fast-buy--disabled" : "" ?>"
        href="#modal-quick-buy"
        data-modal="data-modal"
        data-product-id="<?= $arResult["ID"] ?>"
        data-product-quantity="1"
    >Купить в 1 клик</a>

</div>
