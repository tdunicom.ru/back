<div class="counter product-actions__counter" data-entity="quantity-block">

	<button
        class="counter__change counter__change--decrease"
        type="button"
        id="<?= $itemIds["QUANTITY_DOWN_ID"] ?>"
    ><span class="visually-hidden">Уменьшить</span></button>

	<input
        class="counter__input"
        type="text"
        value="1"
        id="<?= $itemIds["QUANTITY_ID"] ?>"
    />

	<button
        class="counter__change counter__change--increase"
        type="button"
        id="<?= $itemIds["QUANTITY_UP_ID"] ?>"
    ><span class="visually-hidden">Увеличить</span></button>

</div>
