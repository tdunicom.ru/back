<? $comments = getCommentsForProduct($arResult["ID"]) ?>
<h2 class="visually-hidden">Отзывы</h2>

<? if($comments): ?>
	<div class="page-subsection">

		<? foreach ($comments as $comment): ?>
			<div class="review">
				<div class="review__title"><?= $comment["TITLE"] ?></div>
				<div class="review__body par"><?= $comment["TEXT"] ?></div>
				<div class="review__author"><?= $comment["NAME"] ?></div>
			</div>
		<? endforeach ?>

	</div>
<? endif ?>