<div class="grid grid--align-items--center product-properties">

    <div class="grid__cell grid__cell--l--9 grid__cell--xs--12">

		<? if(!empty($arResult["PROPERTIES"]["IMPORTANT_PROPERTIES"]["VALUE"])): ?>
        <h2 class="product-subheader">Важные характеристики:</h2>

        <ul class="list-unordered">
			<? foreach ($arResult["PROPERTIES"]["IMPORTANT_PROPERTIES"]["VALUE"] as $importantProp): ?>
                <li><?= $importantProp ?></li>
			<? endforeach ?>
        </ul>
		<? endif ?>

    </div>

	<? if(!empty($arResult["PROPERTIES"]["DECLARATION"]["VALUE"])): ?>
        <? $file = getImageSrc($arResult["PROPERTIES"]["DECLARATION"]["VALUE"]) ?>
        <a
            class="document grid__cell grid__cell--l--3 grid__cell--xs--6"
            href="<?= $file ?>"
        >Декларация</a>
    <? endif ?>

</div>
