<? if(!empty($arResult["PROPERTIES"]["PROPERTIES"]["VALUE"])): ?>
<div class="grid">
	<div class="properties grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--s--10 grid__cell--xs--12">
        <? foreach ($arResult["PROPERTIES"]["PROPERTIES"]["VALUE"] as $property): ?>
            <? list($propName, $propValue) = array_map('trim', explode('|', $property))  ?>
            <div class="properties__item">
                <div class="properties__name"><span><?= $propName ?></span></div>
                <div class="properties__value"><?= $propValue ?></div>
            </div>
        <? endforeach ?>
    </div>
</div>
<? endif ?>