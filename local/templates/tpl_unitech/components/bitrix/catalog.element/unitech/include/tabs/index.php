<section class="page-section container">
	<div class="content">
		<div class="tabs tabs--swipe">

			<div class="tabs__labels tabs__labels--swipe">
				<? include __DIR__."/headers.php" ?>
			</div>

			<div class="tabs__contents">

				<div class="tabs__item">
					<? include __DIR__."/item-features.php" ?>
				</div>

				<div class="tabs__item">
					<? include __DIR__."/item-use.php" ?>
				</div>

				<div class="tabs__item">
					<? include __DIR__."/item-how-to-buy.php" ?>
				</div>

				<div class="tabs__item">
					<? include __DIR__."/item-pay.php" ?>
				</div>

				<div class="tabs__item">
					<? include __DIR__."/item-delivery.php" ?>
				</div>

				<div class="tabs__item">
					<? include __DIR__."/item-comments.php" ?>
				</div>

			</div>

		</div>
	</div>
</section>