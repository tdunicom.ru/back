<? if(!empty($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"])): ?>
  <ul class="product-benefits">
    <? foreach ($arResult["PROPERTIES"]["ADVANTAGES"]["VALUE"] as $advantage): ?>
        <li class="product-benefits__item"><?= $advantage ?></li>
    <? endforeach ?>
  </ul>
<? endif ?>