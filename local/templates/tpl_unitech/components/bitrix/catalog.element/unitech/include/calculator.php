<?
$consumption = $arResult["PROPERTIES"]["CONSUMPTION"]["VALUE"];
if($consumption): ?>
<div class="product-actions__calculate">
    <a
        class="button button--secondary button--icon button--icon--calculator"
        href="#modal-calculate"
        data-modal=""
    >Рассчитать расход</a>
</div>

<div class="modal modal-calculate" id="modal-calculate">
    <div class="modal__inner">

        <form class="modal-calculate__form form">

            <h2>Рассчитать расход</h2>

            <label class="form-input form-input--number par">
                <span class="form-input__label">Площадь поверхности, м<sup>2</sup></span>
                <input class="form-input__field" type="number" name="area" min="1" value="1" required>
            </label>

            <label class="form-input form-input--select par">
                <span class="form-input__label">Толщина слоя, мм</span>
                <input class="form-input__field" type="number" name="thickness" min="1" value="1" required>
            </label>

            <button
                class="button button--size--s modal-calculate__submit form__submit par"
                type="submit"
            >Посчитать</button>

            <input type="hidden" name="consumption" value="<?= $consumption ?>">
            <input type="hidden" name="action" value="calculation">

        </form>

        <button class="modal__close" type="button"></button>

    </div>
</div>
<? endif ?>