<div class="counter catalog-product__counter" data-entity="quantity-block">

    <button
        class="counter__change counter__change--decrease"
        type="button"
        id="<?= $itemIds['QUANTITY_DOWN'] ?>"
    ><span class="visually-hidden">Уменьшить</span></button>

    <input
        class="counter__input"
        id="<?=$itemIds['QUANTITY']?>"
        type="text"
        name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>"
        value="<?= $measureRatio ?>"
    >

    <button
        class="counter__change counter__change--increase"
        type="button"
        id="<?= $itemIds['QUANTITY_UP'] ?>"
    ><span class="visually-hidden">Увеличить</span></button>

</div>
