<a
	class="catalog-product__image"
	href="<?= $item["DETAIL_PAGE_URL"] ?>"
	data-entity="image-wrapper"
>

    <? if($item['PREVIEW_PICTURE']['SRC']): ?>

        <picture>
            <source
                srcset="<?= WebPHelper::getOrCreateWebPUrl($item['PREVIEW_PICTURE']['SRC'], [375, 750]) ?>"
                type="image/webp"
            />
            <img
                src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>"
                loading="lazy"
                decoding="async"
                lazyload="true"
                alt="<?= $productTitle ?>"
                title="<?= $productTitle ?>"
                width="375"
            />
        </picture>

    <? else: ?>
        <img src="/images/no-photo-270-390.png" alt="">
    <? endif ?>

    <span id="<?= $itemIds['PICT_SLIDER'] ?>"></span>
    <span id="<?= $itemIds['PICT'] ?>"></span>
    <span id="<?= $itemIds['SECOND_PICT'] ?>"></span>

</a>

