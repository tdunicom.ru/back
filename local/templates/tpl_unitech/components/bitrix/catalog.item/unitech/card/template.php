<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>
<? include __DIR__."/block-picture.php" ?>
<? include __DIR__."/block-title.php" ?>
<? include __DIR__."/block-availability.php" ?>
<? include __DIR__."/block-quantity.php" ?>
<? include __DIR__."/block-price.php" ?>
<? include __DIR__."/block-buttons.php" ?>