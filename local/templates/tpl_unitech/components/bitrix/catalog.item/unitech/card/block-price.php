<div class="product-price product-price--small catalog-product__price" data-entity="price-block">
	<div class="product-price__description">Цена за шт.</div>
	<div class="product-price__value" id="<?=$itemIds['PRICE']?>">
		<? if (!empty($price)): ?>
		    <?= $price['PRINT_RATIO_PRICE']; ?>
		<? endif ?>
	</div>
</div>