<div class="product-item-button-container" id="<?= $itemIds['BASKET_ACTIONS'] ?>">

    <a
        class="button button--icon button--icon--cart button--size--m catalog-product__buy <?= !$actualItem['CAN_BUY'] ? "button--disabled" : "" ?>"
        id="<?= $itemIds['BUY_LINK'] ?>"
        href="javascript:void(0)"
    >В корзину</a>

    <a
        class="catalog-product__fast-buy <?= !$actualItem['CAN_BUY'] ? "catalog-product__fast-buy--disabled" : "" ?>"
        href="#modal-quick-buy"
        data-modal="data-modal"
        data-product-id="<?= $item["ID"] ?>"
        data-product-quantity="1"
    >Купить в 1 клик</a>

</div>
