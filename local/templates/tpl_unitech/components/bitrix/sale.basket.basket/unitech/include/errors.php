<?
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

if (!empty($arResult['ERROR_MESSAGE'])) {
	if ($arResult['EMPTY_BASKET']) {
		include(Main\Application::getDocumentRoot() . $templateFolder . '/empty.php');
	} else {
		ShowError($arResult['ERROR_MESSAGE']);
	}
}

if ($arResult['BASKET_ITEM_MAX_COUNT_EXCEEDED']): ?>
	<div id="basket-item-message">
		<?= Loc::getMessage('SBB_BASKET_ITEM_MAX_COUNT_EXCEEDED', array('#PATH#' => $arParams['PATH_TO_BASKET']))?>
	</div>
<? endif ?>
