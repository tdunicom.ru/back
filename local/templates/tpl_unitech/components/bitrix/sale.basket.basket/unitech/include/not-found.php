<? use Bitrix\Main\Localization\Loc;
?>
<div class="basket-search-not-found" id="basket-item-list-empty-result" style="display: none;">
	<div class="basket-search-not-found-icon"></div>
	<div class="basket-search-not-found-text">
		<?= Loc::getMessage('SBB_FILTER_EMPTY_RESULT') ?>
	</div>
</div>
