<div class="grid" id="basket-root">

    <div
        class="cart-list grid__cell grid__cell--l--9 grid__cell--xs--12"
        id="basket-item-table"
    ></div>

    <div
        class="cart-summary grid__cell grid__cell--l--3 grid__cell--s--6 grid__cell--xs--12 grid__cell--align-self--flex-start"
        data-entity="basket-total-block"
    ></div>

</div>
