<? use Bitrix\Main\Localization\Loc;
?>
<div class="alert alert-warning alert-dismissable" id="basket-warning" style="display: none;">
	<span class="close" data-entity="basket-items-warning-notification-close">&times;</span>
	<div data-entity="basket-general-warnings"></div>
	<div data-entity="basket-item-warnings">
		<?=Loc::getMessage('SBB_BASKET_ITEM_WARNING')?>
	</div>
</div>
