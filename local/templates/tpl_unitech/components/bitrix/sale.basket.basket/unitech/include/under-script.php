<?
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

if (!empty($arResult['CURRENCIES']) && Main\Loader::includeModule('currency'))
{
	CJSCore::Init('currency');

	?>
	<script>
      BX.Currency.setCurrencies(<?=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>);
	</script>
	<?
}

$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
$messages = Loc::loadLanguageFile(__FILE__);
?>
<script>
  BX.message(<?=CUtil::PhpToJSObject($messages)?>);
  BX.Sale.BasketComponent.init({
    result: <?=CUtil::PhpToJSObject($arResult, false, false, true)?>,
    params: <?=CUtil::PhpToJSObject($arParams)?>,
    template: '<?=CUtil::JSEscape($signedTemplate)?>',
    signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
    siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
    siteTemplateId: '<?=CUtil::JSEscape($component->getSiteTemplateId())?>',
    templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
  });
</script>
