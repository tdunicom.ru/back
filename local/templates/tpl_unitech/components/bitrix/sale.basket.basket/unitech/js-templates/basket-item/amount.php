<div class="counter cart-list__counter" data-entity="basket-item-quantity-block">

    <button
            class="counter__change counter__change--decrease"
            type="button"
            data-entity="basket-item-quantity-minus"
    ><span class="visually-hidden">Уменьшить</span></button>

    <input
        class="counter__input"
        type="text"
        value="{{QUANTITY}}"
        {{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
        data-value="{{QUANTITY}}"
        data-entity="basket-item-quantity-field"
        id="basket-item-quantity-{{ID}}"
    />

    <button
            class="counter__change counter__change--increase"
            type="button"
            data-entity="basket-item-quantity-plus"
    ><span class="visually-hidden">Увеличить</span></button>

</div>
