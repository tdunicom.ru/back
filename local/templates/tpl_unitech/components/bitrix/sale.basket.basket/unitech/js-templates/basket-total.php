<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
?>
<script id="basket-total-template" type="text/html">

    <div class="cart-summary__description">Итого:</div>

    <div
        class="cart-summary__value"
        data-entity="basket-total-price"
    >{{{PRICE_FORMATED}}}</div>

    <button
        class="button button--size--m cart-summary__button"
        type="button"
        data-entity="basket-checkout-button"
    >Оформить заказ</button>

</script>
