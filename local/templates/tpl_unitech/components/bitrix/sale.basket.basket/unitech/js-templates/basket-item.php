<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
include __DIR__."/basket-item/init.php"
?>
<script id="basket-item-template" type="text/html">
	<div
        class="cart-list__item"
		id="basket-item-{{ID}}"
        data-entity="basket-item"
        data-id="{{ID}}"
    >
		{{#SHOW_RESTORE}}
            <? include __DIR__."/basket-item/notification.php" ?>
		{{/SHOW_RESTORE}}

		{{^SHOW_RESTORE}}
            <? include __DIR__."/basket-item/image.php" ?>
            <? include __DIR__."/basket-item/name.php" ?>
            <? include __DIR__."/basket-item/price-for-one.php" ?>
            <? include __DIR__."/basket-item/amount.php" ?>
            <? include __DIR__."/basket-item/price.php" ?>
            <? include __DIR__."/basket-item/action.php" ?>
		{{/SHOW_RESTORE}}

	</div>
</script>
