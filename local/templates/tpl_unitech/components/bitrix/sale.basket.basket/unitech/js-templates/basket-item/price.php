<div class="product-price product-price--small product-price--no-description cart-list__price-total">
    <div
        class="product-price__value"
        id="basket-item-sum-price-{{ID}}"
    >{{{SUM_PRICE_FORMATED}}}</div>
</div>
