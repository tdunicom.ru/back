<div
    class="cart-list__item cart-list__item--deleted"
    id="basket-item-height-aligner-{{ID}}"
>

    <div class="cart-list__deleted-info">Товар <strong>{{NAME}}</strong> был удален из корзины.</div>

    <a
        class="cart-list__deleted-restore"
        href="javascript:void(0)"
        data-entity="basket-item-restore-button"
    >Восстановить</a>

</div>


