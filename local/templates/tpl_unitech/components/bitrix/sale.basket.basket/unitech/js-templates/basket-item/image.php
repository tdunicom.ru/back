{{#DETAIL_PAGE_URL}}<a href="/shop{{DETAIL_PAGE_URL}}" class="cart-list__image">{{/DETAIL_PAGE_URL}}
    <img
        src="{{{IMAGE_URL}}}{{^IMAGE_URL}}/images/no-photo-161-231.png{{/IMAGE_URL}}"
        loading="lazy"
        decoding="async"
        width="270"
        height="390"
    >
{{#DETAIL_PAGE_URL}}</a>{{/DETAIL_PAGE_URL}}
