<div class="product-price product-price--x-small cart-list__price-value">
    <div
        class="product-price product-price--small cart-list__price-value"
        id="basket-item-price-{{ID}}"
    >
        <div class="product-price__description">Цена за шт.</div>
        <div class="product-price__value">{{{PRICE_FORMATED}}}</div>
    </div>
</div>
