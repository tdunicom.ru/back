<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)){
	return;
}
?>
<section class="page-section container">
    <div class="content">
        <ul class="pagination">
            <? for ($numberPage = 1; $numberPage <= (int)$arResult["NavPageCount"]; $numberPage++): ?>
            <li class="pagination__item <?= $numberPage == $arResult["NavPageNomer"] ? "pagination__item--active" : "" ?>">
                <?
                $url = $arResult["sUrlPath"];
                if($numberPage > 1){
                    $url .= "?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".$numberPage;
                }
                ?>
                <? if($numberPage != $arResult["NavPageNomer"]): ?>
                    <a href="<?= $url ?>"><?= $numberPage ?></a>
                <? else: ?>
                    <span><?= $numberPage ?></span>
                <? endif ?>
            </li>
            <? endfor ?>
        </ul>
    </div>
</section>