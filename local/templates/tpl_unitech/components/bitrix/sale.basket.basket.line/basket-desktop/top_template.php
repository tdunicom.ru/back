<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<div class="header__user-cart ">

    <? if ($USER->IsAuthorized()):
        $name = trim($USER->GetFullName());
        if (!$name){$name = trim($USER->GetLogin());}
        if (mb_strlen($name) > 15){$name = mb_substr($name, 0, 12).'...';}
    ?>
        <div class="header-user header-user--signed-in header__user">
            <a
                class="button button--empty button--icon button--icon--user button--size--s header-user__item"
                href="<?= $arParams['PATH_TO_PROFILE'] ?>"
            ><?= htmlspecialcharsbx($name) ?></a>
            <? if ($USER->IsAuthorized()):?>
                <a href="/?logout=yes&<?=bitrix_sessid_get()?>" class="btn_exit_account">
                    Выйти
                </a>
            <?endif;?>
        </div>
    <? else:
        $arParamsToDelete = array("login", "login_form", "logout", "register", "forgot_password", "change_password", "confirm_registration", "confirm_code", "confirm_user_id", "logout_butt", "auth_service_id", "clear_cache", "backurl");
        $currentUrl = urlencode($APPLICATION->GetCurPageParam("", $arParamsToDelete));
        if ($arParams['AJAX'] == 'N'){
            ?><script type="text/javascript"><?=$cartId?>.currentUrl = '<?=$currentUrl?>';</script><?
        } else {
            $currentUrl = '#CURRENT_URL#';
        }
        $pathToAuthorize = $arParams['PATH_TO_AUTHORIZE'];
        $pathToAuthorize .= (mb_stripos($pathToAuthorize, '?') === false ? '?' : '&');
        $pathToAuthorize .= 'login=yes&backurl='.$currentUrl;

        $pathToRegister = $arParams['PATH_TO_REGISTER'];
        $pathToRegister .= (mb_stripos($pathToRegister, '?') === false ? '?' : '&');
        $pathToRegister .= 'register=yes&backurl='.$currentUrl;
	?>
        <div class="header-user header__user">
            <a
                class="button button--empty button--icon button--icon--enter button--size--s header-user__item"
                href="<?= $pathToAuthorize ?>"
            ><?= GetMessage('TSB1_LOGIN') ?></a>
            <a class="header-user__item" href="<?= $pathToRegister ?>"><?= GetMessage('TSB1_REGISTER') ?></a>
        </div>
    <?endif?>

    <? if($arParams['SHOW_NUM_PRODUCTS'] == 'Y'): ?>
    <a class="cart header__cart" href="<?= $arParams['PATH_TO_BASKET'] ?>">
        <? if($arResult['NUM_PRODUCTS'] > 0): ?>
        <div class="cart__count"><?= $arResult["NUM_PRODUCTS"] ?> <?= pluralDeclension("товар", $arResult["NUM_PRODUCTS"]) ?></div>
        <div class="cart__summary"><?= GetMessage('TSB1_TOTAL_PRICE') ?> <?= $arResult['TOTAL_PRICE']?></div>
        <? else: ?>
            <div class="cart__empty">В корзине пусто</div>
		<? endif ?>
    </a>
    <? endif ?>

</div>
