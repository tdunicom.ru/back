<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>


    <a
            id="<?= $cartId ?>"
            class="header__cart-mobile hidden-l hidden-xl hidden-hd"
            href="<?= $arParams['PATH_TO_BASKET'] ?>"
    >
        <? if($arResult['NUM_PRODUCTS'] > 0): ?>
            <div class="header__cart-mobile-counter"><?= $arResult["NUM_PRODUCTS"] ?></div>
        <? endif ?>
    </a>

