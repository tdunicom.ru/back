<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true) ?>
<form
    action="<?= $arResult["FORM_ACTION"] ?>"
    method="post"
    class="search header__search"
>
    <input class="search__field" type="search" name="q" placeholder="Поиск по сайту" value="" required>
    <button type="submit" class="search__submit" name="s"></button>
</form>