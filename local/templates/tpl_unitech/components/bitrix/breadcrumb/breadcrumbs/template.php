<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult)){return "";}

$lastIndex = count($arResult) - 1;

$string = '';
$string .= '<div class="breadcrumbs">';
foreach ($arResult as $index => $item){
	if($index != $lastIndex) {
$string .= '<a class="breadcrumbs__parent" href="' . $item["LINK"] . '">' . $item["TITLE"] . '</a>';
		$string .= '<div class="breadcrumbs__separator"></div>';
	}else{
		$string .= '<div class="breadcrumbs__current">'.$item["TITLE"].'</div>';
	}
}

$itemListElements = [];
foreach ($arResult as $index => $item){
	$itemListElements[] = [
		"@type" => "ListItem",
		"position" => $index + 1,
		"item" => [
			"@id" => '"'.$item["LINK"].'"',
			"name" => '"'.$item["TITLE"].'"'
		]
	];
}
$string .= '<script type="application/ld+json">';
	$string .= '{';
	$string .= '"@context": "https://schema.org",';
	$string .= '"@type": "BreadcrumbList",';
	$string .= '"itemListElement": '.json_encode($itemListElements);
$string .= '}';
$string .= '</script>';

$string .= '</div>';

return $string;