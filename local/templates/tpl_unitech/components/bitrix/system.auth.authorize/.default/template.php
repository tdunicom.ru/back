<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<? ShowMessage($arParams["~AUTH_RESULT"]) ?>
<? ShowMessage($arResult['ERROR_MESSAGE']) ?>

<p>Пожалуйста, авторизуйтесь:</p>
<div class="grid par">
    <form
        name="form_auth"
        method="post"
        target="_top"
        action="<?= $arResult["AUTH_URL"] ?>"
        class="grid__cell grid__cell--l--4 grid__cell--s--8 grid__cell--xs--12"
    >

        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="AUTH" />

		<?if ($arResult["BACKURL"] <> ''):?>
            <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
		<?endif?>

		<? foreach ($arResult["POST"] as $key => $value): ?>
            <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
		<? endforeach ?>

        <label class="form-input form-input--text par">
            <span class="form-input__label">Логин</span>
            <input class="form-input__field" type="text" name="USER_LOGIN" value="<?= $arResult["LAST_LOGIN"] ?>" maxlength="255">
        </label>

        <label class="form-input form-input--password par">
            <span class="form-input__label">Пароль</span>
            <input class="form-input__field" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off">
        </label>

        <label class="form-input form-input--checkbox par">
            <span class="form-input__label">Запомнить меня на этом компьютере</span>
            <input class="form-input__field" type="checkbox" name="USER_REMEMBER" value="Y">
        </label>

        <input class="button button--type--submit button--size--s par" type="submit" name="Login" value="Войти" />

    </form>
</div>


<p>
    <a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>">Забыли свой пароль?</a> |
    <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>">Зарегистрироваться</a>
</p>

<p>Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму</p>

<script type="text/javascript">
<?if ($arResult["LAST_LOGIN"] <> ''):?>
    try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
<?else:?>
    try{document.form_auth.USER_LOGIN.focus();}catch(e){}
<?endif?>
</script>
