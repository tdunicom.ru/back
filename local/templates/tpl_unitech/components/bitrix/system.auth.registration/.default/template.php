<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<? ShowMessage($arParams["~AUTH_RESULT"]); ?>

<?if($arResult["SHOW_EMAIL_SENT_CONFIRMATION"]):?>
    <p><?= GetMessage("AUTH_EMAIL_SENT")?></p>
<?endif;?>

<?if(!$arResult["SHOW_EMAIL_SENT_CONFIRMATION"] && $arResult["USE_EMAIL_CONFIRMATION"] === "Y"):?>
    <p><?= GetMessage("AUTH_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

<div class="grid par">
    <form
        class="grid__cell grid__cell--l--6 grid__cell--s--10 grid__cell--xs--12"
        method="post"
        action="<?= $arResult["AUTH_URL"] ?>"
        name="bform"
        enctype="multipart/form-data"
    >

        <input type="hidden" name="AUTH_FORM" value="Y" />
        <input type="hidden" name="TYPE" value="REGISTRATION" />

        <label class="form-input form-input--text par">
            <span class="form-input__label">Имя:</span>
            <input class="form-input__field" type="text" name="USER_NAME" maxlength="50" value="<?= $arResult["USER_NAME"] ?>">
        </label>

        <label class="form-input form-input--text par">
            <span class="form-input__label">Фамилия:</span>
            <input class="form-input__field" type="text" name="USER_LAST_NAME" maxlength="50" value="<?= $arResult["USER_LAST_NAME"] ?>">
        </label>

        <label class="form-input form-input--text par">
            <span class="form-input__label"><span class="required-mark">*</span> Логин (мин. 3 символа):</span>
            <input class="form-input__field" type="text" name="USER_LOGIN" maxlength="50" value="<?= $arResult["USER_LOGIN"] ?>">
        </label>

        <label class="form-input form-input--password par">
            <span class="form-input__label"><span class="required-mark">*</span> Пароль:</span>
            <input class="form-input__field" type="password" name="USER_PASSWORD" required="required" maxlength="255" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off">
        </label>

        <label class="form-input form-input--password par">
            <span class="form-input__label"><span class="required-mark">*</span> Подтверждение пароля:</span>
            <input class="form-input__field" type="password" name="USER_CONFIRM_PASSWORD" required="required" maxlength="255" value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off">
        </label>

        <label class="form-input form-input--email par">
            <span class="form-input__label"><span class="required-mark">*</span> E-Mail:</span>
            <input class="form-input__field" type="email" name="USER_EMAIL" required="required" maxlength="255" value="<?= $arResult["USER_EMAIL"] ?>">
        </label>

        <label class="form-input form-input--checkbox par">
            <span class="form-input__label">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a></span>
            <input class="form-input__field" type="checkbox" name="register_agreement" required="required">
        </label>

        <input class="button button--type--submit button--size--s par" type="submit" name="Register" value="Регистрация" />

    </form>
</div>


<p><?= $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>
<p><span class="required-mark">*</span> Обязательные поля</p>
<p><a href="<?= $arResult["AUTH_AUTH_URL"] ?>">Авторизация</a></p>

<script type="text/javascript">
  document.bform.USER_NAME.focus();
</script>
