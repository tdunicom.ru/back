<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
include __DIR__."/include/init.php";
?>
<section class="page-section container" data-entity="<?=$containerName?>">
    <div class="content">
        <div class="grid">
            <?
            $areaIds = [];

            foreach ($arResult['ITEMS'] as $item) {
                $uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
                $areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
            }

            foreach ($arResult['ITEMS'] as $item) {
                $APPLICATION->IncludeComponent(
                    'bitrix:catalog.item',
                    'unitech',
                    array(
                        'RESULT' => array(
                            'ITEM' => $item,
                            'AREA_ID' => $areaIds[$item['ID']],
                            'TYPE' => 'card',
                            'BIG_LABEL' => 'N',
                            'BIG_DISCOUNT_PERCENT' => 'N',
                            'BIG_BUTTONS' => 'N',
                            'SCALABLE' => 'N'
                        ),
                        'PARAMS' => $generalParams + array('SKU_PROPS' => $arResult['SKU_PROPS'][$item['IBLOCK_ID']])
                    ),
                    $component,
                    array('HIDE_ICONS' => 'Y')
                );
            }
            ?>
        </div>
    </div>
</section>

<?= $arResult["NAV_STRING"] ?>

<? if($arResult["UF_FULL_DESCRIPTION"]): ?>
    <section class="page-section container">
        <div class="content"><?= $arResult["~UF_FULL_DESCRIPTION"] ?></div>
    </section>
<? endif ?>

<?
include __DIR__."/include/scripts.php";