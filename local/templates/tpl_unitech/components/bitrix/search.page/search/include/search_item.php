<div class="search-result">

    <div class="search-result__name">
        <a href="<?= $arItem["URL"]?>"><?= $arItem["TITLE_FORMATED"]?></a>
    </div>

    <? if($arItem["CHAIN_PATH"]): ?>
        <div class="search-result__path">
			<?= $arItem["CHAIN_PATH"] ?>
        </div>
    <? endif ?>

    <div class="search-result__body"><?= $arItem["BODY_FORMATED"] ?></div>

</div>
