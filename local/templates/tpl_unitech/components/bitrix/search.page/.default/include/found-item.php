<div class="search-result">

	<div class="search-result__name">
		<a href="<?= $arItem["URL"] ?>"><?= $arItem["TITLE_FORMATED"] ?></a>
	</div>

	<? if($arItem["CHAIN_PATH"]): ?>
		<?= $arItem["CHAIN_PATH"] ?>
	<? endif ?>

	<div class="search-result__body"><?= $arItem["BODY_FORMATED"] ?></div>

</div>
