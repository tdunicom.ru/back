<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {die();} ?>
<? use Bitrix\Main\Localization\Loc; ?>

<? ShowError($arResult["strProfileError"]) ?>
<? $arResult['DATA_SAVED'] == "Y" ? ShowNote(Loc::getMessage('PROFILE_DATA_SAVED')) : print("") ?>

<? if($arResult["ID"] > 0): ?>
<p>
	<? if (strlen($arResult["arUser"]["TIMESTAMP_X"]) > 0): ?>
        <strong>Дата обновления: <?= $arResult["arUser"]["TIMESTAMP_X"] ?></strong><br>
	<? endif ?>

    <? if (strlen($arResult["arUser"]["LAST_LOGIN"]) > 0): ?>
        <strong>Последняя авторизация: <?= $arResult["arUser"]["LAST_LOGIN"] ?></strong>
    <? endif ?>
</p>
<? endif ?>

<div class="grid par">

    <form
        class="grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12"
        method="post"
        name="form1"
        action="<?= POST_FORM_ACTION_URI ?>"
        enctype="multipart/form-data"
        role="form"
    >

        <?= $arResult["BX_SESSION_CHECK"] ?>

        <input type="hidden" name="lang" value="<?= LANG ?>" />
        <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>" />
        <input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>" />

        <label class="form-input form-input--text par">
            <span class="form-input__label">Имя:</span>
            <input class="form-input__field" type="text" name="NAME" value="<?= $arResult["arUser"]["NAME"] ?>">
        </label>

        <label class="form-input form-input--text par">
            <span class="form-input__label">Фамилия:</span>
            <input class="form-input__field" type="text" name="LAST_NAME" value="<?= $arResult["arUser"]["LAST_NAME"] ?>">
        </label>

        <label class="form-input form-input--text par">
            <span class="form-input__label">Отчество:</span>
            <input class="form-input__field" type="text" name="SECOND_NAME" value="<?= $arResult["arUser"]["SECOND_NAME"] ?>">
        </label>

        <label class="form-input form-input--email par">
            <span class="form-input__label">E-Mail:</span>
            <input class="form-input__field" type="email" name="EMAIL" required="required" value="<?= $arResult["arUser"]["EMAIL"] ?>">
        </label>

		<? if ($arResult['CAN_EDIT_PASSWORD']): ?>
            <label class="form-input form-input--password par">
                <span class="form-input__label">Новый пароль:</span>
                <input class="form-input__field" type="password" name="NEW_PASSWORD" autocomplete="off" value="">
            </label>

            <label class="form-input form-input--password par">
                <span class="form-input__label">Подтверждение нового пароля:</span>
                <input class="form-input__field" type="password" name="NEW_PASSWORD_CONFIRM" autocomplete="off" value="">
            </label>
        <? endif ?>

        <label class="form-input form-input--checkbox par">
            <span class="form-input__label">Нажимая на кнопку, я даю свое <a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a></span>
            <input class="form-input__field" type="checkbox" name="register_agreement" required="required" checked="checked">
        </label>

        <div class="grid par">
            <button
                class="button button--size--m grid__cell grid__cell--xs--6"
                name="save"
                type="submit"
                value="<?= $arResult["ID"] > 0 ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD") ?>"
            ><?= $arResult["ID"] > 0 ? Loc::getMessage("MAIN_SAVE") : Loc::getMessage("MAIN_ADD") ?></button>
            <button
                class="button button--size--m button--secondary grid__cell grid__cell--xs--6"
                type="submit"
                name="reset"
                value="<?= GetMessage("MAIN_RESET") ?>"
            >Отмена</button>
        </div>

    </form>

</div>

<p>Пароль должен быть не менее 6 символов длиной.</p>

<p><span class="required-mark">*</span> Обязательные поля</p>
