<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? if($arResult['SECTIONS']): ?>
<section class="page-section container">
    <div class="content">

        <div class="grid page-subsection">

            <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                <? $count = $arSection["ELEMENT_CNT"] ?>
                <div class="grid__cell grid__cell--s--6 grid__cell--xs--12">
                    <a class="range-item par" href="<?= $arSection['SECTION_PAGE_URL'] ?>">

                        <picture>
                            <source
                                srcset="<?= WebPHelper::getOrCreateWebPUrl($arSection['PICTURE']['SRC'], [746, 1492]) ?>"
                                type="image/webp"
                            />
                            <img
                                src="<?= $arSection['PICTURE']['SRC'] ?>"
                                class="range-item__image"
                                loading="lazy" decoding="async" lazyload="true"
                                alt="<?= $arSection["NAME"] ?>" title="<?= $arSection["NAME"] ?>"
                                width="746" height="400"
                            />
                        </picture>

                        <div class="range-item__name">
                            <?= $arSection["NAME"] ?>
                        </div>

                    </a>

                    <?= $arSection["DESCRIPTION"] ?>

                </div>
            <? endforeach ?>

        </div>

    </div>
</section>
<? endif ?>
