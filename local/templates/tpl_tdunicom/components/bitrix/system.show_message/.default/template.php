<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true);?>

<? $color = ($arParams["STYLE"] == "notetext") ? "green" : "red" ?>

<p class="text-marked text-marked--color-<?= $color ?>"><?= $arParams["MESSAGE"] ?></p>

