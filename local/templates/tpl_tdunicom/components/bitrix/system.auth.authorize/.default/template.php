<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {die();} ?>

<? ShowMessage($arParams["~AUTH_RESULT"]) ?>
<? ShowMessage($arResult['ERROR_MESSAGE']) ?>

<p><?= GetMessage("AUTH_PLEASE_AUTH") ?></p>

<div class="grid grid--cols-12">
    <div class="grid__cell grid__cell--m-4 grid__cell--s-8 grid__cell--xs-12">

        <form
            name="form_auth"
            method="post"
            target="_top"
            action="<?= $arResult["AUTH_URL"] ?>"
            class="form-register"
        >

            <input type="hidden" name="AUTH_FORM" value="Y" />
            <input type="hidden" name="TYPE" value="AUTH" />

			<? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
			<? endif?>

			<? foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" />
			<? endforeach ?>

            <div class="form-input form-input--text form-register__field">
                <label class="form-input__label">Логин</label>
                <input class="form-input__field" type="text" name="USER_LOGIN" value="<?= $arResult["LAST_LOGIN"] ?>">
            </div>

            <div class="form-input form-input--password form-register__field">
                <label class="form-input__label">Пароль</label>
                <input class="form-input__field" type="password" name="USER_PASSWORD">
            </div>

			<? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
            <div class="form-input form-input--checkbox form-register__field">
                <label class="form-input__label" for="USER_REMEMBER">Запомнить меня на этом компьютере</label>
                <input class="form-input__field" type="checkbox" name="USER_REMEMBER" id="USER_REMEMBER" value="Y">
            </div>
			<? endif ?>

            <button class="btn form-register__submit" type="submit">Войти</button>

        </form>

    </div>
</div>

<p>
    <a href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>">Забыли свой пароль?</a> |
    <a href="<?= $arResult["AUTH_REGISTER_URL"] ?>">Зарегистрироваться</a>
</p>

<p>Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму</p>

<script type="text/javascript">
    <? if (strlen($arResult["LAST_LOGIN"]) > 0): ?>
        try{document.form_auth.USER_PASSWORD.focus();}catch(e){}
    <? else: ?>
        try{document.form_auth.USER_LOGIN.focus();}catch(e){}
    <? endif ?>
</script>
