<div class="grid grid--padding-y grid--par-like">
    <?
	$areaIds = [];

	foreach ($arResult['ITEMS'] as $item)
	{
		$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
		$areaIds[$item['ID']] = $this->GetEditAreaId($uniqueId);
	}
	?>

    <? foreach ($arResult['ITEMS'] as $item): ?>
        <? $params = $generalParams;
		$params["USE_PRODUCT_QUANTITY"] = false;

		$APPLICATION->IncludeComponent(
			"bitrix:catalog.item",
			"",
			array(
				'RESULT' => array(
					'ITEM' => $item,
					'AREA_ID' => $areaIds[$item['ID']],
					'TYPE' => "CARD",
					'BIG_LABEL' => 'N',
					'BIG_DISCOUNT_PERCENT' => 'N',
					'BIG_BUTTONS' => 'N',
					'SCALABLE' => 'N',
				),
				'PARAMS' => $params
			),
			$component,
			array('HIDE_ICONS' => 'Y')
		); ?>
    <? endforeach ?>

</div>