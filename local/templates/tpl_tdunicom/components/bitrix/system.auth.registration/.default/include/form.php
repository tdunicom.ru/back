<form
    method="post"
    action="<?= $arResult["AUTH_URL"] ?>"
    name="bform"
    class="form-register"
>
	<? if (strlen($arResult["BACKURL"]) > 0): ?>
        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>" />
    <? endif ?>

    <input type="hidden" name="AUTH_FORM" value="Y" />
    <input type="hidden" name="TYPE" value="REGISTRATION" />

	<div class="form-input form-input--text form-register__field">
		<label class="form-input__label">Имя:</label>
		<input
            class="form-input__field"
            type="text"
            name="USER_NAME"
            onkeyup="if (/[^А-Яа-я\sЁё]/g.test(this.value)) this.value = this.value.replace(/[^А-Яа-я\sЁё]/g,'')"
            value="<?= $arResult["USER_NAME"] ?>"
        >
	</div>

    <div class="form-input form-input--text form-register__field">
		<label class="form-input__label">Фамилия:</label>
		<input
            class="form-input__field"
            type="text"
            name="USER_LAST_NAME"
            onkeyup="if (/[^А-Яа-я\sЁё]/g.test(this.value)) this.value = this.value.replace(/[^А-Яа-я\sЁё]/g,'')"
            value="<?= $arResult["USER_LAST_NAME"] ?>"
        >
	</div>

	<div class="form-input form-input--text form-register__field">
		<label class="form-input__label">Логин (мин. 3 символа):<span class="form-register__required-mark">*</span></label>
		<input
            class="form-input__field"
            type="text"
            name="USER_LOGIN"
            required="required"
            value="<?= $arResult["USER_LOGIN"] ?>"
        >
	</div>

	<div class="form-input form-input--password form-register__field">
		<label class="form-input__label">Пароль:<span class="form-register__required-mark">*</span></label>
		<input
            class="form-input__field"
            type="password"
            name="USER_PASSWORD"
            required="required"
            value="<?= $arResult["USER_PASSWORD"] ?>"
        >
	</div>

	<div class="form-input form-input--password form-register__field">
		<label class="form-input__label">Подтверждение пароля:<span class="form-register__required-mark">*</span></label>
		<input
            class="form-input__field"
            type="password"
            name="USER_CONFIRM_PASSWORD"
            required="required"
            value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>"
        >
	</div>

	<div class="form-input form-input--email form-register__field">
		<label class="form-input__label">E-Mail:<span class="form-register__required-mark">*</span></label>
		<input
            class="form-input__field"
            type="email"
            name="USER_EMAIL"
            required="required"
            value="<?= $arResult["USER_EMAIL"] ?>"
        >
	</div>

	<? /* CAPTCHA */ ?>
	<? if ($arResult["USE_CAPTCHA"] == "Y"): ?>
        <div class="form-input form-input--text form-register__field">
            <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>" />
            <label class="form-input__label">
                <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" />
                <br>
                Введите слово с картинки:<span class="form-register__required-mark">*</span>
            </label>
            <input class="form-input__field" type="text" name="captcha_word" required="required" value="">
        </div>
	<? endif ?>
	<? /* CAPTCHA */ ?>

    <div class="form-input form-input--checkbox form-register__field">
		<label class="form-input__label">Нажимая на кнопку, я даю свое
			<a href="/soglasheniya-na-obrabotku-personalnykh-dannykh/">согласие на обработку своих персональных данных</a>
		</label>
		<input class="form-input__field" type="checkbox" name="register_agreement" required="required" checked="checked">
	</div>

	<button class="btn form-register__submit" type="submit">Регистрация</button>

</form>
