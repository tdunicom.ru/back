<?
    $offerItems = [];
    foreach ($item["OFFERS"] as $offer){
        $title = "";
        foreach ($offer["PROPERTIES"] as $propCode => $prop){
            if(!in_array($propCode, ["WEIGHT_REF", "VOLIUME"])){continue;}
            if($prop["VALUE"]){$title = $prop["VALUE"];}
        }
        $offerItems[] = [
            "id" => "products-list-offer_".$offer["ID"],
            "prop_title" => $title,
            "price" => $offer["PRICES"]["BASE"]["PRINT_VALUE"],
            "raw_price" => $offer["PRICES"]["BASE"]["VALUE"],
            "url" => $offer["ADD_URL"]."&ajax_basket=Y",
            "offer_title" => $offer["NAME"],
        ];
    }

    // Сортируем в порядке возрастания цены
    usort($offerItems, function($a, $b){
        return ($a["raw_price"] - $b["raw_price"]);
    });
?>
<div id="<?= $itemIds['PRICE'] ?>"></div> <? // div нужен для работы script.js ?>

<ul class="products-list-element__sizes">
    <? foreach ($offerItems as $offer): ?>
    <li class="products-list-size products-list-element__size">
        <div class="products-list-size__name"><?= $offer["prop_title"] ?></div>
        <div class="products-list-size__price"><?= $offer["price"] ?></div>
        <div class="products-list-size__buy">
            <a
                id="<?= $offer["id"] ?>"
                class="btn products-list-size__button"
                data-url="<?= $offer["url"] ?>"
                data-offer_title="<?= $offer["offer_title"] ?>"
                href="#"
            >Купить</a>
        </div>
    </li>
    <? endforeach ?>
</ul>