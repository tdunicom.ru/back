<? use Bitrix\Main\Localization\Loc; ?>
<?
$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
$clearFromLink = array("filter_history","filter_status","show_all", "show_canceled");
?>
<p>
<?
if ($nothing || $_REQUEST["filter_history"] == 'N')
{
    ?>
    <a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>">
        <?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY")?>
    </a>
    <?
}

if ($_REQUEST["filter_history"] == 'Y')
{
    ?>
    <a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("", $clearFromLink, false)?>">
        <?echo Loc::getMessage("SPOL_TPL_CUR_ORDERS")?>
    </a>
    <?
    if ($_REQUEST["show_canceled"] == 'Y')
    {
        ?>
        <a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>">
            <?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY")?>
        </a>
        <?
    }
    else
    {
        ?>
        <a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false)?>">
            <?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED")?>
        </a>
        <?
    }
}
?>
</p>