<? use Bitrix\Main\Localization\Loc; ?>
<? $orderHeaderStatus = null; ?>

<? if ($_REQUEST["show_canceled"] === 'Y' && count($arResult['ORDERS'])): ?>
	<h2 class="sale-order-title">
		<?= Loc::getMessage('SPOL_TPL_ORDERS_CANCELED_HEADER') ?>
	</h2>
<? endif ?>

<? foreach ($arResult['ORDERS'] as $key => $order): ?>

	<? if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $_REQUEST["show_canceled"] !== 'Y')
	{
		$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
		?>
		<h2 class="sale-order-title">
			<?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?>&raquo;
		</h2>
		<?
	}
	?>

    <div class="orders-history-item">
        <div class="orders-history-item__header">

            <h3 class="orders-history-item__title">
				<?= Loc::getMessage('SPOL_TPL_ORDER') ?>
				<?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
				<?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER'])?>
				<?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
				<?= $order['ORDER']['DATE_INSERT'] ?>,
                <?= count($order['BASKET_ITEMS']); ?>
                <?
                $count = substr(count($order['BASKET_ITEMS']), -1);
                if ($count == '1')
                {
                    echo Loc::getMessage('SPOL_TPL_GOOD');
                }
                elseif ($count >= '2' || $count <= '4')
                {
                    echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
                }
                else
                {
                    echo Loc::getMessage('SPOL_TPL_GOODS');
                }
                ?>
                <?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
                <?= $order['ORDER']['FORMATED_PRICE'] ?>
            </h3>

            <div class="orders-history-item__info">

                <? if ($_REQUEST["show_canceled"] !== 'Y'): ?>
                <div class="orders-history-item__status">
                    <?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED')?>
                </div>
                <? else: ?>
                <div class="orders-history-item__status orders-history-item__status--cancelled">
                    <?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED')?>
                </div>
                <? endif ?>

                <div class="orders-history-item__date-done"><?= $order['ORDER']['DATE_STATUS_FORMATED'] ?></div>
            </div>

        </div>

        <div class="orders-history-item__footer">
            <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>">Подробнее о заказе</a>
            <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>">Повторить заказ</a>
        </div>

    </div>

<? endforeach ?>
