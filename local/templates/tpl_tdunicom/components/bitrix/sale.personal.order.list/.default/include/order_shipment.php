<? use Bitrix\Main\Localization\Loc; ?>
<div class="orders-current-item__section">
	<div class="orders-current-item__section-title">Доставка</div>

	<?
	foreach ($order['SHIPMENT'] as $shipment): ?>

		<? if (empty($shipment)) {continue;} ?>

		<div class="grid grid--padding-y grid--align-center orders-current-item__info">

			<div class="grid__cell grid__cell--xs-12">Отгрузка
				<?
				$shipmentSubTitle = Loc::getMessage('SPOL_TPL_NUMBER_SIGN').htmlspecialcharsbx($shipment['ACCOUNT_NUMBER']);

				if ($shipment['DATE_DEDUCTED'])
				{
					$shipmentSubTitle .= " ".Loc::getMessage('SPOL_TPL_FROM_DATE')." ".$shipment['DATE_DEDUCTED_FORMATED'];
				}

				if ($shipment['FORMATED_DELIVERY_PRICE'])
				{
					$shipmentSubTitle .= ", ".Loc::getMessage('SPOL_TPL_DELIVERY_COST')." ".$shipment['FORMATED_DELIVERY_PRICE'];
				}
				echo $shipmentSubTitle;
				?>
			</div>

			<div class="grid__cell grid__cell--xs-12">
				<?
				if ($shipment['DEDUCTED'] == 'Y')
				{
					?>
					<div class="orders-current-item__status orders-current-item__status--success"><?=Loc::getMessage('SPOL_TPL_LOADED');?></div>
					<?
				}
				else
				{
					?>
					<div class="orders-current-item__status orders-current-item__status--failure"><?=Loc::getMessage('SPOL_TPL_NOTLOADED');?></div>
					<?
				}
				?>
			</div>

		</div>

		<div class="orders-current-item__clarification">Статус отгрузки:
			<div class="orders-current-item__status orders-current-item__status--undefined"><?= htmlspecialcharsbx($shipment['DELIVERY_STATUS_NAME']) ?></div>
		</div>

		<? if (!empty($shipment['DELIVERY_ID'])) : ?>
			<div class="orders-current-item__clarification">
				Служба доставки: <?= $arResult['INFO']['DELIVERY'][$shipment['DELIVERY_ID']]['NAME'] ?>
			</div>
		<? endif ?>

	<? endforeach ?>

</div>
