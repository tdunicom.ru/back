<? use Bitrix\Main\Localization\Loc; ?>
<div class="orders-current-item__section">
	<div class="orders-current-item__section-title">Оплата</div>
	<?
	$showDelimeter = false;
	foreach ($order['PAYMENT'] as $payment):
		?>

		<?
		if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
		{
			$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
				"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
				"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
				"allow_inner" => $arParams['ALLOW_INNER'],
				"refresh_prices" => $arParams['REFRESH_PRICES'],
				"path_to_payment" => $arParams['PATH_TO_PAYMENT'],
				"only_inner_full" => $arParams['ONLY_INNER_FULL']
			);
		}
		?>

		<div class="grid grid--padding-y grid--align-center orders-current-item__info">

			<div class="grid__cell grid__cell--xs-12">
				<strong>Счет №<?= htmlspecialcharsbx($payment['ACCOUNT_NUMBER']) ?> от <?= $payment['DATE_BILL_FORMATED'] ?>,</strong>
				<?= $payment['PAY_SYSTEM_NAME'] ?>
			</div>

			<div class="grid__cell grid__cell--xs-12">
				<?
				if ($payment['PAID'] === 'Y')
				{
					?>
					<div class="orders-current-item__status orders-current-item__status--success"><?=Loc::getMessage('SPOL_TPL_PAID')?></div>
					<?
				}
				elseif ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
				{
					?>
					<div class="orders-current-item__status sale-order-list-status-restricted"><?=Loc::getMessage('SPOL_TPL_RESTRICTED_PAID')?></div>
					<?
				}
				else
				{
					?>
					<div class="orders-current-item__status orders-current-item__status--failure"><?=Loc::getMessage('SPOL_TPL_NOTPAID')?></div>
					<?
				}
				?>
			</div>

		</div>

		<div class="orders-current-item__clarification">
			Сумма к оплате по счету: <?= $payment['FORMATED_SUM'] ?>
		</div>
	<? endforeach ?>
</div>
