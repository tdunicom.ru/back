<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);
?>
<div class="page-section">
	<?
	if (!empty($arResult['ERRORS']['FATAL'])) {
		include __DIR__."/include/errors.php";
	} else {
		include __DIR__."/include/errors_nonfatal.php";
		include __DIR__."/include/no_orders.php";
		include __DIR__."/include/menu.php";

		if ($_REQUEST["filter_history"] !== 'Y') {
			include __DIR__."/include/main.php";
		} else {
			include __DIR__."/include/filter_history.php";
		}
		?>

		<?= $arResult["NAV_STRING"] ?>

		<? include __DIR__."/include/javascript.php" ?>
	<? } ?>
</div>

