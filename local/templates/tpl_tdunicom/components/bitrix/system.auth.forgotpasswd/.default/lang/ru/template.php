<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "<p>Если вы забыли пароль, введите логин или E-Mail.</p><p>Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.</p>";
$MESS ['AUTH_GET_CHECK_STRING'] = "Выслать контрольную строку";
$MESS ['AUTH_SEND'] = "Выслать";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS ['AUTH_LOGIN'] = "Логин:";
$MESS ['AUTH_OR'] = "или";
$MESS ['AUTH_EMAIL'] = "E-Mail:";
?>