<div class="page-subsection">
    <div class="grid grid--par-like grid--padding-y">
        <div class="grid__cell grid__cell--l-8 grid__cell--m-10 grid__cell--xs-12">

            <form action="" method="get" class="grid grid--padding-y">

                <div class="grid__cell grid__cell--m-9 grid__cell--s-7 grid__cell--xs-12">
                    <div class="form-input form-input--text">
                        <input
                            class="form-input__field"
                            type="search"
                            placeholder="Поиск по сайту"
                            name="q"
                            value="<?= $arResult["REQUEST"]["QUERY"] ?>"
                            size="40"
                        >
                    </div>
                </div>

                <div class="grid__cell grid__cell--m-3 grid__cell--s-5 grid__cell--xs-12">
                    <button class="btn btn--wide" type="submit">Найти</button>
                </div>

                <input type="hidden" name="how" value="<?= $arResult["REQUEST"]["HOW"]=="d"? "d": "r" ?>" />

            </form>

        </div>
    </div>
</div>
