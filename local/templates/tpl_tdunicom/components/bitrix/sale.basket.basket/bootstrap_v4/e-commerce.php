<script>
    document.addEventListener("DOMContentLoaded", function() {
        var deleteLink = $('a.cart-table__delete');
        if(deleteLink.length){
         deleteLink.on("click", function(e){
            e.preventDefault();

            var basketItem = $(e.target).closest(".basket-items-list-item-container");
            var basketItemId = basketItem.data("id");
            var BasketComponentItem = BX.Sale.BasketComponent.items[basketItemId];

             dataLayer.push({
                 "ecommerce": {
                     "remove": {
                         "products": [
                             {
                                 "id": BasketComponentItem.PRODUCT_ID,
                                 "name": BasketComponentItem.NAME,
                                 "price": BasketComponentItem.PRICE,
                                 "quantity": BasketComponentItem.QUANTITY
                             }
                         ]
                     }
                 }
             });

             console.log("ECOMMERCE REMOVE EVENT");

         });
        }
    });
</script>