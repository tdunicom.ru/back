<div class="cart-table__name">
    {{#DETAIL_PAGE_URL}}
    <a href="{{DETAIL_PAGE_URL}}">
        {{/DETAIL_PAGE_URL}}
        <span data-entity="basket-item-name">{{NAME}}</span>
        {{#DETAIL_PAGE_URL}}
    </a>
    {{/DETAIL_PAGE_URL}}
</div>

{{#PROPS}}
    <div class="basket-item-property">
        <span>{{{NAME}}}:&nbsp;</span>
        <span
            data-entity="basket-item-property-value"
            data-property-code="{{CODE}}"
        >{{{VALUE}}}</span>
    </div>
{{/PROPS}}