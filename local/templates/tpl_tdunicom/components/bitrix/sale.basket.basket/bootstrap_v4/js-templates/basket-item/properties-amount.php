<div class="cart-table__quantity basket-items-list-item-amount">
	<div
        class="cart-table__amount basket-item-block-amount basket-item-block-amount{{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}"
        data-entity="basket-item-quantity-block"
    >

        <a
            class="cart-table__quantity-dec basket-item-amount-btn-minus"
            data-entity="basket-item-quantity-minus"
            href="javascript:void(0);"
        >-</a>


        <div class="cart-table__quantity-input basket-item-amount-filed-block">
            <div class="form-input form-input--number">
                <input
                    type="number"
                    class="form-input__field basket-item-amount-filed"
                    value="{{QUANTITY}}"
                    {{#NOT_AVAILABLE}} disabled="disabled" {{/NOT_AVAILABLE}}
                    data-value="{{QUANTITY}}"
                    data-entity="basket-item-quantity-field"
                    id="basket-item-quantity-{{ID}}"
                >
            </div>
        </div>

        <a
            class="cart-table__quantity-inc basket-item-amount-btn-plus"
            data-entity="basket-item-quantity-plus"
            href="javascript:void(0);"
        >+</a>

		<div class="basket-item-amount-field-description">
			<? if ($arParams['PRICE_DISPLAY_MODE'] === 'Y'): ?>
				{{MEASURE_TEXT}}
            <? else: ?>
				{{#SHOW_PRICE_FOR}}
				{{MEASURE_RATIO}} {{MEASURE_TEXT}} =
				<span id="basket-item-price-{{ID}}">{{{PRICE_FORMATED}}}</span>
				{{/SHOW_PRICE_FOR}}
				{{^SHOW_PRICE_FOR}}
				{{MEASURE_TEXT}}
				{{/SHOW_PRICE_FOR}}
            <? endif ?>
		</div>

		{{#SHOW_LOADING}}
		<div class="basket-items-list-item-overlay"></div>
		{{/SHOW_LOADING}}

	</div>
</div>