<a
    class="cart-table__delete link-secondary"
    data-entity="basket-item-delete"
    href="javascript:void(0);"
><span class="hidden-hd hidden-xl hidden-l hidden-m">Удалить</span></a>

{{#SHOW_LOADING}}
<div class="basket-items-list-item-overlay"></div>
{{/SHOW_LOADING}}