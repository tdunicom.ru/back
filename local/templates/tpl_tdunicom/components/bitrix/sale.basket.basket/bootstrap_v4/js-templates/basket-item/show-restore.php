<? use Bitrix\Main\Localization\Loc; ?>
{{#SHOW_RESTORE}}
<div class="cart-table__notification basket-items-list-item-notification">
	<div class="basket-items-list-item-notification-inner basket-items-list-item-notification-removed" id="basket-item-height-aligner-{{ID}}">
		{{#SHOW_LOADING}}
		<div class="basket-items-list-item-overlay"></div>
		{{/SHOW_LOADING}}
		<div class="basket-items-list-item-removed-container">
			<div>
				<?=Loc::getMessage('SBB_GOOD_CAP')?> <strong>{{NAME}}</strong> <?=Loc::getMessage('SBB_BASKET_ITEM_DELETED')?>.
			</div>
			<div class="basket-items-list-item-removed-block">
				<a href="javascript:void(0)" data-entity="basket-item-restore-button">
					<?=Loc::getMessage('SBB_BASKET_ITEM_RESTORE')?>
				</a>
				<span class="basket-items-list-item-clear-btn" data-entity="basket-item-close-restore-button"></span>
			</div>
		</div>
	</div>
</div>
{{/SHOW_RESTORE}}
{{^SHOW_RESTORE}}

