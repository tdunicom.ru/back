<? use Bitrix\Main\Localization\Loc; ?>
<div class="cart-table__price basket-items-list-item-price<?=(!isset($mobileColumns['SUM']) ? ' d-none d-sm-block' : '')?>">
    <div class="basket-item-block-price">

        <div class="basket-item-price-current">
            <span class="cart-table__price-main basket-item-price-current-text">
                {{{SUM_FULL_PRICE_FORMATED}}}
            </span>
        </div>

        {{#SHOW_LOADING}}
        <div class="basket-items-list-item-overlay"></div>
        {{/SHOW_LOADING}}

    </div>
</div>