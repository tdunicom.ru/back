<? use Bitrix\Main\Localization\Loc; ?>
{{#DELAYED}}
<div class="basket-items-list-item-warning-container">
	<div class="alert alert-warning text-center">
		<?=Loc::getMessage('SBB_BASKET_ITEM_DELAYED')?>.
		<a href="javascript:void(0)" data-entity="basket-item-remove-delayed">
			<?=Loc::getMessage('SBB_BASKET_ITEM_REMOVE_DELAYED')?>
		</a>
	</div>
</div>
{{/DELAYED}}