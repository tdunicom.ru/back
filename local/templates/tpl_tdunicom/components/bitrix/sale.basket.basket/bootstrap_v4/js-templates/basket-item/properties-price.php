<? use Bitrix\Main\Localization\Loc; ?>
<div class="basket-items-list-item-price basket-items-list-item-price-for-one">
    <div class="basket-item-block-price<?=(!isset($mobileColumns['PRICE']) ? ' d-none d-sm-block' : '')?>">

        <div class="cart-table__price-per-item basket-item-price-current">
            <span class="basket-item-price-current-text">
                {{{FULL_PRICE_FORMATED}}}
            </span>
        </div>

        <div class="basket-item-price-title">
            <?=Loc::getMessage('SBB_BASKET_ITEM_PRICE_FOR')?> {{MEASURE_RATIO}} {{MEASURE_TEXT}}
        </div>

        {{#SHOW_LOADING}}
        <div class="basket-items-list-item-overlay"></div>
        {{/SHOW_LOADING}}

    </div>
</div>