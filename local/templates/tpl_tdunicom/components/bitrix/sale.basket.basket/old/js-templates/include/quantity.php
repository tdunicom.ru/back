<div class="cart-table__quantity basket-items-list-item-amount">
	<div class="basket-item-block-amount cart-table__amount" data-entity="basket-item-quantity-block">

		<a
            class="cart-table__quantity-dec basket-item-amount-btn-minus"
            data-entity="basket-item-quantity-minus"
            href="javascript:void(0);"
        >-</a>

		<div class="cart-table__quantity-input basket-item-amount-filed-block">
            <div class="form-input form-input--number">
                <input
                    type="text"
                    class="form-input__field basket-item-amount-filed"
                    value="{{QUANTITY}}"
                    data-value="{{QUANTITY}}"
                    data-entity="basket-item-quantity-field"
                    id="basket-item-quantity-{{ID}}"
                >
            </div>
		</div>

		<a
            class="cart-table__quantity-inc basket-item-amount-btn-plus"
            data-entity="basket-item-quantity-plus"
            href="javascript:void(0);"
        >+</a>

	</div>
</div>