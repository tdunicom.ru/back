<a href="{{DETAIL_PAGE_URL}}">
    <img
        src="{{{IMAGE_URL}}}{{^IMAGE_URL}}<?= $templateFolder ?>/images/no_photo.png{{/IMAGE_URL}}"
        alt="{{NAME}}"
    />
</a>