<div class="cart-table__price basket-items-list-item-price">
    <div
        class="cart-table__price-main basket-item-price-current-text"
        id="basket-item-sum-price-{{ID}}"
    >
        {{{SUM_PRICE_FORMATED}}}
    </div>
    <div class="cart-table__price-per-item">1&nbsp;шт&nbsp;= {{{PRICE_FORMATED}}}</div>
</div>