<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? use Bitrix\Main\Localization\Loc; ?>
<script id="basket-total-template" type="text/html">
	<div class="basket-checkout-container" data-entity="basket-checkout-aligner">

        <div class="basket-checkout-section">


				<div class="basket-checkout-block basket-checkout-block-total-price">
					<div class="basket-checkout-block-total-price-inner cart-table__summary">

                        <div>Итого: </div>

						<div
                            class="cart-table__sum basket-coupon-block-total-price-current"
                            data-entity="basket-total-price"
                        >
                            {{{PRICE_FORMATED}}}
						</div>

                    </div>
				</div>

				<div class="page-subsection basket-checkout-block basket-checkout-block-btn">
                    <div class="grid grid--justify-flex-end">
                        <div class="grid__cell grid__cell--m-4 grid__cell--s-6 grid__cell--xs-12">
                            <button
                                class="btn btn--wide basket-btn-checkout"
                                data-entity="basket-checkout-button"
                            >
                                <?= Loc::getMessage('SBB_ORDER') ?>
                            </button>
				        </div>
				    </div>
				</div>

		</div>

	</div>
</script>