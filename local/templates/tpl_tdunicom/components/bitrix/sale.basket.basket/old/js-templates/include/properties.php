{{#PROPS}}
<div class="basket-item-property">

    <span class="basket-item-property-name">
        {{{NAME}}}:
    </span>

    <span class="basket-item-property-value"
         data-entity="basket-item-property-value" data-property-code="{{CODE}}">
        {{{VALUE}}}
    </span>

</div>
{{/PROPS}}