<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<? $this->setFrameMode(true) ?>
<div class="header__search">
    <div class="header-search">

        <a class="header-search__open" href="#"></a>

        <form class="header-search__form" action="<?= $arResult["FORM_ACTION"] ?>" method="get">
            <a class="header-search__close" href="#"></a>
            <input class="header-search__field" type="search" name="q" placeholder="Поиск" value="" required>
            <button class="header-search__submit" type="submit"></button>
        </form>

    </div>
</div>