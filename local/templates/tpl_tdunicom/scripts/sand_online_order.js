$(function()
{
    var form = $("form[name='sand_online_order']");
    var checkOrderIsAlreadyPaid = true;
    submitOnlineOrder()
});

function submitOnlineOrder(e)
{
    e.preventDefault();

    var checkOrderIsAlreadyPaid = e.data;

    var fieldsValidated = $(".validated:visible", $(this));
    if(!formFieldsCheck(fieldsValidated)){
        return false;
    }

    var url = "/bitrix/templates/tpl_tdunicom/include_areas/code/onlineOrder/index.php";

    if(checkOrderIsAlreadyPaid){
        url += "?checkOrderIsAlreadyPaid=1"
    }

    var data = $(e.target).serialize();

    $.post(
        url,
        data,
        function(response) {
            if(response.result == "success"){
                console.log(url)
            }
            if(response.result == "confirm"){
                if(confirm(response.message)){
                    e.data = false; // checkOrderIsAlreadyPaid = false
                    submitOnlineOrder(e);
                }
            }
            if(response.result == "error"){
                alert(response.message);
            }
        },
        "json"
    );
}