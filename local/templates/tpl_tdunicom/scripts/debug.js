function ready() {
    var perfData = window.performance.timing;
    var connectTime = perfData.responseEnd - perfData.requestStart;
    var pageLoadTimeFull = perfData.loadEventEnd - perfData.navigationStart;
    var pageLoadTimePure = pageLoadTimeFull - connectTime;

    console.log("Connect Time: " + connectTime);
    console.log("Load Time: " + pageLoadTimePure);
    console.log("Load Time + Connect Time: " + pageLoadTimeFull);
}

window.onload = function(){
    setTimeout(
        function(){
            ready()
        },
        0
    );
};