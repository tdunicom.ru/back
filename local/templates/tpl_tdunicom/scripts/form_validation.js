/***************** Functions *********************/
// set class "validated-form" to form to validate this form fields
// set class "validated" to field - to start validation of this field
// set class "validated-type-or" to field - to start validation of group fields with this class
// set class "required"  to field - set validation rule: field is required

function formFieldsCheck(fields){
    var result = true;
    $.each(fields, function(index, field){
        if(!formFieldCheck($(field))){
            result = false;
        }
    });
    return result;
}
function formFieldsCheckTypeOr(fields, form){
    var result = false;
    $.each(fields, function(index, field){
        if(formFieldCheck($(field))){
            result = true;
            clearFieldsError(form);
            return false;
        }
    });
    return result;
}

function formFieldCheck(field)
{
    var result = true;

    if(field.hasClass('required')){
        result = formFieldCheckForRequired(field);
    }

    if(!result){
        setFieldError(field)
    }else{
        clearFieldError(field)
    }

    return result;
}

function formFieldCheckForRequired(field)
{
    var result = true;
    var type = field.prop("type");

    if(type == 'checkbox'){
        if(!field.prop('checked')){
            result = false;
        }
    }else{
        if(!field.val()){
            result = false;
        }
    }

    return result;
}

function setFieldError(field){
    var fieldParent = field.parent();
    fieldParent.addClass('validation-error');
}

function clearFieldError(field){
    var fieldParent = field.parent();
    fieldParent.removeClass('validation-error');
}

function clearFieldsError(form){
    $('.validation-error', form).removeClass('validation-error');
}
/***************** /Functions *********************/

jQuery(document).ready(function($)
{
    $('form.validated-form').on('submit', function(event)
    {
        var thisForm = $(this);
        var fieldsValidated = $('.validated:visible', thisForm);
        var fieldsValidatedTypeOr = $('.validated-type-or', thisForm);

        clearFieldsError(thisForm);

        if(!formFieldsCheck(fieldsValidated)){
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

        if(!formFieldsCheckTypeOr(fieldsValidatedTypeOr, thisForm)){
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
    });

    $('.validated').on('change', function(){
        formFieldCheck($(this));
    });
    $('.validated-type-or').on('change', function(){
        formFieldsCheckTypeOr($(this));
    });
});


