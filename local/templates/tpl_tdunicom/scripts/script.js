jQuery(document).ready(function($)
{
/********  Toggle function ********/
$('.toggle-me').hide();
$('.toggle-by').on('click', function(){
    $(this).next('.toggle-me').slideToggle();
});
/******** /Toggle function ********/

/********  Fancybox  ********/
$('.image-open').fancybox({
    helpers : {
        title : {
            type : 'inside'
        }
    }
});
/********  /Fancybox  ********/

/********* Запуски Ajax-сабмита для форм *********/
var curPath = window.location.pathname;

$('form.form_calculation_footer').on('submit', function(event){

    var ajaxRootFormSubmitPropsAction = '$(thisForm).parent().html("';
    ajaxRootFormSubmitPropsAction += '<h3>Заказ отправлен</h3><p>В течении рабочего дня мы вам позвоним или напишем на почту для согласования заказа</p>';
    ajaxRootFormSubmitPropsAction += '");';
    ajaxRootFormSubmitPropsAction += 'ym(36383700, "reachGoal", "p_raschet");';
    var ajaxRootFormSubmitProps = {
        event: event,
        form: this,
        handler: 'addOrderCalculationFooterHandler',
        successAction: ajaxRootFormSubmitPropsAction,
    };


    var ajaxPesokFormSubmitPropsAction = '$(thisForm).parent().html("';
    ajaxPesokFormSubmitPropsAction += '<h3>Спасибо за заявку</h3><p>Наши менеджеры отправят вам расчет в течении 15 минут</p>';
    ajaxPesokFormSubmitPropsAction += '");';
    ajaxPesokFormSubmitPropsAction += 'yaCounter36383700.reachGoal("p_raschet_main");';
    var ajaxPesokFormSubmitProps = {
        event: event,
        form: this,
        handler: 'addOrderCalculationFooterHandler',
        addData: "pages_pesok",
        successAction: ajaxPesokFormSubmitPropsAction,
    };

    var ajaxTsementnoPeschanayaSmesFormSubmitProps = {
        event:         event,
        form:          this,
        handler:       'addOrderCalculationFooterHandler',
        addData:       "pages_tsementnoPeschanayaSmes",
        popupTitle:    'Спасибо за заявку',
        popupText:     'Наши менеджеры отправят вам расчет в течении 15 минут',
        btnCloseText:  'Ok',
        successAction: 'yaCounter36383700.reachGoal("cem_raschet_podval")',
    };

    var ajaxSukhieStroitelnyeSmesiFormSubmitProps = {
        event:         event,
        form:          this,
        handler:       'addOrderCalculationFooterHandler',
        addData:       "pages_sukhie_stroitelnye_smesi",
        popupTitle:    'Спасибо за заявку',
        popupText:     'Наши менеджеры отправят вам расчет в течении 15 минут',
        btnCloseText:  'Ok',
        successAction: 'ym(36383700, "reachGoal", "suh_str_smes_raschet")',
    };

    if(curPath == "/"){
        ajaxFormSubmit(ajaxRootFormSubmitProps);
    }else if(curPath == "/tsementno-peschanaya-smes/"){
        ajaxFormSubmit(ajaxTsementnoPeschanayaSmesFormSubmitProps);
    }else if(curPath == "/sukhie-stroitelnye-smesi-tolling/"){
        ajaxFormSubmit(ajaxSukhieStroitelnyeSmesiFormSubmitProps);
    }else{
        ajaxFormSubmit(ajaxPesokFormSubmitProps);
    }

});


$('form.form_calculation_graviy').on('submit', function(event){
    ajaxFormSubmit({
        event:         event,
        form:          this,
        handler:       'addOrderCalculationFooterHandler',
        addData:       "pages_graviy",
        popupTitle:    'Спасибо за заявку',
        popupText:     'Наши менеджеры отправят вам расчет в течении 15 минут',
        btnCloseText:  'Ok',
        successAction: 'ym(36383700, "reachGoal", "g_raschet")',
    });
});

$('form.form_calculation_pesok').on('submit', function(event){
    ajaxFormSubmit({
        event:         event,
        form:          this,
        handler:       'addOrderCalculationFooterHandler',
        addData:       "pages_pesok",
        popupTitle:    'Спасибо за заявку',
        popupText:     'Наши менеджеры отправят вам расчет в течении 15 минут',
        btnCloseText:  'Ok',
        successAction: 'ym(36383700, "reachGoal", "p_raschet")',
    });
});

$('form.get-price-form').on('submit', function(event){
    var getPriceFormAction = '$(thisForm).parent().html("';
    getPriceFormAction += '<h3>Спасибо за заявку</h3><p>Наши менеджеры получили ваше сообщение</p>';
    getPriceFormAction += '");';
    getPriceFormAction += 'ym(36383700, "reachGoal", "p_opt");';
    ajaxFormSubmit({
        event: event,
        form: this,
        handler: 'addGetOptPriceHandler',
        successAction: getPriceFormAction,
    });
});

$('form.home-call-back-form').on('submit', function(event){
    ajaxFormSubmit({
        event:         event,
        form:          this,
        handler:       'addCallbackHandler',
        popupTitle:    'Спасибо за заявку',
        popupText:     'Наши менеджеры получили ваше сообщение',
        btnCloseText:  'Ok',
        successAction: 'ym(36383700, "reachGoal", "zakaz_zvonka")',
    });
});
/********* /Запуски Ajax-сабмита для форм *********/

// -> Event listener
$('a[href="/download/zakaz_peska.xlsx"]').on('click', function(e){
    ym(36383700, "reachGoal", "ssilka_obrazec");
});
$('a[href="/download/zakaz_graviya.xlsx"]').on('click', function(e){
    ym(36383700, "reachGoal", "g_ssilka_obrazec");
});
// <- Event listener

});

  /*$('.banner-home-countdown').countdown({
  until: new Date(2017, 6, 2),
  format: 'DHMS',
  padZeroes: true,
  layout:
    '<div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{d10}</span><span class="banner-home-countdown__digit">{d1}</span><span class="banner-home-countdown__desc">Дней</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{h10}</span><span class="banner-home-countdown__digit">{h1}</span><span class="banner-home-countdown__desc">Часов</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{m10}</span><span class="banner-home-countdown__digit">{m1}</span><span class="banner-home-countdown__desc">Минут</span></div><div class="banner-home-countdown__block"><span class="banner-home-countdown__digit">{s10}</span><span class="banner-home-countdown__digit">{s1}</span><span class="banner-home-countdown__desc">Секунд</span></div>'
});

  $('.ssslider').sss({
    speed : 5000, // Slideshow speed in milliseconds.
  });
$('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 260,
    itemMargin: 20,
    minItems: 1,
    maxItems: 3
  });
*/

(function() {

  // store the slider in a local variable
  var $window = $(window),
      flexslider = { vars:{} };

  // tiny helper function to add breakpoints
  function getGridSize() {
    return (window.innerWidth < 420) ? 1 :
           (window.innerWidth < 1024) ? 2 : 3;
  }


  $window.load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 210,
      itemMargin: 20,
      controlNav: false,
      prevText: "",           //String: Set the text for the "previous" directionNav item
      nextText: "",               //String: Set the text for the "next" directionNav item
      minItems: getGridSize(), // use function to pull in initial value
      maxItems: getGridSize() // use function to pull in initial value
    });
  });

  // check grid size on resize event
  $window.resize(function() {
    var gridSize = getGridSize();

    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());

var menuProductsSwitcher = document.getElementById('menu-products__switcher');
menuProductsSwitcher.addEventListener('click', function(){
  if (document.body.classList.contains('body-fixed')){
    document.body.classList.remove('body-fixed')
  } else {
    document.body.classList.add('body-fixed')
  }
})


var menuProductsItem = document.querySelectorAll ('.menu-products__item')
for ( var i = 0 ; i < menuProductsItem.length ; i++ ) {
  var currentItem = menuProductsItem[i];
  if (menuProductsItem[i].classList.contains('menu-products__item_parent')) {
    var parentSwitcher = document.createElement('div');
    parentSwitcher.classList.add('menu-products__parent-switcher')
    menuProductsItem[i].insertBefore(parentSwitcher, menuProductsItem[i].children[1])

  }
}
var switchers = document.querySelectorAll('.menu-products__parent-switcher');
for ( var i = 0 ; i < switchers.length ; i++ ) {
      switchers[i].addEventListener('click', function() {
        if ( !this.nextElementSibling.classList.contains( 'menu-products__categories_mobile-show' ) ) {
          this.nextElementSibling.classList.add( 'menu-products__categories_mobile-show' )
        } else {
          this.nextElementSibling.classList.remove( 'menu-products__categories_mobile-show' )
        }
      })
}
