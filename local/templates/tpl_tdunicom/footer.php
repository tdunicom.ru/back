<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>

            <? if(isInnerPage()): ?>
                        <? include __DIR__."/include/prefooter/how_to_buy.php" ?>
                    </div> <!-- content -->
                </div> <!-- container -->
            <? endif ?>

            <? include __DIR__."/include/prefooter/order.php" ?>
            <? include __DIR__."/include/prefooter/prefooter_contacts.php" ?>

        </main><!-- .content -->

        <footer class="footer container">
            <div class="footer-inner content">
                <div id="copyright"><? include __DIR__."/include/footer/copyright.php" ?></div>
                <div id="copyright"><? include __DIR__."/include/footer/development.php" ?></div>
            </div>
        </footer><!-- .footer -->

        <link href="<?= SITE_TEMPLATE_PATH ?>/css/jquery.fancybox.css" rel="stylesheet">

        <? include __DIR__."/include/footer/scripts.php" ?>

    </body>
</html>
