<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?><!DOCTYPE html>
<html lang="ru">

	<head>
		<? include __DIR__."/include/header/head.php"?>
    </head>

    <body>

	    <? $APPLICATION->ShowPanel() ?>

        <? include __DIR__."/include/header/top.php" ?>

		<? include __DIR__."/include/header/menu-products.php" ?>
		<? include __DIR__."/include/header/menu-top.php" ?>

        <main>

            <? if(isInnerPage()): ?>
                <div class="container">
                    <div class="content">
                        <? include __DIR__."/include/header/breadcrumbs.php" ?>
                        <h1><? $APPLICATION->ShowTitle(false) ?></h1>
            <? endif ?>
