<?
require __DIR__."/initial.php";

try
{
    $request = new ModelRequest($_REQUEST);
    $modelElements = new ModelElements();

    if($request->checkOrderIsAlreadyPaid && $modelElements->isOrderAlreadyPaid($request)){
        die(json_encode([
            "result" => "confirm",
            "message" => "Заказ с такой стоимостью сегодня вами уже оплачен. Продолжить?"
        ]));
    }

    $newElementId = $modelElements->createNewOrder($request);
	$message = "Id нового элемента инфоблока: ".$newElementId;

    $modelRobokassa = new ModelRobokassa();
	$robokassaForm = $modelRobokassa->getRobokassaForm($newElementId, $request);

	$result = "success";
}
catch(Exception $e)
{
	$message = $e->getMessage();
	$robokassaForm = false;
	$result = "error";
}
finally
{
    die(json_encode([
        "result" => $result,
        "robokassaForm" => $robokassaForm,
        "message" => $message
    ]));
}