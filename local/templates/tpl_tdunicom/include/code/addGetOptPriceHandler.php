<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$iblockId = 7; // "Заявка на оптовый прайс"
$emailEventName = "NEW_ORDER_OPT_PRICE";
$siteCode = "s1";

if(!empty($_POST))
{
	//init data
	$name = htmlspecialcharsbx($_POST['name']);
	$email = htmlspecialcharsbx($_POST['email']);
	$phone = htmlspecialcharsbx($_POST['tel']);
	$comments = htmlspecialcharsbx($_POST['message']);

    if(!$name OR !$email OR !$phone OR !$comments){
         die(
            json_encode(array(
                'result' => 'error',
                'message' => 'Name or E-mail or Phone or Message is empty')
            )
        );
    }

    CModule::IncludeModule('iblock');

	$dateForUser = date('Y/m/d H:i:s');
	$dateForCode = ConvertTimeStamp(time(), "FULL");

	$topicInit = $name;
    $topicForUser = $topicInit.' от '.$dateForUser;
    $topicForCode = $topicInit.'_'.$dateForCode;

	//code generation
	$arParamsCode = array(
	   "max_len" => 255,  
	   "change_case" => "L",  
	   "replace_space" => '-',  
	   "replace_other" => '-',   
	   "delete_repeat_replace" => true
	);
	 
	$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

	$element = new CIBlockElement;
	
	$elementProps = array();
	$elementProps['EMAIL'] = $email;
	$elementProps['PHONE'] = $phone;

	$arElementFields = Array(
		"IBLOCK_ID" => $iblockId,
		"NAME" => $topicForUser,
		"CODE" => $code,
		"PREVIEW_TEXT" => $comments,
		"ACTIVE" => "Y",
		"DATE_ACTIVE_FROM" => $dateForCode,
        "PROPERTY_VALUES" => $elementProps
	);

	if ($newIblockItemId = $element->Add($arElementFields))
	{
        $backendLink = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $backendLink .= '?IBLOCK_ID='.$iblockId.'&type=feedback&ID='.$newIblockItemId;

        $arEventFields = array(
            "NAME" => $name,
            "EMAIL" => $email,
            "PHONE" => $phone,
            "COMMENTS" => $comments,
            "DATE" => $dateForUser,
            "ID" => $newIblockItemId,
            "BE_LINK" => $backendLink
        );

        $returnMsg = "New Iblock Item successfully added";

        if($GLOBALS['prod']){
            $resSend = CEvent::Send($emailEventName, $siteCode, $arEventFields);
            $returnMsg .= "; Message send: ".$resSend;
        }

        $data = array("result" => "success", "message" => $returnMsg);
    }
	else{
		$data = array("result" => "error", "message" => $element->LAST_ERROR);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

} else {
	LocalRedirect('/');
}

?>