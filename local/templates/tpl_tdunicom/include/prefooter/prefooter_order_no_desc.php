<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>
<? CJSCore::Init(array("popup")); ?>
<div class="container prefooter-cost-calculation" id="cost-calculation">
  <div class="content">
    <div class="block-wrap cost-calculation block-wrap_wrap ">
      <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width12 block-wrap__item_s-width6 cost-calculation__form">
        <div class="cost-calculation-form">
          <div class="cost-calculation-form__top">
            <div class="cost-calculation-form__title">Рассчитать стоимость</div><a class="cost-calculation-form__example" href="/download/zakaz_peska.xlsx">Скачать образец</a>
          </div>
          <div class="cost-calculation-form__subtitle">Заполните форму и получите расчет в течение 15 минут!</div>

          <form class="form block-wrap block-wrap_wrap form_calculation_footer validated-form" enctype="multipart/form-data" method="post">

            <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 form__input form__input_nomargin">
              <input type="text" placeholder="Ваше имя" name="name" class="validated required">
				  <div class="validation-msg">Обязательное поле</div>
            </div>

            <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 form__input form__input_nomargin">
              <input type="email" placeholder="E-mail" name="email">
            </div>

            <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
              <div class="form__input">
                <input type="tel" placeholder="Телефон" name="phone">
              </div>
              <div class="form__input form__input_nomargin form__input_file">
                <label>Прикрепить файл
                  <input type="file" name="orderFile">
                </label>
              </div>
            </div>

            <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
              <div class="form__input">
                <textarea placeholder="Состав заказа или коментарий" name="comment"></textarea>
              </div>
              <div class="cost-calculation-form__clarification">Для расчета стоимости доставки - укажите адрес доставки в комментарии.</div>
            </div>

            <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
              <input class="btn" type="submit" value="Получить рассчет">
            </div>

          </form>

        </div>
      </div>

    </div>
  </div>
</div>
