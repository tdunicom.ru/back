<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$scripts = [
    "jquery-2.2.0.min",
    "jquery.fancybox.pack",
    "jquery.plugin.min",
    "jquery.countdown.min",
    "ajax_form",
    "form_validation",
    "sss.min",
    "jquery.flexslider-min",
    "script",
    "script-after"
];

$jsVersion = 2;

foreach($scripts as $script): ?>
	<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/<?= $script ?>.js?v=<?= $jsVersion ?>"></script>
<? endforeach ?>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'Hp5D9C6W3D';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);
    }
    if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}}
    )();
</script>
<!-- {/literal} END JIVOSITE CODE -->
