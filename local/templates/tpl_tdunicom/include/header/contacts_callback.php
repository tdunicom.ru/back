<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>
<a class="image-open callback-link" href="#call-back-form-popup">Заказать звонок</a>

<div class="call-back-form-popup" id="call-back-form-popup">

	<div class="call-back-form-popup__title">Заказать обратный звонок</div>

	<div class="call-back-form-popup__body">
		<div class="call-back-form-popup__info">Укажите свой номер телефона, и мы перезвоним вам в ближайшее время</div>
		<form class="form home-call-back-form validated-form form-in-popup">
			<div class="form__input">
				<input type="tel" placeholder="Телефон" name="phone" class="validated required">
				<div class="validation-msg">Обязательное поле</div>
			</div>
			<input class="btn" type="submit" value="Заказать обратный звонок">
		</form>
	</div>

</div>
