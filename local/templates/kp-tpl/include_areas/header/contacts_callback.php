<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>
<a class="image-open callback-link" href="#call-back-form-popup">Заказать звонок</a>

<div class="call-back-form-popup" id="call-back-form-popup">

    <div class="call-back-form-popup__title">Заказать обратный звонок</div>

    <div class="call-back-form-popup__body">
        <div class="call-back-form-popup__info">Укажите свой номер телефона, и мы перезвоним Вам в ближайшее время</div>
        <form class="form home-call-back-form validated-form form-in-popup" id="custom-callback">
            <div class="form__input">
                <input type="tel" placeholder="Телефон" name="phone" class="validated required form-callback_field">
                <div class="validation-msg">Обязательное поле</div>
            </div>
            <div id="captcha-container-callback" class="smart-captcha"></div>

            <input class="btn form-button" type="submit" disabled="disabled" style="margin-top: 24px;"  value="Заказать обратный звонок">
        </form>
    </div>

</div>

