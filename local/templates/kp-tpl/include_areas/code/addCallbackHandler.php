<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
define('SMARTCAPTCHA_SERVER_KEY', 'ysc2_ZVROda5A5xmaTvgRpl4popgv4tyhF0VvAVeZjYOtdbd8c03a');
$status = false;
function check_captcha($token) {
    $ch = curl_init();
    $args = http_build_query([
        "secret" => SMARTCAPTCHA_SERVER_KEY,
        "token" => $token,
        "ip" => $_SERVER['REMOTE_ADDR'], // Нужно передать IP-адрес пользователя.
        // Способ получения IP-адреса пользователя зависит от вашего прокси.
    ]);
    curl_setopt($ch, CURLOPT_URL, "https://smartcaptcha.yandexcloud.net/validate?$args");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);

    $server_output = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($httpcode !== 200) {
        echo "Allow access due to an error: code=$httpcode; message=$server_output\n";
        return true;
    }
    $resp = json_decode($server_output);
    return $resp->status === "ok";
}

$token = $_POST['smart-token'];
if (check_captcha($token)) {
   $status = true;
} else {
    echo "Robot\n";
}

$iblockId       = 8; // "Обратный звонок"
$emailEventName = "NEW_CALLBACK_ORDER";
$siteCode       = "s1";

if($status === true)
{
	//init data
	$name     = htmlspecialcharsbx($_POST['name']);
	$phone    = htmlspecialcharsbx($_POST['phone']);

    if(!$phone){
         die(
            json_encode(array(
                'result' => 'error',
                'message' => 'Phone is empty')
            )
        );
    }

    CModule::IncludeModule('iblock');

	$dateForUser  = date('Y/m/d H:i:s');
	$dateForCode  = ConvertTimeStamp(time(), "FULL");

	$topicInit    = $name;
    $topicForUser = $topicInit.' от '.$dateForUser;
    $topicForCode = $topicInit.'_'.$dateForCode;

	//code generation
	$arParamsCode = array(
	   "max_len" => 255,
	   "change_case" => "L",
	   "replace_space" => '-',
	   "replace_other" => '-',
	   "delete_repeat_replace" => true
	);

	$code = CUtil::translit($topicForCode, "ru", $arParamsCode);

	$element = new CIBlockElement;

	$elementProps = array();
	$elementProps['PHONE'] = $phone;

	$arElementFields = Array(
		"IBLOCK_ID"        => $iblockId,
		"NAME"             => $topicForUser,
		"CODE"			   => $code,
		"ACTIVE"           => "Y",
		"DATE_ACTIVE_FROM" => $dateForCode,
        "PROPERTY_VALUES"  => $elementProps
	);

	if ($newIblockItemId = $element->Add($arElementFields))
	{
        $backendLink  = 'http://'.SITE_SERVER_NAME.'/bitrix/admin/iblock_element_edit.php';
        $backendLink .= '?IBLOCK_ID='.$iblockId.'&type=feedback&ID='.$newIblockItemId;

        $arEventFields = array(
            "NAME"     => $name,
            "PHONE"    => $phone,
            "DATE"     => $dateForUser,
            "ID"       => $newIblockItemId,
            "BE_LINK"  => $backendLink
        );

        $returnMsg = "New Iblock Item successfully added";

        if($GLOBALS['prod']){
            $resSend    = CEvent::Send($emailEventName, $siteCode, $arEventFields);
            $returnMsg .= "; Message send: ".$resSend;
        }

        $data = array("result" => "success", "message" => $returnMsg);
    }
	else{
		$data = array("result" => "error", "message" => $element->LAST_ERROR);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

} else {
	LocalRedirect('/');
}

?>
