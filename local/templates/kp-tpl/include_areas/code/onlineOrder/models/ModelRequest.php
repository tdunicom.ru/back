<?
class ModelRequest
{
    public $quantity;
    public $cost;
    public $name;
    public $phone;
    public $email;
    public $checkOrderIsAlreadyPaid;

    function __construct($request)
    {
        $this->quantity = (int)$request["quantity"];
        $this->cost = (int)(str_replace(' ', '', $request["cost"]));
        $this->name = $request["name"];
        $this->phone = $request["phone"];
        $this->email = $request["email"];
        $this->checkOrderIsAlreadyPaid = (bool)$request["checkOrderIsAlreadyPaid"];
    }

}



