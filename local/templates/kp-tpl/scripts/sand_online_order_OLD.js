$(function(){
    $("input[name='delivery_type']").on("change", function () {
        var deliveryTypeVal = $(this).val();
        var deliveryCostDom = $(".payment-form__group--delivery");
        var deliveryCostDesc = $(".payment-form__group--delivery-description");
        if(deliveryTypeVal == "without"){
           deliveryCostDom.hide();
           deliveryCostDesc.hide();
        }else{
            deliveryCostDom.show();
            deliveryCostDesc.show();
        }

    });
});

$(function(){
    $("input[name='sand_type']").on("change", function () {
        var sandTypeVal = $(this).val();
        var fractionFormingDom = $("select[name='fraction_forming']").parent();
        var fractionBuildingDom = $("select[name='fraction_building']").parent();

        if(sandTypeVal == "forming"){
           fractionFormingDom.show();
           fractionBuildingDom.hide();
        }else{
           fractionFormingDom.hide();
           fractionBuildingDom.show();
        }

    });
});

$(function(){
    $("input[name='delivery_type']").on("change", ajaxCalculate);
    $("input[name='delivery_cost']").on("change", ajaxCalculate);
    $("input[name='sand_type']").on("change", ajaxCalculate);
    $("select[name='fraction_forming']").on("change", ajaxCalculate);
    $("select[name='fraction_building']").on("change", ajaxCalculate);
    $("select[name='packaging']").on("change", ajaxCalculate);
    $("input[name='quantity']").on("change", ajaxCalculate);

    function ajaxCalculate(){
        var url = "/bitrix/templates/kp-tpl/include_areas/code/onlineOrder/index.php?ajaxCalculate";
        var form = $("form[name='sand_online_order']");
        $.post(
            url,
            form.serialize(),
            function(response) {
                $("#cost_sum h3").html(response.message);
            },
            "json"
        );
    }
    
    ajaxCalculate();
});

$(function()
{
    var form = $("form[name='sand_online_order']");
    var checkOrderIsAlreadyPaid = true;
    form.on("submit", checkOrderIsAlreadyPaid, submitOnlineOrder);
});

function submitOnlineOrder(e)
{
    e.preventDefault();

    var checkOrderIsAlreadyPaid = e.data;

    var fieldsValidated = $(".validated:visible", $(this));
    if(!formFieldsCheck(fieldsValidated)){
        return false;
    }

    var url = "/bitrix/templates/kp-tpl/include_areas/code/onlineOrder/index.php";

    if(checkOrderIsAlreadyPaid){
        url += "?checkOrderIsAlreadyPaid=1"
    }

    var data = $(e.target).serialize();

    $.post(
        url,
        data,
        function(response) {
            if(response.result == "success"){
                $(response.robokassaForm).appendTo("body").submit();
            }
            if(response.result == "confirm"){
                if(confirm(response.message)){
                    e.data = false; // checkOrderIsAlreadyPaid = false
                    submitOnlineOrder(e);
                }
            }
            if(response.result == "error"){
                alert(response.message);
            }
        },
        "json"
    );
}