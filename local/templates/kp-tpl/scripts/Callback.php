<?
class Callback extends BaseModel
{
    const EVENT_NAME = "NEW_CALLBACK_ORDER";
    const SUBJECT_INIT = "Запрос звонка";
    const IBLOCK_TYPE = "feedback";
    const IBLOCK_ID = 8; // "Обратный звонок"
    const NAME_FIELD_NAME = "";
    const MESSAGE_FIELD_NAME = "";
    const SMARTCAPTCHA_SERVER_KEY = 'ysc2_ZVROda5A5xmaTvgRpl4popgv4tyhF0VvAVeZjYOtdbd8c03a';

    public $phone;
    public  $status = false;
    public $token = '';

    public function __construct($request)
    {
        parent::__construct($request);

        if(static::MAKE_IBLOCK_ELEMENT){
            $this->iblockId = self::IBLOCK_ID;
        }

        $this->phone = $request["phone"];
        $this->token = $_POST['smart-token'];
        $ch = curl_init();
        $args = http_build_query([
            "secret" => SMARTCAPTCHA_SERVER_KEY,
            "token" => $this->token,
            "ip" => $_SERVER['REMOTE_ADDR'], // Нужно передать IP-адрес пользователя.
            // Способ получения IP-адреса пользователя зависит от вашего прокси.
        ]);
        curl_setopt($ch, CURLOPT_URL, "https://smartcaptcha.yandexcloud.net/validate?$args");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        $server_output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpcode !== 200) {
            echo "Allow access due to an error: code=$httpcode; message=$server_output\n";
            return true;
        }
        $resp = json_decode($server_output);
        if (!empty($this->token)) {
            $this->status = true;
        } else {
            echo "Robot\n";
        }



        if (empty($this->phone) && $this->status === false) {
            throw new Exception('Спам Бот');
        }
        return $resp->status === "ok";


    }



    public function createIblockElement()
    {
        $subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
        $subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

        $element = new CIBlockElement;

        $arElementFields = [
            "IBLOCK_ID" => $this->iblockId,
            "NAME" => $subjectForUser,
            "CODE" => $this->getCode($subjectForCode),
            "ACTIVE" => "N",
            "DATE_ACTIVE_FROM" => $this->dateForCode,
            "PROPERTY_VALUES" => [
                "PHONE" => $this->phone,
            ]
        ];

        if (!$newElementId = $element->Add($arElementFields)) {
            throw new Exception($element->LAST_ERROR);
        }

        return $newElementId;
    }

    public function getEmailFields($newElementId)
    {
        $emailFields = parent::getEmailFields($newElementId);
        $emailFields["PHONE"] = $this->phone;

        return $emailFields;
    }

}


