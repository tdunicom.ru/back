function ajaxFormSubmit(params)
{
    var event = params['event'];
    var thisForm = params['form'];
    var dataHandler = params['handler'];
    var addData = params['addData'];
    var successRedirect = params['redirect'];
    var successAction = params['successAction'];
    var popupTitle = params['popupTitle'];
    var popupText = params['popupText'];
    var btnCloseText = params['btnCloseText'];
    var redirectToAnswer = params['redirectToAnswer'];

    var fieldsValidated = $('.validated', thisForm);

    event.preventDefault();
    event.stopPropagation();

    if(!formFieldsCheck(fieldsValidated)){
        return false;
    }

    var data = new FormData(thisForm);
    var url  = '/local/templates/kp-tpl/include_areas/code/';
        url += dataHandler + '.php';

    if(addData){
        data.append("addData", addData);
    }

    jQuery.ajax({
        url: url,
        data: data,
        type: 'POST',  cache: false, dataType: 'json', processData: false, contentType: false,
        success: function(data){
            if (data.result == "success")
            {
                console.log(data.message);

                if(successAction){
                    if(~successAction.indexOf("yaCounter")){
                        if(window.yaCounter36383700){
                            eval(successAction);
                        }
                    }else{
                        eval(successAction);
                    }
                }

                if(successRedirect){
                    document.location.href = successRedirect;
                }

                if(redirectToAnswer){
                    document.location.href = data.urlRedirect;
                }

                var formClass = $(thisForm).attr('class');
                if(formClass != undefined && ~formClass.indexOf('popup')){

                    var formTitle = $(".call-back-form-popup__title", $(thisForm).parent().parent());
                    if(formTitle.length && popupTitle){
                        formTitle.text(popupTitle);
                    }
                    var formBody = $(".call-back-form-popup__body", $(thisForm).parent().parent());
                    if(formBody.length && popupText){
                        formBody.html('<p>' + popupText + '</p>');
                    }

                    if(btnCloseText){
                        var closeBtn = '<a class="btn" href="#" onclick="$.fancybox.close(); return false;">' + btnCloseText + '</a>';
                        formBody.after(closeBtn);
                    }

                }else{
                    if(popupText)
                    {
                        var objButtonClose = false;
                        if(btnCloseText != undefined){
                            objButtonClose = new BX.PopupWindowButton({
                                text     : btnCloseText,
                                className: '',
                                events   : {
                                    click : function(){this.popupWindow.close()}
                                }
                            })
                        }

                        var oPopup = new BX.PopupWindow('call-offer-form-wrapper', window.body, {
                            autoHide : true,
                            offsetTop : 1,
                            offsetLeft : 0,
                            lightShadow : true,
                            closeIcon : true,
                            closeByEsc : true,
                            overlay: {
                                backgroundColor: 'black', opacity: '80'
                            },
                            buttons: [objButtonClose],
                            titleBar: {content: BX.create("span", {html: popupTitle})},
                        });

                        oPopup.setContent(popupText);
                        oPopup.show();
                        thisForm.reset();
                    }
                }

            }else{
                alert(data.message);
            }

        },
        error: function(data, textStatus, errorThrown){},
        complete: function(data, textStatus){}

    });

}






