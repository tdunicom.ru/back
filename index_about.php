<h2>О компании</h2>

<p>ТД&nbsp;Юником производит сухие строительные смеси для организаций и&nbsp;частных покупателей. Ассортимент продукции включает в&nbsp;себя штукатурку цементную, штукатурку цементно-известковую, штукатурку гипсовую, клей для керамогранита и&nbsp;газобетона, кладочную смесь и&nbsp;ЦПС. Вся продукция производится на&nbsp;современном оборудовании. Качество продукции обеспечивается технологическим контролем и&nbsp;отслеживается в&nbsp;лаборатории.</p>

