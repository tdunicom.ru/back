<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");

$APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "unitech", //
    Array(
        "COLUMNS_LIST" => array(
            0 => "NAME",
            1 => "PRICE",
            2 => "QUANTITY",
            3 => "SUM",
            4 => "DELETE",
        ),
        "PATH_TO_ORDER"	=> "/personal/order/make/",
        "HIDE_COUPON" => "Y",
        "SET_TITLE"	=> "Y",
        "SHOW_FILTER" => "N",
        "PRICE_VAT_SHOW_VALUE" => "N",
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
