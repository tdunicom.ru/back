<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе Вы можете узнать как можно оформить заказ на песок. Если у Вас возникли трудности, вы можете обратиься к нашим специалистам.");
$APPLICATION->SetPageProperty("TITLE", 'Как купить песок оптом и  в розницу в Санкт-Петербурге');
$APPLICATION->SetTitle("Как купить");
?><div class="container">
	<div class="content">
		<div class="block-wrap block-wrap_wrap">
			<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
				<h3>1. Позвоните менеджеру</h3>
				<p>
					 Позвоните по телефонам: <strong>+7 (812) 414-97-93</strong> (для юр. лиц и ИП) или <strong>+7 (812) 414-97-82</strong> (для физ. лиц). Мы вам поможем узнать стоимость, наличие на складе, оформить заказ, выбрать нужный товар и согласуем время доставки.
				</p>
				<p>
					 или
				</p>
				<p>
 <a href="#cost-calculation" class="goto_form">Заполните форму на сайте</a>. <br>
					 Мы рассчитаем стоимость.
				</p>
				<p>
 <br>
				</p>
				<h3>2. Оплатите заказ</h3>
				<p>
					 а) <strong>для юридических лиц</strong> - по выставленным счетам (после контакта с менеджером по продажами и выяснения всех нюансов). Для выставления счета, пришлите свои реквизиты на почту:&nbsp;<a href="mailto:mainsale@tdunicom.ru">mainsale@tdunicom.ru</a>
				</p>
				<p>
					 б) <strong>для физических лиц:</strong>
				</p>

				<? require "_payment_form.php"?>

				<ul class="list-marked">
					<li>по реквизитам (доступны по <a href="blank_rekvizity_unikom.doc" onclick="yaCounter36383700.reachGoal('ssilka_rekvizity'); return true;">ссылке</a>, а также могут быть вам отправлены после контакта с менеджером по продажами и выяснения всех нюансов)</li>
					 <!--li>переводом на карту Сбербанка: <strong><a href="https://online.sberbank.ru/">4276 5500 4588 4643</a></strong> Александр Владимирович К (также после выяснения всех нюансов с менеджером и полных договоренностей)</li-->
				</ul>
 <br>
				<h3>3. Получите заказ</h3>
				<p>
 <strong>Доставка</strong><br>
					 Доставим на собственном автотранспорте до пункта назначения в выбранное время.
				</p>
				<p>
 <strong>Самовывоз</strong><br>
					 Склад: Всеволожский район, д. Щеглово, ул. Инженерная. Время работы – <strong>круглосуточно, без выходных</strong>.
				</p>
				<p class="important">
					 Пожалуйста, согласуйте время приезда за заказом с менеджером.<br>
					 Связаться с менеджером можно по телефону +7 (812) 414-97-93 (для юр. лиц и ИП) или +7 (812) 414-97-82 (для физ. лиц)
				</p>
				<p class="important">
					 Пожалуйста, не забудьте предъявить электронный чек об оплате, который будет отправлен на ваш телефон.<br>
				</p>
				<p class="important">
					 Если машина крытая – обязательно наличие поддона!
				</p>
				<p>
 <a href="#contacts" class="image-open">Как нас найти</a>
				</p>
 <br>
 <br>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
				<div class="obrazec_zakaza">
					<div class="obrazec_zakaza_title">
						 Образец заказа
					</div>
 <a href="/download/zakaz_peska.xlsx" onclick="yaCounter36383700.reachGoal('ssilka_obrazec'); return true;">Скачать</a>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<div style="display: none; max-width: 600px" id="contacts">
	<h2>Производственный склад</h2>
	<p>
 <strong>Адрес</strong><br>
		 Всеволожский район, д.Щеглово, ул. Инженерная
	</p>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"ZOOM",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:60.02426525998756;s:10:\"yandex_lon\";d:30.75973191438553;s:12:\"yandex_scale\";i:12;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:30.758015300616;s:3:\"LAT\";d:60.024179347868;s:4:\"TEXT\";s:43:\"Производственный склад\";}}}",
		"MAP_HEIGHT" => "362",
		"MAP_ID" => "",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DBLCLICK_ZOOM",2=>"ENABLE_DRAGGING",)
	)
);?>
	<p>
 <br>
 <strong>Как доехать?</strong><br>
	</p>
	<ul class="nice">
		<li>При въезде в Щеглово со стороны М. Романовки, двигаетесь до Администрации Щегловского поселения.</li>
		<li>Поверните направо по главной дороге.</li>
		<li>Проехав 100 метров поверните налево по главной дороге.</li>
		<li>Проехав 200 метров поверните направо в район Плинтовка, есть вывеска - Плинтовка.</li>
		<li>Проехав около 500 метров увидите ориентир - участок с белым забором и ангаром.</li>
	</ul>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
