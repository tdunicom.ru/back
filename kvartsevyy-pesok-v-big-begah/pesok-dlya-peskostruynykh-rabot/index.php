<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для пескоструя - купить, пескоструйный песок с доставкой в Спб");
$APPLICATION->SetPageProperty("keywords", "пескоструйный песок купить для пескоструя");
$APPLICATION->SetPageProperty("description", "Пескоструйный песок в Санкт-Петербурге и области. Купить в компании «Юником». Цены на кварцевый песок для пескоструя, расчет стоимости в зависимости от веса.");
$APPLICATION->SetTitle("Кварцевый песок для пескоструя");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#kak-vybrat">Как выбрать?</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#raschet">Расчёт объёма</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Пескоструйный песок" src="/images/primenenie-peska/pesok-dlya-peskostruynykh-rabot.png" class="title-image__img" title="Пескоструйный песок" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "details",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цена кварцевого песка для пескоструя</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width10 block-wrap__item_s-width6">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20 тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					 0 - 0,63<br>
					 ГОСТ 8736-2014*
				</td>
				<td>
					 1550
				</td>
				<td>
					 1700
				</td>
				<td>
					 1700<br>
				</td>
				<td>
					 1700<br>
				</td>
				<td>
					 75 за мешок<br>
					 (3000 за тонну)
				</td>
			</tr>
			<tr>
				<td>
					 0,63 - 2,5<br>
					 ГОСТ 8736-2014
				</td>
				<td>
					 1800
				</td>
				<td>
					 1950
				</td>
				<td>
					 1950<br>
				</td>
				<td>
					 1950<br>
				</td>
				<td>
					 75 за мешок<br>
					 (3000 за тонну)<br>
				</td>
			</tr>
			<tr>
				<td>
					 0 - 0,63<br>
					 ГОСТ 2138-91 **
				</td>
				<td>
					 2100
				</td>
				<td>
					 2350
				</td>
				<td>
					 2350<br>
				</td>
				<td>
					 2350<br>
				</td>
				<td>
					 87,5 за мешок<br>
					 (3500 за тонну)
				</td>
			</tr>
			<tr>
				<td>
					 Под заказ - любая фракция
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width5 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
			<div class="small">
				 ГОСТ 8736-2014 * - содержание кварца около 60%, менее чистый
			</div>
			<div class="small">
				 ГОСТ 2138-91 ** - содержание кварца около 98%, более чистый
			</div>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<p>
		 Пескоструйная обработка — эффективный способ подготовки поверхностей к последующей обработке, путем очистки от загрязнений при помощи песочной дроби, подающейся под напором.
	</p>
	<h2 id="primenenie">Применение</h2>
	<p>
		 Кварцевый песок подходит для подготовки большинства материалов:
	</p>
	<ul class="list-marked">
		<li>металл (очистка от загрязнений, обезжиривания поверхностей перед этапом окрашивания)</li>
		<li>дерево</li>
		<li>бетон</li>
		<li>кирпич</li>
		<li>пластик</li>
		<li>стекло (матирование)</li>
	</ul>
	<h2 id="kak-vybrat">Как выбрать песок?</h2>
	<p>
		 Выбор фракции песка для обработки определяет характер выполняемых работ, а также диаметр выходного сопла пескоструйного аппарата.
	</p>
	<ul class="list-marked">
		<li>
		Прочность и толщина удаляемого загразняния<br>
		 Если выбрать слишком мелкий песок, пескоструй может не справиться с работой.</li>
		<li>
		Необходимое состояние плоскости<br>
		 Чем крупнее фракция, тем более грубой будет обработка.</li>
	</ul>
	<div class="block-wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width10 block-wrap__item_l-width10 block-wrap__item_m-width12 block-wrap__item_s-width6">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
					 Применение
				</th>
				<th>
					 Фракции, мм
				</th>
			</tr>
			<tr>
				<td>
					 Абразивная обработка хрупких материалов<br>
					 Чтобы ободрать стойкое лакокрасочное покрытие, не повредив материал
				</td>
				<td>
					 0,3 - 0,63
				</td>
			</tr>
			<tr>
				<td>
					 Начистить до блеска медную или алюминиевую поверхность
				</td>
				<td>
					 0,1 - 0,3
				</td>
			</tr>
			<tr>
				<td>
					 Для матирования стекла, нанесения красивых узорных пескоструйных рисунков на зеркала шкафа-купе и стеклянные межкомнатные двери
				</td>
				<td>
					 0 - 0,63
				</td>
			</tr>
			<tr>
				<td>
					 Удалить въевшуюся ржавчину
				</td>
				<td>
					 0,63 - 1,25
				</td>
			</tr>
			<tr>
				<td>
					 Грубая абразивная обработка<br>
					 удалить окалину после сварки или снять толстое битумное покрытие
				</td>
				<td>
					 1,25 - 2,5
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
	<h2>Подбор диаметра сопла и фракции песка</h2>
	<p>
		 Важно для качественной пескоструйной обработки и для разумного расхода абразива.
	</p>
	<p>
		 Максимальный размер зерна песка должен быть в три раза меньше диаметра отверстия сопла.
	</p>
	<p>
 <strong>Подобрать фракцию песка к соплу</strong><br>
		 Диаметр сопла делим на три. Получаем максимальный размер зерна песка.
	</p>
	<p>
 <strong>Подобрать сопло к фракции песка</strong><br>
		 Максимальный размер зерна песка умножаем на 3. Получаем оптимальны диаметр сопла. <br>
		 Используем сопло меньше - плохая проходимость абразива. Используем сопло больше - перерасход абразивного материла и плохая ударная способность.
	</p>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0–0,63 мм" src="/images/fract-pesok/pesok-0-063-8736-2014.gif" class="figure-mark__image" title="0–0,63 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0–0,63 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 8736-2014
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0,63–2,5 мм" src="/images/fract-pesok/pesok-063-25-8736-2014.gif" class="figure-mark__image" title="0,63–2,5 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0,63–2,5 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 8736-2014
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0–0,63 мм" src="/images/fract-pesok/pesok-0-063-2138-91.gif" class="figure-mark__image" title="0–0,63 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0–0,63 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138-91
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2>Преимущества кварцевого песка для использования при пескоструйных работах</h2>
	<ul class="list-marked list-marked_full">
		<li>Дешевле других абразивов</li>
		<li>Возможность обрабатывать большие площади в короткие сроки</li>
		<li>Возможно использовать его после просеивания по второму разу или перепродать отработанный песок крупных фракций в качестве строительного.</li>
		<li>Относительно низкая прочность и твердость песка снижается вероятность нанесения обрабатываемой поверхности существенных повреждений (в течение короткого времени), если неправильно подберет величину зерна. Можно безопасно работать с цветными металлами.</li>
		<li>Допустимы любые виды сопел. Скорость износа дорогостоящих сопел из карбида бора или карбида вольфрама в несколько раз меньше, чем при использовании более твердых абразивов.</li>
		<li>Широчайший диапазон фракций, что дает возможность точно подобрать материал для достижения нужного эффекта.</li>
		<li>Хорошая сыпучесть и высокая насыпная плотность – удобно работать и транспортировать такой материал</li>
		<li>Высокая прочность и долговечность — зерна практически не подвержены истиранию</li>
	</ul>
	<h2 id="raschet">Расчет необходимого объема песка</h2>
	<p>
		 Средний расход для очистки <strong>1 метра<sup>2</sup> – 60-110 кг.</strong>
	</p>
	<p>
		 В таблице представлен ориентировочный расход кварцевого песка для пескоструйного аппарата:
	</p>
	<div class="block-wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width10 block-wrap__item_l-width10 block-wrap__item_m-width12 block-wrap__item_s-width6">
			<table class="table-price">
			<tbody>
			<tr>
				<td rowspan="3" style="vertical-align:middle;width: 40%;">
					 Давление воздуха в аппарате
				</td>
				<td colspan="3">
					 Диаметр сопла, мм
				</td>
			</tr>
			<tr>
				<td>
					 3
				</td>
				<td>
					 5,5
				</td>
				<td>
					 6
				</td>
			</tr>
			<tr>
				<td colspan="3">
					 Расход песка, м<sup>3</sup>/час
				</td>
			</tr>
			<tr>
				<td>
					 3,5
				</td>
				<td>
					 23
				</td>
				<td>
					 73
				</td>
				<td>
					 103
				</td>
			</tr>
			<tr>
				<td>
					 7,0
				</td>
				<td>
					 43
				</td>
				<td>
					 133
				</td>
				<td>
					 175
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-how-to-choose.php"
	)
);?>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="kak-kupit">Как купить песок для пескоструя?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
