<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для песочницы оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Кварцевый песок для песочницы в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Кварцевый песок для песочницы Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области. ");
$APPLICATION->SetTitle("Песок для песочницы");
?>
     <div class="container">
       <div class="content">
         <ul class="list-anchors">
           <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
           <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
           <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
           <li class="list-anchors__item"><a href="#svoystva">Свойства</a></li>
           <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
         </ul>
       </div>
     </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/primenenie-peska/kvartsevyy-pesok-dlya-pesochnitsy.jpg" alt="Кварцевый песок для песочницы" title="Кварцевый песок для песочницы" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на песок для песочницы</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
<table class="table-price">
              <tbody><tr>
                <th></th>
                <th class="">
                   Навалом
                </th>
                <th class="" colspan="3">
                  Биг-бэги (за штуку, 1 тонна)
                </th>
                <th class="">
                  Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
                </th>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td class="price-table__sub-th">
                  до 10 тонн
                </td>
                <td class="price-table__sub-th">
                  от 10 до 20 тонн
                </td>
                <td class="price-table__sub-th">
                  больше 20 тонн
                </td>
                <td></td>
              </tr>
        			<tr>
                <td>
                  <strong style="white-space: nowrap">1,25 - 2,5</strong>
                  <br>ГОСТ 8736-2014
                </td>
                <td>1850</td>
                <td>2000</td>
                <td>2000</td>
                <td>2000</td>
                <td>3000</td>
        			</tr>
              
              <tr>
                <td style="white-space: nowrap">
                  <strong>0,63 - 1,5</strong>
                  <br>ГОСТ 8736-2014
                </td>
                <td colspan="5">договорная</td>
              </tr>
            </tbody></table>
<a href="/tseny/">Все цены</a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
			)
		);?>


<section class="container page-section">
  <div class="content">
    <h2 id="primery">Примеры фракций</h2>
    <div class="flexslider-container">
      <div class="flexslider">
        <ul class="slides">
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-08-125-2138-91.gif" alt="0,63 - 1,25 мм" title="0,63 - 1,25 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">0,63 - 1,25 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="1,25–2,5 мм" title="1,25–2,5 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">1,25–2,5 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="container page-section">
  <div class="content">
    <p>Кварцевый песок для песочницы — это однородный материал, полученный путем дробления натурального кварца. Главные отличия массы — светлый оттенок, чистота и безопасность: отсутствуют примеси и вредоносные компоненты.</p>
    <p>Подбирая песок для песочницы, важно учитывать, что для детей нужен качественный и чистый материал, именно таковым и является кварцевый наполнитель.</p>
  </div>
</section>

    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">
        <h2 id="svoystva">Свойства материала</h2>
        <p>Главная особенность получения этого наполнителя — особая технология производства. Кварцевый песок для песочницы обязательно прокалывают. В результате этой процедуры полностью уничтожаются вредоносные бактерии, а также создаются условия для препятствования их повторного появления и размножения. Поэтому материал получается абсолютно безопасным для здоровья ребенка.</p>
        <h2>Преимущества использования</h2>
        <p>Ребенка сложно удивить примитивной песочницей и классическим набором формочек. Поэтому действительно впечатлить детей и создать все условия для их творческого самовыражения позволяют цветные наполнители. Лучшим решением в этом случае станет окрашенный кварцевый песок для песочницы, который не теряет своих свойств под воздействием окружающей среды. Достаточно подобрать любую цветовую гамму, и устроить для детей настоящий праздник.</p>
        <div class="block-wrap">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
              <img src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-pesochnitsyi/okrashennyiy-kvartsevyiy-pesok.jpg" title="Окрашенный кварцевый песок" alt="Окрашенный кварцевый песок" itemprop="contentUrl">
              <figcaption itemprop="caption">
                Окрашенный кварцевый песок
              </figcaption>
            </figure>
          </div>
        </div>
        <p><a href="/tsvetnoy-kvartsevyy-pesok/">Цветной кварцевый песок позволит</a> не только украсить песочницу, выгодно подчеркнуть ландшафтный дизайн участка, но и развить массу полезнейших навыков. В процессе игр дети будут развивать фантазию, нестандартное мышление, выучат цвета.</p>
        <p>В пользу выбора этого материала говорят и другие его достоинства:</p>
        <ul class="list-marked">
          <li>мягкость;</li>
          <li>богатая палитра цветовых оттенков;</li>
          <li>идеальный материал для лепки;</li>
          <li>устойчивость к влаге;</li>
          <li>сохраняет яркость цветовой гаммы.</li>
        </ul>
        <p>Выбирайте и заказывайте идеальный наполнитель для песочницы в нашей компании!</p>
        <div class="block-wrap">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
              <img src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-pesochnitsyi/proizvodstvo-kvartsevogo-peska-dlya-pesochnitsyi.jpg" title="Производство кварцевого песка для песочницы" alt="Производство кварцевого песка для песочницы" itemprop="contentUrl">
              <figcaption itemprop="caption">
                Производство кварцевого песка для песочницы
              </figcaption>
            </figure>
          </div>
        </div>

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
