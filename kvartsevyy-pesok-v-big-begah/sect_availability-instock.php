            <div class="availability availability_in-stock"><img class="availability__image" src="/images/fract/availability-in-stock.jpg" alt="Фракция в наличии"/>
              <div class="availability__info">
                <div class="availability__title">Все фракции в наличии</div>
                <div class="availability__text">Можем отгрузить сразу после оплаты</div>
              </div>
            </div>
