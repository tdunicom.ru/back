<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Формовочный кварцевый песок оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("keywords", "пескоструйный песок купить для пескоструя");
$APPLICATION->SetPageProperty("description", "Реализуем Формовочный кварцевый песок в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Формовочный кварцевый песок Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Формовочный песок");
?>
      </div>
    </div>

    <div class="container">
      <div class="content">
        <ul class="list-anchors">
          <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
          <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
          <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
          <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
          <li class="list-anchors__item"><a href="#klassifikatsiya">Классификация</a></li>
          <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
        </ul>
      </div>
    </div>
    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image"><img class="title-image__img" src="/images/primenenie-peska/formovochnyy-kvartsevyy-pesok.jpg" alt="Формовочный песок" title="Формовочный песок"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на формовочный песок</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width7 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th></th>
                <th>Навалом<br>(за тонну)</th>
                <th>В биг-бегах (шт.)</th>
              </tr>
              <tr>
                <td>0 - 0,63<br>ГОСТ 2138-91</td>
                <td>2200 руб.</td>
                <td>2500 руб.</td>
              </tr>
              <tr>
                <td>0,4 - 0,63<br>ГОСТ 2138-91</td>
                <td>2200 руб.</td>
                <td>2500 руб.</td>
              </tr>
              <tr>
                <td>Под заказ</td>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
            </table>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width5 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Значительно чище, меньше примесей оксида алюминия и железа. Содержание кремния до 99%</p>
        <p>Кварцевый песок соответствует ГОСТ: 22551-77.</p>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure class="image-inarticle">
              <img src="/images/kvartsevyy-pesok-v-big-begah/formovochnij-kvarcevii-pesok/formovochnij-kvarcevii-pesok.jpg" title="Формовочный кварцевый песок" alt="Формовочный кварцевый песок">
              <figcaption>
               Формовочный кварцевый песок
              </figcaption>
            </figure>
          </div>
        </div>
        <p>
          Формовочные кварцевые пески представлены сыпучими материалами, в составе которых присутствует кристаллический кварц в зернах, а также глинистые примеси, количество которых может достигать 50%. Размер <a href="/informatsiya/fraktsiya-kvartsevogo-peska/">фракции таких кварцевых песков</a> обычно составляет от 0,05 до 2,5 миллиметров.
        </p>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="primery">Примеры фракций</h2>
        <div class="flexslider-container">
          <div class="flexslider">
            <ul class="slides">
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-2138-91.gif" alt="0 - 0,63" title="0 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0 - 0,63</div>
                    <div class="figure-mark__desc">ГОСТ 2138-91</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-03-06-2138-91.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0,4 - 0,63</div>
                    <div class="figure-mark__desc">ГОСТ 2138-91</div>
                  </figcaption>
                </figure>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <section class="container page-section">
      <div class="content">
        <h2>Способы добычи</h2>
        <p>Формовочные кварцевые пески обычно добывают исключительно открытым способом. В составе полученного материала могут присутствовать не только частицы глины, но и полевые шпаты, оксиды железа, примеси слюды, всевозможных минералов. Последние компоненты понижают огнеупорность сырья, повышая вероятность появления пригара на отливках.</p>
        <p>Сырье обогащают непосредственно в местах добыч, очищают от лишних примесей. В результате его ценность и качественные свойства существенно повышаются. После этого песок фракционируют, ориентируясь на величину зерен.</p>
        <h2 id="primenenie">Сфера применения</h2>
        <p>По своим свойствам добытое сырье представляет собой осадочные горные породы, из которых получают стержневые, а также формовочные смеси. Смеси необходимы для применения в литейном производстве: их отличительная огнеупорность позволяет производить качественные литейные стержни и формы. Также из песков этого вида создают синтетические смолы, активно используют их в качестве основного материала для изготовления бетонита, цемента.</p>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle">
              <img src="/images/kvartsevyy-pesok-v-big-begah/formovochnij-kvarcevii-pesok/litejnie-sterjni.jpg" title="Литейные стержни" alt="Литейные стержни">
              <figcaption>
                Литейные стержни
              </figcaption>
            </figure>
          </div>
        </div>
        <p>Использование обогащенного формовочного песка высокого качества — экономически целесообразно и оправдано. Материал не только позволяет сократить расходы на дорогостоящие связующие компоненты, но и добиться нужных физико-механических показателей смесей.</p>
        <h2>Качественные свойства</h2>
        <p>Главная особенность формовочных кварцевых песков — наличие в составе кремнезема. От его количества напрямую зависит качество материала.</p>
        <p>Отличительные свойства кварцевых зерен кроются в хорошей огнеупорности, достаточной твердости и высокой степени их прозрачности. Последнее качество определяет светлый оттенок сырья. Прочие цвета материал получает за счет наличия тех или иных примесей. Большее количество примесей повышает огнеупорные свойства, а также делает песок гораздо светлее.</p>

      </div>
    </section>

    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">
        <h2 id="klassifikatsiya">Классификация материалов</h2>
        <p>Процентное присутствие в составе материала глинистых компонентов определяет его классификацию. Так, его классифицируют на одиннадцать классов. Выделяют 4 класса кварцевых, в составе которых присутствует до 2% глинистых примесей, а содержание кремнезема достигает как минимум 90%. Три класса относят к обогащенным кварцевым пескам, процентное содержание кремнезема в которых не меньше 97%, а глинистых примесей — до 1%.</p>
        <p>Остальные пески классифицированы по наличию глинистых компонентов. Среди них выделяют: очень жирные (треть или половина приходится на глинистые примеси), жирные (не более трети глинистых добавок), полужирные (до 20% глинистых компонентов) и тощие (не более 10% глинистых составляющих).</p>
        <p>Полужирные и тощие пески находят применение в создании форм на этапе изготовления отливок из чугуна и цветных сплавов. Обогащенные нужны для выпуска стержней и форм, которые можно бы было отверждать в холодной, а также горячей оснастке.</p>
        <p>По величине зерен пески также классифицируют. Выделяют восемь групп, к которым относятся: пылевидные, тонкие, мелкие и очень мелкие, средние, крупные и очень крупные, а также грубые. Фракционный состав определяют, просеивая смесь через набор сит, отличающихся разностью диаметров ячеек.</p>
        <p>Выделяют также такое понятие как основная фракция песка, определяемая остатком смеси, оставшейся на трех смежных ситах. Ее оптимальный показатель находится в пределах 70%. Также по этому критерию песок классифицируют на две категории: А и Б.</p>
        <p>В первую попадают смеси, остатки основной фракции которых на крайнем сите, расположенном вверху, больше, нежели на аналогичном сите, но расположенном снизу. Ко второй категории относятся смеси с противоположными значениями.</p>
        <p>Существует и еще одна классификация по форме зерен. По этому показателю пески бывают: остроугольной, полуокруглой либо округлой формы.</p>
        <h2>Как разобраться в маркировке?</h2>
        <p>Разобраться в маркировке очень просто, если знать принцип обозначения сырья. Так, первым пишут класс, после обозначают зерновую группу, а затем уже указывают категорию.</p>

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
