<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("keywords", "сухой кварцевый песок купить цена спб опт");
$APPLICATION->SetPageProperty("description", "Купить кварцевый песок от производителя, без наценок с доставкой. Цена на сухой кварцевый песок в Санкт-Петербурге (СПб). Соответствует ГОСТу, есть сертификат.");
$APPLICATION->SetPageProperty("title", "Кварцевый песок - купить в спб, цена на песок сухой кварцевый");
$APPLICATION->SetTitle("Песок кварцевый, цветной - Цементно-песчаная смесь");
?>
<?/*$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_RECURSIVE" => "Y",
    "AREA_FILE_SHOW" => "page",
    "AREA_FILE_SUFFIX" => "banner",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => ""
  )
);*/?>
    <div class="container home-section home-section_home-top">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <a href="/kvartseviy-pesok/">
              <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
                  <picture>
                        <source srcset="/images/suhoj-pesok.webp">
                        <img class="figure-mark__image" src="/images/suhoj-pesok.jpg" width="614" height="492" alt="Сухой песок" title="Сухой песок" itemprop="contentUrl">
                  </picture>
                <figcaption class="figure-mark__caption" itemprop="caption">
                  <div class="figure-mark__title" itemprop="name">Сухой песок</div>
                </figcaption>
              </figure>
            </a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <a href="/sukhie-stroitelnye-smesi-tolling/">
              <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
                  <picture>
                        <source srcset="/images/sukhie-stroitelnye-smesi-tolling.webp">
                        <img class="figure-mark__image" src="/images/sukhie-stroitelnye-smesi-tolling.jpg" width="614" height="492" alt="Толлинг" title="Толлинг" itemprop="contentUrl">
                  </picture>
                <figcaption class="figure-mark__caption" itemprop="caption">
                  <div class="figure-mark__title" itemprop="name">Толлинг</div>
                </figcaption>
              </figure>
            </a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <a href="/graviy/">
              <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
                  <picture>
                        <source srcset="/images/gravij.webp">
                        <img class="figure-mark__image" src="/images/gravij.jpg" width="614" height="492" alt="Гравий" title="Гравий" itemprop="contentUrl">
                  </picture>
                <figcaption class="figure-mark__caption" itemprop="caption">
                  <div class="figure-mark__title" itemprop="name">Гравий</div>
                </figcaption>
              </figure>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="container home-section home-section_home-advantages">
      <div class="content">
        <h2>Почему с нами работают?</h2>
        <ul class="list-advantages block-wrap block-wrap_wrap">
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Выгодная цена</div>
            <p>Нет наценки посредника за&nbsp;счет собственного производства и&nbsp;прямых поставок сырья с&nbsp;заводов производителей.</p>
          </li>
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Ассортимент</div>
            <p>Постоянное наличие на&nbsp;складе основных товарных позиций.</p>
          </li>
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Высокие стандарты производства</div>
            <p>Оборудование ведущего производителя с&nbsp;компьютизированным контролем технологической цепочки.</p>
          </li>
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Контроль качества</div>
            <p>Оперативный контроль сырья и&nbsp;готовой продукции за&nbsp;счет собственной лаборатории.</p>
          </li>
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Гибкая система оплаты</div>
            <p>Индивидуальные условия для каждого клиента, наличный и&nbsp;безналичный расчет.</p>
          </li>
          <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="list-advantages__header">Удобство расположения склада (всего 15 минут от КАД).<br>Отгрузка в режиме 24/7</div>
          </li>
        </ul>
      </div>
    </div>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "manufacturing",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "getprice",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<div class="container home-section home-section_home-how-to-buy">
	<div class="content">
		<h2>Как купить кварцевый песок?</h2>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_RECURSIVE" => "Y",
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
</div>
<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>
