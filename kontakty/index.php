<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Наш адрес: г. Санкт-Петербург, Большой пр. ПС, д. 48, оф. 408. Время работы с понедельника по пятницу с 10:00 до 20:00. Телефон +7 (812) 414-97-93.");
$APPLICATION->SetPageProperty("TITLE", 'Наши контакты - Компания Юником');
$APPLICATION->SetTitle("Контакты");
?><div class="container">
	<div class="content">
		 <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "addressCountry": "Россия",
    "addressRegion": "Ленинградская область",
    "addressLocality": "Санкт-Петербург",
    "postalCode": "197198",
    "streetAddress": "Большой пр. ПС, д. 48, оф. 408"
},
  "name": "Юником",
  "url":"http://www.kvarc-pesok.ru",
  "description": "Специализируемся на производстве кварцевого песка высокого качества и практически с полным отсутствием посторонних примесей.",
  "email": "zakaz@kvarc-pesok.ru",
  "telephone": "+7 (812) 313-72-39",
  "openingHours": "Mo, Tu, We, Th, Fr 10:00-20:00",
  "geo": {
   "@type": "GeoCoordinates",
    "latitude": "59.95972356413336",
    "longitude": "30.301621000000022"
  },
  "hasMap": " https://www.google.ru/maps/place/%D0%AE%D0%BD%D0%B8%D0%BA%D0%BE%D0%BC/@59.9598198,30.2993843,17z/data=!4m5!3m4!1s0x4696314671bec16d:0x8e7d0703cc59706a!8m2!3d59.959793!4d30.301573"
}
</script>
		<div class="block-wrap block-wrap_wrap">
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Офис</h2>
				<p>
 <strong>Адрес</strong><br>
					 197022,&nbsp;Санкт-Петербург,<br>пр.Медиков, д.5, лит.Б, оф.337а
				</p>
				<p>
 <strong>Время работы офиса</strong><br>
					 пн-пт (10:00-18:00)
				</p>
				<p>
 <strong>Телефон</strong><br>
				</p>
        <p><span style="font-size: 1.5rem; letter-spacing: 0.03125rem">+7 (812) 414-97-93</span> для юр. лиц и ИП</p>
        <p><span style="font-size: 1.5rem; letter-spacing: 0.03125rem">+7 (812) 414-97-82</span> для физ. лиц</p>
        <p><span style="font-size: 1.5rem; letter-spacing: 0.03125rem">+7 (812) 500-52-90</span> офис (вопросы и предложения)</p>

				<p>
 <strong>E-mail</strong><br>
 <a href="mailto:info@tdunicom.ru">info@tdunicom.ru</a>
				</p>
        <h2>Фото офиса</h2>
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <img src="/images/kontakty/office-1.jpg" alt="">
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <img src="/images/kontakty/office-2.jpg" alt="">
          </div>
        </div>
				<div>
          <h2 style="margin-top: 2rem">Реквизиты ООО "Юником"</h2>
					<p>
						 Юридический адрес: Россия, г. Санкт-Петербург, пр.Медиков, д.5, лит.Б, оф.337а<br>
						 Почтовый адрес: 197022, г. Санкт-Петербург, пр.Медиков, д.5, лит.Б, оф.337а <br>
						 ИНН: 7840412267<br>
						 КПП: 781301001<br>
						 ОГРН: 1097847117455<br>
						 ОКПО: 60982759<br>
						 ОКАТО: 40298566000<br>
						 ОКВЭД: 51.70, 52.48, 52.12
					</p>
					<p>
						 Банк: ОА БАНК «ПСКБ» г. Санкт-Петербург<br>
						 БИК: 044030852<br>
						 к/сч. 301 018 100 00 00 00 00 852<br>
						 р/сч. 407 028 106 00 00 00 274 26
					</p>
					<h3>По розничным продажам</h3>
					<p>
						Общество с ограниченной ответственностью «Унитех»<br>
						Юр. адрес: 197198, гор. Санкт-Петербург, пр. Большой ПС, д. 48 лит А, оф 307а<br>
						Факт. адрес: 197198, гор. Санкт-Петербург, пр. Большой ПС, д. 48 лит А, оф 307а<br>
						E-mail: <a href="mailto:unitechspb@yandex.ru">unitechspb@yandex.ru</a>
					</p>
					<p>
						ИНН/КПП 7838469611/781301001<br>
						ОГРН 1117847656321
					</p>
					<p>
						р/сч 40702810300000028709<br>
						в АО БАНК «ПСКБ»<br>
						к/сч 30101810000000000852<br>
						БИК 044030852
					</p>
					<p>
						ОКПО 30728437<br>
						ОКТМО 40393000000<br>
						ОКВЭД 28.41
					</p>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Производственный склад</h2>
				<p>
 <strong>Адрес</strong><br>
					 Всеволожский район, д.Щеглово, ул. Инженерная, 17
				</p>
				 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"ZOOM",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:60.02426525998756;s:10:\"yandex_lon\";d:30.75973191438553;s:12:\"yandex_scale\";i:12;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:30.758015300616;s:3:\"LAT\";d:60.024179347868;s:4:\"TEXT\";s:43:\"Производственный склад\";}}}",
		"MAP_HEIGHT" => "360",
		"MAP_ID" => "",
		"MAP_WIDTH" => "470",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DBLCLICK_ZOOM",2=>"ENABLE_DRAGGING",)
	)
);?>
				<p>
 <br>
 <strong>Как доехать?</strong><br>
				</p>
				<ul class="nice">
					<li>При въезде в Щеглово со стороны М. Романовки, двигаетесь до Администрации Щегловского поселения.</li>
					<li>Поверните направо по главной дороге.</li>
					<li>Проехав 100 метров поверните налево по главной дороге.</li>
					<li>Проехав 200 метров поверните направо в район Плинтовка, есть вывеска - Плинтовка.</li>
					<li>Проехав около 500 метров увидите ориентир - участок с белым забором и ангаром.</li>
				</ul>
        <p><a href="https://yandex.ru/maps/-/CCQtANh7~D" class="link-address">Геопозиция для навигатора</a></p>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
