<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", 'К сожалению запрашиваемая вами страница не найдена. Для продолжения работы с сайтом Вы можете вернуться на главную страницу или воспользоваться картой сайта');
?>
<section class="page-section container">
    <div class="content">
        <div class="breadcrumbs"></div>
        <h1><? $APPLICATION->ShowTitle("h1", false) ?></h1>
    </div>
</section>

<section class="page-section container">
    <div class="content">
        <p>Вы перешли по ссылке, которая не существует.<br/>Возможно набрали неправильно адрес или страница удалена.</p>
        <p>С главной страницы можно найти любую информацию на нашем сайте,<br><a href="/">перейти на главную</a>?</p>
    </div>
</section>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
