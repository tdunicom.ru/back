<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Реализуем кварцевый песок в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Вы можете, отправив заявку через сайт. Доставка кварцевого песка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок в биг-бегах (тонна)");
$APPLICATION->SetTitle("Кварцевый песок в биг-бегах");
?>
<section class="container">
<div class="content">
		<p class="subtitle">
			Продажа и доставка в Санкт-Петербурге и Ленинградской области
		</p>
			<p>
				Наша компания реализует в Санкт-Петербурге и Ленинградской области кварцевый песок различных фракций. Менеджеры помогут вам купить кварцевый песок высокого качества и практически с полным отсутствием посторонних примесей. Мы работает с партиями кварцевого песка любого объема, в том числе, организуем его доставку потребителям собственным грузовым автомобильным транспортом.
			</p>
	 <br>
			<h2>Фракции песка</h2>
			<ul class="list-marked">
				<li>песок — 0,1-0,8 миллиметров</li>
				<li>крупный песок — 0,5-2 миллиметра</li>
				<li>пылевидный кварц — менее 0,1 миллиметра</li>
			</ul>
			<p>
				<strong>Под заказ возможно <a href="/informatsiya/proizvodstvo-i-dobycha-kvartsevogo-peska/">производство песка</a> определенных фракций</strong>
			</p>
			<br>
			<p>
				 Доставка собственным автотранспортом: тонаром, цементовозом.
			</p>
		<div class="rozn-prod block-wrap  block-wrap_wrap">

			<div class="rozn-prod__item block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6" itemscope itemtype="http://schema.org/ImageObject">
		 <span class="rozn-prod__label" itemprop="caption">Биг-бэг - мешок около 1 тонны</span><img src="/upload/medialibrary/8bb/8bb7a8a92f5682b7c50dea06e9b9221e.jpg" alt="" itemprop="contentUrl">
			</div>
		</div>




		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_RECURSIVE" => "Y",
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "kv_pesok",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>