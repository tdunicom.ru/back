<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>








<div class="block-wrap block-wrap_wrap">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">

<!--noindex-->
		<h2>Отправьте заявку для расчета стоимости</h2>
		<p>
				Или отправьте заявку со списком заказа на расчет стоимости.<br>
				Укажите в комментариях, куда доставить и мы рассчитаем стоимость доставки.<br>
				Перезвоним в течение 15 минут в рабочее время (пн-пт с 10:00-20:00).<br>
				Или на следующий рабочий день
		</p>
		<br>

		<h2>Собственное производство песка</h2>
		<p>ООО "Юником" обрабатывает кварцевый песок с 2012 года.</p>
		<p>Контролируем и ведем самостоятельно всю техническую цепочку: грубое просеивание, сушку, охлаждение, мелкое просеивание (фракционирование) и фасовка в биг-беги.</p>
<!--/noindex-->
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
		<div class="obrazec_zakaza">
        <div class="obrazec_zakaza_title">Образец заказа</div>
        <a href="/download/zakaz_peska.xlsx">Скачать</a>
    </div>
	</div>
</div>