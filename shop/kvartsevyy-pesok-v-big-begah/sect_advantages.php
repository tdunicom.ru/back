<section class="container page-section page-section_list-advantages">
<div class="content">
	<ul class="list-marked list-marked_half list-marked_xl-col2 list-marked_s-col1">
		<li><strong>Выгодная цена</strong><br>
		 От производителя&nbsp;без наценок и посредников!</li>
		<li><strong>Высокие стандарты производства</strong><br>
 Качество фракционирования отслеживается высококвалифицированными сотрудниками нашей лаборатории</li>
		<li><strong>Ассортимент</strong><br>
		 Постоянное наличие на&nbsp;складе основных товарных позиций<br>
 </li>
		<li><strong>Гибкая система оплаты</strong><br>
		 Индивидуальные условия для каждого клиента, наличный и безналичный расчет<br>
 </li>
	</ul>
</div>
 </section>
