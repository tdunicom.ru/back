<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>

<div class="how_to_buy wrapper">
    <h2 class="subtitle">Как купить кварцевый песок в Санкт-Петербурге?</h2>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">01</div>
            <span>Заявка</span>
        </div>
        <div class="how_to_buy_text">
            <p>Отправьте заявку с сайта или позвоните по телефону:<br/>
            <span>+7 (812) 414-97-93</span></p>
            <p>Уточните наличие и сроки поставки.</p>
        </div>
    </div>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">02</div>
            <span>Оплата</span>
        </div>
        <div class="how_to_buy_text">
            <p>Выставим счет на вашу организацию, после оплаты отправляем полностью заказ.</p>
            <p>Для выставления счета пришлите реквизиты на почту: </p>
            <p><a href="mailto:zakaz@kvarc-pesok.ru" class="email">zakaz@kvarc-pesok.ru</a></p>
        </div>
    </div>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">03</div>
            <span>Доставка</span>
        </div>
        <div class="how_to_buy_text">
            <p>Доставляем на собственном транспорте по Санкт-Петербургу и Ленинградской области.</p>
            <p>Проработанная система поставок с месторождений Карелии делает возможным продажу кварцевого песка в любых объемах.</p>
            <p><a href="/dostavka/">Подробнее о доставке</a></p>
        </div>
    </div>

</div>