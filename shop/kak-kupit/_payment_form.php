<style>
	.form-group{ margin: 20px 0;}
	.form-group label{ margin: 10px 0; display: block;}
</style>

<script src="<?= SITE_TEMPLATE_PATH ?>/scripts/sand_online_order.js?v=4"></script>

<form class="payment-form validated-form" method="post" name="sand_online_order">
  <h3>Состав заказа</h3>
  <p>Песок сухой кварцевый, 1 тонна.</p>
  <div class="block-wrap  block-wrap_wrap ">

	 <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 payment-form__group">
		<label class="payment-form__group-title" for="quantity">Количество (т)</label>
		<input
            class="form-control"
            id="quantity"
            type="number"
            name="quantity"
            min="1"
            step="1"
            value="1"
        >
	 </div>

	 <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6v payment-form__group payment-form__group--delivery">
		<label class="payment-form__group-title" for="cost">Стоимость (руб.)</label>
		<input
            class="form-control validated required"
            id="cost"
            type="text"
            name="cost"
        >
		<div class="validation-msg">Обязательное поле</div>
	 </div>

	 <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6 payment-form__group payment-form__group--delivery-description">
	  <p style="margin-top: -1rem;" class="important">Укажите стоимость, согласованную с менеджером</p>
		<p>Свяжитесь с менеджером по телефонам +7&nbsp;(812)&nbsp;414-97-93 (для&nbsp;юр. лиц и&nbsp;ИП) или&nbsp;+7&nbsp;(812)&nbsp;414-97-82 (для физ. лиц), чтобы узнать стоимость.</p>
	 </div>

  </div>

  <h4>Контактная информация</h4>

  <div class="block-wrap payment-form__group payment-form__group--contact block-wrap_wrap ">

	 <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<label for="name" class="payment-form__group-title">ФИО</label>
		<input
            class="form-control validated required"
            id="name"
            type="text"
            name="name"
        >
		<div class="validation-msg">Обязательное поле</div>
	 </div>

	 <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<label for="phone" class="payment-form__group-title">Телефон</label>
		<input
            class="form-control validated required"
            id="phone"
            type="text"
            name="phone"
        >
		<div class="validation-msg">Обязательное поле</div>
	 </div>

	 <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
		<label for="email" class="payment-form__group-title">E-mail</label>
		<input
            class="form-control validated required"
            id="email"
            type="text"
            name="email"
        >
		<div class="validation-msg">Обязательное поле</div>
	 </div>

  </div>

  <button class="btn btn-primary button" type="submit" name="action" value="pay">Оплатить</button>

</form>
