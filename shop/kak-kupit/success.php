<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе Вы можете узнать как можно оформить заказ на песок. Если у Вас возникли трудности, вы можете обратиься к нашим специалистам.");
$APPLICATION->SetPageProperty("TITLE", 'Как купить песок оптом и  в розницу в Санкт-Петербурге');
$APPLICATION->SetTitle("Как купить");
?>
<div class="container">
  <div class="content">
    <h3 style="color: green">Спасибо за оплату заказа</h3>
    <p>Номер вашего заказа: №<?= $_GET["orderId"] ?></p>
    <p>Скоро с вами свяжется наш менеджер и подробно расскажет про получение заказа.</p>
    <br>
    <p>Пожалуйста, не забудьте предъявить электронный чек об оплате, который был отправлен на ваш телефон.</p>
    <br>
    <br>
  </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>