<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В данном разделе Вы можете узнать как можно оформить заказ на песок. Если у Вас возникли трудности, вы можете обратиься к нашим специалистам.");
$APPLICATION->SetPageProperty("TITLE", 'Как купить песок оптом и  в розницу в Санкт-Петербурге');
$APPLICATION->SetTitle("Как купить");
?>
                  <div class="container">
                    <div class="content">
<div class="block-wrap block-wrap_wrap">
	<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
		<h3>1. Сделайте заявку</h3>
		<p>
			Узнайте стоимость и наличие на складе.
		</p>
		<p>
 <a href="#cost-calculation" class="goto_form">Заполните форму на сайте</a>. <br>
			 Мы рассчитаем стоимость доставки, для этого укажите в комментариях адрес
		</p>
		<p>
			или позвоните по телефону: +7 (812) 414-97-93. Мы вам поможем оформить заказ, выбрать нужный товар и согласуем время доставки.
		</p>
		<h3>2. Оплатите заказ</h3>
		<p>
			Для выставления счета, пришлите свои реквизиты на почту:<br>
			 kvarc-pesok@yandex.ru
		</p>
		<p style="">
			 а) <strong>для юридических лиц</strong> - по выставленным счетам (после контакта с менеджером по продажами и выяснения всех нюансов)<br>
			 б) <strong>для физических лиц:</strong>
		</p>
			<ul class="list-marked">
				<li>по реквизитам (доступны по <a href="blank_rekvizity_unikom.doc" onclick="yaCounter36383700.reachGoal('ssilka_rekvizity'); return true;">ссылке</a>, а также могут быть вам отправлены после контакта с менеджером по продажами и выяснения всех нюансов)</li>
				<!--li>переводом на карту Сбербанка: <strong><a href="https://online.sberbank.ru/">4276 5500 4588 4643</a></strong> Александр Владимирович К (также после выяснения всех нюансов с менеджером и полных договоренностей)</li-->
				<li><p>Оплатить наши услуги можно в любом терминале ПСКБ Банка - а их больше 250 штук по всему Санкт-Петербургу!</p>

<p>Для оплаты выберите раздел "Оплата товаров" и найдите наш логотип.<br>
Далее укажите свои контактные данные, количество тонн и фракцию песка. </p>
<div class="block-wrap__item block-wrap block-wrap_wrap">
    <div class="block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
      <a href="/images/payment-delivery/payment-1.jpg" class="image-open">
          <img src="/images/payment-delivery/payment-1.jpg" alt="Оплата через терминал ПСКБ Банка">
      </a>
    </div>
    <div class="block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
      <a href="/images/payment-delivery/payment-2.jpg" class="image-open">
          <img src="/images/payment-delivery/payment-2.jpg" alt="Оплата через терминал ПСКБ Банка">
      </a>
    </div>
</div>
<p><a href="http://www.pscb.ru/locations/terminals/" target="_blank">Список терминалов ПСКБ на карте.</a></p></li>
			</ul>
 <br>
		<h3>3. Получите заказ</h3>
		<p>
 <strong>Доставка</strong><br>
			 Доставим на собственном автотранспорте до пункта назначения в выбранное время.
		</p>
		<p>
 <strong>Самовывоз</strong><br>
			 Склад: Всеволожский район, д. Щеглово. Время работы с 10:00-20:00
		</p>
 <br>
 <br>
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
		
		<div class="obrazec_zakaza">
			<div class="obrazec_zakaza_title">
				Образец заказа
			</div>
	 		<a href="/download/zakaz_peska.xlsx" onclick="yaCounter36383700.reachGoal('ssilka_obrazec'); return true;">Скачать</a>
		</div>
	</div>
</div>

</div>
	</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>