<section class="page-section container home-header">
	<div class="content">

		<div class="home-header__inner">


			<h1 class="home-header__title">Сухие строительные смеси оптом и&nbsp;в&nbsp;розницу по&nbsp;ценам производителя</h1>

			<p class="home-header__description">Получите бесплатную консультацию</p>

			<form class="home-header__form">

                <input type="text" class="req_name" name="req_name">

                <label class="form-input form-input--tel home-header__phone">
                    <input class="form-input__field" type="tel" name="phone" required placeholder="+7 (000) 000-00-00">
                </label>
								<div class="yandex-captcha">
									<div
										style="height: 100px"
										id="captcha-container"
										class="smart-captcha"
										data-sitekey="ysc1_ZVROda5A5xmaTvgRpl4pLlrGVyFi0kV7Cf2Dzw3Fe780524d"
									></div>
								</div>
                <button class="button home-header__submit" type="submit">Подобрать смесь</button>

                <input type="hidden" name="action" value="callback">

			</form>

		</div>

	</div>
</section>



