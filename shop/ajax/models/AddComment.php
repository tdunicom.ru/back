<?
class AddComment extends BaseModel
{
	const EVENT_NAME = "NEW_COMMENT";
	const SUBJECT_INIT = "Новый комментарий";
	const IBLOCK_TYPE = "feedback";
	const IBLOCK_ID = IBLOCK_ID_COMMENTS;

	const NAME_FIELD_NAME = "name";
	const MESSAGE_FIELD_NAME = "comment";

	public $name;
	public $email;
	public $title;
	public $text;
	public $productId;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->name = $request["name"];
		$this->email = $request["email"];
		$this->title = $request["title"];
		$this->text = $request["text"];
		$this->productId = $request["product_id"];

		if (empty($this->name) || empty($this->email) || empty($this->title) || empty($this->text)) {
			throw new Exception("Не все поля формы заполнены");
		}
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $this->title,
			"CODE" => $this->getCode($subjectForCode),
			"PREVIEW_TEXT" => $this->text,
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"EMAIL" => $this->email,
				"PRODUCT" => $this->productId,
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		return $newElementId;
	}

	private function getProductName()
	{
		$product = $this->getProduct();

		$productName = '<a href="'.$product["url"].'">'.$product["name"].'</a>';
		$result = $productName." (id ".$product["id"]."), ";
		$result .= "раздел ".$product["section"];

		return $result;
	}

	private function getProduct()
	{
		if(empty($this->productId)){
			return "";
		}

		$product = ["id" => $this->productId];

		$resProduct = CIBlockElement::GetByID($this->productId);
		$resultProduct = $resProduct->GetNext();
		$product["name"] = $resultProduct["NAME"];
		$product["section_id"] = $resultProduct["IBLOCK_SECTION_ID"];
		$product["url"] = $resultProduct["DETAIL_PAGE_URL"];

		$resSection = CIBlockSection::GetList([], ["ID" => $product["section_id"]], false, ["NAME"], false);
		$resultSection = $resSection->Fetch();
		$product["section"] = $resultSection["NAME"];

		return $product;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);

		$emailFields["NAME"] = $this->name;
		$emailFields["EMAIL"] = $this->email;
		$emailFields["TITLE"] = $this->title;
		$emailFields["TEXT"] = $this->text;
		$emailFields["PRODUCT"] = $this->getProductName();

		return $emailFields;
	}

}



