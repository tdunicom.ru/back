<?
class Calculation
{
	private $area;
	private $thickness;
	private $consumption;
	private $result;

	const MAKE_IBLOCK_ELEMENT = false;
	const SEND_EMAIL = false;
	const BAG_CAPACITY = 25;

	public function __construct($request)
	{
		$this->area = $request["area"];
		$this->thickness = $request["thickness"];
		$this->consumption = $request["consumption"];

		if(empty($this->area) || empty($this->thickness) || empty($this->consumption)){
			throw new \Exception("Не все поля формы заполнены");
		}
	}

	public function doAction()
	{
		$this->result = $this->calculate();
	}

	public function getMessage()
	{
		$result = $this->result;
		$message = "Требуется ".$result["total_consumption"]." кг: ";

		if($result["total_consumption_raw"] < self::BAG_CAPACITY){
			$message .= "менее 1 мешка";
		}else{
			$message .= $result["bags_count"]." ".$result["bags_count_label"]." ".self::BAG_CAPACITY." кг";
		}

		return $message;
	}

	private function calculate()
	{
		$this->consumption = str_replace(',', '.', $this->consumption);

		$volume = $this->area * $this->thickness;
		$totalConsumption = $volume * $this->consumption;
		$bagsCount = ceil($totalConsumption / self::BAG_CAPACITY);
		$bagsCountLabel = pluralDeclension("мешок", $bagsCount);

		return [
			"total_consumption_raw" => $totalConsumption,
			"total_consumption" => str_replace(',0', '', number_format($totalConsumption, 1, ',', ' ')),
			"bags_count" => $bagsCount,
			"bags_count_label" => $bagsCountLabel,
		];
	}

}