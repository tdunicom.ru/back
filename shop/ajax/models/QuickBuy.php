<?
class QuickBuy extends BaseModel
{
	const EVENT_NAME = "NEW_QUICK_BUY";
	const SUBJECT_INIT = "Быстрая покупка";
	const IBLOCK_TYPE = "feedback";
	const IBLOCK_ID = IBLOCK_ID_QUICK_BUY;

	const NAME_FIELD_NAME = "name";
	const MESSAGE_FIELD_NAME = "";

	public $name;
	public $phone;
	public $productId;
	public $quantity;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->name = $request["name"];
		$this->phone = $request["phone"];
		$this->productId = $request["product_id"];
		$this->quantity = $request["product_quantity"];

		if (empty($this->name)) {throw new Exception("Имя не заполнено");}
		if (empty($this->phone)) {throw new Exception("Телефон не заполнен");}
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = $this->name." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"NAME" => $this->name,
				"PHONE" => $this->phone,
				"PRODUCT_ID" => $this->productId,
				"STATUS" => 3, // "Поступил",
				"QUANTITY" => $this->quantity,
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);
		$emailFields["NAME"] = $this->name;
		$emailFields["PHONE"] = $this->phone;
		$emailFields["PRODUCT_DESCRIPTION"] = $this->getProductDescription();
		$emailFields["PRODUCT_NAME"] = $this->getProductName();

		return $emailFields;
	}

	private function getProductDescription()
	{
		$product = $this->getProduct();

		$productName = '<a href="'.$product["url"].'">'.$product["name"].'</a>';
		$result = $productName." (id ".$product["id"]."), ";
		$result .= "количество ".$product["quantity"].", ";
		$result .= "цена ".$product["price"].", ";
		$result .= "раздел ".$product["section"];

		return $result;
	}

	private function getProductName()
	{
		$product = $this->getProduct();
		return $product["name"];
	}

	private function getProduct()
	{
		if(empty($this->productId)){
			return "";
		}

		$product = ["id" => $this->productId];

		$resProduct = CIBlockElement::GetByID($this->productId);
		$resultProduct = $resProduct->GetNext();
		$product["name"] = $resultProduct["NAME"];
		$product["section_id"] = $resultProduct["IBLOCK_SECTION_ID"];
		$product["url"] = $resultProduct["DETAIL_PAGE_URL"];

		$resultPrice = CPrice::GetBasePrice($this->productId);
		$product["price"] = CurrencyFormat($resultPrice["PRICE"], $resultPrice["CURRENCY"]);

		$product["quantity"] = $this->quantity;

		$resSection = CIBlockSection::GetList([], ["ID" => $product["section_id"]], false, ["NAME"], false);
		$resultSection = $resSection->Fetch();
		$product["section"] = $resultSection["NAME"];

		return $product;
	}

}



