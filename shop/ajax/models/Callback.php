<?
class Callback extends BaseModel
{
	const EVENT_NAME = "NEW_CALLBACK_ORDER";
	const SUBJECT_INIT = "Запрос звонка";
	const IBLOCK_TYPE = "feedback";
	const IBLOCK_ID = 8; // "Обратный звонок"
	const NAME_FIELD_NAME = "";
	const MESSAGE_FIELD_NAME = "";

	public $phone;

	public function __construct($request)
	{
		parent::__construct($request);

		if(static::MAKE_IBLOCK_ELEMENT){
			$this->iblockId = self::IBLOCK_ID;
		}

		$this->phone = $request["phone"];

		if (empty($this->phone)) {throw new Exception("Phone is empty");}
	}

	public function createIblockElement()
	{
		$subjectForCode = self::SUBJECT_INIT."_".$this->dateForCode;
		$subjectForUser = self::SUBJECT_INIT." от ".$this->dateForUser;

		$element = new CIBlockElement;

		$arElementFields = [
			"IBLOCK_ID" => $this->iblockId,
			"NAME" => $subjectForUser,
			"CODE" => $this->getCode($subjectForCode),
			"ACTIVE" => "N",
			"DATE_ACTIVE_FROM" => $this->dateForCode,
			"PROPERTY_VALUES" => [
				"PHONE" => $this->phone,
			]
		];

		if (!$newElementId = $element->Add($arElementFields)) {
			throw new Exception($element->LAST_ERROR);
		}

		return $newElementId;
	}

	public function getEmailFields($newElementId)
	{
		$emailFields = parent::getEmailFields($newElementId);
		$emailFields["PHONE"] = $this->phone;

		return $emailFields;
	}

}



