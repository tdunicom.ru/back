<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Сертификаты соответствия");
$APPLICATION->SetPageProperty("TITLE", 'Сертификаты');
$APPLICATION->SetTitle("Сертификаты");
?>

<div class="grid">
    <div class="grid__cell grid__cell--s--6 grid__cell--xs--12">
        <a href="/images/certificate.jpg" data-glightbox>
            <picture>
                <source srcset="/images/certificate.webp" type="image/webp"/>
                <source srcset="/images/certificate.jpg"/>
                <img src="/images/certificate.jpg" loading="lazy" decoding="async" alt="Сертификат соответствия" title="Сертификат соответствия" width="1240" height="1754"/>
            </picture>
        </a>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
