<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий фракция 5 - 20 мм цена м3 купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Гравий фракции 5 - 20 мм. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 5 - 20. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий 5 - 20 мм: цены от производителя, купить фракцию 5 - 20 с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий 5 - 20 мм");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/fract-graviy/gravii-5-20.jpg" alt="Размер: 5 - 20 мм" title="Размер: 5 - 20 мм" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия 5 - 20</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>

          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_availability-instock.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Применение гравия 5 - 20</h2>
        <h3>Строительство </h3>
        <p>Гравий 5-20 нужен в строительной сфере. Из него производят железобетонные конструкции, бетон и изделия из него, фундамент. </p>
        <h3>Дорожные работы </h3>
        <p>Используется в качестве слоя в балластной подушке. Также ним благоустраивают территории с целью воплощения идей ландшафтного дизайна. Идет он и на производство асфальтобетона, мостовых опор, оформление взлетно-посадочных полос.</p>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
