<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий крупный цена купить мытый округлый без пыли фракции от производителя доставка биг-беги санкт-петербург");
$APPLICATION->SetPageProperty("description", "Крупный гравий по цене от производителя. Купить с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 5 - 20. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Крупный гравий фракции: купить с доставкой в Санкт-Петербурге, цены от производителя");
$APPLICATION->SetTitle("Крупный гравий");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/krupnii-gravii-1.jpg" alt="Крупный гравий" title="Крупный гравий" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: > 10 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена крупного гравия</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Крупный гравий — сыпучая смесь, которую мы получаем в результате обработки крупного кварцевого песка.</p>
        <p>Очищенный кварцевый песок фракционируется на специальном оборудовании соответствующими ситами, в результате чего получается гравий.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">

        <h2>Сферы использования</h2>
        <p>К таковым относят зерна величиной от 20 мм и более.</p>
        <h3>Создание бетона</h3>
        <p>Крупный гравий фракционного состава 20-70 и 40-70 используется для изготовления бетонных изделий.</p>
        <p>Главные потребители — заводы по выпуску бетонных конструкций (неармированных и армированных). Важно учесть, что бетонное производство предполагает применение горной разновидности материала. Это объяснимо высокой степенью шероховатости зернышек, которая приводит к адгезии наполнителя с цементным раствором. </p>
        <p>Что касается водных видов крупного гравия, то его поверхность гладкая, поэтому адгезия с цементным раствором может не произойти как положено, что снизит прочностные характеристики готовых конструкций из бетона. Также, на этапе выбора материала под эти цели, оценивают его устойчивость к щелочной среде, которая свойственна цементным растворам.</p>
        <h3>Строительство</h3>
        <p>Как заполнитель применяется на этапе возведения дорог, находящихся вблизи населенных пунктов, при возведении сооружений производственного назначения.</p>
        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/krupnii-gravii-2.jpg" alt="Сферы использования крупного гравия" itemprop="contentUrl"></p>
        <p>Часто крупный гравий используют в строительстве дорог. Прежде чем провести отсыпку оснований автомобильных трасс, железнодорожных полотен, оценивают такие показатели: </p>
        <ul class="list-marked">
          <li>морозостойкость,</li>
          <li>прочность,</li>
          <li>устойчивость к истиранию и дроблению.</li>
        </ul>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
