<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий мелкий садовых дорожек дача цена купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Мелкий гравий для садовых дорожек. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Гравий для дорожек на даче - мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для садовых дорожек на даче: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Мелкий гравий");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/melkii-gravii-1.jpg" alt="Мелкий гравий" title="Мелкий гравий" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 2,5 - 5 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена мелкого гравия</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>На производстве компании “Юником” мелкий гравий мы получаем в ходе фракционирования и сушки крупного кварцевого песка.</p>
        <p>Мы&nbsp;закупаем намывной (промытый водой) кварцевый песок, и&nbsp;после сушки просеиваем его на&nbsp;специальном оборудовании. В&nbsp;ходе этих процессов получаем гравий различных фракций.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Сфера использования</h2>
        <p itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/melkii-gravii-2.jpg" alt="Использование мелкого гравия" title="Использование мелкого гравия" itemprop="contentUrl"/></p>
        <p>Гравий мелкой фракции необычайно востребован в дорожном строительстве и ландшафтном дизайне. Используется для:</p>
        <ul class="list-marked">
          <li>оформления подъездных дорог,</li>
          <li>пешеходных дорожек,</li>
          <li>садовых и парковых тропинок,</li>
          <li>аквариума.</li>
        </ul>
        <p>Из него создают брусчатку, используют в бетонных работах. </p>
        <p>Востребован он и в зодчестве. Больше подходит сырье горного происхождения. Поскольку у него неровная поверхность, сцепление с раствором получается оптимальным. Повышенную прочность приобретает и бетон, созданный с его добавлением.</p>
        <p>Другие популярные направления использования:</p>
        <ul class="list-marked">
          <li>Производство керамических изделий.</li>
          <li>Обустройство дренажей.</li>
          <li>Для декоративных целей: украшения экстерьера домов, дач.</li>
        </ul>
        <h3>Направления использования</h3>
        <h4>Декорирование ландшафтов</h4>
        <p>Оттеняет красоту садовых насаждений, нечувствителен к непогоде (сохраняет вид после града, дождя и других неблагоприятных погодных условий). Также облагораживает каменистую местность или скудную почву в условиях засушливого или чрезмерно влажного климата.</p>
        <h4>Особенности использования в строительстве</h4>
        <p>Мелкий гравий при малых дорожно-строительных работах. Потому, что гораздо дешевле тротуарной плитки и бетона. Поэтому он материал выбора при возведении тротуаров, а также заполнении дорог районного назначения (увеличивает стабильность дороги после дождей).</p>
        <h4>Использование в растениеводстве</h4>
        <p>Мелкий гравий нужен для улучшения «дыхания» почвы и впитывания воды. Выступает в роли мульчирующего материала. Охлаждает почву в знойные дни, разрыхляет.</p>
        <h4>Другие декоративные работы</h4>
        <p>Мелким гравием оформляют фонтаны и небольшие водоемы. Подбирая похожие оттенки, создают имитацию прудов на дачных участках.</p>
        <p>Материал используют в городском зодчестве. С его помощью создают поверхность площадок (детских, спортивных), прочих декоративных элементов.</p>

        <h2>Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
