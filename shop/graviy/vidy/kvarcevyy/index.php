<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий кварцевый цена купить мытый округлый без пыли фракции для фильтров от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Кварцевый гравий по цене от производителя. Купить с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, для фильтров, фракция 5 - 20. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый гравий: купить с доставкой в Санкт-Петербурге, цены от производителя");
$APPLICATION->SetTitle("Кварцевый гравий");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/kvarcevii-gravii-1.jpg" alt="Кварцевый гравий" title="Кварцевый гравий" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена кварцевого гравия</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <th>за 1м<sup>3</sup></th>
              </tr>
              <tr>
                <td>1000 руб.</td>
                <td>1200 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах. Стоимость мешка – 320 руб.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Кварцевый гравий — минерал получаемый на нашем производстве в процессе просеивания крупного кварцевого песка, в котором преобладает оксид кремния.</p>
        <p>В ходе производственной цепочки получаем состав с высокой степенью чистоты. Такового результата достигают после тщательной промывки сырья водой, а также грубого просеивания.</p>
        <p>Главные отличия кварцевого гравия:</p>
        <ul class="list-marked">
          <li>однородность состава,</li>
          <li>мономинеральность,</li>
          <li>невысокая пористость между зернами,</li>
          <li>устойчивость к воздействиям химического и механического характера,</li>
          <li>высокая грязеемкость.</li>
        </ul>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Фракции кварцевого гравия и сферы использования</h2>
        <h3>Кварцевый гравий для фильтров</h3>
        <p>Кварцевый гравий 2-5 мм идет в системы водоподготовки, а также его используют как поддерживающий слой, фильтрующий материал. Востребованность в этой отрасли объяснима оптимальным гранулометрическим составом и химической инертностью.</p>
        <p>Кварцевый гравий 2-4 мм применяют в системах водоподготовки муниципального и промышленного назначения: для освобождения воды от взвешенных частиц. Также ним заполняют мультимедийные фильтры. Сырье увеличивает скорость потока, а также распределение водной среды в поддерживающем слое. Высота слоя в этом случае — восемь миллиметров.</p>
        <p>Для водоочистительных целей используют дробленный материал. Дело в том, что, по сравнению с окатанным кварцевым гравием, у этого типа сырья меньшая насыпная плотность, а это значит, что скорость пропускания воды ускоряется, и показатель фильтрации улучшается за счет пористости материала.</p>
        <h3>В аквариумистике</h3>
        <p>Мельчайшие фракции (до 2 мм) используют как аквариумный грунт, чтобы выращивать растения, улучшая среду водных обитателей и эстетику оформления аквариумного дна.</p>
        <p>Материал бывает прозрачным или окрашенным в черный цвет.Удерживает и развивает корни растений, питает их. Критерии выбора:</p>
        <ul class="list-marked">
          <li>нейтральный по отношению к воде,</li>
          <li>устойчивый к углекислому газу,</li>
          <li>не увеличивает жесткость воды.</li>
        </ul>
        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/kvarcevii-gravii-2.jpg" alt="Кварцевый гравий в аквариумистике" title="Кварцевый гравий в аквариумистике" style="max-width: 640px" itemprop="contentUrl"/></p>
        <h3>Для фундаментных работ</h3>
        <p>В этой сфере материал используют самостоятельно и как компонент-наполнитель.</p>
        <h3>В бетонах</h3>
        <p>Из материала создают бетон и другие строительные растворы. Сырье выступает в роли «закрепителя», а также снижает себестоимость создания изделий.</p>
        <h3>В декоре</h3>
        <p>С помощью материала:</p>
        <ul class="list-marked">
         <li>оформляют дорожки,</li>
         <li>тропинки,</li>
         <li>цветники,</li>
         <li>клумбы,</li>
         <li>заполняют объектов садово-паркового дизайна,</li>
         <li>создают мини-пляжи,</li>
         <li>декоративные водоемы,</li>
         <li>пруды,</li>
         <li>другие зоны, где важно оригинальное декоративное оформление.</li>
        </ul>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
