<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "Гравий для отсыпки дренажных систем. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Гравий для дренажа - мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для отсыпки дренажных систем: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для отсыпки дренажных систем");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/graviy-dlya-otsypki-drenazhnykh-sistem-1.jpg" alt="Гравий для отсыпки дренажных систем" title="Гравий для отсыпки дренажных систем" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для отсыпки дренажных систем</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Гравий для отсыпки дренажных систем&nbsp;&mdash; это сыпучий материал, подобранный по&nbsp;фракции и&nbsp;объему под оптимальное обустройство участков разного назначения.</p>

        <h2>Виды применения</h2>
        <p>Для обустройства дренажа используют:</p>

        <ul class="list-marked list-marked--w-prev-par">
          <li>гравий: достаточно прочный, может быть колотым (полученным после механического дробления) и&nbsp;натуральным (полученным в&nbsp;естественном виде в&nbsp;природных условиях); </li>
          <li>гранитный щебень: с&nbsp;высокой степенью морозостойкости, прочности, но&nbsp;стоит выше и&nbsp;может иметь повышенный радиационный фон;</li>
          <li>доломитовый щебень: нулевая радиоактивность, невысокая стоимость, отсутствие вредоносных примесей, но&nbsp;не&nbsp;слишком прочный и&nbsp;долговечный, по&nbsp;сравнению с&nbsp;гравием.</li>
        </ul>
        <p><img src="/images/graviy/graviy-dlya-otsypki-drenazhnykh-sistem-2.jpg" alt="Дренаж" /></p>
        <h2>Как выбрать?</h2>
        <ul class="list-marked">
          <li>важна шероховатость линии разлома: повышает фильтрующую способность дренажа, поскольку такая поверхность притягивает грязевые частицы;</li>
          <li>учитывают показатели радиоактивности: он&nbsp;должен быть в&nbsp;пределах допустимого значения;</li>
          <li>величину зерна подбирают, ориентируясь на&nbsp;глубину дренажной канавы.</li>
        </ul>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th>Применение</th>
                <th>Фракции, мм</th>
              </tr>
              <tr>
                <td>Содержит весь отсев, смоется грунтовыми водами, потому для дренирования не&nbsp;подходит</td>
                <td>до&nbsp;5</td>
              </tr>
              <tr>
                <td>В&nbsp;строительстве, создании железобетонных изделий</td>
                <td>5-20</td>
              </tr>
              <tr>
                <td>Оптимальный вариант для дренирования, без пыли и&nbsp;отсевов</td>
                <td>20-40</td>
              </tr>
              <tr>
                <td>Подходит для создания дренажной системы не&nbsp;вручную, а&nbsp;с&nbsp;помощью техники</td>
                <td>40-90</td>
              </tr>
              <tr>
                <td>Для декоративных целей в&nbsp;ландшафтном, экстерьерном, интерьерном дизайне</td>
                <td>100-300</td>
              </tr>
            </table>
          </div>
        </div>
        <h2>Преимущества использования гравия для дренажных систем</h2>
        <ul class="list-marked">
          <li>Эффективность фильтрации за&nbsp;счет практически полного отсутствия примесей.</li>
          <li>Оптимальная шероховатость и&nbsp;характерная угловатая форма, повышающие функциональность дренажа.</li>
          <li>Долговечность: дренажная система прослужит от&nbsp;20&nbsp;и&nbsp;более лет.</li>
          <li>Высокая прочность для оптимального устройства дренажа.</li>
          <li>Отсутствие уплотнения и&nbsp;сильной трамбовки ускоряет отвод воды.</li>
        </ul>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
