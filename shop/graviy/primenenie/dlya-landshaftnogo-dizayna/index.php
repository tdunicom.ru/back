<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "Гравий для ландшафтного дизайна. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Гравий для ландшафта - мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для ландшафтного дизайна: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для ландшафтного дизайна");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/graviy-dlya-landshaftnogo-dizayna-1.jpg" alt="Гравий для ландшафтного дизайна" title="Гравий для ландшафтного дизайна" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для ландшафтного дизайна</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Гравий для ландшафтного дизайна&nbsp;&mdash; сыпучая смесь, полученная после обработки и&nbsp;фракционирования кварцевого песка, которую используют для декоративных целей.</p>

        <h2>Виды применения </h2>
        <p>Для дизайна ландшафтов используют:</p>
        <ul class="list-marked list-marked--w-prev-par">
          <li>гранитный щебень: имеет неровные грани, красноватый оттенок, подбирают разные фракции, чтобы внизу сделать отсыпку их&nbsp;крупных камешков, а&nbsp;сверху&nbsp;&mdash; из&nbsp;мелких;</li>
          <li>гравий: имеет характерный сероватый оттенок, лучше подбирать материал без острых граней;</li>
          <li>галька: щебень малых фракций и&nbsp;правильной формы, обычно округлой;</li>
          <li>известняковый, сланцевый: менее прочные, чем гравий, потому подходят для обустройства участков с&nbsp;малой нагрузкой.</li>
        </ul>
        <p><img src="/images/graviy/graviy-dlya-landshaftnogo-dizayna-2.jpg" alt="" /></p>
        <h2>Как выбрать?</h2>

        <p></p>
        <ul class="list-marked list-marked--w-prev-par">
          <li>цвет: влияет на&nbsp;стоимость и&nbsp;дизайнерскую ценность гравия, если камень окрашен искусственно, то&nbsp;важно подбирать надежного поставщика, использующего безопасные красители;</li>
          <li>текстура: в&nbsp;горных породах присутствуют разные вкрапления, формирующие замысловатые рисунки, потому такие камни идут на&nbsp;создание композиций;</li>
          <li>фактура: гладкие окатанные камешки подойдут для оформления пруда, но&nbsp;не&nbsp;вариант для засыпки дорожек, поскольку по&nbsp;ним может скользить нога, не&nbsp;подойдут и&nbsp;острые;</li>
          <li>вид поверхности: на&nbsp;гладкой рисунок будет ярче, на&nbsp;неровной&nbsp;&mdash; будет эффект игры света;</li>
          <li>размер: используют зерна диаметром от&nbsp;5&nbsp;до&nbsp;120&nbsp;мм, подбирая фракцию по&nbsp;назначению, самые мелкие и&nbsp;крупные не&nbsp;используют;</li>
          <li>форма: оптимальной считается кубическая, но&nbsp;подойдет треугольная, конусовидная; плоские используют для оформления водоемов, крупные камешки&nbsp;&mdash; для украшения японского сада. </li>
        </ul>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th>Применение</th>
                <th>Фракции, мм</th>
              </tr>
              <tr>
                <td rowspan="2">Для создания декоративных покрытий и&nbsp;отсыпок</td>
                <td>10-20</td>
              </tr>
              <tr>
                <td>20-40</td>
              </tr>
              <tr>
                <td rowspan="3">Для создания габионов, альпийских горок и&nbsp;всевозможных малых архитектурных форм</td>
                <td>40-70</td>
              </tr>
              <tr>
                <td>40-120</td>
              </tr>
              <tr>
                <td>80-120</td>
              </tr>
            </table>
          </div>
        </div>
        <p><img src="/images/graviy/graviy-dlya-landshaftnogo-dizayna-3.jpg" alt="" /></p>
        <h2>Преимущества использования гравия для ландшафтного дизайна</h2>
        <ul class="list-marked">
          <li>Экологичность: не&nbsp;нарушает дыхание грунта и&nbsp;впитывание влаги, идеален для мульчирования, улучшает развитие корневой системы, хороший разрыхлитель почвы.</li>
          <li>Идеально сочетается с&nbsp;другими материалами: металлом, искусственным камнем, древесиной.</li>
          <li>Декоративный внешний вид: заменит тротуарную плитку, газон, красиво оттенит растения, не&nbsp;теряет презентабельность вида в&nbsp;любое время года.</li>
          <li>Простота ухода: гравийный газон не&nbsp;надо поливать, стричь.</li>
          <li>Хорошая ремонтопригодность: просто собирать, перемещать, добавлять фракции. </li>
        </ul>
        <h2>Расчет необходимого количества</h2>
        <p>Для слоя до&nbsp;3&nbsp;см понадобится 50&nbsp;кг для покрытия 1&nbsp;кв.&nbsp;м&nbsp;поверхности.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Как купить?</h2>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
