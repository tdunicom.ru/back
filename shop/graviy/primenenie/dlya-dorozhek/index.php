<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий мелкий садовых дорожек дача цена купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Мелкий гравий для садовых дорожек. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Гравий для дорожек на даче - мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для садовых дорожек на даче: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для дорожек");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/gravii-dlya-dorojek-1.jpg" alt="Гравий для дорожек" title="Гравий для дорожек" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для дорожек</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Почему гравий используется для создания дорожек?</h2>
        <p>По сравнению с тротуарным камнем, плиткой и другими покрытиями, использование гравия экономически выгодно:</p>
        <ul class="list-marked">
          <li>материал недорогой</li>
          <li>укладку можно сделать собственными руками, не нанимая рабочих.</li>
        </ul>
        <p>Гравий для дорожек устойчив к нагрузкам и неблагоприятным погодным воздействиям. </p>
        <p>Низкая степень изнашиваемости дает длительное сохранение функциональности. За такими тропинками не нужно тщательно ухаживать. </p>
        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/gravii-dlya-dorojek-2.jpg" alt="Гравийная дорожка" title="Гравийная дорожка" itemprop="contentUrl"></p>
        <h2>Какой вид природных материалов и какие фракции лучше использовать для дорожек?</h2>
        <p>Выделяют три вида материала, которые подходят для оформления:</p>
        <ul class="list-numbered">
          <li><strong>Гравий</strong> — материал круглой формы, приятный на ощупь. Может быть использован мелкий гравий для дорожек. Стоит значительно дешевле щебня. Лучше держит объем, дольше остается чистым, быстро высыхает. </li>
          <li><strong>Галечник</strong> — камушки мельчайшей фракции, округлой формы. Величина фракций — в диапазоне от 10 мм до 1 см. Имеет свойство “расплываться” под ногами.</li>
          <li><strong>Гранитный щебень.</strong> Получают после дробления в промышленных условиях. Камни выглядят грубо. Обладают острой формой, что может стать причиной травм. Фракция — в пределах 1-70 мм. </li>
        </ul>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Расчет количества</h2>
        <p>Расход будет зависеть от типа материала, толщины подсыпки, выбранной фракции и других особенностей. Расчет ведется следующим образом:</p>
        <div class="block-wrap  ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th>Толщина слоя, см</th>
                <th>Расход на 1 м<sup>2</sup>, кг</th>
              </tr>
              <tr>
                <td>2-3</td>
                <td>35-50</td>
              </tr>
              <tr>
                <td>3-5</td>
                <td>60-80</td>
              </tr>
              <tr>
                <td>5-7</td>
                <td>90-115</td>
              </tr>
            </table>
          </div>
        </div>
        <h2>Как сделать дорожки: этапы</h2>

        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/gravii-dlya-dorojek-3.png" alt="Структура гравийной дорожки" title="Структура гравийной дорожки" itemprop="contentUrl"></p>
        <h3>Разметка</h3>
        <p>Для разметки извилистой — подойдет садовый шланг, для прямой — колышки с бечевкой. Ширину лучше делать в пределах 0,5-1,2 метров. </p>
        <h3>Создание траншеи</h3>
        <p>Размеченный участок освободите от дерна. Освободите его от сорняков, корневищ, мусора. Глубина — 15-20 см. Разровняйте и утрамбуйте дно. </p>
        <h3>Оформление бордюрами</h3>
        <p>Нужны чтобы гравий для дорожек на даче не «растекся» по участку. Высота — 3-7 см. Установите их до этапа создания канавы, подсыпать гравий будет проще. </p>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
