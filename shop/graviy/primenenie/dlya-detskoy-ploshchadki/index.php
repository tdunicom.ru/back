<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий детская площадка цена купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Гравий для детской площадки. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для детской площадки: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для детской площадки");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/gravii-dlya-detskoi-ploschadki-1.jpg" alt="Гравий для детской площадки" title="Гравий для детской площадки" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для детской площадки</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Какими свойствами должен обладать гравий?</h2>
        <p>Гравий для детской площадки, полученный после отсева гравийно-песчаной смеси, не должен: </p>
        <ul class="list-marked">
          <li>содержать загрязнений,</li>
          <li>острых граней,</li>
          <li>посторонних травмоопасных предметов,</li>
          <li>быть опасным класс радиоактивности.</li>
        </ul>
        <h2>Особенности покрытия, созданного из гравия</h2>
        <ul class="list-marked">
          <li>надежное и прочное,</li>
          <li>не «плывет» под ногами, приводя к падению,</li>
          <li>не задерживает осадки,</li>
          <li>размер частичек ровно такой, чтобы не возникало препятствий в их очищении.</li>
        </ul>
        <h2>Какой тип материалов и какие фракции лучше использовать для детской площадки?</h2>
        <p>В качестве сыпучих покрытий для детских площадок используют:</p>
        <ul class="list-marked">
          <li>окатанный гравий (5-20 мм),</li>
          <li>песок (0,2-2 мм).</li>
        </ul>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Расчет количества</h2>
        <p>Рассчитать количество гравия для детской площадки можно по формуле:</p>
        <p>Кол-во тонн = ширина &times; длина &times; высота &times; 1,45*</p>
        <p>* коэффициент плотности гравия</p>
        <h2>Как сделать площадку: этапы</h2>
        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/gravii-dlya-detskoi-ploschadki-2.jpg" alt="Создание детской площадки с использованием гравия" title="Создание детской площадки с использованием гравия" itemprop="contentUrl"/></p>
        <h3>Подготовительный этап</h3>
        <p>Определитесь с местом: хорошо освещенное, без попадания прямых солнечных лучей. Продумайте планировку площадки. Разметьте площадку, используя вбитые колышки и натянутую между ними веревку.</p>
        <p>Проведите расчет материалов и закупите их. </p>
        <h3>Подготовка участка</h3>
        <p>Выровняйте территорию. Удалите корни вместе с корневой системой.</p>
        <h4>Подготовьте почву</h4>
        <p>Снимите с поверхности слой грунта (толщина до 10-15 см).</p>
        <h4>Повторно удалите сорняки</h4>
        <p>Увлажните землю в котловане, оставьте его на семь-десять дней. За это время вырастут те сорняки, корневая система которых осталась в земле. Удалите их.</p>
        <h4>Создание дренажа</h4>
        <p>Перекопайте грунт, внесите разрыхлители. В качестве них подойдут: крупнозернистый песок. Излишек влаги будет отводится от нижнего слоя, не повреждая целостность площадки.</p>
        <h3>Непосредственно этап возведения площадки</h3>
        <h4>Защита от сорняков</h4>
        <p>Утрамбуйте почву так, чтобы ровно застелить ее геотекстилем. Он будет препятствовать росту сорняков, защитит слой гравия для детской площадки от проседания.</p>
        <h4>Сделайте насыпь</h4>
        <p>Засыпьте гравий для детской площадки: на толщину 10 сантиметров. Затем на такую же толщину насыпьте гравий. Каждый слой плотно утрамбуйте. Важно: крупные камни при создании площадки использовать нельзя.</p>
        <h4>Оформите границы</h4>
        <p>Завершите оформление небольшими бордюрчиками. Они будут препятствовать размыванию насыпи дождем, а также проникновению корней сорняков.</p>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
