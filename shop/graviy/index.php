<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий спб цена дешево сколько стоит купить мытый округлый без пыли фракции от производителя доставка биг-беги 1 м3 куб тонна заказать санкт-петербург");
$APPLICATION->SetPageProperty("description", "Купить гравий в спб. Дешево, от производителя, с доставкой по Санкт-Петербургу и области. Цена за 1 м3 (куб) и тонну. Мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Купить гравий в спб с доставкой, гравий дешево: цена за 1 м3 (куб) и тонну от производителя");
//$APPLICATION->SetTitle("Гравий");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "top",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<div class="container">
	<div class="content">
		 <? /* <div class="banner-home banner-home_graviy">
      <div class="banner-home__inner">
        <div class="banner-home__main-text">
          <div class="banner-home__action">Акция</div><span>Гравий 2,5-5 мм</span><span> за 1600 руб. за тонну </span>
          <div class="banner-home__mark">* При оплате по безналичному расчету с НДС.</div>
        </div>
        <div class="banner-home__countdown">
          <div class="banner-home__countdown-title">До конца акции осталось:</div>
          <div class="banner-home-countdown"></div>
        </div>
      </div>
    </div> */ ?>
		<div class="block-wrap">
			<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
				<p>
					Гравий&nbsp;— нерудный материал, который мы&nbsp;получаем в&nbsp;ходе просеивания кварцевого песка.
				</p>
				<p itemscope="" itemtype="http://schema.org/ImageObject">
					<img src="/images/gravii_5-20mm.jpg" alt="Гравий" title="Гравий" itemprop="contentUrl">
				</p>
    <div class="table-wrap">
				<table class="table-price">
				<tbody>
				<tr>
					<th>
						Фракция
					</th>
					<th>
						Цена
					</th>
				</tr>
				<tr>
					<td>
					</td>
					<td class="price-table__sub-th">
						За тонну
					</td>
				</tr>
				<tr>
					<td>
						5–20
					</td>
					<td>
						1000 руб.
					</td>
				</tr>
				</tbody>
				</table>
    </div>
				<h2>Основные параметры:</h2>
				<ul class="nice">
					<li>Размер зерна: 2.5&nbsp;— 20.0</li>
					<li>Форма зерен: округлая</li>
					<li>Влажность: сухой (не&nbsp;более 0,5%)</li>
				</ul>
				<h2>Фасовка</h2>
				<p>
					Предлагаем гравий в&nbsp;биг-бегах по&nbsp;1&nbsp;тонне.
				</p>
				<div class="block-wrap">
					<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_m-width8 block-wrap__item_xs-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="В биг-бегах" src="/images/big-bag-1-ton.jpg" class="figure-mark__image" title="В биг-бегах" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
						<div class="figure-mark__title" itemprop="name">
							В биг-бегах
						</div>
						<div class="figure-mark__desc" itemprop="description">
							по 1 тонне
						</div>
 </figcaption> </figure>
					</div>
				</div>
				<h2>Доставка</h2>
				<ul class="list-marked">
					<li>
					Гравий с&nbsp;доставкой по&nbsp;Северо-Западу, Санкт-Петербургу и&nbsp;Ленинградской области.<br>
					Для того чтобы купить гравий в&nbsp;Спб с&nbsp;доставкой уточните у&nbsp;менеджеров цену доставки.</li>
					<li>При самовывозе&nbsp;— поможем погрузить на&nbsp;ваш транспорт погрузчиком.</li>
				</ul>
				<p>
					Хранить гравий можно в&nbsp;помещениях или на&nbsp;открытых площадках, что не&nbsp;влияет на&nbsp;его технические характеристики.
				</p>
				<h2>Производство</h2>
			</div>
		</div>
		<div class="block-wrap">
			<div class="block-wrap__item block-wrap__item_xl-width8">
				<p>
					Рассев происходит на&nbsp;специальном грохоте, на&nbsp;ситах с&nbsp;определенной ячейкой (соответствует фракции) и&nbsp;фасуется в&nbsp;биг-беги.
				</p>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4" style="position: relative" itemscope="" itemtype="http://schema.org/ImageObject">
 <img src="/upload/medialibrary/f26/f26844cded4bd0b0fe1a88ce68c468e2.jpg" alt="" style="width: 100%; position: absolute; top: 0; left: 0" itemprop="contentUrl">
			</div>
		</div>
		<div class="block-wrap">
			<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
				<h2>Характеристики</h2>
				<table class="table">
				<tbody>
				<tr>
					<th>
						Свойство
					</th>
					<th>
						Значение
					</th>
				</tr>
				<tr>
					<td>
						Насыпная плотность
					</td>
					<td>
						от&nbsp;1,43 до&nbsp;1,61 тонн на&nbsp;кубометр
					</td>
				</tr>
				<tr>
					<td>
						Средняя плотность
					</td>
					<td>
						2,6-2,7 тонн на&nbsp;один кубометр
					</td>
				</tr>
				<tr>
					<td>
						Прочность
					</td>
					<td>
						ДР8, 12, 16, 24
					</td>
				</tr>
				<tr>
					<td>
						Мера истираемости
					</td>
					<td>
						И&nbsp;I-IV
					</td>
				</tr>
				</tbody>
				</table>
				<h2>Сферы использования</h2>
				<p>
					<strong>Декоративный материал</strong><br>
					 Для оформления мини-пляжей (нет острых граней&nbsp;— можно ходить по&nbsp;поверхности босиком), детских площадок, пешеходных гравийных дорожек, зон отдыха на&nbsp;дачных участках, фонтанов, украшения аквариума.
				</p>
				<p>
					<strong>Для растворов и&nbsp;бетонов</strong><br>
					 Приготовление бетонов, где гравий применяют в&nbsp;качестве заполнителя
				</p>
				<p>
					<strong>Строительные работы</strong><br>
					 Монолитные фундаменты, железобетонные конструкции, гидротехнических конструкции, для организации дренажа, обустройства наливных полов
				</p>
				<p>
					<strong>Для отсыпки дренажных систем</strong><br>
					 Дешевле обычного гравия и&nbsp;дырочек между камнями больше&nbsp;— лучше дренируется.
				</p>
				<h2>Чем гравий лучше щебня?</h2>
				<ol class="list-numbered">
					<li>Цена за&nbsp;тонну гравия заметно меньше, купить его выгоднее</li>
					<li>Имеет более правильную форму, минимум зерен игловатой и&nbsp;пластинчатой формы. Для смесей это свойство уменьшает межзерновую пустотность, снижает расход связующего компонента и&nbsp;стоимость растворов. Также сокращает износ трубопроводов и&nbsp;насосов для подачи растворов.</li>
					<li>Имеет более гладкую и&nbsp;округлую поверхность. Хорошо использовать в&nbsp;качестве декоративного материала.</li>
					<li>Выше показатели экологической чистоты</li>
				</ol>
				<h2>Цена гравия</h2>
				<p>
					Продаем гравий от&nbsp;производителя, без посредников.
				</p>
				<p>
					При больших объемах&nbsp;— предоставим скидку!
				</p>
			</div>
		</div>
		<h2>Как купить гравий в Спб?</h2>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
