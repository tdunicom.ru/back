<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить машину гравия. Цены от производителя, с доставкой по Санкт-Петербургу и области. Сколько стоит камаз гравия. Мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Машина (камаз) гравия: цена от производителя, сколько стоит купить машину гравия с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Машина гравия");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/mashina-graviya.jpg" alt="Машина гравия" title="Машина гравия" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Виды машин и их объемы</h2>
        <p>Используют самосвалы разной грузоподъемности.</p>
        <p>Вместительность машин:</p>
        <ul class="list-marked">
          <li>в ЗИЛ входит 5 м3 гравия</li>
          <li>в КамАЗ – 10 м3</li>
          <li>в Тонар – 20 м3.</li>
        </ul>
        <h2>Цена для различных машин</h2>
        <p>Цена машины гравия зависит от:</p>
        <ul class="list-marked">
          <li>фракционного состава смеси</li>
          <li>вместительности транспортного средства</li>
        </ul>
        <h3>Рассчитаем цену машин гравия различных моделей</h3>
        <p>Объем, который вмещает машина, умножают на стоимость куба.</p>
        <p>1) Сколько стоит КамАЗ гравия?</p>
        <p>Поскольку стоимость одной тонны составляет 300 руб., а один м<sup>3</sup> гравия (фракционного состава от 5 до 20 мм) – это 1,36 тонн, в КамАЗ входит 10 м<sup>3</sup>, то получается, что цена КамАЗА гравия составит 9000 рублей.</p>
        <p>2) купить машину гравия марки ЗИЛ можно по стоимости 3400 рублей</p>
        <p>3) машина марки Тонар будет стоить – 13 600 рублей.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/big-bag-1-ton.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
