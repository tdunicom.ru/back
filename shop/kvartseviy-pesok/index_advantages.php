<div class="container home-section home-section_home-advantages">
  <div class="content">
    <h2>Почему нам доверяют?</h2>
    <ul class="list-advantages block-wrap block-wrap_wrap">
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Выгодная цена</div>
        <p>Нет наценки посредника за&nbsp;счет собственного производства и&nbsp;прямых поставок сырья с&nbsp;заводов производителей.</p>
      </li>
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Ассортимент</div>
        <p>Постоянное наличие на&nbsp;складе основных товарных позиций.</p>
      </li>
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Высокие стандарты производства</div>
        <p>Оборудование ведущего производителя с&nbsp;компьютизированным контролем технологической цепочки.</p>
      </li>
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Контроль качества</div>
        <p>Оперативный контроль сырья и&nbsp;готовой продукции за&nbsp;счет собственной лаборатории.</p>
      </li>
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Гибкая система оплаты</div>
        <p>Индивидуальные условия для каждого клиента, наличный и&nbsp;безналичный расчет</p>
      </li>
      <li class="list-advantages__item block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
        <div class="list-advantages__header">Удобство расположения склада (всего 15 минут от КАД).<br>Отгрузка в режиме 24/7</div>
      </li>
    </ul>
  </div>
</div>
