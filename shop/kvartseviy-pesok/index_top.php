<div class="top-form container">
      <div class="block-wrap block-wrap_wrap content">
        <div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width12 block-wrap__item_s-width6 top-form__right top-form__left">
          <h1 class="top-form__title">Сухой кварцевый песок с доставкой по Северо-Западу, Санкт-Петербургу и Ленинградской области</h1>
          <ul>
            <li>Вы можете купить кварцевый песок по низкой стоимости, от производителя, <strong>без наценок</strong> и посредников!</li>
            <li><strong>Гарантируем</strong> соблюдение сроков, прописанных в договоре</li>
            <li>В наличии <strong>основные фракции</strong> песка. <strong>Любые фракции</strong> произведем под заказ.</li>
            <li>Кварцевый песок соответствует <strong>ГОСТу</strong>, есть <strong>сертификат соответствия</strong></li>
          </ul>
        </div>
        <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width12 block-wrap__item_s-width6 top-form__right top-form__right">
          <div class="top-form-form">
            <div class="top-form-form__title">Заполните форму и получите расчет в течение 15 минут!</div>

            <form class="top-form-form__form validated-form form_calculation_pesok" method="post">

					<div class="form-input">
						<input class="top-form-form__input validated required" type="text" placeholder="Имя" name="name">
						<div class="validation-msg">Обязательное поле</div>
					</div>

					<div class="form-input">
					  <input class="top-form-form__input validated required" type="email" placeholder="E-mail" name="email">
					  <div class="validation-msg">Обязательное поле</div>
					</div>

					<div class="form-input">
					  <input class="top-form-form__input validated required" type="tel" placeholder="Телефон" name="phone">
					  <div class="validation-msg">Обязательное поле</div>
					</div>

					<div class="form-input">
					  <textarea class="top-form-form__input validated required" placeholder="Объем, фракция, фасовка, ГОСТ (если знаете)" name="comment"></textarea>
					  <div class="validation-msg">Обязательное поле</div>
					</div>

              <input class="top-form-form__submit btn" type="submit" value="Получить расчёт">

            </form>

          </div>
        </div>
      </div>
  </div>
