<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Песок для сухих строительных смесей оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Песок для сухих строительных смесей в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Песок для сухих строительных смесей Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Песок для сухих строительных смесей");
?><div class="container">
  <div class="content">
    <ul class="list-anchors">
      <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
      <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
      <li class="list-anchors__item"><a href="#beton-tsement">Для бетона и цемента</a></li>
      <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
      <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
    </ul>
  </div>
</div>
 <section class="container page-section">
<div class="content">
  <div class="block-wrap block-wrap_wrap">
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Песок для сухих строительных смесей" src="/images/primenenie-peska/pesok-dlya-sukhikh-stroitelnykh-smesey.jpg" class="title-image__img" title="Песок для сухих строительных смесей" itemprop="contentUrl"> </figure>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
      <ul class="class list-marked list-marked_half list-marked_xl-col2 list-marked_s-col1">
      	<li>высокое содержание <strong>кварца - не менее 70%</strong></li>
      	<li><strong>сухой</strong> - влажность не более 0,5%</li>
      	<li>используем только <b>намывной песок</b><br>
       </li>
      	<li>контроль на всех этапах производства <b>собственной лабораторией</b><br>
       </li>
      	<li>чистота: мытый, с полным <strong>отсутствием посторонних примесей</strong></li>
      </ul>

    </div>
  </div>
</div>
 </section> <section class="container page-section">
<div class="content">
  <h2 id="tseny">Цены на песок для сухих строительных смесей</h2>
  <div class="block-wrap block-wrap_wrap ">
    <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
      <table class="table-price">
      <tbody>
      <tr>
        <th>
        </th>
        <th>
           Навалом
        </th>
        <th colspan="3">
           Биг-бэги (за штуку, 1 тонна)
        </th>
        <th>
           Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
        </th>
      </tr>
      <tr>
        <td>
        </td>
        <td>
        </td>
        <td class="price-table__sub-th">
           до 10 тонн
        </td>
        <td class="price-table__sub-th">
           от 10 до 20&nbsp;тонн
        </td>
        <td class="price-table__sub-th">
           больше 20&nbsp;тонн
        </td>
        <td>
        </td>
      </tr>
      <tr>
        <td>
           0-0,63 <br>
           ГОСТ 2138
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      <tr>
        <td>
           0,315-0,63 <br>
           ГОСТ 2138
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      <tr>
        <td>
           0-0,315 <br>
           ГОСТ 2138
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      <tr>
        <td>
           0-0,63 <br>
           ГОСТ 8736
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      <tr>
        <td>
           0,63-2,5 <br>
           ГОСТ 8736
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      <tr>
        <td>
           0-2,5 <br>
           ГОСТ 8736
        </td>
				<td colspan="5">
					 договорная
				</td>
      </tr>
      </tbody>
      </table>
    </div>
 <a href="/tseny/">Все цены</a>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width5 block-wrap__item_s-width6">
       <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
  )
);?>
    </div>
  </div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
  )
);?> <section class="container page-section">
<div class="content">
  <h2 id="primery">Примеры фракций</h2>
  <div class="flexslider-container">
    <div class="flexslider">
      <ul class="slides">
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="0,63 - 1,25 мм" src="/images/fract-pesok/pesok-08-125-2138-91.gif" class="figure-mark__image" title="0,63 - 1,25 мм" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                 0,63 - 1,25 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                 ГОСТ 8736
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="1,25–2,5 мм" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" class="figure-mark__image" title="1,25–2,5 мм" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                 1,25–2,5 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                 ГОСТ 8736
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="" src="/images/fract-pesok/pesok-03-06-2138-91.gif" class="figure-mark__image" title="" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                 0-0,63 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                 ГОСТ 2138
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="" src="/images/fract-pesok/pesok-03-063-2138-91.gif" class="figure-mark__image" title="" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                 0,315-0,63 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                 ГОСТ 2138
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="" src="/images/fract-pesok/pesok-01-03-2138-91.gif" class="figure-mark__image" title="" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                 0-0,315 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                 ГОСТ 2138
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject">
            <img alt="" src="/images/fract-pesok/pesok-0-063.gif" class="figure-mark__image" title="" itemprop="contentUrl">
            <figcaption class="figure-mark__caption" itemprop="caption">
              <div class="figure-mark__title" itemprop="name">
                0-0,315 мм
              </div>
              <div class="figure-mark__desc" itemprop="description">
                ГОСТ 2138
              </div>
            </figcaption>
          </figure>
        </li>
      </ul>
    </div>
  </div>
</div>
 </section> <section class="container page-section">
   <div class="content">
<h2 id="beton-tsement">Кварцевый песок для смесей</h2>
 <? /*  <h3>Какой для бетона необходим песок?</h3>
  <p>
     Бетон — смесь, состоящая из песка, цемента, щебня и воды. Иногда щебень заменяют гравием, а вот песок невозможно заменить ничем. Так какой же песок необходим для получения качественного бетона?
  </p>
  <p>
     Бетон с одинаковым успехом готовят как из природного, так и из искусственного песка. Материал первого вида — результат естественного распада каменных пород, а второй получают после предварительного измельчения горных частиц до получения нужной величины зерна.
  </p>
  <p>
     Наиболее распространен кварцевый песок для бетона. Причем, оптимальным вариантом считается, если присутствует кварцевый песок и в цементе. Дело в том, что этот материал считается наиболее ценным для строительства, так как прочие виды не настолько устойчивы к химическим воздействиям, а также отличаются гораздо меньшей прочностью.
  </p>
  <div class="block-wrap block-wrap_wrap ">
    <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Цемент с кварцевым песком" src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-betona-i-tsementa/cement-s-kvarcevim-peskom.png" title="Цемент с кварцевым песком" itemprop="contentUrl"> <figcaption itemprop="caption">
      Цемент с кварцевым песком </figcaption> </figure>
    </div>
  </div>
  <p>
     Наиболее распространен кварцевый песок для бетона. Причем, оптимальным вариантом считается, если присутствует кварцевый песок и в цементе. Дело в том, что этот материал считается наиболее ценным для строительства, так как прочие виды не настолько устойчивы к химическим воздействиям, а также отличаются гораздо меньшей прочностью.
  </p>
  <h3>Требования к песку</h3>
  <p>
     Для цемента и бетона песок подбирается в строгом соответствии с требованиями, <a href="http://www.kvarc-pesok.ru/informatsiya/gost-na-kvartsevyy-pesok/">регламентирующимися ГОСТом</a>.
  </p>
  <p>
     В первую очередь материал отбирают по зерновому составу, учитывая наличие примесей. Чтобы определить зерновой состав, производят просеивание. В результате определяют качество материала. Допускается, чтобы в нем было 85% зерен размером не менее 10 миллиметров, 10% — меньше 0,14 миллиметров и 5% размеров до 5 миллиметров.
  </p>
  <p>
     Что касается крупности зернового состава, то для этого введена некая величина, именующаяся модулем крупности. Так, этот показатель распределяют в следующем порядке:
  </p>
  <ul class="list-marked">
    <li>очень мелкий — 1,5-1,0;</li>
    <li>мелкий — 2,0-1,5;</li>
    <li>средний — 2,5-2,0;</li>
    <li>крупный — 3,5-2,5.</li>
  </ul>
  <p>
     Чтобы определить, в каком количестве нужно использовать кварцевый песок для бетона и какое процентное соотношение должен занимать кварцевый песок в цементе, обязательно учитывают модуль крупности (Мкр). Чтобы получить максимально качественный раствор, потребуется остановить выбор на крупном и среднем песке (модуль крупности должен быть в пределах 1,5-3,5, не более). Если предпочесть большую крупность, то это приведет к тому, что кварцевый песок в цементе будет перерасходован.
  </p>
  <div class="block-wrap block-wrap_wrap ">
    <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Фракции кварцевого песка" src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-betona-i-tsementa/frakcii-kvarcevogo-peska.jpg" title="Фракции кварцевого песка" itemprop="contentUrl"> <figcaption itemprop="caption">
      Фракции кварцевого песка </figcaption> </figure>
    </div>
  </div>
  <h3>Содержание примесей</h3>
  <p>
     Прежде чем выбрать песок для изготовления бетона, важно определить процентное соотношение примесей в его составе. Дело в том, что они приводят к формированию на поверхности материала пленки, препятствующей сцеплению песчаных зернышек с бетонным заполнителем. Поэтому количество примесей в природном песке должно быть в пределах 3%, а в искусственном их может быть не больше 5%.
  </p>
  <h3>Особенности использования кварца для приготовления бетона</h3>
  <p>
     Кварцевый песок нужен для создания сухих бетонов, а также бетонных блоков. Наличие в них кварца придает им пастельные оттенки.
  </p>
  <p>
     Почему же этот материал так популярен? Все дело в остроугольной форме его зернышек, за счет чего получается гораздо более качественная адгезия всех компонентов. По этой причине кварцевый песок для бетона применяют для создания высококачественных строительных смесей и конструкций.
  </p>
  <p>
     Также в состав бетона нередко добавляют кварцевую муку, которая обеспечивает кислотоупорные свойства, а также существенно повышает его плотность. Ведь кварц относится к инертным минералам, устойчивым к воздействию щелочей и кислот. Бетон, полученный с помощью добавления этого компонента, применяют как защитный элемент для металла и железобетона. Кварцевой муке также свойственен пониженный коэффициент поглощения влаги, а также высокая стойкость к износу. Благодаря этому бетонные конструкции, полученные с ее использованием, приобретают оптимальные прочностные свойства: во время эксплуатации их верхние слои гораздо меньше подвергаются износу.
  </p>*/ ?>
<h3>Преимущества</h3>
<ul class="list-marked">
  <li>составляем композиции из песка разных фракций,</li>
  <li>10 лет опыта производства,</li>
  <li>современное оборудование по рассеву,</li>
  <li>сушильные печи высокой производительности,</li>
  <li>система охлаждения,</li>
  <li>работаем с песками разных месторождений,</li>
  <li>доставляем железной дорогой в регионы,</li>
  <li>содержание влаги не более 0,5%.</li>
</ul>
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Общий вид песка для сухих строительных смесей" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-suhih-stroitelnyih-smesey/obshchij-vid-peska-dlya-suhih-stroitelnyh-smesej.jpg" title="Общий вид песка для сухих строительных смесей" itemprop="contentUrl"> <figcaption itemprop="caption">
Общий вид песка для сухих строительных смесей </figcaption> </figure>
<p>
   Песок для сухих строительных смесей – это наполнитель, который входит в состав, где уже присутствует связующее (гипс или цемент) и химические добавки, определяющие специфические свойства смеси.
</p>
<h3>Применение</h3>
 Кварцевый песок идет на изготовление:
<ul class="list-marked">
  <li>грунтовки: обеспечивает наиболее прочное сцепление с материалом, исключает осыпание и нарушение целостности поверхности; <br>
 <img alt="Кварцевая грунтовка" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-suhih-stroitelnyih-smesey/kvarcevaya-gruntovka.jpg" title="Кварцевая грунтовка"> </li>
  <li>тонких штукатурок: получают хорошие адгезивные свойства, придают однородность, прочность основанию, быстро высыхает и устойчивы к атмосферным воздействиям; <br>
 <img alt="Пример кварцевой штукатурки" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-suhih-stroitelnyih-smesey/primer-kvarcevoj-shtukaturki.jpg" title="Пример кварцевой штукатурки"> </li>
  <li>плиточных клеев: приобретает прочность и надежность, быстрое схватывание; <br>
 <img alt="Плиточный клей" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-suhih-stroitelnyih-smesey/plitochnyj-klej.jpg" title="Плиточный клей" style="max-width: 427px"> </li>
  <li>шпаклевок: упрочняет свойства поверхности, получая крупнозернистую структуру, позволяет заделать существенные щели, трещины и неровности; <br>
 <img alt="Кварцевая шпаклевка" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-suhih-stroitelnyih-smesey/kvarcevaya-shpaklevka.jpg" title="Кварцевая шпаклевка" style="max-width: 427px"> </li>
  <li>затирок: получают цвет, который не тускнеет со временем, а также широкие возможности для получения оттенков с любыми свойствами (прозрачностью, сверкающими частичками), придает раствору эластичность и плотность, свойство не терять привлекательный внешний вид десятилетиями.</li>
</ul>
<h3>Как выбрать?</h3>
<table class="table-price">
<tbody>
<tr>
  <th>
     Применение
  </th>
  <th>
     Фракции, мм
  </th>
</tr>
<tr>
  <td>
     Цементно-песочные, кладочные смеси
  </td>
  <td>
     0,63-2,5
  </td>
</tr>
<tr>
  <td>
     Штукатурки
  </td>
  <td>
     0-0,63, 0,63-2,5
  </td>
</tr>
<tr>
  <td>
     Текстурная штукатурка «Короед»
  </td>
  <td>
     0-0,63
  </td>
</tr>
<tr>
  <td>
     Ровнители
  </td>
  <td>
     0-0,63, 0,63-2,5
  </td>
</tr>
<tr>
  <td>
     Клеи, смеси для каминов и печей
  </td>
  <td>
     0-0,63
  </td>
</tr>
</tbody>
</table>
<h3>Преимущества кварцевого песка для использования в сухих строительных смесях</h3>
<ul class="list-marked">
  <li>Считается наиболее ценным для строительства, так как прочие виды не настолько устойчивы к химическим воздействиям, а также отличаются гораздо меньшей прочностью.</li>
  <li>Остроугольная форма зерен способствует качественной адгезии всех компонентов.</li>
  <li>Относится к инертным минералам, устойчивым к воздействию щелочей и кислот.</li>
  <li>Низкий коэффициент поглощения влаги (хорошая водостойкость), высокая стойкость к износу.</li>
  <li>Грунтовка с кварцевым песком способствует укреплению поверхности, повышению степени адгезии, позволяет сформировать оптимальную структуру основания.</li>
  <li>Грунтовка с кварцевым песком не только упрощает процесс нанесения декоративных покрытий, но и существенно уменьшает расход финишного покрытия.</li>
</ul>
<h3>Расчет необходимого объема песка</h3>
<p>
   Расход будет зависеть от сферы применения.
</p>
<h4>Для классической и полусухой стяжки</h4>
<p>
   Пропорция наполнителя и связующего должна быть 3 к 1.
</p>
<h4>Для штукатурки</h4>
<p>
   Например, площадь стен равна 50 м2, толщина слоя, после установки маяков, равняется 2,5 см.
</p>
<p>
   Расчет:
</p>
<ul class="list-marked">
  <li>10,025 м = 0,025 м3 – столько понадобится смеси на 1 м2 поверхности.</li>
  <li>при пропорции 1 к 4 на 1 м2 наполнителя понадобится 0,02 м3, а цемента – 0,005 м3</li>
  <li>на всю площадь нужно будет: 0,0250 = 0,25 м3</li>
</ul>
<h4>Расчет количества песка на 1 метр кубический раствора</h4>
<p>
   Например, нормативный объем цемента на 1 куб. метр смеси составляет 410 кг. Объем раствора на 10 м2 составит 0,30 кубов. Цемента понадобится: 4100,30 = 123 кг (2,5 мешка). Три части песка составит: 123 кг3 = 369 кг (порядка 7,5 мешков)
</p>
 </section> <section class="container page-section">
<div class="content">
  <h2 id="fasovka">Фасовка</h2>
  <div class="block-wrap block-wrap_wrap ">
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
       <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
  )
);?>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
       <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
  )
);?>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
       <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
  )
);?>
    </div>
  </div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
  )
);?> <section class="container page-section">
<div class="content">
  <h2 id="kak-kupit">Как купить?</h2>
   <?$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_SHOW" => "file",
    "AREA_FILE_SUFFIX" => "how-to-buy",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => "",
    "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
  )
);?>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
