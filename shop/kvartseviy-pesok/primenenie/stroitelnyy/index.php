<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Строительный кварцевый песок - купить в Санкт-Петебурге");
$APPLICATION->SetPageProperty("description", "Реализуем Строительный песок в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Строительный песок Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Строительный песок");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Строительный песок" src="/images/primenenie-peska/stroitelnyy-pesok.jpg" class="title-image__img" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			<ul class="class list-marked list-marked_half">
				<li>содержание кварца <b>60-70%</b><br>
 </li>
				<li>песок для строительства <b>по&nbsp;ГОСТу</b>;</li>
				<li><b>сухой и&nbsp;чистый</b> песок с&nbsp;минимальными примесями;</li>
				<li>производство <b>любых фракций</b> на&nbsp;заказ;</li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny"> Цены на строительный кварцевый песок </h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20 тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td style="white-space: nowrap">
 <strong>0-0,63</strong> <br>
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
 <strong>0,63-2,5</strong> <br>
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
 <strong>0-2,5</strong> <br>
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
 <strong>0,8-1,25</strong> <br>
					 ГОСТ 8736
				</td>
				<td>
					 -
				</td>
				<td colspan="3">
					 договорная
				</td>
				<td>
					 -
				</td>
			</tr>
			</tbody>
			</table>
    </div>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<p id="primenenie">
		 Строительный кварцевый песок&nbsp;— это материал серого цвета&nbsp;с различным размером зерен. Широко используется в&nbsp;строительстве, отделочных работах и&nbsp;ландшафтном дизайне.<br>
 <br>
	</p>
	<h2>Применение</h2>
	<p>
 <strong>Для заливки ленточных фундаментов, плит, блоков, бетонных изделий.<br>
 </strong>
	</p>
	<p>
 <strong>При изготовлении штукатурки и&nbsp;строительных смесей.&nbsp;</strong>Кварцевый песок добавляют в&nbsp;декоративную штукатурку для финальной обработки поверхностей. Благодаря разнообразию кварца, покрытие может отличаться по&nbsp;фактуре и&nbsp;цвету. Ровно ложится и&nbsp;долго служит.<strong><br>
 </strong>
	</p>
	<p>
 <strong>При заливке полиуретановых и&nbsp;эпоксидных полов.</strong> Такие полы кладут в&nbsp;складских и&nbsp;производственных помещениях, медицинских учреждениях. Покрытия отличаются высокой износостойкостью, экологичностью и&nbsp;пожаробезопасностью.
	</p>
	<h2>Правило выбора</h2>
	<h3>Как выбрать фракцию строительного кварцевого песка</h3>
	<p>
		 Всего выделяют три основные размерности: мелкий (0.0 - 0.63 мм), средний (0.63 - 1.25&nbsp;мм), крупный (0.63 - 2.5&nbsp;мм).
	</p>
	<ul class="list-marked">
		<li>Мелкий песок используют для приготовления строительных смесей. Чем меньше зерна, тем более ровнее ложится покрытие.</li>
		<li>Песок средних размеров нашел применение в&nbsp;изготовлении кирпича. </li>
		<li>Песчинки размером 2,-2,5&nbsp;мм идут на&nbsp;изготовление бетона, асфальта и&nbsp;заливку полов.</li>
	</ul>
	<p>
		 &nbsp;
	</p>
	<h3>Критерии качества</h3>
	<ol class="list-numbered">
		<li>Фракционный состав. Очень важен при выборе, так как&nbsp; размерность&nbsp;гранул определяет его применение.<br>
 </li>
		<li>Влажность. Не более 0.5%<br>
 </li>
		<li>Химический состав.</li>
		<li>Соответствие стандартам. Качественный кварцевый песок соответствует ГОСТу&nbsp;8736.</li>
		<li>Форма гранул.&nbsp;Морской – зерна более округлые. Карьерный –&nbsp;более полигранны.&nbsp;<br>
 <br>
 </li>
	</ol>
	<h3>Преимущества применения кварцевого песка в&nbsp;строительстве</h3>
	<ol class="list-numbered">
		<li>Высокая химическая устойчивость.</li>
		<li>Цветовая стабильность. </li>
		<li>Устойчивостью к&nbsp;дроблению и&nbsp;стиранию.</li>
		<li>Однородный состав.</li>
		<li>Высокая межзерновая пористость.</li>
		<li>Устойчивость к&nbsp;атмосферным воздействиям.<br>
 <br>
 </li>
	</ol>
	<h2>Расход кварцевого песка в&nbsp;строительстве</h2>
	<p>
		 Расход кварцевого песка зависит от&nbsp;типа применения.<br>
 <br>
	</p>
	<h3>Расход песка для основания под асфальт</h3>
	<p>
		 Для расчета правильного количества материала используют величины: общая площадь укладки, толщина слоя песка, удельный вес по&nbsp;техпаспорту, коэффициент уплотнения песка (1,11).<br>
 <br>
	</p>
	<p>
		 Приблизительный расчет расхода песка на&nbsp;1&nbsp;кв.&nbsp;м. выглядит так:<br>
 <br>
	</p>
	<p>
		 Исходные данные:
	</p>
	<ul class="list-marked">
		<li>площадь укладки&nbsp;— 200&nbsp;кв.&nbsp;м.</li>
		<li>толщина уплотняемого слоя&nbsp;— 20&nbsp;см (0,2&nbsp;м).</li>
		<li>кварцевый песок плотностью 1,5&nbsp;т/куб.&nbsp;м.<br>
 <br>
 </li>
	</ul>
	<p>
		 Расчет:<br>
		 0,2&nbsp;м (толщина слоя) х&nbsp;1&nbsp;м. (ширина слоя) х&nbsp;1&nbsp;м. (длина слоя) х&nbsp;1,5&nbsp;т/куб.&nbsp;м.&nbsp;х&nbsp;1,1 (коэффициент уплотнения песка = 0,33&nbsp;т/кв.&nbsp;м.
	</p>
	<p>
		 То&nbsp;есть чтобы соорудить 1&nbsp;кв.&nbsp;м. кварцевого песка толщиной 20&nbsp;см нужно 330&nbsp;кг. При общей площади 200&nbsp;кв.&nbsp;м., суммарный объем песка составляет 66&nbsp;тонн.<br>
 <br>
	</p>
	<h3>Расход песка для изготовления штукатурки</h3>
	<p>
		 Два параметра влияют на&nbsp;объем песка: марка цемента и&nbsp;область использования.
	</p>
	<p>
		 За&nbsp;норму приняты соотношения:
	</p>
	<ul class="list-marked">
		<li>для основного слоя&nbsp;— 1:4 или 1:5;</li>
		<li>для набрызга&nbsp;— 1:3 или 1:2,5;</li>
		<li>для накрывочного слоя&nbsp;— 1:2 или 1:1.<br>
 <br>
 </li>
	</ul>
	<p>
		 Место применения также влияет на&nbsp;соотношение.
	</p>
	<p>
		 Для внутренних работ в&nbsp;сухих обогреваемых помещениях используют 1&nbsp;часть песка на&nbsp;4-5 частей цемента марки М400.<br>
 <br>
	</p>
	<p>
		 Для отделки ванных комнат и&nbsp;фасадов берут 2-3 части песка на&nbsp;1&nbsp;часть цемента М400 или 3-4 части песка на&nbsp;1&nbsp;часть цемента М500.<br>
 <br>
	</p>
	<p>
		 Допустим, кварцевый песок вам нужен для основного слоя штукатурки в&nbsp;спальне. В&nbsp;этом случае, при соотношении 1:4, площади покрытия 8&nbsp;кв.&nbsp;м.&nbsp;и&nbsp;толщине слоя 2&nbsp;см, расход будет равен около 5-6&nbsp;кг цемента на&nbsp;метр квадратный плоскости и&nbsp;16&nbsp;килограмм песка.
	</p>
</div>
 </section>
<!--section class="container page-section">
  <div class="content">
    <h2 id="primery">Примеры фракций</h2>
    <div class="flexslider-container">
      <div class="flexslider">
        <ul class="slides">
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-08-125-2138-91.gif" alt="0,63 - 1,25 мм" title="0,63 - 1,25 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">0,63 - 1,25 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="1,25–2,5 мм" title="1,25–2,5 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">1,25–2,5 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736</div>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section--> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
	<h3>Как выбрать фасовку?</h3>
	<p>
		 Оптимальный тип фасовки для большинства наших покупателей&nbsp;— мешки по&nbsp;25&nbsp;кг. Они экономно расходуются и&nbsp;удобны в&nbsp;транспортировке.
	</p>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
