<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Песок для черепицы оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Песок для черепицы в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Песок для черепицы Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Песок для черепицы");
?>
      </div>
    </div>

     <div class="container">
       <div class="content">
         <ul class="list-anchors">
           <!--li class="list-anchors__item"><a href="#tseny">Цены</a></li-->
           <!--li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li-->
           <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
           <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
         </ul>
       </div>
     </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/primenenie-peska/pesok-dlya-cherepitsy.jpg" alt="Песок для черепицы" title="Песок для черепицы" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <!--h2 id="tseny">Цены на песок для котлов с топками кипящего слоя</h2-->
        <div class="block-wrap  block-wrap_wrap ">
          <!--div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width7 block-wrap__item_s-width6">
            <div class="table-wrap">
                <table class="table-price">
                  <tr>
                    <th></th>
                    <th>Навалом<br>(за тонну)</th>
                    <th>В биг-бегах (шт.)</th>
                  </tr>
                  <tr>
                    <td>0,63 - 1,25<br>ГОСТ 8736-2014</td>
                    <td>договорная</td>
                    <td>договорная</td>
                  </tr>
                  <tr>
                    <td>1,25 - 2,5<br>ГОСТ 8736-2014</td>
                    <td>договорная</td>
                    <td>договорная</td>
                  </tr>
                </table>
              </div>
          </div-->
          <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width5 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
			)
		);?>


<!--section class="container page-section">
  <div class="content">
    <h2 id="primery">Примеры фракций</h2>
    <div class="flexslider-container">
      <div class="flexslider">
        <ul class="slides">
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-08-125-2138-91.gif" alt="0,63 - 1,25 мм" title="0,63 - 1,25 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">0,63 - 1,25 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="1,25–2,5 мм" title="1,25–2,5 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">1,25–2,5 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section-->


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>

      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
