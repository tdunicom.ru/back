<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Песок для спортивных покрытий оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Песок для спортивных покрытий в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Песок для спортивных покрытий Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Песок для спортивных площадок и покрытий");
?>
     <div class="container">
       <div class="content">
         <ul class="list-anchors">
           <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
           <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
           <li class="list-anchors__item"><a href="#preimuschestva">Преимущества</a></li>
           <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
           <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
           <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
         </ul>
       </div>
     </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/primenenie-peska/pesok-dlya-sportivnykh-pokrytiy.jpg" alt="Песок для спортивных покрытий" title="Песок для спортивных покрытий" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>



    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на песок для спортивных покрытий</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
<table class="table-price">
              <tbody><tr>
                <th></th>
                <th class="">
                   Навалом
                </th>
                <th class="" colspan="3">
                  Биг-бэги (за штуку, 1 тонна)
                </th>
                <th class="">
                  Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
                </th>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td class="price-table__sub-th">
                  до 10 тонн
                </td>
                <td class="price-table__sub-th">
                  от 10 до 20 тонн
                </td>
                <td class="price-table__sub-th">
                  больше 20 тонн
                </td>
                <td></td>
              </tr>

              <tr>
                <td>
                  <strong style="white-space: nowrap">0 - 0,3 </strong>
                </td>
                <td colspan="5">договорная</td>
              </tr>
              <tr>
                <td>
                  <strong style="white-space: nowrap">0,3 - 0,63</strong>
                </td>
                <td colspan="5">договорная</td>
              </tr>
              <tr>
                <td>
                  <strong style="white-space: nowrap">0,63 - 1,5</strong>
                </td>
                <td colspan="5">договорная</td>
              </tr>
              <tr>
                <td>
                  <strong style="white-space: nowrap">1,25 - 2,5</strong>
                </td>
                <td colspan="5">договорная</td>
              </tr>
            </tbody></table>
    </div>
<a href="/tseny/">Все цены</a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
			)
		);?>


<section class="container page-section">
  <div class="content">
    <h2 id="primery">Примеры фракций</h2>
    <div class="flexslider-container">
      <div class="flexslider">
        <ul class="slides">
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-01-03-2138-91.gif" alt="0 - 0,3 мм" title="0 - 0,3 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">0 - 0,3 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 2138</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-2138-91.gif" alt="0,3 - 0,63 мм" title="0,3 - 0,63 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">0,3 - 0,63 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 2138</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-08-125-2138-91.gif" alt="0,63 - 1,25 мм" title="0,63 - 1,25 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">0,63 - 1,25 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 8736</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="1,25–2,5 мм" title="1,25–2,5 мм" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">1,25–2,5 мм</div>
                <div class="figure-mark__desc" itemprop="description">ГОСТ 8736</div>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="container page-section">
  <div class="content">
    <h2 id="preimuschestva">Кварцевый песок для покрытий: основные преимущества</h2>
    <p>Материал позволяет создавать дорожную разметку, которая продержится несколько лет без необходимости обновления. Ведь свойства песка из кварца безупречны: материал устойчив к атмосферным явлениям, выдерживает механическую нагрузку. А большой выбор цветов позволит быстро определиться с наиболее подходящим вариантом для конкретного участка.</p>

    <div class="block-wrap  ">
      <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
        <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
          <img alt="Кварцевый песок для покрытий дорог" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-sportivnyih-pokryitiy/kvartsevyiy-pesok-dlya-pokryitiy-dorog.jpg" title="Кварцевый песок для покрытий дорог" itemprop="contentUrl">
          <figcaption itemprop="caption">
            Кварцевый песок для покрытий дорог
          </figcaption>
        </figure>
      </div>
    </div>
    <p><a href="/informatsiya/molotyy-kvarts-kvartsevyy-pesok/">Молотый кварцевый песок для покрытий</a> рекомендован к использованию экспертами. Такое решение они приняли на основании хорошей прочности, высокой устойчивости к износу материала, а также оптимизации сцепления дорожного полотна с шинами. В итоге дорожное полотно, созданное с использованием песка из кварца, становится гораздо безопаснее: минимизируется вероятность заноса, удобным становится переход в зимний период. Кроме того, материал позволяет создать хорошее антискользящее покрытие, сделать участки повышенной опасности гораздо заметнее.</p>

  </div>
</section>

    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">

        <h2 id="primenenie">Кварцевый песок для спортивных площадок</h2>

        <div class="block-wrap  ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
              <img alt="Кварцевый песок для спортивных площадок" src="/images/kvartsevyy-pesok-v-big-begah/pesok-dlya-sportivnyih-pokryitiy/kvartsevyiy-pesok-dlya-sportivnyih-ploschadok.jpg" title="Кварцевый песок для спортивных площадок" itemprop="contentUrl">
              <figcaption itemprop="caption">
                Кварцевый песок для спортивных площадок
              </figcaption>
            </figure>
          </div>
        </div>
        <p>Высокотехнологичные спецобъекты, к которым относятся и площадки для спортивных занятий, создают из надежных материалов, важнейшим из которых считается кварцевый песок.</p>
        <p>В этом случае он выполняет такие функции:</p>
        <ol class="list-numbered">
          <li>Выступает в роли фундамента площадки.</li>
          <li>Необходим как дренажный материал.</li>
          <li>Является укрепляющей составляющей верхних покрытий.</li>
          <li>Применяется для газонов, созданных из искусственной травы. В этом случае его используют в качестве засыпного материала.</li>
          <li>Необходимая составляющая для детских площадок. Кварцевый песок в этом случае создает некий натуральный стиль детского участка.</li>
        </ol>
        <h2>Достоинства песка из кварца для организации площадок такие:</h2>
        <ul class="list-marked list-marked_full">
          <li><strong>Однородность.</strong> Материал не используют, прежде чем не проведут процесс фракционирования. В результате частички песка получаются одинаковыми. Такое свойство позволяет создавать комфортное и качественное покрытие, улучшающее сцепление спортивной обуви с поверхностью.</li>
          <li><strong>Чистота состава.</strong> В данном песке содержится не менее 90% оксида кремния и полностью отсутствуют посторонние примеси.</li>
          <li><strong>Упругость.</strong> Мячи гораздо лучше отскакивают от поверхности, в основе которой присутствует песок из кварца.</li>
          <li><strong>Радиационная безопасность.</strong> Материал не способен накапливать радиационное излучение, чего не скажешь об иных материалах природного происхождения.</li>
          <li><strong>Повышенная твердость.</strong> Материалу присуща хорошая механическая прочность. А это значит, что он не изменит свою форму со временем, не будет истираться, причем вне зависимости от степени интенсивности нагрузок.</li>
          <li><strong>Повышенная гигроскопичность.</strong> Быстро впитывает влагу, создавая комфортные условия для спортивных занятий.</li>
        </ul>
        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
