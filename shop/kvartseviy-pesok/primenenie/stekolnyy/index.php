<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Стекольный песок оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetTitle("Стекольный песок");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#osobennosti">Особенности</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image"><img alt="Стекольный песок" src="/images/primenenie-peska/stekolnyy-pesok.jpg" class="title-image__img" title="Стекольный песок"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
	)
);?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на стекольный песок</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td>
					 до 10 тонн
				</td>
				<td>
					 от 10 до 20 тонн
				</td>
				<td>
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
 <strong>0-0,63</strong> <br>
					 ГОСТ 2138,&nbsp;22551
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
 <strong>0,315-0,63</strong> <br>
					 ГОСТ 2138,&nbsp;22551
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
 <strong>0-0,315</strong> <br>
					 ГОСТ 2138,&nbsp;22551
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width5 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark"><img alt="1,25–2,5 мм" src="/images/fract-pesok/pesok-125-25-8736-2017.jpg" class="figure-mark__image" title="1,25–2,5 мм"> <figcaption class="figure-mark__caption">
				<div class="figure-mark__title">
					 1,25–2,5 мм
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="primenenie">Применение</h2>
	<p>
		 Кварцевый песок для стекла применяют для изготовления:
	</p>
	<ul class="list-marked list-marked_full">
		<li>стеклоблоков;</li>
		<li>витрин;</li>
		<li>бутылок;</li>
		<li>сигнального стекла;</li>
		<li>стекла, использующегося в автомобильной сфере;</li>
		<li>листового стекла для промышленных нужд;</li>
		<li>стеклянной посуды;</li>
		<li>консервной тары;</li>
		<li>парфюмерного, медицинского и лабораторного стекла.</li>
	</ul>
	<p>
		 К стекольному кварцевому песку обычно предъявляются следующие требования. Материал должен отличаться оптимальным соотношением качественных свойств и фракционного состава. В песке не допускается превышение показателя по содержанию влаги. Материал, соответствующий заявленным требованиям, позволяет существенно уменьшить использование энергоносителей в стекольном производстве.
	</p>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="osobennosti">Особенности применения материала</h2>
	<p>
		 Кварцевый песок используют для производства стекла, оптических изделий и жидкого стекла.
	</p>
	<div class="block-wrap">
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle"> </figure>
		</div>
	</div>
	<h3>Жидкое стекло</h3>
	<p>
		 Этот материал стал использоваться практически повсеместно: для воплощения облицовочных, декоративных или дизайнерских задумок, укрепления фундаментов, а также прочих строительных и ремонтных нужд. Основа жидкого стекла — натриевая соль кремниевой кислоты или, говоря другими словами, силикатный клей. Играет весомую роль в строительной сфере, так как отличается хорошей устойчивостью к механическим, а также атмосферным воздействиям.
	</p>
	<div class="block-wrap">
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle"> <img alt="Жидкое стекло с кварцевым песком" src="/images/kvartsevyy-pesok-v-big-begah/stekolnyiy-pesok/zhidkoe-steklo-s-kvarcevim-peskom.png" title="Жидкое стекло с кварцевым песком"> <figcaption>
			Жидкое стекло с кварцевым песком </figcaption> </figure>
		</div>
	</div>
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
