<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок в мешках в розницу");
$APPLICATION->SetTitle("Кварцевый песок в мешках в розницу");
?><div class="container">
<div class="content">
<div class="block-wrap block-wrap_wrap page-layout">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-layout__content page-layout__content_s-bottom">
		<p>
			 Кварцевый песок — продукт переработки кварца. Его получают, используя способ дробления и рассева. Наиболее популярен <a href="/">фасованный кварцевый песок</a> в мешках по 50 кг и 25 кг.
		</p>
 <br>
		<h2>Основные свойства материала:</h2>
		<ul class="list-marked">
			<li>хорошая сорбционная способность;</li>
			<li>оптимальная износостойкость;</li>
			<li>минимальное содержание примесей:</li>
			<li>устойчивость к механическим, атмосферным, а также химическим воздействиям.</li>
		</ul>
 <br>
		<h2>Кварцевый песок в мешках: сферы применения</h2>
		<p>
			 Используется для водоподготовки: очищает воду от нежелательных химических элементов, грязи и других частиц.
		</p>
		<p>
			 Используется в процессе создания материалов, использующихся в отделочных работах. Является важным составным элементом штукатурок, наливных полов, искусственного камня.
		</p>
		<p>
			 Применяется в ландшафтном дизайне, в процессе производства бетонных блоков. Высока востребованность <a href="/kvartseviy-pesok/primenenie/dlya-sportivnykh-pokrytiy/">кварцевого песка для напольных покрытий</a>.
		</p>
		<p>
			 Также существуют отдельные виды песка для детского творчества, аквариумов, красок.
		</p>
		<p>
			 Предлагаем купить кварцевый песок в розницу или оптом по доступной цене. Стандартно в мешки фасуем песок фракции 0-0,63 мм. По запросу возможна фасовка других фракций: услуга действительна при оптовых закупках.
		</p>
 <br>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "sect",
		"AREA_FILE_SUFFIX" => "kv_pesok",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
 
 	</div>
 	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 page-layout__content page-layout__content_s-top">


<div class="module-list sfery_primeneniya" style="margin-bottom: 24px">
		<div class="module-list__title">Сферы применения</div>
		 

    <ul>
                                <li>
                <a href="/kvartseviy-pesok/primenenie/dlya-peskostruynykh-rabot/" class="module-list-title">Песок для пескоструйных работ</a>
            </li>
                                <li>
                <a href="/kvartseviy-pesok/primenenie/dlya-sukhikh-stroitelnykh-smesey/" class="module-list-title">Песок для сухих строительных смесей</a>
            </li>
                                <li>
                <a href="/kvartseviy-pesok/primenenie/dlya-filtrov-ochistki-vody/" class="module-list-title">Кварцевый песок для фильтров очистки воды</a>
            </li>
                                <li>
                <a href="/kvartseviy-pesok/primenenie/stroitelnyy/" class="module-list-title">Строительный песок</a>
            </li>
                                <li>
                <a href="/kvartseviy-pesok/primenenie/beton/" class="module-list-title">Кварцевый песок для бетона и цемента</a>
            </li>
            </ul>
	</div>



<figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
  <img src="/upload/iblock/93b/93b0ed4b8a76625069b32ae6708ae995.jpg" title="Песок для пескоструйных работ" alt="Песок для пескоструйных работ" itemprop="contentUrl">
  <figcaption itemprop="caption">
    Фасовка в мешки по 50, 30, 25, 20, 10 кг
  </figcaption>
</figure>
</div>
</div>
</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
