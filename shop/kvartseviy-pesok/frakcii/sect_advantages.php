    <section class="container page-section page-section_list-advantages">
      <div class="content">
        <ul class="list-marked list-marked_half list-marked_xl-col2 list-marked_s-col1">
          <li>От производителя <strong>без наценок</strong> и посредников!</li>
          <li><strong>Гарантируем</strong> соблюдение сроков, прописанных в договоре</li>
          <li>В наличии <strong>6 фракций</strong> песка. <strong>Любые фракции</strong> произведем под заказ</li>
          <li>Кварцевый песок соответствует <strong>ГОСТу, </strong> есть <strong><a href="/kompaniya/">сертификат соответствия</a></strong></li>
        </ul>
      </div>
    </section>
