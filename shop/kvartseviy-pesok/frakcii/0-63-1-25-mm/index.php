<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок фракция 0,63 - 1,25 мм: цены от производителя, купить в Санкт-Петербурге");
$APPLICATION->SetPageProperty("keywords", "кварцевый песок 0,63 - 1,25 мм цена сухой мытый округлый без пыли от производителя доставка ГОСТ навалом биг-беги мешки");
$APPLICATION->SetPageProperty("description", "Сухой кварцевый песок 0,63 - 1,25 мм. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 0,63 - 1,25. ГОСТ 8736-2014. Фасовка: навалом, в биг-бегах по тонне, в мешках по 25, 50 кг.");
$APPLICATION->SetTitle("Песок кварцевый 0,63-1,25 мм");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Размер: 0,63 - 1,25 мм" src="/images/fract-pesok/pesok-08-125-2138-91.gif" class="title-image__img" title="Размер: 0,63 - 1,25 мм" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			<?
        $page_gost = '8736';
        include '../sect_details.php';
      ?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на кварцевый песок 0,63 - 1,25</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20&nbsp;тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20&nbsp;тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					 ГОСТ 2138
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td style="white-space: nowrap">
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width6 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="primenenie">Сферы использования кварцевого песка фракции 0,63 - 1,25</h2>
	<ul class="list-marked list-marked_full">
		<li>
      <strong>В&nbsp;строительстве</strong><br>
      Для создания наливных полов, строительных смесей, клинкерных, звукоизоляционных термопанелей, посыпки кровельных материалов.
    </li>
		<li>
      <strong>Для фильтров</strong><br>
      Для фильтрации питьевой воды.
    </li>
		<li>
      <strong>Для пескоструйных работ</strong><br>
      Песком заполняют пескоструйные аппараты.
    </li>
		<li>
      <strong>Для промышленности</strong><br>
      Заполняют песочницы локомотивов, используют для энергокотлов кипящего слоя, применяют в&nbsp;литейной, стекольной сфере.
    </li>
		<li>
      <strong>Для спорта</strong><br>
      Для обустройства манежей, стадионов, спортивных покрытий.
    </li>
		<li>
      <strong>Для коммунальной сферы</strong><br>
      Для посыпки тротуаров, автодорог.
    </li>
	</ul>
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_how-to-buy.php"
	)
);?>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
