<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок 0,1 - 0,3 мм: цены от производителя, купить в Санкт-Петербурге");
$APPLICATION->SetTitle("Песок кварцевый 0,1-0,3 мм");
?>
    <div class="container">
      <div class="content">
        <ul class="list-anchors">
          <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
          <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
          <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
          <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
        </ul>
      </div>
    </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/fract-pesok/pesok-01-03-2138-91.gif" alt="Размер: 0,1 - 0,3 мм" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на кварцевый песок 0,1 - 0,3</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>Навалом</th>
                <th>В биг-бегах</th>
              </tr>
              <tr>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
            </table>
    </div>
<a href="/tseny/">Все цены</a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
        		<?$APPLICATION->IncludeComponent(
        			"bitrix:main.include",
        			".default",
        			Array(
        				"AREA_FILE_SHOW" => "file",
        				"COMPONENT_TEMPLATE" => ".default",
        				"EDIT_TEMPLATE" => "",
        				"PATH" => "/frakcii/sect_availability-instock.php"
        			)
        		);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/frakcii/sect_calc-anchor.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/frakcii/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">
        <h2 id="primenenie">Применение кварцевого песка 0,1-0,3 мм</h2>
        <ul class="list-marked list-marked_full">
          <li>
            <strong>Очистка воды</strong>
            <br>Кварцевый песок, фракционированный до одной десятой - одной третьей миллиметра, применяют для заполнения бытовых и промышленных фильтров.
          </li>
          <li>
            <strong>Пескоструйные работы</strong>
            <br>Песок этого фракционного состава используют на заключительном этапе очистки и шлифовки. Главная характеристика выбора материала – угловатая форма зерен.
          </li>
          <li>
            <strong>Шлифовка</strong>
            <br>Создают абразивы для обработки хрупких материалов. Например, полировки стекла.
          </li>
          <li>
            <strong>Стекольная промышленность</strong>
            <br>Для создания стекловолокон, стеклотары.
          </li>
          <li>
            <strong>Фарфоровая промышленность</strong>
            <br>Для создания фарфора, керамики.
          </li>
          <li>
            <strong>Строительная сфера</strong>
            <br>Создают сухие строительные смеси, клей, шпатлевки.
          </li>
        </ul>

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/frakcii/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
