<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("keywords", "сухой кварцевый песок купить цена спб опт");
$APPLICATION->SetPageProperty("description", "Купить кварцевый песок от производителя, без наценок с доставкой. Цена на сухой кварцевый песок в Санкт-Петербурге (СПб). Соответствует ГОСТу, есть сертификат.");
$APPLICATION->SetPageProperty("title", "Кварцевый песок - купить в спб, цена на песок сухой кварцевый");
$APPLICATION->SetTitle("Песок кварцевый, цветной - Цементно-песчаная смесь");
?>
<div class="block-wrap  block-wrap_wrap ">
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <ul class="list-marked list-marked_half">
      <li>высокое содержание <strong>кварца - более 98%</strong></li>
      <li><strong>сухой</strong> - влажность не более 0,5%</li>
      <li>температура: <strong>охлажденный</strong></li>
    </ul>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <ul class="list-marked list-marked_half">
      <li>форма зерен: <strong>округлая</strong></li>
      <li>чистота: мытый, практически с полным <strong>отсутствием посторонних примесей</strong></li>
    </ul>
  </div>
</div>
<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>
