

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Штукатурка с кварцевым песком: цена, фото, купить в СПб");
$APPLICATION->SetPageProperty("keywords", "штукатурка кварцевый песок купить фото цена");
$APPLICATION->SetPageProperty("description", "Штукатурка с кварцевым песком. Основное применение – выравнивание поверхностей. Цена, фото, свойства штукатурки. Заказать онлайн.");
$APPLICATION->SetTitle("Штукатурка с кварцевым песком");
?>
      </div>
    </div>

     <div class="container">
       <div class="content">
         <ul class="list-anchors">
           <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
           <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
           <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
           <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
           <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
         </ul>
       </div>
     </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image"><img class="title-image__img" src="/images/tsvetnoy-kvartsevyy-pesok/shtukaturka-s-kvartsevyim-peskom/shtukaturka-s-kvartsevyim-peskom.jpg" alt="Штукатурка с кварцевым песком" title="Штукатурка с кварцевым песком"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на песок для штукатурки</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
						<table class="table-price">
			        <tr>
								<th></th>
			        	<th>
			        		 Навалом
			        	</th>
			        	<th>
			        		 В биг-бегах
			        	</th>
			        </tr>
			        <tr>
			        	<td>
			         		0 - 0,63<br>
			        		 ГОСТ 8736-2014
			        	</td>
			        	<td>
			        		 1600
			        	</td>
			        	<td>
			        		 1900
			        	</td>
			        </tr>
			        <tr>
			        	<td>
			         0 - 0,63 <br>
			        		 ГОСТ 2138-91
			        	</td>
			        	<td>
			        		 2200
			        	</td>
			        	<td>
			        		 2500
			        	</td>
			        </tr>
							<tr>
								<td>0,1 - 0,3</td>
			        	<td>
			        		 договорная
			        	</td>
			        	<td>
			        		 договорная
			        	</td>
							</tr>
							<tr>
								<td>0,3 - 0,63</td>
			        	<td>
			        		 договорная
			        	</td>
			        	<td>
			        		 договорная
			        	</td>
							</tr>
			        <tr>
			        	<td>
			         0,63 - 1,25<br>
			        		 ГОСТ 8736-2014
			        	</td>
			        	<td>
			        		 договорная
			        	</td>
			        	<td>
			        		 договорная
			        	</td>
			        </tr>
		        </table>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
        		<?$APPLICATION->IncludeComponent(
        			"bitrix:main.include",
        			".default",
        			Array(
        				"AREA_FILE_SHOW" => "file",
        				"COMPONENT_TEMPLATE" => ".default",
        				"EDIT_TEMPLATE" => "",
        				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_availability-instock.php"
        			)
        		);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_calc-anchor.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_advantages.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="primery">Примеры фракций</h2>
        <div class="flexslider-container">
          <div class="flexslider">
            <ul class="slides">
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-2138-91.gif" alt="0 - 0,63" title="0 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0 - 0,63</div>
                    <div class="figure-mark__desc">ГОСТ 2138-91</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-8736-2014.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0 - 0,63</div>
                    <div class="figure-mark__desc">ГОСТ 8736-2014</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-01-03-2138-91.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0,1 - 0,3</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-03-06-2138-91.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0,3 - 0,63</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-08-125-2138-91.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0,63 - 1,25</div>
                    <div class="figure-mark__desc">ГОСТ 8736-2014</div>
                  </figcaption>
                </figure>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <section class="container page-section">
      <div class="content">
				<p id="primenenie"><strong>Штукатурка с кварцевым песком</strong> — это отделочный материал, необходимый для выравнивания поверхностей. Высокую популярность завоевал благодаря оптимальным декоративным и защитным свойствам.</p>
				<p>В состав материала входят пигменты, смесь наполнителей, а также вспомогательные добавки, главной из которых является кварцевое зерно мельчайшей фракции.</p>
				<h3>Свойства штукатурки</h3>
				<p>Штукатурка с кварцевым песком повышает адгезию, придает прочность основанию, позволяет создать однородную поверхность любого оттенка. Для этого предварительно проводят колеровку специальными красителями.</p>
				<p>Расширяют сферу применения кварцевой штукатурки и прочие ее преимущества:</p>
				<ul class="list-marked">
				<li>превосходная паропроницаемость;</li>
				<li>отсутствие растворителей;</li>
				<li>стойкость к атмосферному воздействию;</li>
				<li>быстрое высыхание.</li>
				</ul>
				<h3>Применение материала</h3>
				<p>Штукатурка с кварцевым песком применяется для отделки гипсокартона, железобетонных поверхностей, монолитного бетона, покрытия гипсовой штукатурки (в качестве лепного декора).</p>
				<h3>Особенности применения</h3>
				<p>Прежде чем наносить отделочный материал, нужно качественно обработать поверхность: освободить ее от пыли, грязи и пятен жира, полностью высушить. Если предполагается работа с сыпучими поверхностями, то для них подойдет специальная отделка глубокого проникновения.</p>
				<p>Работа с декоративными пластерами вносит свои коррективы в использование материала: их наносят только после абсолютного высыхания кварцевой штукатурки. Обычно на это уходит от шести часов.</p>
				<p>Прежде чем использовать штукатурку, ее не только перемешивают до однородной консистенции, но и разбавляют водой, которой должно быть не более 10%. Наносят материал блочной кистью или меховым валиком. Обычно необходимо от двух и более слоев, чтобы добиться одинакового оттенка поверхности.</p>

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
