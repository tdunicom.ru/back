            <ul class="class list-marked list-marked_half list-marked_xl-col2 list-marked_s-col1">
              <li>высокое содержание <strong>кварца - не менее 90%</strong></li>
              <li><strong>сухой</strong> - влажность не более 0,5%</li>
              <li>температура: <strong>охлажденный</strong></li>
              <li>форма зерен: <strong>округлая</strong></li>
              <li>чистота: мытый, с полным <strong>отсутствием посторонних примесей</strong></li>
            </ul>
