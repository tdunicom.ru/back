<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для пола – Цена и  расход материала, купить в СПб");
$APPLICATION->SetPageProperty("keywords", "кварцевый песок пол цена расход материал");
$APPLICATION->SetPageProperty("description", "Кварцевый песок для пола в основном применяют в торговых центрах, паркингах и общественных учреждениях. Цена и расход материала будут различными в зависимости от площади заливки.");
$APPLICATION->SetTitle("Кварцевый песок для пола");
?>
      </div>
    </div>

    <div class="container">
     <div class="content">
       <ul class="list-anchors">
         <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
         <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
         <li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
         <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
         <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
       </ul>
     </div>
    </div>


    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image"><img class="title-image__img" src="/images/tsvetnoy-kvartsevyy-pesok/kvartsevyiy-pesok-dlya-pola/kvartsevyiy-pesok-dlya-pola.jpg" alt="Кварцевый песок для пола" title="Кварцевый песок для пола"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на песок для пола</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th></th>
                <th>Навалом</th>
                <th>В биг-бегах</th>
              </tr>
              <tr>
                <td>0,63 - 1,25<br>ГОСТ 8736-2014</td>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
              <tr>
                <td>1,25 - 2,5<br>ГОСТ 8736-2014</td>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
            </table>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
        		<?$APPLICATION->IncludeComponent(
        			"bitrix:main.include",
        			".default",
        			Array(
        				"AREA_FILE_SHOW" => "file",
        				"COMPONENT_TEMPLATE" => ".default",
        				"EDIT_TEMPLATE" => "",
        				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_availability-instock.php"
        			)
        		);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_calc-anchor.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_advantages.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="primery">Примеры фракций</h2>
        <div class="flexslider-container">
          <div class="flexslider">
            <ul class="slides">
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-063-25-8736-2014.gif" alt="0 - 0,63" title="0 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">0,63 - 1,25</div>
                    <div class="figure-mark__desc">ГОСТ 8736-2014</div>
                  </figcaption>
                </figure>
              </li>
              <li>
                <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="0,4 - 0,63" title="0,4 - 0,63"/>
                  <figcaption class="figure-mark__caption">
                    <div class="figure-mark__title">1,25 - 2,5</div>
                    <div class="figure-mark__desc">ГОСТ 8736-2014</div>
                  </figcaption>
                </figure>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>

    <section class="container page-section">
      <div class="content">
    		<p id="primenenie">Кварценаполненные полы создают в торговых центрах, паркингах, гаражах и общественных учреждениях. Крайне редко их делают в загородных домах. Кварцевый песок для пола преимущественно используют в качестве главного компонента, а связующим ингредиентом бывает полиуретан либо эпоксидная смола.</p>
    		<p>Поскольку в состав напольного покрытия может входить песок любого размера, а также красители самого разнообразного цвета, существенно повышаются эстетические функции материала. Например, с помощью него можно создавать специализированную разметку или узоры.</p>
    		<h3>Преимущества кварценаполненных полов</h3>
    		<p>Кварцевый песок для пола привносит такие достоинства в наливные полы:</p>
    		<ol class="list-marked">
    			<li>Исключено появление пыли. Поскольку компоненты материала связывают частички, предотвращая возникновение пыли.</li>
    			<li>Отсутствие швов. Нанесение финишного слоя позволяет добиться полной герметичности.</li>
    			<li>Экологичность. Напольные покрытия не становятся источником вредоносных веществ при повышении температурного режима или внешнем воздействии.</li>
    			<li>Стойкость к химическому воздействию. Кварцевый песок для пола не вступает во взаимодействие с моющими растворами.</li>
    			<li>Безопасность. Напольные покрытия этого вида не воспламеняются.</li>
    			<li>Антискользящая поверхность. Даже мельчайшая фракция песка способствует формированию шероховатой поверхности, препятствующей скольжению, повышая сцепление.</li>
    			<li>Высокая стойкость к износу. В обычных условиях кварц очень прочен, а в сочетании с полимерами этот критерий возрастает в разы. </li>
    		</ol>
    		<p>Если в точности следовать всем правилам технологии укладки, качественно подготовить основание, то напольные покрытия с кварцевым песком прослужат от одного до двух десятилетий. Срок долговечности определяется интенсивностью использования.</p>

        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
