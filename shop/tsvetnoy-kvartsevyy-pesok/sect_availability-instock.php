            <div class="availability availability_in-stock"><img class="availability__image" src="/images/fract/availability-in-stock.jpg" alt="Фракции в наличии" title="Фракции в наличии"/>
              <div class="availability__info">
                <div class="availability__title">Фракции в наличии</div>
                <div class="availability__text">Можем отгрузить на следующий день</div>
              </div>
            </div>
