<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Клиенты");
$APPLICATION->SetTitle("Клиенты");
?>
<div class="block-wrap block-wrap_wrap " style="margin-bottom: 1.5rem">

    <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6">
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/o-kompanii/nashi-klienty/tn-logo.png" alt="Технониколь" title="Технониколь" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             ТЕХНОНИКОЛЬ является одним из крупнейших международных производителей надежных и эффективных строительных материалов. Сегодня ТЕХНОНИКОЛЬ — это 53 производственные площадки в 7 странах мира (Россия, Беларусь, Литва, Чехия, Италия, Великобритания, Германия), 18 Учебных центров, 6 Научных центров, 22 представительства в 18 странах мира. Продукция компании поставляется в 95 государств.
        </p>
        <h2 style="background-color: #cccccc; max-width: 284px; padding: 10px" itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/o-kompanii/nashi-klienty/shpatler-logo.png" alt="Шпатлер" title="Шпатлер" style="max-width: 300px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Бренд ШПАТЛЕР - это новое слово на профессиональном рынке сухих строительных смесей. Сухие смеси ШПАТЛЕР производятся на современной немецкой автоматизированной линии SSB CCC, рассчитанной на выпуск 100 тонн готовой продукции в сутки в г. Пушкин.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/o-kompanii/nashi-klienty/acron-logo.png" alt="Акрон" title="Акрон" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Группа «Акрон» входит в число крупнейших мировых производителей минеральных удобрений.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="НТВ-энерго" src="/images/o-kompanii/nashi-klienty/ntv-energo-logo.png" title="НТВ-энерго" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Компания «НТВ-энерго» создана 1992 году специалистами кафедры «Реакторо- и парогенераторостроение» Санкт-Петербургского государственного политехнического университета для внедрения в энергетику и промышленность наукоемких технологий.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/o-kompanii/nashi-klienty/pesko-logo.png" alt="ПодьемТехСервис" title="ПодьемТехСервис" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             На сегодняшний день компания является одним из крупнейших поставщиков услуг по абразивоструйной обработке на территории Санкт-Петербурга и Ленинградской области.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="ПКФ Спектр" src="/images/o-kompanii/nashi-klienty/spektr-logo.jpg" title="ПКФ Спектр" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             АО «ПКФ Спектр» - один из ведущих отечественных производителей широкого ассортимента высокотехнологичных лакокрасочных материалов (лаки, грунтовки, шпатлевка, спецэмаль и т.п.), изготавливаемых на основе сополимеров винилхлорида, алкидных, эпоксидных, фенолоалкидных и кремнийорганических смол, поливинилацетатов, поливинилбутиралей и др.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/o-kompanii/nashi-klienty/antega.png" alt="Антега" title="Антега" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Компания ANTEGA уже не первый год плодотворно работает на строительном рынке и предлагает комплекс профессиональных отделочных товаров, для проведения полного цикла ремонтных работ.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"><img src="/images/o-kompanii/nashi-klienty/white-mix.png" alt="ВайтМикс" title="ВайтМикс" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Компания "ВАЙТМИКС" - Российский производитель безусадочных сухих строительных смесей, предназначенных для устранения дефектов и повреждений бетонных и каменных конструкций. Основной задачей деятельности нашей компании является восстановление утраченных свойств строительных конструкций.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"><img src="/images/o-kompanii/nashi-klienty/kvarc-polimer.png" alt="" title="" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             ООО КВАРЦПОЛИМЕР специализируется на производстве фракционированого натурального и цветного кварцевого песка. На сегодняшний день мы являемся ведущим предприятием в России и СНГ по производству цветных кварцевых песков.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Ленинградский зоопарк" src="/images/o-kompanii/nashi-klienty/lenin-zoo.png" title="Ленинградский зоопарк" style="max-width: 320px; display:block;" itemprop="contentUrl"></h2>
        <p>
             Ленинградский зоопарк – уникальный музей живой природы в самом сердце Санкт-Петербурга.
        </p>
        <h2 itemscope="" itemtype="http://schema.org/ImageObject"><img src="/images/o-kompanii/nashi-klienty/akc.png" alt="АКС" title="АКС" itemprop="contentUrl"></h2>
        <p>
             Завод Арматуры Контактной сети работает на рынке точного литья с 1994 года. Предприятие изготавливает отливки из сталей, легированных и нержавеющих, бронзы различных марок, алюминия, латуни, меди, для предприятий машиностроения, станкостроения, энергомашиностроения, судостроения, железнодорожного транспорта и др.
        </p>
        <h2>ООО "Арена-микс"</h2>
        <p>
             Производство строительных материалов
        </p>
        <h2>ООО "Карст"</h2>
        <p>
             Строительная компания, занималась реконструкцией Токсовского тоннеля.
        </p>
    </div>

    <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6">
        <p>
<img alt="Казаков Александр Владимирович" src="/images/kompaniya/kazakov.jpg">
        </p>
        <p>
             Здравствуйте, меня зовут<br>
             Волков Александр Сергеевич,<br>
             директор ООО "Юником"
        </p>
        <p>
             Если у вас есть предложения по сотрудничеству буду рад их рассмотреть. Присылайте на почту:<br>
<a href="mailto:info@tdunicom.ru" title="Написать письмо">info@tdunicom.ru</a>
        </p>
    </div>

</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
