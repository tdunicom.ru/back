<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");
?>
<div class="block-wrap block-wrap_wrap feedback-item">
  <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6 feedback-item__image">
    <a href="/images/kompaniya/otzyvy/leninec.jpg" class="image-open"><img src="/images/kompaniya/otzyvy/leninec.jpg" alt="Отзыв ЦППР Ленинец"></a>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6 ">
    <h2>ООО «ЦППР Ленинец»</h2>
    <div class="feedback-item__text">
      <p>Благодаря вашим оперативным действиям, все вопросы, возникающие в&nbsp;процессе сотрудничества, решались быстро.</p>
      <p>Коллектив нашей компании желает вашей команде профессиональных успехов в&nbsp;сфере деятельности и&nbsp;достижения всех поставленных коммерческих целей.</p>
    </div>
  </div>
</div>

<div class="block-wrap block-wrap_wrap feedback-item">
  <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6 feedback-item__image">
    <a href="/images/kompaniya/otzyvy/uniteh.jpg" class="image-open"><img src="/images/kompaniya/otzyvy/uniteh.jpg" alt="Отзыв ООО Унитех"></a>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6 ">
    <h2>ООО «Унитех»</h2>
    <div class="feedback-item__text">
      <p>ООО &laquo;Унитех&raquo;, в&nbsp;лице генерального директора Натальи Евгеньевны Семеновой, выражает благодарность коллективу и&nbsp;руководству ООО &laquo;Юником&raquo; за&nbsp;успешное долговременное сотрудничество и&nbsp;бесперебойность поставок сухого песка на&nbsp;склады нашей компании.</p>
      <p>Искренне надеемся на&nbsp;дальнейшее укрепление существующих отношений и&nbsp;рост показателей, достигнутых в&nbsp;ходе совместной деятельности.</p>
    </div>
  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
