<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Наша компания специализируется на производстве кварцевого песка высокого качества и практически с полным отстутствием посторонних примесей. Производим до 120 тонн в сутки.");
$APPLICATION->SetPageProperty("TITLE", 'Компания Юником - Продажа песка оптом и в розницу');
$APPLICATION->SetTitle("О компании");
?>
<div class="block-wrap block-wrap_wrap">
    <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6">
<p>ООО &laquo;Юником&raquo; занимается реализацией нерудных материалов с&nbsp;2012&nbsp;года. В&nbsp;2014 году мы&nbsp;запустили свою первую линию по&nbsp;сушке, фракционированию и&nbsp;фасовке кварцевого песка. За&nbsp;эти годы накоплен значительный опыт в&nbsp;производстве и&nbsp;технологии.</p>
<p>В&nbsp;2020 году мы&nbsp;запустили вторую линию по&nbsp;сушке песка и&nbsp;запустили завод по&nbsp;производству сухих строительных смесей, что дало возможность компании увеличить обьемы производства песка (до&nbsp;200 тонн в&nbsp;сутки) и&nbsp;выйти на&nbsp;новые рынки, освоив новые направления.</p>
<p>Штат компании укомплектован молодыми, энергичными сотрудниками, которые постоянно совершенствуют свои навыки в&nbsp;производстве, внедряют современные технологии и&nbsp;неординарные решения, сказывающиеся, в&nbsp;конечном итоге, на&nbsp;всех процессах, происходящих в&nbsp;компании.</p>
        <h2>Производство и&nbsp;склад удобно расположены в&nbsp;15&nbsp;км от&nbsp;КАД</h2>
<p>Склад расположен в&nbsp;Всеволожском районе, д.Щеглово.</p>
<p class="map_toggle toggle-by">
             Посмотреть на карте
        </p>
        <div class="map toggle-me">
             <?$APPLICATION->IncludeComponent(
"bitrix:map.yandex.view",
".default",
Array(
"COMPONENT_TEMPLATE" => ".default",
"CONTROLS" => array(0=>"ZOOM",),
"INIT_MAP_TYPE" => "MAP",
"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:60.02512436893973;s:10:\"yandex_lon\";d:30.754925395831332;s:12:\"yandex_scale\";i:12;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:30.758015300616;s:3:\"LAT\";d:60.024179347868;s:4:\"TEXT\";s:43:\"Производственный склад\";}}}",
"MAP_HEIGHT" => "250",
"MAP_ID" => "",
"MAP_WIDTH" => "100%",
"OPTIONS" => array(0=>"ENABLE_DBLCLICK_ZOOM",1=>"ENABLE_DRAGGING",)
)
);?>
        </div>
        <h2>Лучшее предприятие отрасли 2019</h2>
        <p>За&nbsp;весомый вклад в&nbsp;социально-экономическое развитие региона, приоритетность в&nbsp;отрасли, а также высокие результаты в&nbsp;рамках своего сегмента, центром аналитических исследований организация <a href="https://analit-centr.ru/7718900829" target="_blank">рекомендована</a> к&nbsp;региональной программе &laquo;Лучшее предприятие отрасли 2019&raquo;.</p>
        <h2>Наши проекты</h2>
        <ul class="list-marked list-marked_half">
  <li>Поставка сухого песка для ремонта дамбы в&nbsp;Санкт-Петербурге</li>
  <li>Поставка сухого песка для пескоструйных работ при чистке фасадов Петропавловской крепости</li>
  <li>Поставка сухого песка для пескоструйных работ при чистке фасадов в&nbsp;Новой Голландии</li>
        </ul>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6">
        <p>
<img alt="Казаков Александр Владимирович" src="/images/kompaniya/kazakov.jpg">
        </p>
        <p>
             Здравствуйте, меня зовут <br>
             Волков Александр Сергеевич,<br>
             директор ООО "Юником"
        </p>
        <p>
             Если у вас есть предложения по сотрудничеству, буду рад их рассмотреть. Присылайте на почту:<br>
<a href="mailto:info@tdunicom.ru" title="Написать письмо">info@tdunicom.ru</a>
        </p>
    </div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
