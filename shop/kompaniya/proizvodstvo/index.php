<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производство");
?>
<div class="block-wrap block-wrap_wrap">
      <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6 block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <figcaption itemprop="caption">
                <h3>Отгружаем "навалом" в цементовозы и тоннары.</h3>
                <p>Для отгрузки используемы силосы. В них же формируем запас песка и готовы отгрузить в любой момент</p>
              </figcaption>
              <a href="/upload/medialibrary/0f7/0f7514586107fce98b5d91b5fbc96008.jpg" class="image-open">
                <img src="/upload/medialibrary/0f7/0f7514586107fce98b5d91b5fbc96008.jpg" alt="" itemprop="contentUrl">
              </a>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <figcaption itemprop="caption">
                <h3>Фасуем в биг-бэги</h3>
                <p>После разделения на&nbsp;фракции в&nbsp;автоматическом режиме фасуем в&nbsp;полипропиленовые мешки (биг беги).</p>
              </figcaption>

              <a href="/upload/medialibrary/f26/f26844cded4bd0b0fe1a88ce68c468e2.jpg" class="image-open">
                <img src="/upload/medialibrary/f26/f26844cded4bd0b0fe1a88ce68c468e2.jpg" alt="" itemprop="contentUrl">
              </a>
            </figure>
          </div>
        </div>
        <h3 class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">Наше оборудование для сушки и фасовки песка</h3>
        <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6 block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/787/787e563e409fba2f6923e1482e82978a.jpg" class="image-open">
                <img src="/upload/medialibrary/787/787e563e409fba2f6923e1482e82978a.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Циклон удаляет пыль и топочные газы из сушильно барабана</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/54e/54e2d21801cee963248e32344856ee35.jpg" class="image-open">
                <img src="/upload/medialibrary/54e/54e2d21801cee963248e32344856ee35.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Циклон, бункер силосов, переключатель</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/25b/25bc0b96aab9ea8014d691e2273d3913.jpg" class="image-open">
                <img src="/upload/medialibrary/25b/25bc0b96aab9ea8014d691e2273d3913.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Сушильный барабан</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/e72/e727d089af33df890bfa5d875d68dd5a.jpg" class="image-open">
                <img src="/upload/medialibrary/e72/e727d089af33df890bfa5d875d68dd5a.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Приемочный бункер силосов с пневматической загрузкой</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/17d/17d8ad76920b202f42467f445b7b5e9a.jpg" class="image-open">
                <img src="/upload/medialibrary/17d/17d8ad76920b202f42467f445b7b5e9a.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Приемочный бункер загрузки силосов</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <a href="/upload/medialibrary/bff/bff54f014ce4ff660afa25a17f7b905c.jpg" class="image-open">
                <img src="/upload/medialibrary/bff/bff54f014ce4ff660afa25a17f7b905c.jpg" alt="" itemprop="contentUrl">
              </a>
              <figcaption itemprop="caption">
                <p>Приемочный бункер фасовки с пультом управления</p>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <figure itemscope itemtype="http://schema.org/ImageObject">
              <figcaption itemprop="caption">
                <h3>Отапливаем помещения и работаем круглогодично</h3>
              </figcaption>
              <a href="/upload/medialibrary/37d/37d3f127d3fb2c94b3e0cf6c1560dc72.jpg" class="image-open">
                <img src="/upload/medialibrary/37d/37d3f127d3fb2c94b3e0cf6c1560dc72.jpg" alt="" itemprop="contentUrl">
              </a>
            </figure>
          </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
