<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оплата");
$APPLICATION->SetTitle("Оплата");
$APPLICATION->SetPageProperty("description", "Оплата для физических и юридических лиц");
?>

<h2>Для юридических лиц безналичный расчет</h2>
<p>Оплата товара производится по&nbsp;безналичному расчету путем предоплаты на&nbsp;расчетный счет Поставщика.</p>
<p>Для получения консультации свяжитесь с&nbsp;нашим менеджером любым удобным Вам способом:</p>
<ul class="list-unordered">
	<li>по&nbsp;телефону&nbsp;+7 (921) 338-50-02;</li>
	<li>в&nbsp;Ватсапе&nbsp;+7 (921) 338-50-02;</li>
	<li>по&nbsp;электронной почте <a href="mailto:smesi@tdunicom.ru">smesi@tdunicom.ru</a>.</li>
</ul>
<p>Реквизиты для выставления счета&nbsp;Вы можете прислать на&nbsp;электронную почту <a href="mailto:smesi@tdunicom.ru">smesi@tdunicom.ru</a>.</p>
<h2>Для физических лиц оплата через &laquo;ROBOKASSA&raquo;</h2>
<p>1. Для получения консультации и&nbsp;подтверждения наличия товара свяжитесь с&nbsp;нашим менеджером любым удобным Вам способом:</p>
<ul class="list-unordered">
	<li>по&nbsp;телефону&nbsp;+7 (921) 338-50-02;</li>
	<li>в&nbsp;Ватсапе&nbsp;+7 (921) 338-50-02;</li>
	<li>по&nbsp;электронной почте <a href="mailto:smesi@tdunicom.ru">smesi@tdunicom.ru</a>.</li>
</ul>
<p>2. Добавьте товар в&nbsp;корзину.</p>
<p>3. Заполните необходимые поля при оплате.</p>
<p>4. Оплатите заказ через &laquo;ROBOKASSA&raquo;.</p>
<p>5. Сообщите менеджеру номер чека.</p>

<? /*<h3>Онлайн оплата на сайте</h3>
<p>Частное лицо может оплатить заказ только онлайн на сайте. Оплата в офисе для частных лиц не доступна.</p>

 <p>
	<a href="/pokupatelyam/oplata/">Подробнее о методах оплаты</a>
</p> */ ?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
