<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Расчет материала");
$APPLICATION->SetTitle("Расчет материала");
?>
<div class="grid">
    <form class="calculate-form form grid grid__cell grid__cell--l--4 grid__cell--m--6 grid__cell--s--8 grid__cell--xs--12">

        <label class="form-input form-input--select grid__cell grid__cell--xs--12">
            <span class="form-input__label">Выберите продукт</span>
            <select class="form-input__field" name="consumption">
                <? foreach (getProductsForCalculation() as $product): ?>
                    <option value="<?= $product["CONSUMPTION"] ?>"
                    ><?= $product["NAME"] ?></option>
                <? endforeach ?>
            </select>
        </label>

        <label class="form-input form-input--text grid__cell grid__cell--xs--12">
            <span class="form-input__label">Площадь поверхности, м<sup>2</sup></span>
            <input class="form-input__field" type="number" name="area" min="1" value="1" required>
        </label>

        <label class="form-input form-input--select grid__cell grid__cell--xs--12">
            <span class="form-input__label">Толщина слоя, мм</span>
            <input class="form-input__field" type="number" name="thickness" min="1" value="1" required>
        </label>

        <button class="button button--size--m grid__cell grid__cell--xs--12" type="submit">Посчитать</button>

        <input type="hidden" name="action" value="calculation">

    </form>
</div>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');