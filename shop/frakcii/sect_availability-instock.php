<div class="availability availability_in-stock" itemscope itemtype="http://schema.org/ImageObject"><img class="availability__image" src="/images/fract/availability-in-stock.jpg" alt="Фракция в наличии" itemprop="contentUrl" />
              <div class="availability__info" itemprop="caption">
                <div class="availability__title" itemprop="name">Фракция в наличии</div>
                <div class="availability__text" itemprop="description">Можем отгрузить сразу после оплаты</div>
              </div>
            </div>
