<?
class Menu_Cest
{
    /** @var AcceptanceTester */
    protected $tester;

    public function testMenu()
    {
		$this->tester->amOnPage("/");

		$expected = file_get_contents(__DIR__."/../_data/expected_menu.html");
		$expected = self::j($expected);

		$actual = $this->tester->grabFragmentSource("nav.header-navigation");
		$actual = self::j($actual);

		$this->tester->assertEquals($expected, $actual);
    }

    // Justify
    protected static function j(string $string): string
    {
        $string = html_entity_decode($string);
        $string = str_replace(' ', ' ', $string);
        $string = str_replace('–', '-', $string);
        $string = str_replace(["\n", "\r", "\n\r", "\r\n", "\t"], '', $string);
		$string = preg_replace('#\s{1,}#', ' ', $string);

		return $string;
    }

    public function _before(AcceptanceTester $I){$this->tester = $I;}
}
