<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Доставка песка осуществялется нашим собственным транспортом по Санкт-Петербургу и Ленинградской области. Возможность доставки в ваш район можно узнать, связавшись с консультантом по телефону: +7 (812) 414-97-93.");
$APPLICATION->SetPageProperty("TITLE", 'Доставка кварцевого песка Санкт-Петербурге и Ленинградской области');
$APPLICATION->SetTitle("Доставка");
?>
<div class="block-wrap block-wrap_wrap">
    <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
        <p>
             Доставляем по Северо-Западному региону, Санкт-Петербургу и Ленинградской области.
        </p>
        <p>
             Работаем с частными и юридическими лицами.
        </p>
        <p>
             Доставка кварцевого песка и гравия возможна для опта и розницы от 1&nbsp;тонны.
        </p>
        <p>
             Осуществляем доставку любыми видами автомобильного транспорта, в том числе манипуляторами и цементовозами.
        </p>
        <h2> Стоимость доставки</h2>
        <p>
             Возможность и стоимость доставки кварцевого песка и гравия в ваш район можно узнать, связавшись с нашими менеджерами по телефонам: <strong>+7 (812) 414-97-93</strong> (для юр. лиц и ИП) или <strong>+7 (812) 414-97-82</strong> (для физ. лиц)
        </p>
<br>
        <h2>Доставка собственным автотранспортом</h2>
        <div class="block-wrap block-wrap_wrap">
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Длинномер (шаланда)" src="/images/dostavka/dlinnomer.jpg" title="Длинномер (шаланда)" class="mw100" itemprop="contentUrl"><br>
      <span itemprop="name">Длинномер (шаланда)</span> <br>
      <span style="font-size: 14px;" itemprop="description">(20 тонн (20 биг-бегов))</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Бортовая шаланда" src="/images/dostavka/bortovaya-shalanda.jpg" title="Бортовая шаланда" itemprop="contentUrl"><br>
      <span itemprop="name">Бортовая шаланда</span> <br>
      <span style="font-size: 14px;" itemprop="description">(до 20 тонн)</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Бортовой грузовик" src="/images/dostavka/bortovoj-gruzovik.jpg" title="Бортовой грузовик" itemprop="contentUrl"><br>
      <span itemprop="name">Бортовой грузовик</span> <br>
      <span style="font-size: 14px;" itemprop="description">(1-3 тонны)</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="" src="/images/dostavka/bortovoj-kamaz.jpg" title="" itemprop="contentUrl"><br>
      <span itemprop="name">Бортовой камаз</span> <br>
      <span style="font-size: 14px;" itemprop="description">(до 10 тонн)</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Манипулятор" src="/images/dostavka/manipulyator-1.jpg" title="Манипулятор" itemprop="contentUrl"><br>
      <span itemprop="name">Манипулятор</span> <br>
      <span style="font-size: 14px;" itemprop="description">(1–5 тонн)</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Манипулятор" src="/images/dostavka/manipulyator-2.jpg" title="Манипулятор" itemprop="contentUrl"><br>
      <span itemprop="name">Манипулятор</span> <br>
      <span style="font-size: 14px;" itemprop="description">(до 10 тонн)</span>
    </p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <p itemscope itemtype="http://schema.org/ImageObject">
      <img alt="Цементовоз" src="/images/dostavka/cementovoz.jpg" title="Цементовоз" itemprop="contentUrl"><br>
      <span itemprop="name">Цементовоз</span> <br>
      <span style="font-size: 14px;" itemprop="description">(от 20 кубов)</span>
    </p>
  </div>
        </div>
    </div>
</div>

<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
