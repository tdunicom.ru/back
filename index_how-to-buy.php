<div class="block-wrap how-to-buy block-wrap_wrap">
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Заявка</div>
      <div class="how-to-buy__count">01</div>
    </div>
    <p class="how-to-buy__info">Для уточнения стоимости отправьте <a href="#cost-calculation">заявку на&nbsp;расчет стоимости с&nbsp;сайта</a> или позвоните по телефону: </p>
    <div class="how-to-buy__phone">
        <p>+7 (812) <span>414-97-93</span> <small style="font-size: 0.875rem; font-weight: normal">для юр. лиц и ИП</small></p>
        <p>+7 (812) <span>414-97-82</span> <small style="font-size: 0.875rem; font-weight: normal">для физ. лиц</small></p>
    </div>
    <p class="how-to-buy__info">Либо <a href="/download/zakaz_peska.xlsx">скачайте образец заказа</a> и отправьте его на почту: <a class="how-to-buy__mail" href="mailto:mainsale@tdunicom.ru">mainsale@tdunicom.ru</a></p>
    <p class="how-to-buy__info">В ответе мы уточним наличие и сроки поставки.</p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Оплата</div>
      <div class="how-to-buy__count">02</div>
    </div>
    <p class="how-to-buy__info">Работаем с&nbsp;<strong>юридическими лицами и&nbsp;ИП</strong> по&nbsp;договору, по&nbsp;безналичному расчтеу.</p>
    <p class="how-to-buy__info">С&nbsp;<strong>физическими лицами</strong> за&nbsp;наличный расчет (оплата через сайт).</p>
    <p class="how-to-buy__info">Для определения стоимости, выставления счета и&nbsp;подписания договора пришлите заявку и&nbsp;реквизиты на&nbsp;почту: <a class="how-to-buy__mail" href="mailto:mainsale@tdunicom.ru">mainsale@tdunicom.ru</a></p>
    <p class="how-to-buy__info">Предоставим <strong>индивидуальные условия</strong> для постоянных заказчиков.</p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Доставка</div>
      <div class="how-to-buy__count">03</div>
    </div>
    <p class="how-to-buy__info">Доставляем по&nbsp;<strong>Северо-Западу, Санкт-Петербургу и&nbsp;Ленинградской области</strong>!</p>
    <p class="how-to-buy__info">Доставка возможна <strong>от&nbsp;1&nbsp;тонны</strong>!</p>
    <p class="how-to-buy__info">Осуществляем доставку <strong>любыми видами автомобильного транспорта</strong>, в&nbsp;том числе манипуляторами и&nbsp;цементовозами.</p>
    <p class="how-to-buy__info"> <a href="/dostavka/">Подробнее о доставке</a></p>
  </div>
</div>
