<?
require $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
require __DIR__."/models/BaseModel.php";

$dir = new RecursiveDirectoryIterator(__DIR__."/models");
foreach (new RecursiveIteratorIterator($dir) as $file) {
	if (!is_dir($file)) {
		if( fnmatch('*.php', $file)){
			require_once $file;
		}
	}
}

try{
	if(empty($_POST["action"])){
		throw new Exception("Не передан action формы");
	}

	$modelName = camelize($_POST["action"]);

	if(!class_exists($modelName)){
		throw new Exception("Не найден обработчик формы");
	}

	$model = new $modelName($_POST);

	if($model::MAKE_IBLOCK_ELEMENT){
		$newElementId = $model->createIblockElement();
	}else{
		$newElementId = 0;
	}

	if ($GLOBALS["prod"] && $model::SEND_EMAIL) {
		$emailFields = $model->getEmailFields($newElementId);
		$emailFiles = $model->getEmailFiles($newElementId);
		$model->sendEmail($emailFields, $emailFiles);
	}

	$model->doAction();

	$result = "success";
	$message = $model->getMessage();
}catch (Exception $e){
	$result = "error";
	$message = $e->getMessage();
}finally{
	die(json_encode([
		"result" => $result,
		"message" => $message
	], JSON_UNESCAPED_UNICODE));
}