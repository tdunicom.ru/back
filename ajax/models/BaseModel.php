<?
abstract class BaseModel
{
	const SITE_CODE = "s1";
	const MAKE_IBLOCK_ELEMENT = true;
	const NAME_FIELD_NAME = "";
	const MESSAGE_FIELD_NAME = "";
	const SEND_EMAIL = true;

	public $iblockId;

	protected $messages = [];

	protected $arParamsCode;

	protected $dateForCode;
	protected $dateForUser;

	public function __construct($request)
	{
		if (empty($request)) {
			die("No data in request");
		}

		$spamHandler = new SpamHandler($request);
		if($spamMessage = $spamHandler->isHasSpam(static::NAME_FIELD_NAME, static::MESSAGE_FIELD_NAME)){
			throw new Exception($spamMessage);
		}

		$this->dateForCode = ConvertTimeStamp(time(), "FULL");
		$this->dateForUser = date("d.m.Y");

		//code generation
		$this->arParamsCode = array(
			"max_len" => 255,
			"change_case" => "L",
			"replace_space" => '-',
			"replace_other" => '-',
			"delete_repeat_replace" => true
		);

		CModule::IncludeModule("iblock");
	}

	protected function getCode($subject)
	{
		return CUtil::translit($subject, "ru", $this->arParamsCode);
	}

	public function sendEmail($emailFields, $files = [])
	{
		$duplicate = "N";
		$messageId = "";

		if(!CEvent::SendImmediate(static::EVENT_NAME, self::SITE_CODE, $emailFields, $duplicate, $messageId, $files)){
			throw new Exception("Ошибка при отправке e-mail");
		}
	}

	public function getEmailFields($newElementId)
	{
		$backendLink = "http://" . SITE_SERVER_NAME . "/bitrix/admin/iblock_element_edit.php";
		$backendLink .= "?IBLOCK_ID=" . $this->iblockId . "&type=" . static::IBLOCK_TYPE . "&ID=" . $newElementId;

		return [
			"ID" => $newElementId,
			"DATE" => $this->dateForUser,
			"ADMIN_RESULT_URL" => $backendLink
		];
	}

	public function getEmailFiles($elementId)
	{
		return [];
	}


	public function getMessage()
	{
		$message = "";

		if ($this->messages) {
			$message = implode(" - ", $this->messages);
		}

		return $message;
	}

	public function doAction(){}


}