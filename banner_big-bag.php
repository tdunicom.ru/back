    <div class="banner-home banner-home_big-bag">
      <div class="banner-home__inner">
        <div class="banner-home__main-text">
          <div class="banner-home__action">Акция</div><span>Песок для пескоструя</span><span>за 2050 за тонну в биг-бегах</span>
			<div class="banner-home__mark"><div style="text-align:right;">ГОСТ 8736, фракция 0.63-2.5</div>* При оплате по безналичному расчету с НДС.</div>
        </div>
        <div class="banner-home__countdown">
          <div class="banner-home__countdown-title">До конца акции осталось:</div>
          <div class="banner-home-countdown"></div>
        </div>
      </div>
    </div>