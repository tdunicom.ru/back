<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>








<div class="how_to_buy wrapper">
    <h2 class="subtitle">Как купить кварцевый песок в Санкт-Петербурге?</h2>

<div class="block-wrap how-to-buy block-wrap_wrap">
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Заявка</div>
      <div class="how-to-buy__count">01</div>
    </div>
    <p class="how-to-buy__info">Для уточнения стоимости - отправьте заявку на расчет стоимости с сайта или позвоните по телефону: <span class="how-to-buy__phone">+7 (812) 414-97-93.</span></p>
    <p class="how-to-buy__info">Либо <a href="/download/zakaz_peska.xlsx">скачайте образец заказа</a> и отправьте его на почту: </p>
    <p class="how-to-buy__info"> <a class="how-to-buy__mail" href="mailto:zakaz@kvarc-pesok.ru">zakaz@kvarc-pesok.ru </a></p>
    <p class="how-to-buy__info">В ответе мы уточним наличие и сроки поставки.</p><a class="btn btn_secondary how-to-buy__info" href="">Рассчитать стоимость</a>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Оплата</div>
      <div class="how-to-buy__count">02</div>
    </div>
    <p class="how-to-buy__info">Работаем по договору, по <strong>безналичному расчету</strong> с НДС и без НДС и за <strong>наличный расчет</strong>.</p>
    <p class="how-to-buy__info"><strong>Выставим счет</strong> на вашу организацию.</p>
    <p class="how-to-buy__info">Для физических лиц - <strong>предоставим реквизиты</strong>. После оплаты отправляем полностью заказ.</p>
    <p class="how-to-buy__info">Для выставления счета пришлите реквизиты на почту: </p>
    <p class="how-to-buy__info"> <a class="how-to-buy__mail" href="mailto:zakaz@kvarc-pesok.ru">zakaz@kvarc-pesok.ru</a></p>
    <p class="how-to-buy__info">Предоставим <strong>рассрочку оплаты</strong> для постоянных заказчиков.</p>
  </div>
  <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6 how-to-buy__item">
    <div class="how-to-buy__header">
      <div class="how-to-buy__title">Доставка</div>
      <div class="how-to-buy__count">03</div>
    </div>
    <p class="how-to-buy__info">Доставим кварцевый песок <strong>на собственном транспорте</strong> по Санкт-Петербургу и Ленинградской области.</p>
    <p class="how-to-buy__info">При <strong>самовывозе</strong> - погрузим на ваш транспорт.</p>
    <p class="how-to-buy__info">Удобство расположения склада (всего <strong>15 минут от КАД</strong>).</p><a class="btn btn_secondary how-to-buy__info" href="">Рассчитать стоимость доставки</a>
    <p class="how-to-buy__info"> <a href="/dostavka/">Подробнее о доставке</a></p>
  </div>
</div>

</div>