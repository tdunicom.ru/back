            <div class="availability availability_analog" itemscope itemtype="http://schema.org/ImageObject"><img class="availability__image" src="/images/fract/availability-in-stock.jpg" alt="В наличии — аналог фракции" itemprop="contentUrl"/>
              <div class="availability__info" itemprop="caption">
                <div class="availability__title" itemprop="name">В наличии другие фракции</div>
                <div class="availability__text" itemprop="description">В наличии есть схожая фракция: <a href="/graviy/frakcii/5-20-mm/">5 - 20 мм</a></div>
              </div>
            </div>
