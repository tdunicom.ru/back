<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий средний садовых дорожек дача цена купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Средний гравий по цене от производителя. Купить с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 5 - 20. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий средней фракции – купить с доставкой в Санкт-Петербурге, цены от производителя");
$APPLICATION->SetTitle("Средний гравий");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/sredniy.jpg" alt="Средний гравий" title="Средний гравий" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 10 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия средней фракции</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>На&nbsp;производстве компании &laquo;Юником&raquo; гравий средний фракции мы&nbsp;получаем в&nbsp;ходе фракционирования и&nbsp;сушки кварцевого песка.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <p>Фракция среднего гравия&nbsp;&mdash; сыпучая смесь, которую получаем после обработки кварцевого песка. Промытый и&nbsp;просушенный кварцевый песок просеиваем на&nbsp;специальном оборудовании ситами 5&ndash;10&nbsp;мм и&nbsp;получаем гравий соответствующей фракции.</p>
        <h2>Сфера использования</h2>
        <p>Основные направления использования гравия среднего размера:</p>
        <ul class="list-marked">
          <li>строительство,</li>
          <li>дорожные работы,</li>
          <li>ландшафтный дизайн,</li>
          <li>декоративная отделка.</li>
        </ul>
        <h3>Для изготовления бетонных конструкций</h3>
        <p>Гравий этой фракции применяют для производства армированных и&nbsp;неармированных конструкций из&nbsp;бетона. Подбирают материал с&nbsp;большей шероховатостью: для лучшего сцепления цементного раствора с&nbsp;наполнителем.</p>
        <h3>Дорожное строительство</h3>
        <p>Хорошие показатели морозоустойчивости и&nbsp;выдерживание механических нагрузок разной силы (истирание, дробление) позволяют использовать материал для отсыпки железнодорожных путей, автомобильных дорог, создания аэродромных покрытий.</p>
        <h3>Дизайнерские работы</h3>
        <p>Такой гравий используют в&nbsp;ландшафтном, интерьерном, экстерьерном дизайне. Для разбивки клумб, парков, создания декоративной насыпи, оформления водоемов, покрытия дорожек, засыпки площадок для отдыха, мини-пляжей.</p>

        <h2>Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
        <h2>Вопросы и ответы</h2>
        <h3 class="map_toggle toggle-by faq-item-title">Что такое гравий средней фракции и каковы его основные характеристики?</h3>
        <p class="map toggle-me faq-item-text">Гравий средней фракции — это натуральный сыпучий материал, получаемый в результате обработки кварцевого песка. Он имеет размер частиц от 5 до 10 мм и используется в различных строительных и ландшафтных проектах. Гравийный материал обладает высоким качеством и прочностью, что делает его идеальным для укладки дорожек, создания клумб и декоративных элементов. Природный гравий может быть представлен в различных цветах, включая серый, белый, красный и жёлтый.</p>

        <h3 class="map_toggle toggle-by faq-item-title">В каких сферах применяется гравий средней фракции?</h3>
        <p class="map toggle-me faq-item-text">Гравий средней фракции широко используется в строительстве, дорожных работах и ландшафтном дизайне. Он идеально подходит для создания садовых дорожек, укладки на площадках для отдыха и оформления клумб. Благодаря своим свойствам, он также применяется для укрепления почвы и создания декоративных насыпей. Гравий для благоустройства участка становится все более популярным благодаря своей универсальности.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какова цена на гравий средней фракции?</h3>
        <p class="map toggle-me faq-item-text">Цена на гравий средней фракции зависит от объема заказа и типа материала. Мы предлагаем конкурентоспособные цены на всю нашу продукцию. Например, стоимость составляет 1000 рублей за тонну. Для получения точной информации о стоимости и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашим менеджером.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка гравия на участок?</h3>
        <p class="map toggle-me faq-item-text">Мы осуществляем доставку гравия по всей территории Санкт-Петербурга и области. У нас есть собственный автопарк техники, что позволяет нам быстро доставлять заказы в течение дня. Доставка осуществляется различными машинами в зависимости от объема заказа. Мы можем предложить покупку среднего гравия с доставкой прямо на ваш участок.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какие сертификаты качества есть у вашего гравия?</h3>
        <p class="map toggle-me faq-item-text">Наш гравий имеет все необходимые сертификаты качества, подтверждающие его соответствие стандартам. Мы контролируем качество на всех этапах добычи и обработки материала, чтобы гарантировать высокие характеристики нашей продукции. Это особенно важно для клиентов, использующих гравий для создания садовых дорожек и других ландшафтных работ.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какой размер частиц у вашего гравия средней фракции?</h3>
        <p class="map toggle-me faq-item-text">Гравий средней фракции включает частицы размером от 5 до 10 мм. Этот размер обеспечивает отличное сцепление с другими материалами, такими как цемент или песок. Мелкий и средний гравий идеально подходит для различных строительных задач, включая укладку дорожек и создание декоративных элементов.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как правильно укладывать гравий средней фракции?</h3>
        <p class="map toggle-me faq-item-text">Правильная укладка гравия требует тщательной подготовки основания. Важно следовать всем правилам технологии укладки, чтобы обеспечить долговечность конструкции. После укладки необходимо провести уплотнение слоя гравия для достижения оптимальных свойств поверхности.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на гравий средней фракции?</h3>
        <p class="map toggle-me faq-item-text">Чтобы купить гравий средней фракции оптом, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
