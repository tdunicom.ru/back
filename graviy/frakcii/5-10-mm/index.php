<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий фракция 5 - 10 мм цена м3 купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Гравий 5 - 10 мм. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 5 - 10. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий 5 - 10 мм: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий 5 - 10 мм");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/fract-graviy/gravii-5-10.jpg" alt="Размер: 5 - 10 мм" title="Размер: 5 - 10 мм" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия 5 - 10</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>

          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_availability-analog.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Применение гравия 5 - 10</h2>
        <h3>Строительство </h3>
        <p>Гравий 5-10 нашел применение преимущественно в строительной области. Чаще из него производят бетон и бетонные изделия. Причем, из этого материала получаются бетонные конструкции прочных марок. </p>
        <p>Также из гравия 5-10 возводят дорожные покрытия. Например, производят отсыпку верхней части асфальта. Немалую роль материал играет и на этапе выполнения фундаментных работ. </p>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
        <h2>Вопросы и ответы</h2>
        <h3 class="map_toggle toggle-by faq-item-title">Что такое гравий 5-10 мм и каковы его основные характеристики?</h3>
        <p class="map toggle-me faq-item-text">Гравий 5-10 мм — это натуральный сыпучий материал, который используется в строительстве и ландшафтном дизайне. Он состоит из различных пород камней, включая гранитный и известняковый, что обеспечивает ему высокую прочность и долговечность. Гравийный материал хорошо подходит для создания дорожек, фундамента и других строительных объектов благодаря своим характеристикам и высокой устойчивости к внешним воздействиям.</p>

        <h3 class="map_toggle toggle-by faq-item-title">В каких сферах применяется гравий 5-10 мм?</h3>
        <p class="map toggle-me faq-item-text">Гравий 5-10 мм широко используется в строительстве, включая дорожные работы, создание фундаментов и укладку декоративных элементов. Он также отлично подходит для ландшафтного дизайна, где может использоваться для оформления садов и парков. Гравий для строительства становится все более популярным благодаря своей универсальности.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какова цена на гравий от производителя?</h3>
        <p class="map toggle-me faq-item-text">Цена на гравий 5-10 мм зависит от объема заказа и типа материала. Мы предлагаем конкурентоспособные цены на всю нашу продукцию. Например, стоимость составляет от 1000 рублей за кубический метр (м³). Для получения точной информации о стоимости и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашим офисом.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка гравия?</h3>
        <p class="map toggle-me faq-item-text">Мы осуществляем доставку гравия по всей территории Санкт-Петербурга и области. У нас есть собственный автопарк самосвалов и спецтехники, что позволяет нам быстро доставлять заказы в течение дня. Доставка осуществляется различными машинами в зависимости от объема заказа, что обеспечивает оперативное выполнение заказов.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какие сертификаты качества есть у гравия?</h3>
        <p class="map toggle-me faq-item-text">Наш гравий имеет все необходимые сертификаты качества, подтверждающие его соответствие стандартам. Мы контролируем качество на всех этапах добычи и обработки материала, чтобы гарантировать высокие характеристики нашей продукции. Это особенно важно для клиентов, использующих гравий для создания фундаментов и других строительных работ.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какой размер частиц у гравия?</h3>
        <p class="map toggle-me faq-item-text">Гравий включает частицы размером от 5 до 10 мм. Этот размер обеспечивает отличное сцепление с другими материалами, такими как цемент или песок. Гравий хорошо подходит для различных строительных задач благодаря своей универсальности.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как правильно укладывать гравий 5-10 мм?</h3>
        <p class="map toggle-me faq-item-text">Правильная укладка гравия требует тщательной подготовки основания. Важно следовать всем правилам технологии укладки, чтобы обеспечить долговечность конструкции. После укладки необходимо провести уплотнение слоя гравия для достижения оптимальных свойств поверхности.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на гравий?</h3>
        <p class="map toggle-me faq-item-text">Чтобы купить гравий 5-10 мм оптом, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>

</div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
