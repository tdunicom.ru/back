<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить машину гравия. Цены от производителя, с доставкой по Санкт-Петербургу и области. Сколько стоит камаз гравия. Мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Машина (камаз) гравия: цена от производителя, сколько стоит купить машину гравия с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Машина гравия");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/mashina-graviya.jpg" alt="Машина гравия" title="Машина гравия" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Виды машин и их объемы</h2>
        <p>Используют самосвалы разной грузоподъемности.</p>
        <p>Вместительность машин:</p>
        <ul class="list-marked">
          <li>в ЗИЛ входит 5 м3 гравия</li>
          <li>в КамАЗ – 10 м3</li>
          <li>в Тонар – 20 м3.</li>
        </ul>
        <h2>Цена для различных машин</h2>
        <p>Цена машины гравия зависит от:</p>
        <ul class="list-marked">
          <li>фракционного состава смеси</li>
          <li>вместительности транспортного средства</li>
        </ul>
        <h3>Рассчитаем цену машин гравия различных моделей</h3>
        <p>Объем, который вмещает машина, умножают на стоимость куба.</p>
        <p>1) Сколько стоит КамАЗ гравия?</p>
        <p>Поскольку стоимость одной тонны составляет 300 руб., а один м<sup>3</sup> гравия (фракционного состава от 5 до 20 мм) – это 1,36 тонн, в КамАЗ входит 10 м<sup>3</sup>, то получается, что цена КамАЗА гравия составит 9000 рублей.</p>
        <p>2) купить машину гравия марки ЗИЛ можно по стоимости 3400 рублей</p>
        <p>3) машина марки Тонар будет стоить – 13 600 рублей.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/big-bag-1-ton.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>

      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
        <h2>Вопросы и ответы </h2>
        <h3 class="map_toggle toggle-by faq-item-title">Какие виды гравия предлагает ваша компания?</h3>
        <p class="map toggle-me faq-item-text">Наша компания предлагает широкий ассортимент гравия, включая гранитный, известняковый и речной. Мы также предоставляем вторичный гравий, который является отличным выбором для экологически чистого строительства. Все наши продукты проходят строгий контроль качества, что обеспечивает высокое качество для всех ваших строительных нужд. В нашем ассортименте также есть гравийный бой и намывной гравий для различных применений.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какова вместимость КамАЗа для перевозки гравия?</h3>
        <p class="map toggle-me faq-item-text">КамАЗ вмещает до 10 м³ гравия, что делает его идеальным выбором для крупных строительных проектов. Мы также предоставляем услуги по вывозу гравия с использованием машин различной грузоподъемности, включая ЗИЛ и Тонар. Это позволяет выбрать наиболее подходящий вариант в зависимости от объема необходимых материалов, включая щебень и пгс (песчано-гравийные смеси).</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как рассчитать стоимость доставки гравия?</h3>
        <p class="map toggle-me faq-item-text">Стоимость доставки гравия зависит от объема и типа материала. Например, цена за 1 м³ гравия составляет около 900 рублей. Для точного расчета стоимости вы можете обратиться к нашему прайсу, который доступен на сайте. Мы предлагаем конкурентоспособные цены на все виды продукции, включая щебень, отсев и другие строительные материалы.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как быстро можно получить гравий?</h3>
        <p class="map toggle-me faq-item-text">Мы гарантируем быстрый вывоз и доставку гравия в течение одного рабочего дня после оформления заказа. У нас есть собственный транспорт, что позволяет нам контролировать сроки поставки и обеспечивать своевременное выполнение всех обязательств перед клиентами. Вы можете оставить заявку на сайте или позвонить нам для уточнения деталей.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какие услуги по доставке вы предлагаете?</h3>
        <p class="map toggle-me faq-item-text">Мы предлагаем полный спектр услуг по доставке нерудных материалов, включая аренду машин для перевозки гравия. Наши услуги включают доставку как для юридических лиц, так и для физических лиц с возможностью безналичного расчета. Мы также осуществляем самовывоз из нашего склада, расположенного всего в 15 минутах от КАД.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какой тип грунта лучше использовать с вашим гравием?</h3>
        <p class="map toggle-me faq-item-text">Для оптимального использования нашего гравия рекомендуется выбирать грунт с хорошей дренажной способностью. Например, намывной грунт или супесь идеально подходят для создания прочных оснований. Использование качественных материалов обеспечивает долговечность и надежность конструкций в процессе строительства.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Каковы преимущества использования гранитного гравия?</h3>
        <p class="map toggle-me faq-item-text">Гранитный гравий обладает высокой прочностью и долговечностью, что делает его идеальным выбором для бетонных конструкций и дорожных работ. Он устойчив к внешним воздействиям и не теряет своих свойств со временем. Это делает его отличным вариантом для различных строительных проектов, включая укладку дорожек и создание фундаментов.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на машину с гравием?</h3>
        <p class="map toggle-me faq-item-text">Чтобы оформить заказ на машину с гравием, вы можете оставить заявку на нашем сайте или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о доступных продуктах и условиях доставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какие дополнительные материалы можно заказать вместе с гравием?</h3>
        <p class="map toggle-me faq-item-text">Кроме гравия, мы предлагаем различные строительные материалы, такие как керамзит, кирпичи и бетонные смеси. Это позволяет вам получить все необходимые компоненты для вашего проекта в одном месте. Мы также можем предложить услуги по доставке этих материалов вместе с гравием.</p>
</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
