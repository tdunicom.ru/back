<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "");
$APPLICATION->SetPageProperty("description", "Гравий для бетона. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Гравий для бетонных конструкций - мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для бетона: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для бетона");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/graviy-dlya-betona-1.jpg" alt="Гравий для бетона" title="Гравий для бетона" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для бетона</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <p>Гравий для бетона&nbsp;&mdash; это сыпучая смесь, выступающий в&nbsp;роли заполнителя для бетонной смеси, влияющего на&nbsp;ее&nbsp;прочность и&nbsp;прочие эксплуатационные и&nbsp;технические параметры.</p>
        <p><img src="/images/graviy/graviy-dlya-betona-2.jpg" alt="" /></p>
        <h2>Виды применения</h2>
        <p>Для создания бетона используют такой гравий:</p>
        <ul class="list-marked list-marked--w-prev-par">
          <li>горный,</li>
          <li>морской,</li>
          <li>озерный,</li>
          <li>речной.</li>
        </ul>
        <p>Лучший выбор&nbsp;&mdash; горный гравий, его шероховатая поверхность обеспечивает лучшее сцепление. У&nbsp;остальных видов поверхность гладкая, потому сильно влияет на&nbsp;прочностные показатели. </p>
        <p>Существует и&nbsp;другая классификация, куда, кроме гравия, входит: гранитный, шлаковый, известняковый и&nbsp;вторичный щебень.</p>
        <p><img src="/images/graviy/graviy-dlya-betona-3.jpg" alt="" /></p>
        <h2>Как выбрать?</h2>
        <ol class="list-numbered">
          <li>Учесть назначение и&nbsp;класс бетона: для фундамента выбирают гранитный щебень, в&nbsp;том числе, когда марка бетонной конструкции должна быть от&nbsp;М300, для всех остальных целей подойдет гравий.</li>
          <li>Обратить внимание на&nbsp;фракцию: оптимальное значение&nbsp;&mdash; 5-20&nbsp;миллиметров, если взять больше, то&nbsp;крупные зерна могут заполнить состав неравномерно.</li>
          <li>Для получения бетона высокой прочности, смешивают две фракции в&nbsp;диапазоне от&nbsp;5&nbsp;до&nbsp;40&nbsp;мм: за&nbsp;счет плотного прилегания повышается качество готового продукта.</li>
          <li>Учитывают лещадность: оптимальная форма&nbsp;&mdash; кубическая, с&nbsp;добавлением не&nbsp;более&nbsp;10% плоских камушков. Чем больше лещадных частичек, тем лучшие дренажные свойства приобретает бетон, и&nbsp;становится пригоден для бетонирования дорожек, площадок.</li>
        </ol>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6">
            <table class="table-price">
              <tr>
                <th>Применение</th>
                <th>Фракции, мм</th>
              </tr>
              <tr>
                <td>Больше подходит для декорирования, поскольку не&nbsp;имеет необходимых параметров прочности</td>
                <td>1-2,5</td>
              </tr>
              <tr>
                <td>Для построек среднего размера</td>
                <td>2,5-5,0</td>
              </tr>
              <tr>
                <td>Для небольших строений</td>
                <td>5-20</td>
              </tr>
              <tr>
                <td>Для фундамента крупных построек</td>
                <td>20-40</td>
              </tr>
              <tr>
                <td>Для многоэтажных, промышленных зданий</td>
                <td>более 40</td>
              </tr>
            </table>
          </div>
        </div>

        <h2>Преимущества использования гравия для бетона</h2>
        <ul class="list-marked">
          <li>практически полное отсутствие радиоактивности, по&nbsp;сравнению с&nbsp;гранитом,</li>
          <li>достаточная прочность,</li>
          <li>хорошие показатели морозостойкости,</li>
          <li>невысокая стоимость,</li>
          <li>удобен и&nbsp;легок в&nbsp;использовании, если готовить бетон вручную,</li>
          <li>долговечность, позволяющая использовать материал практически повсеместно.</li>
        </ul>
        <h2>Расход гравия</h2>
        <p>Расход сопоставляют с&nbsp;объемом готового раствора. На&nbsp;1&nbsp;куб.&nbsp;м&nbsp;бетона нужно такое&nbsp;же количество наполнителя.</p>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Как купить?</h2>
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
        <h2>Вопросы и ответы</h2>
        <h3 class="map_toggle toggle-by faq-item-title">Что такое гравий для бетона и каковы его основные характеристики?</h3>
        <p class="map toggle-me faq-item-text">Гравий для бетона — это натуральный сыпучий материал, который используется в строительстве для создания прочных бетонных смесей. Он состоит из различных пород камней и имеет высокую прочность, что делает его идеальным для использования в строительных работах. Гравийные смеси могут включать отсев и крошку, что позволяет улучшить характеристики бетона. Гравий для бетонной смеси часто применяется в различных строительных проектах, включая фундаменты и дорожные покрытия.</p>

        <h3 class="map_toggle toggle-by faq-item-title">В каких сферах применяется гравий для бетона?</h3>
        <p class="map toggle-me faq-item-text">Гравий используется в различных сферах строительства, включая дорожное строительство, создание фундаментов и заливку бетонных полов. Он также применяется в производстве железобетонных изделий и других строительных материалов. Благодаря своим свойствам, гравий является универсальным товаром, который подходит для различных строительных проектов, таких как гравий для дорог и гравий для отмостки.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какова цена на гравий для бетона?</h3>
        <p class="map toggle-me faq-item-text">Цена на гравий зависит от объема заказа и типа материала. Мы предлагаем конкурентоспособные цены на всю нашу продукцию, включая гравий с доставкой на участок и гравий для строительных работ. Для получения точной информации о стоимости и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашим менеджером.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка гравия для бетона?</h3>
        <p class="map toggle-me faq-item-text">Мы осуществляем доставку гравия по всей территории Санкт-Петербурга и области. У нас есть собственный автопарк техники, что позволяет нам быстро доставлять заказы в течение дня. Доставка осуществляется различными машинами в зависимости от объема заказа. Мы можем доставить гравий в мешках или большими партиями на ваш участок.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какие сертификаты качества есть у вашего гравия?</h3>
        <p class="map toggle-me faq-item-text">Наш гравий имеет все необходимые сертификаты качества, подтверждающие его соответствие стандартам. Мы контролируем качество на всех этапах добычи и обработки материала, чтобы гарантировать высокие характеристики нашей продукции. Это особенно важно для клиентов, использующих гравий для бетонных работ.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Какой размер частиц у вашего гравия для бетона?</h3>
        <p class="map toggle-me faq-item-text">Мы предлагаем различные фракции гравия, включая мелкие (до 5 мм) и крупные (до 20 мм) частицы. Размер частиц влияет на характеристики бетона: мелкие фракции обеспечивают лучшую укладку, а крупные — повышают прочность конструкции. Также у нас есть отсевы от дробления камня, которые могут использоваться в различных строительных работах.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как правильно укладывать гравий для бетона?</h3>
        <p class="map toggle-me faq-item-text">Правильная укладка гравия требует тщательной подготовки основания. Важно следовать всем правилам технологии укладки, чтобы обеспечить долговечность конструкции. После укладки необходимо провести уплотнение слоя гравия для достижения оптимальных свойств.</p>

        <h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на гравий для бетона?</h3>
        <p class="map toggle-me faq-item-text">Чтобы купить гравий для бетона оптом, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
