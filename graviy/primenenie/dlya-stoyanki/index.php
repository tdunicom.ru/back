<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "гравий щебень стоянка парковка цена купить мытый округлый без пыли от производителя доставка биг-беги мешки санкт-петербург");
$APPLICATION->SetPageProperty("description", "Гравий или щебень для стоянки или парковки. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли. Фасовка: в биг-бегах");
$APPLICATION->SetPageProperty("TITLE", "Гравий для стоянки или парковки: цены от производителя, купить с доставкой в Санкт-Петербурге");
$APPLICATION->SetTitle("Гравий для стоянки и парковки");
?>
    <section class="container page-section">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/graviy/gravii-dlya-parkovki-1.jpg" alt="Гравий для парковки" title="Гравий для парковки" itemprop="contentUrl"/>
              <figcaption class="title-image__name" itemprop="caption">Размер: 5 - 20 мм</figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Цена гравия для парковки</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>За тонну</th>
                <td>1000 руб.</td>
              </tr>
            </table>
    </div>
            <div class="small">* Обратите внимание: мы продаем гравий только в биг-бегах.</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/graviy/sect_calc-anchor.php"
			)
		);?>
    <section class="container page-section">
      <div class="content">
        <h2>Почему гравий используется для стоянок и парковок?</h2>
        <ol class="list-numbered">
          <li>У гравия, высокая пропускная способность. Камешки равномерно замерзают и оттаивают. Поэтому осадки не задерживаются.</li>
          <li>Гравий устойчив к износу и долговечен.</li>
          <li>Значительно дешевле других щебня.</li>
        </ol>
        <h2>Какой вид горных пород и какие фракции лучше использовать для стоянки?</h2>
        <p>Для обустройства парковки берут гравий, фракцией от 5-60 мм.</p>
        <p>Выбор такого материала — экономный и практичный вариант, особенно, если почва на участке подвержена вспучиванию. </p>
        <p>Подходит два вида материла:</p>
        <ul class="list-marked">
          <li><strong>Гравий</strong>. Для верхнего слоя площадок для стоянок берут материал с размером зерен 5-20 мм, а для нижнего — 30-60 мм. Мелкий гравий заполняет пустоты, выравнивает поверхность. Пропускает воду, не слеживается и не крошится.</li>
          <li><strong>Известковая щебенка</strong>. После дождя нижний слой быстро превратится в порошок, а потом в грязь. Потому нужно сразу продумать водоотвод, а также подумать о достаточной прочности сырья.</li>
        </ul>
        <h2>Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="figure-mark" itemscope itemtype="http://schema.org/ImageObject"><img class="figure-mark__image" src="/images/graviy/6_bigbag-stein_0.jpg" alt="В биг-бегах" title="В биг-бегах" itemprop="contentUrl"/>
              <figcaption class="figure-mark__caption" itemprop="caption">
                <div class="figure-mark__title" itemprop="name">В биг-бегах</div>
                <div class="figure-mark__desc" itemprop="description">по 1 тонне</div>
              </figcaption>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6 page-section page-section_list-advantages page-section_inner">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"AREA_FILE_SUFFIX" => "how-to-buy",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/graviy/sect_advantages.php",
							)
						);?>
          </div>
        </div>
      </div>
    </section>
    <section class="container page-section">
      <div class="content">
        <h2>Расчет количества гравия для парковки</h2>
        <p>Чтобы определить нужный объем сырья, воспользуйтесь формулой.</p>
        <p>Объем = площадь х толщина слоя *</p>
        <p>* В случае обратной засыпки, под слой песка в 10 см, при нагрузке средней интенсивности, понадобится 15-20 см гравия.</p>
        <p>По нормативам, для подсчета объемов материала для покрытий указан коэффициент уплотнения, который равен 1,3. Но все же лучше предусмотреть небольшой запас: на случай возможного проседания.</p>
<h3>Пример расчёта стоимости</h3>
<p>Возьмём стандартную парковочную площадку площадью 12,5&nbsp;м<sup>2</sup> (5&nbsp;м &times; 2,5&nbsp;м). На её примере расчитаем стоимость матриалов для 1м<sup>2</sup> гравийного покрытия</p>
<div class="block-wrap">
  <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width8">
    <table class="table-price">
      <tr>
        <th>Материал</th>
        <th>Количество</th>
        <th>Цена</th>
        <th>Сумма</th>
      </tr>
      <tr>
        <td>Бетонный бордюр, 1&nbsp;м</td>
        <td>12 штук</td>
        <td>300 руб.</td>
        <td>4500 руб.</td>
      </tr>
      <tr>
        <td>Георешётка 694&nbsp;мм &times; 400&nbsp;мм</td>
        <td>7 штук</td>
        <td>130 руб.</td>
        <td>910 руб.</td>
      </tr>
      <tr>
        <td>Геотекстиль*, ширина 1&nbsp;м</td>
        <td>15&nbsp;м</td>
        <td>40 руб.</td>
        <td>16 руб.</td>
      </tr>
      <tr>
        <td>Гравий, фракция 2-5&nbsp;мм</td>
        <td>1,54 т</td>
        <td>3800 руб.</td>
        <td>5852 руб.</td>
      </tr>
      <tr>
        <td>Итого</td>
        <td></td>
        <td></td>
        <td>11&nbsp;862 руб.</td>
      </tr>
      <tr>
        <td>За квадратный метр</td>
        <td></td>
        <td></td>
        <td>950 руб.</td>
      </tr>
    </table>
  </div>
</div>

<p class="small">* Полотнища геотекстиля укладываются внахлёст &mdash; учитывайте это при подсчёте нужного количества.
<br>
** Насыпать гравий нужно слоем 7&nbsp;см (0,07&nbsp;м). С учётом объёмного веса 1,6&nbsp;т&nbsp;/&nbsp;куб и 10% на трамбовку для площадки 12,5&nbsp;м<sup>2</sup> нам понадобится: 12,5 &times; 0,07 &times; 1,6 &times; 1,1 = 1,54&nbsp;т.</p>

        <h2>Как сделать стоянку: этапы</h2>
        <h3>Выбираем место</h3>
        <p>Уровень над дорогой: оборудовать стоянку лучше выше нее.</p>
        <h3>Делаем разметку и боремся с сорняками</h3>
        <p>Определившись с размерами, сделайте прямоугольную разметку. После снимите плодородный почвенный слой.</p>
        <h3>Подготовительные работы</h3>
        <p>Если место под парковку широкое, то нужно избежать «растекания» гравия. Для этого предварительно сделать заливку, либо бордюр по периметру.</p>
        <h3>Переходим к возведению площадки</h3>
        <p itemscope itemtype="http://schema.org/ImageObject"><img src="/images/graviy/gravii-dlya-parkovki-2.jpg" alt="Структура гравийной парковки" title="Структура гравийной парковки" itemprop="contentUrl"></p>
        <h4>Выравниваем поверхность</h4>
        <p>Покрываем песчаным слоем, утрамбовываем, попутно формируя уклоны, необходимые для схода воды к дороге и на края парковочного места.</p>
        <h4>Выбираем геотекстиль</h4>
        <p>Плотность материала должно быть не менее 200 г/м<sup>2</sup>. Такой геотекстиль справится с сорняками , не даст гравию для стоянки, насыпанному поверх «нетканки», «утонуть».</p>
        <h4>Укладываем щебень</h4>
        <p>Теперь уложите слой гравий (толщина 15-20 см). Нижний слой, с целью экономии, вымостите вторичным щебнем.</p>
        <h2>Как купить?</h2>


				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/graviy/index_how-to-buy.php"
					)
				);?>
</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
