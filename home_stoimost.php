<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die(); ?>



<div class="how_to_buy wrapper">

    <h2 class="subtitle">Цена на песок</h2>
    <p style="font-size: 16px;">Кварцевый песок валом - 1500 руб. за тонну.</p>
    <p style="font-size: 16px;">Кварцевый песок в биг-бегах - 1800 руб. за мешок.</p>

    <h2 class="subtitle">Как купить кварцевый песок в Санкт-Петербурге?</h2>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">01</div>
            <span>Заявка</span>
        </div>
        <div class="how_to_buy_text">
            <p>Отправьте заявку с сайта или позвоните по телефону:<br/>
            <span>+7 (812) 414-97-93</span></p>
            <p>Уточните стоимость, наличие и сроки поставки кварцевого песка.</p>
        </div>
    </div>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">02</div>
            <span>Оплата</span>
        </div>
        <div class="how_to_buy_text">
            <p>Выставим счет на вашу организацию, после оплаты отправляем полностью заказ.</p>
            <p>Для выставления счета на кварцевый песок — пришлите реквизиты на почту: </p>
            <p><a href="mailto:kvarc-pesok@yandex.ru" class="email">kvarc-pesok@yandex.ru</a></p>
        </div>
    </div>

    <div class="how_to_buy_item">
        <div class="how_to_buy_title">
            <div class="how_to_buy_counter">03</div>
            <span>Доставка</span>
        </div>
        <div class="how_to_buy_text">
            <p>Доставляем кварцевый песок на собственном транспорте по Санкт-Петербургу и Ленинградской области.</p>
            <p>Проработанная система поставок с месторождений Карелии делает возможным продажу кварцевого песка в любых объемах.</p>
            <p><a href="/dostavka/">Подробнее о доставке</a></p>
        </div>
    </div>

</div>