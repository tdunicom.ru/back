<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Доставка");
$APPLICATION->SetTitle("Доставка");
$APPLICATION->SetPageProperty("description", "Доставка и способы получения товара");
?>

<h2>Способы получения заказа</h2>
<p>1. Самовывозом с&nbsp;нашего склада по&nbsp;адресу&nbsp;&mdash; Всеволожский р-н, д.Щеглово, ул.Инженерная, д.17, ангар с&nbsp;желтой вывеской &laquo;Сухие строительные смеси от производителя&raquo;.</p>
<p>2. Грузовым транспортом при согласовании нашим менеджером стоимости доставки с&nbsp;компаниями-партнерами. </p>
<p>3. Транспортными компаниями.</p>
<p>Для уточнения стоимости доставки, пожалуйста, воспользуйтесь калькулятором на&nbsp;сайте транспортной компании:</p>
<ul class="list-unordered">
	<li>
		<a href="https://gruzovichkof.ru/" target="_blank">https://gruzovichkof.ru/</a>
	</li>
	<li>
		<a href="https://spb.gazelkin.ru/" target="_blank">https://spb.gazelkin.ru/</a>
	</li>
</ul>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
