<h2>Почему у нас покупают?</h2>

<ul class="grid page-subsection">

	<li class="benefit grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">
		<h3 class="benefit__title">Выгодная цена</h3>
		<div class="benefit__body">
			<p>Нет наценки посредника за счет прямых поставок с завода производителя.</p>
		</div>
	</li>

	<li class="benefit grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">
		<h3 class="benefit__title">Ассортимент</h3>
		<div class="benefit__body">
			<p>Постоянное наличие на складе основных товарных позиций.</p>
		</div>
	</li>

	<li class="benefit grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">
		<h3 class="benefit__title">Высокие стандарты качества</h3>
		<div class="benefit__body">
			<p>Вся продукция имеет соответствующие сертификаты и паспорты качества.</p>
		</div>
	</li>

</ul>