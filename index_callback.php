<div class="container home-section home-section_home-call-back">
  <div class="content">
    <div class="block-wrap home-call-back block-wrap_wrap ">
      <div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width7 block-wrap__item_s-width6">
        <div class="home-call-back__title">Остались вопросы?</div>
        <div class="home-call-back__info">Задайте их менеджеру по телефонам<br><span class="home-call-back__strong" >+7 (812) 414-97-93</span> <span class="home-call-back__or">для юр. лиц и ИП</span><br><span class="home-call-back__strong" >+7 (812) 414-97-82</span> <span class="home-call-back__or">для физ. лиц</span><br><span class="home-call-back__or">или</span> <br class='xl-hidden l-hidden m-hidden'><span class="home-call-back__strong">оставьте данные<br></span>и мы вам перезвоним в ближайшие часы</div>
      </div>
      <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width5 block-wrap__item_s-width6">

        <form class="form home-call-back-form validated-form">
          <div class="form__input">
            <input type="text" placeholder="Ваше имя" name="name" class="validated required">
            <div class="validation-msg">Обязательное поле</div>
          </div>
          <div class="form__input">
            <input type="tel" placeholder="Телефон" name="phone" class="validated required">
            <div class="validation-msg">Обязательное поле</div>
          </div>
          <input class="btn" type="submit" value="Заказать обратный звонок">
        </form>

      </div>
    </div>
  </div>
</div>





<?/*<span class="home-call-back__strong">+7 (812) 414-97-93</span><br class='xl-hidden l-hidden m-hidden'>*/?>