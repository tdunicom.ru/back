<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("TITLE", "Страница не существует");
$APPLICATION->SetPageProperty("description", 'К сожалению запрашиваемая вами страница не найдена. Для продолжения работы с сайтом Вы можете вернуться на главную страницу или воспользоваться картой сайта');
$APPLICATION->AddChainItem("Страница не найдена");
?>
<div class="container">
  <div class="content">
    <div class="content-anons group">
        <p class="subtitle">Ошибка 404</p>
        <p>
            Вы перешли по ссылке которая не существует.<br/>
            Возможно набрали не правильно или страница удалена.
        </p>
        <p>
            С главной старницы можно найти любую инфрмцию на нашем сайте,<br>
            <a href="/">перейти на главную</a>?
        </p>
    </div>
  </div>
</div>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
