<div class="container home-section home-section_home-products">
	<div class="content">
		<h2>Применение кварцевого песка и цены</h2>
		<div class="home-products block-wrap block-wrap_wrap">
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/dlya-filtrov-ochistki-vody/">Песок для фильтров очистки воды</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Песок для фильтров очистки воды" src="../images/home-products/for-filters.jpg" title="Песок для фильтров очистки воды" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 51641</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,4 - 0,8 - <a href="/kvartseviy-pesok/frakcii/0-4-0-8-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,8 - 2 - <a href="/kvartseviy-pesok/frakcii/0-8-2-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 1 - 2 - <a href="/kvartseviy-pesok/frakcii/1-2-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/dlya-peskostruynykh-rabot/">Песок для пескоструйных работ</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Песок для пескоструйных работ" src="../images/home-products/peskostruy.jpg" title="Песок для пескоструйных работ" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 8736</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,1 - 0,3 - <a href="/kvartseviy-pesok/frakcii/0-1-0-3-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,4 - 0,8 - <a href="/kvartseviy-pesok/frakcii/0-4-0-8-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,8 - 2 - <a href="/kvartseviy-pesok/frakcii/0-8-2-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 1 - 2 - <a href="/kvartseviy-pesok/frakcii/1-2-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/dlya-sportivnykh-pokrytiy/">Песок для спортивных покрытий</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Песок для спортивных покрытий" src="../images/home-products/dlya-sportivnyih-ploschadok.jpg" title="Песок для спортивных покрытий" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 8736</span>, <span style="white-space: nowrap;">2138</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,4 - 0,8 - <a href="/kvartseviy-pesok/frakcii/0-4-0-8-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,8 - 2 - <a href="/kvartseviy-pesok/frakcii/0-8-2-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 1 - 2 - <a href="/kvartseviy-pesok/frakcii/1-2-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/dlya-sukhikh-stroitelnykh-smesey/">Песок для сухих строительных смесей</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Песок для сухих строительных смесей" src="../images/home-products/str-smesi.jpg" title="Песок для сухих строительных смесей" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 8736</span>, <span style="white-space: nowrap;">2138</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,4 - 0,8 - <a href="/kvartseviy-pesok/frakcii/0-4-0-8-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,8 - 2 - <a href="/kvartseviy-pesok/frakcii/0-8-2-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 1 - 2 - <a href="/kvartseviy-pesok/frakcii/1-2-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/formovochnyy/">Формовочный песок</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Формовочный песок" src="../images/home-products/form-pesok.jpg" title="Формовочный песок" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 2138</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,1 - 0,3 - <a href="/kvartseviy-pesok/frakcii/0-1-0-3-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/stroitelnyy/">Для строительства</a>
				</div>
				<div class="home-product__body">
					<div class="home-product__info">
						<div class="home-product__image" itemscope="" itemtype="http://schema.org/ImageObject">
							<img alt="Песок для строительства" src="../images/home-products/for-building.jpg" title="Песок для строительства" itemprop="contentUrl">
						</div>
						<div class="home-product__fractions">
							<div class="home-product__subtitle">
								Фракции <span style="white-space: nowrap;">ГОСТ 8736</span>
							</div>
							<ul>
                <li class="home-product__fraction"> 0,2 - 0,5 - <a href="/kvartseviy-pesok/frakcii/0-2-0-5-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,4 - 0,8 - <a href="/kvartseviy-pesok/frakcii/0-4-0-8-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,5 - 1 - <a href="/kvartseviy-pesok/frakcii/0-5-1-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 0,8 - 2 - <a href="/kvartseviy-pesok/frakcii/0-8-2-mm/" class="link-buy"></a></li>
                <li class="home-product__fraction"> 1 - 2 - <a href="/kvartseviy-pesok/frakcii/1-2-mm/" class="link-buy"></a></li>
							</ul>
						</div>
					</div>
					<div class="home-product__price">
						цена договорная
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/dlya-pola/">Песок для наливных полов</a>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartseviy-pesok/primenenie/stekolnyy/">Стекольный песок<br>
					<span style="white-space: nowrap;">ГОСТ 22551</span></a>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6 home-product">
				<div class="home-product__title">
					<a href="/kvartsevyy-pesok-v-big-begah/pesok-dlya-konnogo-sporta/">Песок для конного спорта</a>
				</div>
			</div>
		</div>
		<div class="home-buttons">
			<div class="home-buttons__item">
				<a class="btn image-open" href="#call-back-form-popup">Заказать звонок</a>
			</div>
			<div class="home-buttons__item">
				<a class="btn btn_secondary" href="#cost-calculation">Рассчитать стоимость</a>
			</div>
		</div>
		<h2>Фракции кварцевого песка</h2>
		<div class="block-wrap block-wrap_wrap">
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0-0,315мм" src="/images/home-fractions/gost-2138-fr-0-0,315.jpg" class="figure-mark__image" title="0-0,315мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0-0,315мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 2138
				</div>
 </figcaption> </figure>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0,16-0,315мм" src="/images/home-fractions/gost-2138-fr-0,16-0,315.jpg" class="figure-mark__image" title="0,16-0,315мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0,16-0,315мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 2138
				</div>
 </figcaption> </figure>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0-0,63мм" src="/images/home-fractions/gost-8736-fr-0-0,63.jpg" class="figure-mark__image" title="0-0,63мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0-0,63мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0,315-0,63мм" src="/images/home-fractions/gost-2138-fr-0,315-0,63.jpg" class="figure-mark__image" title="0,315-0,63мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0,315-0,63мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0-2,5мм" src="/images/home-fractions/gost-8736-fr-0-2,5.jpg" class="figure-mark__image" title="0-2,5мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0-2,5мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="0,63-2,5мм" src="/images/home-fractions/gost-8736-fr-0,63-2,5.jpg" class="figure-mark__image" title="0,63-2,5мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption figure-mark__caption--grey-corner" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0,63-2,5мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure>
			</div>
		</div>
	</div>
</div>
<br>
