<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("keywords", "сухой кварцевый песок купить цена спб опт");
$APPLICATION->SetPageProperty("description", "Купить кварцевый песок от производителя, без наценок с доставкой. Цена на сухой кварцевый песок в Санкт-Петербурге (СПб). Соответствует ГОСТу, есть сертификат.");
$APPLICATION->SetPageProperty("title", "Кварцевый песок - купить в спб, цена на песок сухой кварцевый");
$APPLICATION->SetTitle("Песок кварцевый, цветной - Цементно-песчаная смесь");
?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "top",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?/*$APPLICATION->IncludeComponent(
  "bitrix:main.include",
  ".default",
  Array(
    "AREA_FILE_RECURSIVE" => "Y",
    "AREA_FILE_SHOW" => "page",
    "AREA_FILE_SUFFIX" => "banner",
    "COMPONENT_TEMPLATE" => ".default",
    "EDIT_TEMPLATE" => ""
  )
);*/?>

    <div class="container home-section home-section_home-top-list">
      <div class="content">

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <ul class="list-marked list-marked_half">
              <li>высокое содержание <strong>кварца - более 98%</strong></li>
              <li><strong>сухой</strong> - влажность не более 0,5%</li>
              <li>температура: <strong>охлажденный</strong></li>
            </ul>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <ul class="list-marked list-marked_half">
              <li>форма зерен: <strong>округлая</strong></li>
              <li>чистота: мытый, практически с полным <strong>отсутствием посторонних примесей</strong></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "products",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "advantages",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "manufacturing",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "getprice",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "info",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
<div class="container">
  <div class="content">
    <h2>Вопросы и ответы</h2>
    <h3 class="map_toggle toggle-by faq-item-title">Что такое кварцевый песок и где его можно купить?</h3>
		<p class="map toggle-me faq-item-text">Кварцевый песок — это натуральный материал, состоящий в основном из кварца. Он широко используется в строительстве, фильтрации воды и пескоструйных работах. В России существует множество компаний, предлагающих купить кварцевый песок различных фракций и качеств. Вы можете найти его как в специализированных магазинах, так и на онлайн-платформах.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Как выбрать качественный кварцевый песок для фильтрации воды?</h3>
		<p class="map toggle-me faq-item-text">При выборе кварцевого песка для фильтрации воды важно обратить внимание на его чистоту и фракцию. Белый кварцевый песок является идеальным вариантом, так как он содержит минимальное количество примесей. Также стоит обратить внимание на поставщиков, которые предоставляют подробную информацию о своих продуктах, чтобы убедиться в их качестве.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Для каких целей используется кварцевый песок?</h3>
		<p class="map toggle-me faq-item-text">Кварцевый песок используется в различных отраслях: от строительства до очистки воды. Он незаменим для пескоструйных работ, а также для создания спортивных площадок и фильтров для очистки воды. При покупке кварцевого песка важно учитывать его предназначение, чтобы выбрать подходящий продукт.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Какие преимущества у белого кварцевого песка?</h3>
		<p class="map toggle-me faq-item-text">Белый кварцевый песок отличается высокой чистотой и прочностью, что делает его идеальным для использования в фильтрации воды и строительстве. Он также устойчив к воздействию химических веществ, что увеличивает срок службы фильтров и других конструкций. При покупке стоит выбирать проверенных поставщиков, чтобы гарантировать высокое качество продукта.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Как заказать кварцевый песок оптом?</h3>
		<p class="map toggle-me faq-item-text">Для заказа кварцевого песка оптом рекомендуется обратиться напрямую к производителям или крупным дистрибьюторам. Многие компании предлагают гибкие условия сотрудничества и доставку по всей России. Убедитесь, что у вас есть подробная информация о необходимом количестве и характеристиках песка.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Какой кварцевый песок лучше всего подходит для спортивных площадок?</h3>
		<p class="map toggle-me faq-item-text">Для спортивных площадок лучше всего использовать мелкий белый кварцевый песок, который обеспечивает отличную дренажную способность и безопасность при использовании. Такой песок позволяет избежать травм во время игр и тренировок. При покупке важно уточнять характеристики у продавца.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Как влияет качество кварцевого песка на эффективность фильтрации воды?</h3>
		<p class="map toggle-me faq-item-text">Качество кварцевого песка напрямую влияет на эффективность фильтрации воды. Чем чище и однороднее фракция, тем лучше он справляется с удалением загрязнений. Использование высококачественного белого кварцевого песка для фильтров обеспечивает лучшую очистку воды и продлевает срок службы системы.</p>

		<h3 class="map_toggle toggle-by faq-item-title">Где найти подробную информацию о кварцевом песке?</h3>
		<p class="map toggle-me faq-item-text">Подробную информацию о кварцевом песке можно найти на специализированных сайте производителя, а также в каталогах строительных материалов. Компания предоставляет технические характеристики своих товаров и советы по выбору подходящего продукта для различных нужд.</p>
  </div>
</div>
<? require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php'); ?>
