<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок фракция 1 - 2 мм: цены от производителя, купить в Санкт-Петербурге");
$APPLICATION->SetTitle("Кварцевый песок фракция 1-2 мм");
?>
    <div class="container">
      <div class="content">
        <ul class="list-anchors">
          <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
          <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
          <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
          <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
        </ul>
      </div>
    </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" alt="Размер: 1 - 2 мм" title="Размер: 1 - 2 мм" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены на кварцевый песок 1 - 2</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th>Навалом</th>
                <th>В биг-бегах</th>
              </tr>
              <tr>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
            </table>
    </div>
<a href="/tseny/">Все цены</a>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
            <div class="availability availability_none" itemscope itemtype="http://schema.org/ImageObject"><img class="availability__image" src="/images/fract/availability-out-of-stock.png" alt="Произведем фракцию под заказ" itemprop="contentUrl"/>
              <div class="availability__info" itemprop="caption">
                <div class="availability__title" itemprop="name">Произведем фракцию под заказ</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/frakcii/sect_calc-anchor.php"
			)
		);?>


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/frakcii/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/frakcii/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">
        <h2 id="primenenie">Применение кварцевого песка фракции 1-2 мм</h2>
        <ul class="list-marked list-marked_full">
          <li>
            <strong>Строительная отрасль</strong>
            <br>В качестве сухого наполнителя на изготовление материалов строительного назначения. Для возведения зданий, создания дорожного полотна, выполнения отделочных работ.
          </li>
          <li>
            <strong>Для пескоструя</strong>
          </li>
          <li>
            <strong>Для производства отделочных материалов</strong>
            <br>Интерьерные и фасадные штукатурки, краски, шпатлевки
          </li>
          <li>
            <strong>Для создания полимерных полов</strong>
          </li>
          <li>
            <strong>Производство полимерных изделий и черепицы, сварочных материалов</strong>
          </li>
          <li>
            <strong>В литейной и химической промышленности.</strong>
          </li>
          <li>
            <strong>В ландшафтном дизайне</strong>
          </li>
          <li>
            <strong>Для систем фильтрования воды</strong>
          </li>
        </ul>
        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/frakcii/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
