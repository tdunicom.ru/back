<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок фракция 0,63 - 1,25 мм: цены от производителя, купить в Санкт-Петербурге");
$APPLICATION->SetPageProperty("keywords", "кварцевый песок 0,63 - 1,25 мм цена сухой мытый округлый без пыли от производителя доставка ГОСТ навалом биг-беги мешки");
$APPLICATION->SetPageProperty("description", "Сухой кварцевый песок 0,63 - 1,25 мм. По низкой цене от производителя, с доставкой по Санкт-Петербургу и области. Мытый, округлый, без пыли, фракция 0,63 - 1,25. ГОСТ 8736-2014. Фасовка: навалом, в биг-бегах по тонне, в мешках по 25, 50 кг.");
$APPLICATION->SetTitle("Песок кварцевый 0,63-1,25 мм");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Размер: 0,63 - 1,25 мм" src="/images/fract-pesok/pesok-08-125-2138-91.gif" class="title-image__img" title="Размер: 0,63 - 1,25 мм" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			<?
        $page_gost = '8736';
        include '../sect_details.php';
      ?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на кварцевый песок 0,63 - 1,25</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20&nbsp;тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20&nbsp;тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					 ГОСТ 2138
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td style="white-space: nowrap">
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width6 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="primenenie">Сферы использования кварцевого песка фракции 0,63 - 1,25</h2>
	<ul class="list-marked list-marked_full">
		<li>
      <strong>В&nbsp;строительстве</strong><br>
      Для создания наливных полов, строительных смесей, клинкерных, звукоизоляционных термопанелей, посыпки кровельных материалов.
    </li>
		<li>
      <strong>Для фильтров</strong><br>
      Для фильтрации питьевой воды.
    </li>
		<li>
      <strong>Для пескоструйных работ</strong><br>
      Песком заполняют пескоструйные аппараты.
    </li>
		<li>
      <strong>Для промышленности</strong><br>
      Заполняют песочницы локомотивов, используют для энергокотлов кипящего слоя, применяют в&nbsp;литейной, стекольной сфере.
    </li>
		<li>
      <strong>Для спорта</strong><br>
      Для обустройства манежей, стадионов, спортивных покрытий.
    </li>
		<li>
      <strong>Для коммунальной сферы</strong><br>
      Для посыпки тротуаров, автодорог.
    </li>
	</ul>
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_how-to-buy.php"
	)
);?>
	<h2>Вопросы и ответы</h2>
	<h3 class="map_toggle toggle-by faq-item-title">Что такое кварцевый песок с фракцией 0,63 - 1,25 мм и где он используется?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок с фракцией 0,63 - 1,25 мм — это натуральный сыпучий материал, который широко используется в строительстве, фильтрации воды и ландшафтном дизайне. Он идеально подходит для создания строительных смесей, укладки тротуарной плитки и обустройства песочниц. Благодаря высокому качеству и чистоте, этот песок также используется в промышленных и коммунальных целях.</p>
		
	<h3 class="map_toggle toggle-by faq-item-title">Какова цена кварцевого песка с фракцией 0,63 - 1,25 мм?</h3>
	<p class="map toggle-me faq-item-text">Цена на кварцевый песок может варьироваться в зависимости от объема заказа и условий доставки. Обычно стоимость составляет от 800 до 1200 рублей за кубический метр. Для получения точной информации о ценах и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашим магазином.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Мы предлагаем доставку кварцевого песка по всей территории России. Доставка осуществляется в течение нескольких дней после оформления заказа. У нас есть собственный автопарк, что позволяет нам гарантировать быструю доставку и высокое качество обслуживания.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как можно оплатить заказ на кварцевый песок?</h3>
	<p class="map toggle-me faq-item-text">Оплата за кварцевый песок может быть произведена различными способами: наличными при самовывозе или банковским переводом. Мы также принимаем оплату картой через интернет, что делает процесс покупки удобным для наших клиентов.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие преимущества имеет окатанный кварцевый песок?</h3>
	<p class="map toggle-me faq-item-text">Окатанный кварцевый песок отличается высокой прочностью и однородной фракцией. Он менее пылевитый и лучше подходит для использования в строительных смесях и декоративных работах. Этот тип песка также подходит для укладки плитки и создания ландшафтных элементов.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Где купить кварцевый песок с доставкой?</h3>
	<p class="map toggle-me faq-item-text">Вы можете купить кварцевый песок в нашем магазине с доставкой на дом или в офис. Мы предлагаем широкий ассортимент строительных материалов, включая песок для различных нужд — от строительства до ландшафтного дизайна.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как выбрать качественный кварцевый песок?</h3>
	<p class="map toggle-me faq-item-text">При выборе качественного кварцевого песка обратите внимание на его чистоту и однородность фракции. Также важно наличие сертификатов качества, подтверждающих соответствие стандартам. Мы гарантируем высокое качество нашей продукции.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие изделия можно изготовить из кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок используется для производства различных изделий, включая строительные смеси, фильтры для воды и декоративные элементы для ландшафта. Он также применяется в производстве стекла и других промышленных материалов.</p>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
