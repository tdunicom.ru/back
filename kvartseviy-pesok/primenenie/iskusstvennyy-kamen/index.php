<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для искусственного камня оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Кварцевый песок для искусственного камня в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Кварцевый песок для искусственного камня Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Песок для искусственного камня");
?>
      </div>
    </div>

     <div class="container">
       <div class="content">
         <ul class="list-anchors">
           <li class="list-anchors__item"><a href="#tseny">Цены</a></li>
           <li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
           <li class="list-anchors__item"><a href="#kak-vybrat">Как выбрать?</a></li>
           <!--li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li-->
           <li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
           <li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
         </ul>
       </div>
     </div>

    <section class="container page-section">
      <div class="content">
        <div class="block-wrap block-wrap_wrap">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
            <figure class="title-image" itemscope itemtype="http://schema.org/ImageObject"><img class="title-image__img" src="/images/primenenie-peska/iskusstvennyy-kamen.jpg" alt="Песок для искусственного камня" itemprop="contentUrl"/>
            </figure>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


    <section class="container page-section">
      <div class="content">
        <h2 id="tseny">Цены песок для пескоструйных работ</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width7 block-wrap__item_s-width6">
    <div class="table-wrap">
            <table class="table-price">
              <tr>
                <th></th>
                <th>Навалом<br>(за тонну)</th>
                <th>В биг-бегах (шт.)</th>
              </tr>
              <tr>
                <td>0 - 0,63<br>ГОСТ 8736-2014*</td>
        				<td colspan="2">
        					 договорная
        				</td>
              </tr>
              <tr>
                <td>0,63 - 2,5<br>ГОСТ 8736-2014</td>
        				<td colspan="2">
        					 договорная
        				</td>
              </tr>
              <tr>
                <td>0 - 0,63<br>ГОСТ 2138-91 **</td>
        				<td colspan="2">
        					 договорная
        				</td>
              </tr>
              <tr>
                <td>Под заказ - любая фракция</td>
                <td>договорная</td>
                <td>договорная</td>
              </tr>
            </table>
    </div>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width5 block-wrap__item_l-width5 block-wrap__item_m-width5 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
							)
						);?>
            <div class="small">ГОСТ 8736-2014 * - содержание кварца около 60%, менее чистый</div>
            <div class="small">ГОСТ 2138-91 ** - содержание кварца около 98%, более чистый</div>
          </div>
        </div>
      </div>
    </section>
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
			)
		);?>


<!--section class="container page-section">
  <div class="content">
    <h2 id="primery">Примеры фракций</h2>
    <div class="flexslider-container">
      <div class="flexslider">
        <ul class="slides">
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-8736-2014.gif" alt="0–0,63 мм" title="0–0,63 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">0–0,63 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-063-25-8736-2014.gif" alt="0,63–2,5 мм" title="0,63–2,5 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">0,63–2,5 мм</div>
                <div class="figure-mark__desc">ГОСТ 8736-2014</div>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure class="figure-mark"><img class="figure-mark__image" src="/images/fract-pesok/pesok-0-063-2138-91.gif" alt="0–0,63 мм" title="0–0,63 мм"/>
              <figcaption class="figure-mark__caption">
                <div class="figure-mark__title">0–0,63 мм</div>
                <div class="figure-mark__desc">ГОСТ 2138-91</div>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section-->


    <section class="container page-section">
      <div class="content">
        <h2 id="fasovka">Фасовка</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
							)
						);?>
          </div>
          <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							".default",
							Array(
								"AREA_FILE_SHOW" => "file",
								"COMPONENT_TEMPLATE" => ".default",
								"EDIT_TEMPLATE" => "",
								"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
							)
						);?>
          </div>
        </div>
      </div>
    </section>


		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_SHOW" => "file",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
			)
		);?>




    <section class="container page-section">
      <div class="content">
        <h2 id="primenenie">Особенности создания искусственного камня</h2>
        <p>Прежде стоит сказать, что по внешнему виду искусственные камни практически не отличаются от натуральных. Но у материалов искусственного происхождения есть весомое преимущество: возможность создать имитацию под любой натуральный камень, повторив его поверхность до мелочей, в том числе и фактуру. В результате из кварцевого песка для искусственного камня и прочих компонентов, соединенных в уникальную смесь, можно получить такие варианты поверхностей:</p>
        <ul class="list-marked">
          <li>Декоративную.</li>
          <li>Произвольную, являющуюся воплощением всех задумок дизайнера относительно вида, формы и текстуры изделия.</li>
          <li>Бутовую, которая похожа на классические валуны.</li>
          <li>Колотую, характеризующуюся различными неровностями, словно ее отбивали молотком.</li>
          <li>Пиленую, для которой характерны гладкие края.</li>
        </ul>
        <p>Прежде чем закупить кварцевый песок для искусственного камня и приступить к его созданию, важно учесть, для отделки каких поверхностей планируется использование материала. Ведь можно использовать камень как элемент декора, для облицовки, создания покрытий, других целей.</p>

        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject"> <img alt="Кварцевый искусственный камень" src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-iskusstvennogo-kamnya/kvartsevyiy-iskusstvennyiy-kamen.jpg" title="Кварцевый искусственный камень" itemprop="contentUrl"> <figcaption itemprop="caption">
Кварцевый искусственный камень </figcaption> </figure>
          </div>
        </div>
        <h2>Что понадобится для создания?</h2>
        <p>Чтобы сделать искусственный камень из цементной смеси, понадобится следующее:</p>
        <ul class="list-marked list-marked_half">
          <li>вода;</li>
          <li>песок (обязательно в сухом и чистом виде);</li>
          <li>кварцевый песок белого цвета;</li>
          <li>шарики из пенопласта;</li>
          <li>картонная коробка;</li>
          <li>распылитель;</li>
          <li>емкость для красящей смеси;</li>
          <li>сеточка из проволоки небольшого размера;</li>
          <li>инструментарий для резьбы;</li>
          <li>белый портландцемент;</li>
          <li>резиновые перчатки;</li>
          <li>кисть;</li>
          <li>емкость для замешивания цементного раствора.</li>
        </ul>
        <h2>Как происходит создание?</h2>
        <div class="block-wrap  block-wrap_wrap ">
          <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
            <figure class="image-inarticle" itemscope itemtype="http://schema.org/ImageObject">
              <img src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-iskusstvennogo-kamnya/sozdanie-iskusstvennogo-kamnya.jpg" title="Создание искусственного камня" alt="Создание искусственного камня" itemprop="contentUrl">
              <figcaption itemprop="caption">
                Создание искусственного камня
              </figcaption>
            </figure>
          </div>
        </div>
        <p>Вначале создают форму будущего изделия. Например, это можно сделать так: обернуть картонную коробку сеткой из проволоки.</p>
        <p>Как только форма будет создана, на поверхность наносят смесь, в составе которой присутствует портландцемент, песок и шарики из пенопласта.</p>
        <p>Как только процесс формирования подойдет к концу, изделие покрывают тонким цементным слоем, состоящего кварцевого песка белого оттенка, воды и белого портландцемента. Такую смесь не обязательно готовить самостоятельно, поскольку в продаже можно найти уже готовую.</p>
        <p>Как только искусственный камень полностью высохнет, можно приступить к этапу окрашивания. Для этого надо надеть резиновые перчатки, налить нужное количество красителя в емкость, а потом приступить к натиранию поверхности. Толщина слоя красителя определяется в индивидуальном порядке. Если стоит цель: придать камню гладкий вид, то достаточно будет нанести краску тончайшим слоем. А вот матовую текстуру можно получить уже другим способом: нанести толстый слой в полсантиметра.</p>
        <h2 id="kak-kupit">Как купить?</h2>

				<?$APPLICATION->IncludeComponent(
					"bitrix:main.include",
					".default",
					Array(
						"AREA_FILE_SHOW" => "file",
						"AREA_FILE_SUFFIX" => "how-to-buy",
						"COMPONENT_TEMPLATE" => ".default",
						"EDIT_TEMPLATE" => "",
						"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
					)
				);?>
      </div>
    </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
