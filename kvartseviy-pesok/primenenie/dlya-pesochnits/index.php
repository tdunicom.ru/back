<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для песочницы оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Кварцевый песок для песочницы в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Кварцевый песок для песочницы Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области. ");
$APPLICATION->SetTitle("Песок для песочницы");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#svoystva">Свойства</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Кварцевый песок для песочницы" src="/images/primenenie-peska/kvartsevyy-pesok-dlya-pesochnitsy.jpg" class="title-image__img" title="Кварцевый песок для песочницы" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_details.php"
	)
);?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на песок для песочницы</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20 тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
 <strong style="white-space: nowrap">1,25 - 2,5</strong> <br>
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td style="white-space: nowrap">
 <strong>0,63 - 1,5</strong> <br>
					 ГОСТ 2138
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0,63 - 1,25 мм" src="/images/fract-pesok/pesok-08-125-2138-91.gif" class="figure-mark__image" title="0,63 - 1,25 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0,63 - 1,25 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="1,25–2,5 мм" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" class="figure-mark__image" title="1,25–2,5 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 1,25–2,5 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 8736
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<p>
		 Кварцевый песок для песочницы — это однородный природный материал. Главные отличия — светлый или серый оттенок, чистота (промытый) и безопасность.
	</p>
	<p>
		 Главная особенность безопасности – это прокаливание песка в процессе обработки до 200 градусов Цельсия!
	</p>
	<p>
		 Подбирая песок для песочницы, важно учитывать, что для детей нужен качественный и чистый материал, именно таковым и является кварцевый песок.
	</p>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="svoystva">Свойства материала</h2>
	<p>
		 Главная особенность получения этого наполнителя — особая технология производства. Кварцевый песок для песочницы обязательно прокаливают. В результате этой процедуры полностью уничтожаются вредоносные бактерии, а также создаются условия для препятствования их повторного появления и размножения. Поэтому материал получается абсолютно безопасным для здоровья ребенка.
	</p>
	<div class="block-wrap">
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Производство кварцевого песка для песочницы" src="/images/kvartsevyy-pesok-v-big-begah/kvartsevyiy-pesok-dlya-pesochnitsyi/proizvodstvo-kvartsevogo-peska-dlya-pesochnitsyi.jpg" title="Производство кварцевого песка для песочницы" itemprop="contentUrl"> <figcaption itemprop="caption">
			Производство кварцевого песка для песочницы </figcaption> </figure>
		</div>
	</div>
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
	<h2>Вопросы и ответы</h2>
	<h3 class="map_toggle toggle-by faq-item-title">Как можно заказать кварцевый песок для песочницы в биг-бегах?</h3>
	<p class="map toggle-me faq-item-text">Заказать кварцевый песок для песочницы оптом можно через сайт компании или по телефону. Вам нужно указать необходимое количество и тип продукции. Менеджер свяжется с вами в течение дня, чтобы подтвердить заказ и обсудить детали. Мы предлагаем различные способы оплаты, включая наличный расчет и банковский перевод.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие характеристики имеет кварцевый песок для песочницы?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок для песочницы должен быть чистым, без примесей и острых камней. Он обычно имеет гранитный или речной состав, что обеспечивает безопасность детей при игре. Также важно, чтобы песок соответствовал техническим требованиям и имел сертификаты качества, подтверждающие его безопасность.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какова цена на кварцевый песок в биг-бегах?</h3>
	<p class="map toggle-me faq-item-text">Цена на кварцевый песок может варьироваться в зависимости от его качества и типа. Для получения актуального прайса рекомендуется обратиться к менеджерам компании. Мы также предлагаем скидки на оптовые заказы, что делает покупку более выгодной.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие виды кварцевого песка доступны для покупки?</h3>
	<p class="map toggle-me faq-item-text">В нашем ассортименте представлены различные виды кварцевого песка, включая карьерный, речной и крошку. Каждый из них подходит для создания безопасных условий в песочнице. Кроме того, мы также предлагаем известняковый и гравийный песок для других нужд. Подробную информацию о каждом виде можно найти в нашем прайсе.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как проверить качество кварцевого песка перед покупкой?</h3>
	<p class="map toggle-me faq-item-text">Качество кварцевого песка подтверждается сертификатами, которые предоставляются производителем. Рекомендуется запрашивать эти документы перед покупкой, чтобы убедиться в соответствии продукции техническим требованиям. Также стоит обратить внимание на отзывы клиентов о качестве товара.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Каковы сроки поставки кварцевого песка для песочниц?</h3>
	<p class="map toggle-me faq-item-text">Сроки поставки зависят от вашего местоположения и наличия товара на складе. Обычно доставка осуществляется в течение нескольких дней после оформления заказа. Мы обеспечиваем быструю доставку по всей России, включая Московскую область.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие дополнительные услуги предлагает компания при покупке кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Мы предлагаем услуги самовывоза для клиентов, которые предпочитают забирать товар самостоятельно. Также доступны услуги доставки по области, что значительно упрощает процесс получения товара. Уточните у менеджеров все доступные варианты при оформлении заказа.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Где можно узнать подробнее о применении кварцевого песка в песочницах?</h3>
	<p class="map toggle-me faq-item-text">Для получения более подробной информации о применении кварцевого песка в песочницах можно ознакомиться с материалами на нашем сайте или обратиться к специалистам компании. Они помогут ответить на ваши вопросы и предложат оптимальные решения для ваших нужд.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какой тип грунта лучше использовать вместе с кварцевым песком?</h3>
	<p class="map toggle-me faq-item-text">Для создания комфортной среды в песочнице рекомендуется использовать чистый речной грунт или керамзит в сочетании с кварцевым песком. Это обеспечит хорошую дренажную способность и предотвратит образование луж после дождя.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие реагенты можно использовать для обработки кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">При необходимости обработки кварцевого песка можно использовать противогололедные реагенты, которые помогут предотвратить образование льда в зимний период. Однако важно выбирать безопасные для детей реагенты, чтобы не нанести вреда здоровью.</p>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
