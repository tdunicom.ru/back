<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Песок для конного спорта оптом в биг-бегах - Продажа в Санкт-Петербурге и области");
$APPLICATION->SetPageProperty("description", "Реализуем Песок для конного спорта в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Песок для конного спорта Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Песок для конного спорта");
?><div class="container">
  <div class="content">


<div class="block-wrap block-wrap_wrap">
    <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
        <p class="subtitle">Продажа и доставка в Санкт-Петербурге и Ленинградской области</p>
                    <p>
	Песок используют для конного спорта: покрытия ипподрома, манежа и полей.
</p>

<p>
	Значительно чище, меньше примесей оксида алюминия и железа. Содержание кремния до 99%
</p>
<p>
	Кварцевый песок соответствует ГОСТ: 22551
</p>
<h2>Цены</h2>
    <div class="table-wrap">
        <table class="price-table">
          <tbody><tr>
            <th rowspan="2">Фракция</th>
            <th colspan="2">Цена, руб.</th>
          </tr>
          <tr>
            <td class="price-table__sub-th">Навалом</td>
            <td class="price-table__sub-th">В биг-бегах</td>
          </tr>
          <tr>
            <td><strong>0 - 0,3</strong>
              <br>ГОСТ 2138</td>
            <td>договорная</td>
            <td>договорная</td>
          </tr>
          <tr>
            <td><strong>0,3 - 0,63</strong>
              <br>ГОСТ 2138</td>
            <td>договорная</td>
            <td>договорная</td>
          </tr>
          <tr>
            <td><strong>0,63 - 1,25</strong>
              <br>ГОСТ 8736</td>
            <td>договорная</td>
            <td>договорная</td>
          </tr>
          <tr>
            <td><strong>1,25 - 2,5</strong>
              <br>ГОСТ 8736</td>
            <td>договорная</td>
            <td>договорная</td>
          </tr>
        </tbody></table>
    </div>
    <p>Узнайте полную стоимость вашего заказа у наших менеджеров.<br>
    Звоните по телефону: <strong>+7 (812) 414-97-93</strong></p><br>
<h2>Оптовая продажа и мелкий опт</h2>
<p>
	Оптовая продажа сухого кварцевого песка от 1 тонны. Доставка собственным автотранспортом: тонаром, цементовозом.
</p>            </div>
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
                    <img class="detail_picture" border="0" src="/upload/iblock/5d6/5d646f0e2fb85d63ce452bf976432534.jpg" alt="Песок для конного спорта" title="Песок для конного спорта">
            </div>
    <div class="block-wrap__item block-wrap__item_xl-width12 block-wrap__item_l-width12 block-wrap__item_m-width12 block-wrap__item_s-width6">
    	    		<div class="rozn-prod">
<p>
        <span class="rozn-prod-label">Опт - от 1 тонны</span><img src="/upload/medialibrary/79e/79ef40c225ac5b80511a0a3996118866.jpg" alt="">
	</p>
    <p><span class="rozn-prod-label">Биг-бэг - мешок около 1 тонны</span><img src="/upload/medialibrary/8bb/8bb7a8a92f5682b7c50dea06e9b9221e.jpg" alt=""></p>
</div>    	    </div>
</div>



<br>

</div>
</div>
<div class="container">
  <div class="content">








<div class="block-wrap block-wrap_wrap">
	<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">

<!--noindex-->
		<h2>Отправьте заявку для расчета стоимости</h2>
		<p>
				Или отправьте заявку со списком заказа на расчет стоимости.<br>
				Укажите в комментариях, куда доставить и мы рассчитаем стоимость доставки.<br>
				Перезвоним в течение 15 минут в рабочее время (пн-пт с 10:00-20:00).<br>
				Или на следующий рабочий день
		</p>
		<br>

		<h2>Собственное производство песка</h2>
		<p>ООО "Юником" обрабатывает кварцевый песок с 2012 года.</p>
		<p>Контролируем и ведем самостоятельно всю техническую цепочку: грубое просеивание, сушку, охлаждение, мелкое просеивание (фракционирование) и фасовка в биг-беги.</p>
<!--/noindex-->
	</div>
	<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
		<div class="obrazec_zakaza">
        <div class="obrazec_zakaza_title">Образец заказа</div>
        <a href="/download/zakaz_peska.xlsx">Скачать</a>
    </div>
	</div>
</div>
      </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
