<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Купить песок формовочный кварцевый лпк 5 гост 2138 91 по низкой цене за кг");
$APPLICATION->SetPageProperty("keywords", "пескоструйный песок купить для пескоструя");
$APPLICATION->SetPageProperty("description", "Реализуем Формовочный кварцевый песок в биг-бегах. Продажа оптом и в розницу. Расчитать стоимость Формовочный кварцевый песок Вы можете, отправив заявку через сайт. Доставка по Санкт-Петербургу и Ленинградской области.");
$APPLICATION->SetTitle("Формовочный песок");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Формовочный песок" src="/images/primenenie-peska/formovochnyy-kvartsevyy-pesok.jpg" class="title-image__img" title="Формовочный песок" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			<ul class="class list-marked list-marked_half">
				<li>песок соответствует <strong>ГОСТ 2138</strong></li>
				<li>песок содержит свыше&nbsp;<strong>98% кварца</strong>, без пыли и&nbsp;примесей</li>
				<li>позволяет <strong>экономить</strong> на&nbsp;количестве связующего материала </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на песок кварцевый (лпк 5) за тонну</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
            <div class="table-wrap">
                			<table class="table-price">
                			<tbody>
                			<tr>
                				<th>
                				</th>
                				<th>
                					 Навалом
                				</th>
                				<th colspan="3">
                					 Биг-бэги (за штуку, 1 тонна)
                				</th>
                				<th>
                					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
                				</th>
                			</tr>
                			<tr>
                				<td>
                				</td>
                				<td>
                				</td>
                				<td class="price-table__sub-th">
                					 до 10 тонн
                				</td>
                				<td class="price-table__sub-th">
                					 от 10 до 20 тонн
                				</td>
                				<td class="price-table__sub-th">
                					 больше 20 тонн
                				</td>
                				<td>
                				</td>
                			</tr>
                			<tr>
                				<td>
                					 ГОСТ 2138
                				</td>
                				<td colspan="5">
                					 договорная
                				</td>
                			</tr>
                			<tr>
                				<td>
                					 Под заказ
                				</td>
                				<td colspan="5">
                					 договорная
                				</td>
                			</tr>
                			</tbody>
                			</table>
            </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width5 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<p>
		 Формовочный песок (лпк 5) — это песок с&nbsp;высоким содержанием кварца, который используется для процессов изготовления форм, применяемых для производства крупного, среднего, мелкого стального и&nbsp;чугунного, цветного литья.
	</p>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Формовочный кварцевый песок" src="/images/kvartsevyy-pesok-v-big-begah/formovochnij-kvarcevii-pesok/formovochnij-kvarcevii-pesok.jpg" title="Формовочный кварцевый песок" itemprop="contentUrl"> <figcaption itemprop="caption">
			Формовочный кварцевый песок </figcaption> </figure>
		</div>
	</div>
	<h2 id="primenenie">Применение</h2>
	<p>
		 Кварцевый песок для формовки&nbsp;— это главный материал для изготовления литейных форм. С&nbsp;такими связующими материалами как жидкое стекло, синтетическая смола и&nbsp;другими, материал нашел широкое применение в&nbsp;литейном производстве.
	</p>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
 <figure class="image-inarticle" itemscope="" itemtype="http://schema.org/ImageObject"> <img alt="Литейные стержни" src="/images/kvartsevyy-pesok-v-big-begah/formovochnij-kvarcevii-pesok/litejnie-sterjni.jpg" title="Литейные стержни" itemprop="contentUrl"> <figcaption itemprop="caption">
			Литейные стержни </figcaption> </figure>
		</div>
	</div>
	<p>
		 Сферы применения:
	</p>
	<ol class="list-numbered">
		<li>Литейное производство: изготовление литейных форм.</li>
		<li>В&nbsp;газовой промышленности.</li>
		<li>В&nbsp;химической промышленности (смеси для разрыва гидропласта, для изготовления клеевых смесей)</li>
	</ol>
	<p>
		 Применение обогащенного формовочного песка экономит на&nbsp;дорогом связующем материале, гарантируя качественную формовочную смесь с&nbsp;нужными характеристиками.
	</p>
	<h2>Как выбрать фракцию формовочного песка</h2>
	<p>
		 При изготовлении высоких, больших форм для габаритных, сложных отливок, где тяжело создать равномерное разряжение во&nbsp;всем теле формы, желательно использовать кварцевый песок для литья более <strong>крупных фракций</strong>, обладающие повышенной газопроницаемостью.
	</p>
	<p>
		 Чем тоньше стенки литой детали, тем <strong>мельче фракция песка</strong>. Кроме того, мелкий песок используется для литья из&nbsp;цветных сплавов.
	</p>
	<h3>Критерии качества</h3>
	<p>
		 Качество формовочного песка оценивают по&nbsp;содержанию в&nbsp;нем диоксида кремния. Чем его больше, тем качественнее песок.
	</p>
	<ol class="list-numbered">
		<li> <strong>Цвет</strong> <br>
		 Зерна кварца являются прозрачными, твердыми и&nbsp;огнеупорными. Разные оттенки песку придают примеси. Чем светлее песок, тем меньше примесей и&nbsp;тем выше его огнеупорность. </li>
		<li> <strong>Однородность</strong> <br>
		 Важно, чтобы песчинки были однородными по&nbsp;размерам фракции. Когда коэффициент однородности достигает&nbsp;72%, песок считается качественным и&nbsp;пригодным для металлургии. </li>
	</ol>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0 - 0,63" src="/images/fract-pesok/pesok-0-063-2138-91.gif" class="figure-mark__image" title="0 - 0,63" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0 - 0,63
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0,4 - 0,63" src="/images/fract-pesok/pesok-03-06-2138-91.gif" class="figure-mark__image" title="0,4 - 0,63" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0,4 - 0,63
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2>Преимущества кварцевого песка для использования в&nbsp;формовке</h2>
	<p>
		Используют для литья форм в&nbsp;металлургии из-за таких качеств:
	</p>
	<ul class="list-marked">
		<li>Материал собирает минимум пыли в&nbsp;процессе формовки.</li>
		<li>Влажность не&nbsp;превышает 1,0%</li>
		<li>Высокая огнеупорность</li>
		<li>Экономичность</li>
		<li>Высокая уплотняемость</li>
		<li>Высокая газопроницаемость</li>
		<li>Низкие потери при прокаливании</li>
	</ul>
 <br>
	<p>
		 У&nbsp;нас вы&nbsp;можете купить песок кварцевый формовочный гост 2138 91&nbsp;в различной фасовке.
	</p>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/frakcii/sect_fasovka-navalom.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
	

</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> 
<section class="container page-section">
<div class="content">
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
	<h2>Вопросы и ответы</h2>
	<h3 class="map_toggle toggle-by faq-item-title">Что такое формовочный песок и каковы его свойства?</h3>
	<p class="map toggle-me faq-item-text">Формовочный песок — это высококачественный кварцевый песок, который используется для изготовления литейных форм в металлургии. Он обладает высокой прочностью, огнеупорностью и низкими потерями при прокаливании. Состав песка включает свыше 98% диоксида кремния, что обеспечивает его отличные характеристики для литья металлов. Благодаря своим свойствам, формовочный песок позволяет экономить на количестве связующего материала.</p>

	<h3 class="map_toggle toggle-by faq-item-title">В каких областях применяется формовочный песок?</h3>
	<p class="map toggle-me faq-item-text">Формовочный песок широко используется в литейном производстве для создания форм для стальных, чугунных и цветных металлов. Он также находит применение в газовой и химической промышленности, где используется в смесях для изготовления клеевых составов и других материалов. Кроме того, пески формовочные могут применяться в строительстве для создания прочных конструкций.</p>
	
	<h3 class="map_toggle toggle-by faq-item-title">Как выбрать подходящий размер зерна формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Выбор размера зерна формовочного песка зависит от типа литейной формы. Для крупных отливок рекомендуется использовать более крупные фракции, которые обеспечивают хорошую газопроницаемость. Мелкий песок подходит для деталей с тонкими стенками и цветных сплавов. Мы предлагаем различные фракции песка, чтобы вы могли выбрать оптимальный вариант для своих требований.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какова цена на формовочный песок и какие способы оплаты доступны?</h3>
	<p class="map toggle-me faq-item-text">Цены на формовочный песок зависят от его фракции и объема заказа. Для получения актуального прайса вы можете обратиться к нашему менеджеру или ознакомиться с информацией на сайте. Мы предлагаем гибкую систему оплаты, включая наличный и безналичный расчет, что делает процесс покупки удобным для всех клиентов.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Мы обеспечиваем доставку формовочного песка по Санкт-Петербургу и Ленинградской области на собственном транспорте. При самовывозе мы поможем погрузить материал на ваш транспорт. Доставка осуществляется быстро, что позволяет вам получить необходимые материалы в кратчайшие сроки.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Каковы преимущества использования вашего формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Наш формовочный песок предлагает высокое качество благодаря строгому контролю на всех этапах производства. Он собирает минимум пыли при использовании и обладает высокой уплотняемостью. Кроме того, мы предоставляем сертификаты качества на нашу продукцию, что подтверждает ее соответствие всем требованиям.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Можно ли получить консультацию по выбору формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Да, наши менеджеры готовы предоставить подробную информацию о нашей продукции и помочь вам выбрать подходящий формовочный песок в зависимости от ваших потребностей. Вы можете оставить заявку на сайте или позвонить нам для получения консультации.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на формовочный песок?</h3>
	<p class="map toggle-me faq-item-text">Чтобы оформить заказ, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие услуги вы предлагаете вместе с продажей формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Кроме продажи формовочного песка, мы предлагаем услуги по его доставке по всей России. Наши менеджеры помогут вам выбрать необходимый объем и фракцию, а также предоставят информацию о скидках на оптовые закупки. Мы также можем предложить дробленый кварцевый песок для других нужд.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Каковы химические свойства формовочного песка?</h3>
	<p class="map toggle-me faq-item-text">Формовочный песок имеет низкое содержание глин и других примесей, что делает его идеальным для литейного производства. Его химические свойства обеспечивают стабильность при высоких температурах и минимизируют реакции с металлами во время литья. Это гарантирует высокое качество отливок.</p>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
