<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для пескоструя - купить, пескоструйный песок с доставкой в Спб");
$APPLICATION->SetPageProperty("keywords", "пескоструйный песок купить для пескоструя");
$APPLICATION->SetPageProperty("description", "Пескоструйный песок в Санкт-Петербурге и области. Купить в компании «Юником». Цены на кварцевый песок для пескоструя, расчет стоимости в зависимости от веса.");
$APPLICATION->SetTitle("Кварцевый песок для пескоструя");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#kak-vybrat">Как выбрать?</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#raschet">Расчёт объёма</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Пескоструйный песок" src="/images/primenenie-peska/pesok-dlya-peskostruynykh-rabot.png" class="title-image__img" title="Пескоструйный песок" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "details",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цена кварцевого песка для пескоструя</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width10 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20 тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					 0 - 0,63<br>
					 ГОСТ 8736*
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
					 0,63 - 2,5<br>
					 ГОСТ 8736
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
					 0 - 0,63<br>
					 ГОСТ 2138 **
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td>
					 Под заказ - любая фракция
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width5 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_availability-instock.php"
	)
);?>
			<div class="small">
				ГОСТ 8736 * - содержание кварца около 70%
			</div>
			<div class="small">
				ГОСТ 2138 ** - содержание кварца около 90%
			</div>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<p>
		 Пескоструйная обработка — эффективный способ подготовки поверхностей к последующей обработке, путем очистки от загрязнений при помощи песочной дроби, подающейся под напором воздуха.
	</p>
	<h2 id="primenenie">Применение</h2>
	<p>
		 Кварцевый песок подходит для подготовки большинства материалов:
	</p>
	<ul class="list-marked">
		<li>металл</li>
		<li>дерево</li>
		<li>бетон</li>
		<li>кирпич</li>
		<li>пластик</li>
		<li>стекло (матирование)</li>
	</ul>
	<h2 id="kak-vybrat">Как выбрать песок?</h2>
	<p>
		 Выбор фракции песка для обработки определяет характер выполняемых работ, а также диаметр выходного сопла пескоструйного аппарата.
	</p>
	<ul class="list-marked">
		<li>
		Прочность и толщина удаляемого загрязнения<br>
		 Если выбрать слишком мелкий песок, пескоструй может не справиться с работой.</li>
		<li>
		Необходимое состояние поверхности для обработки<br>
		 Чем крупнее фракция, тем более грубой будет обработка.</li>
	</ul>
	<div class="block-wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width10 block-wrap__item_l-width10 block-wrap__item_m-width12 block-wrap__item_s-width6">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
					 Применение
				</th>
				<th>
					 Фракции, мм
				</th>
			</tr>
			<tr>
				<td>
					 Абразивная обработка хрупких материалов<br>
					 Чтобы ободрать стойкое лакокрасочное покрытие, не повредив материал
				</td>
				<td>
					 0,3 - 0,63
				</td>
			</tr>
			<tr>
				<td>
					 Начистить до блеска медную или алюминиевую поверхность
				</td>
				<td>
					 0,1 - 0,3
				</td>
			</tr>
			<tr>
				<td>
					 Для матирования стекла, нанесения красивых узорных пескоструйных рисунков на зеркала шкафа-купе и стеклянные межкомнатные двери
				</td>
				<td>
					 0 - 0,63
				</td>
			</tr>
			<tr>
				<td>
					 Удалить въевшуюся ржавчину
				</td>
				<td>
					 0,63 - 1,25
				</td>
			</tr>
			<tr>
				<td>
					 Грубая абразивная обработка<br>
					 удалить окалину после сварки или снять толстое битумное покрытие
				</td>
				<td>
					 1,25 - 2,5
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
	<h2>Подбор диаметра сопла и фракции песка</h2>
	<p>
		 Важно для качественной пескоструйной обработки и для разумного расхода абразива.
	</p>
	<p>
		 Максимальный размер зерна песка должен быть в три раза меньше диаметра отверстия сопла.
	</p>
	<p>
 <strong>Подобрать фракцию песка к соплу</strong><br>
		 Диаметр сопла делим на три. Получаем максимальный размер зерна песка.
	</p>
	<p>
 <strong>Подобрать сопло к фракции песка</strong><br>
		 Максимальный размер зерна песка умножаем на 3. Получаем оптимальны диаметр сопла. <br>
		 Используем сопло меньше - плохая проходимость абразива. Используем сопло больше - перерасход абразивного материла и плохая ударная способность.
	</p>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0–0,63 мм" src="/images/fract-pesok/pesok-0-063-8736-2014.gif" class="figure-mark__image" title="0–0,63 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0–0,63 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 8736
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0,63–2,5 мм" src="/images/fract-pesok/pesok-063-25-8736-2014.gif" class="figure-mark__image" title="0,63–2,5 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0,63–2,5 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 8736
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0–0,63 мм" src="/images/fract-pesok/pesok-0-063-2138-91.gif" class="figure-mark__image" title="0–0,63 мм" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0–0,63 мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2>Преимущества кварцевого песка для использования при пескоструйных работах</h2>
	<ul class="list-marked list-marked_full">
		<li>Дешевле других абразивов</li>
		<li>Возможность обрабатывать большие площади в короткие сроки</li>
		<li>Относительно невысокая прочность и твердость песка снижает&nbsp;вероятность нанесения обрабатываемой поверхности существенных повреждений (в течение короткого времени), если неправильно подобрана величина зерна. Можно безопасно работать с цветными металлами.</li>
		<li>Допустимы любые виды сопел. Скорость износа дорогостоящих сопел из карбида бора или карбида вольфрама в несколько раз меньше, чем при использовании более твердых абразивов.</li>
		<li>Широчайший диапазон фракций, что дает возможность точно подобрать материал для достижения нужного эффекта.</li>
		<li>Хорошая сыпучесть и высокая насыпная плотность – удобно работать и транспортировать материал</li>
		<li>Высокая прочность и долговечность — зерна практически не подвержены истиранию</li>
	</ul>
	<h2 id="raschet">Расчет необходимого объема песка</h2>
	<p>
		 Средний расход для очистки <strong>1 метра<sup>2</sup> – 60-110 кг.</strong>
	</p>
	<p>
		 В таблице представлен ориентировочный расход кварцевого песка для пескоструйного аппарата:
	</p>
	<div class="block-wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width10 block-wrap__item_l-width10 block-wrap__item_m-width12 block-wrap__item_s-width6">
			<table class="table-price">
			<tbody>
			<tr>
				<td rowspan="3" style="vertical-align:middle;width: 40%;">
					 Давление воздуха в аппарате
				</td>
				<td colspan="3">
					 Диаметр сопла, мм
				</td>
			</tr>
			<tr>
				<td>
					 3
				</td>
				<td>
					 5,5
				</td>
				<td>
					 6
				</td>
			</tr>
			<tr>
				<td colspan="3">
					 Расход песка, м<sup>3</sup>/час
				</td>
			</tr>
			<tr>
				<td>
					 3,5
				</td>
				<td>
					 23
				</td>
				<td>
					 73
				</td>
				<td>
					 103
				</td>
			</tr>
			<tr>
				<td>
					 7,0
				</td>
				<td>
					 43
				</td>
				<td>
					 133
				</td>
				<td>
					 175
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
	)
);?>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
	)
);?>
		</div>
	</div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-how-to-choose.php"
	)
);?>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="kak-kupit">Как купить песок для пескоструя?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
	<h2>Вопросы и ответы</h2>
	<h3 class="map_toggle toggle-by faq-item-title">Что такое кварцевый песок для пескоструя и каковы его основные характеристики?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок для пескоструя — это высококачественный абразивный материал, который используется для очистки и подготовки различных поверхностей. Он отличается высокой прочностью, низким содержанием примесей и хорошей сыпучестью. Песок бывает разных фракций, что позволяет выбрать оптимальный размер зерна в зависимости от типа обрабатываемого материала. Окатанный кварцевый песок идеально подходит для промышленных работ, так как он минимизирует риск повреждения поверхности.</p>

	<h3 class="map_toggle toggle-by faq-item-title">В каких сферах используется кварцевый песок для пескоструйных работ?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок используется в различных промышленных областях, включая металлургию, строительство и производство. Он подходит для обработки металлических, деревянных и бетонных поверхностей, а также для матирования стекла. Благодаря своим свойствам, кварцевый песок является идеальным выбором для подготовки поверхностей перед покраской или нанесением других покрытий.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как выбрать подходящую фракцию кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Выбор фракции кварцевого песка зависит от характера выполняемых работ. Для грубой обработки рекомендуется использовать более крупные фракции, такие как 0,63 - 1,25 мм. Для более деликатной работы, например, матирования стекла или удаления краски, лучше подойдут мелкие фракции от 0,1 до 0,3 мм. Правильный выбор фракции обеспечит эффективность обработки и защитит поверхность от повреждений.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какова цена на кварцевый песок для пескоструя?</h3>
	<p class="map toggle-me faq-item-text">Цена на кварцевый песок варьируется в зависимости от фракции и объема заказа. Мы предлагаем конкурентоспособные цены на всю продукцию. Для получения точной информации о стоимости и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашими менеджерами.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Мы осуществляем доставку кварцевого песка по Санкт-Петербургу и Ленинградской области на собственном транспорте. Доставка производится быстро и удобно. Также возможен самовывоз из нашего склада, который находится всего в 15 минутах от КАД. При самовывозе мы поможем погрузить материал на ваш транспорт.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие преимущества использования кварцевого песка для пескоструйных работ?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок обладает рядом преимуществ: он дешевле других абразивов, позволяет обрабатывать большие площади в короткие сроки и имеет высокую прочность. Кроме того, его использование снижает вероятность повреждения обрабатываемой поверхности благодаря относительной мягкости зерен. Это особенно важно при работе с цветными металлами и хрупкими материалами.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Каковы технические характеристики вашей продукции?</h3>
	<p class="map toggle-me faq-item-text">Наша продукция соответствует высоким стандартам качества и имеет сертификаты соответствия. Мы контролируем качество фракционирования в нашей лаборатории на сертифицированном оборудовании. Это гарантирует вам получение надежного материала для ваших производственных нужд.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на кварцевый песок для пескоструя?</h3>
	<p class="map toggle-me faq-item-text">Чтобы купить кварцевый песок для пескоструя, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Можно ли использовать дробленый камень вместо кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Дробленый камень может использоваться в некоторых случаях вместо кварцевого песка, однако он не всегда обеспечивает такую же эффективность при обработке поверхностей. Кварцевый песок имеет более однородную структуру и меньшее количество острых граней, что делает его более безопасным для обработки деликатных материалов.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какой белый кварцевый песок вы предлагаете?</h3>
	<p class="map toggle-me faq-item-text">Мы предлагаем белый кварцевый песок различных фракций, который идеально подходит для декоративных целей и использования в строительстве. Он часто применяется в производстве краски и других строительных материалов благодаря своей чистоте и эстетическим качествам.</p>

</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
