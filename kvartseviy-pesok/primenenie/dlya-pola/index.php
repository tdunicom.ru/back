<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Кварцевый песок для пола – Цена и  расход материала, купить в СПб");
$APPLICATION->SetPageProperty("keywords", "кварцевый песок пол цена расход материал");
$APPLICATION->SetPageProperty("description", "Кварцевый песок для пола в основном применяют в торговых центрах, паркингах и общественных учреждениях. Цена и расход материала будут различными в зависимости от площади заливки.");
$APPLICATION->SetTitle("Кварцевый песок для пола");
?><div class="container">
	<div class="content">
		<ul class="list-anchors">
			<li class="list-anchors__item"><a href="#tseny">Цены</a></li>
			<li class="list-anchors__item"><a href="#fasovka">Фасовка</a></li>
			<li class="list-anchors__item"><a href="#primery">Примеры фракций</a></li>
			<li class="list-anchors__item"><a href="#primenenie">Применение</a></li>
			<li class="list-anchors__item"><a href="#kak-kupit">Как купить?</a></li>
		</ul>
	</div>
</div>
 <section class="container page-section">
<div class="content">
	<div class="block-wrap block-wrap_wrap">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure class="title-image" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="Кварцевый песок для пола" src="/images/tsvetnoy-kvartsevyy-pesok/kvartsevyiy-pesok-dlya-pola/kvartsevyiy-pesok-dlya-pola.jpg" class="title-image__img" title="Кварцевый песок для пола" itemprop="contentUrl"> </figure>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width8 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_details.php"
	)
);?>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<h2 id="tseny">Цены на песок для пола</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width9 block-wrap__item_s-width6">
    <div class="table-wrap">
			<table class="table-price">
			<tbody>
			<tr>
				<th>
				</th>
				<th>
					 Навалом
				</th>
				<th colspan="3">
					 Биг-бэги (за штуку, 1 тонна)
				</th>
				<th>
					 Мешки&nbsp;25&nbsp;кг (мин. заказ - 1 тонна)
				</th>
			</tr>
			<tr>
				<td>
				</td>
				<td>
				</td>
				<td class="price-table__sub-th">
					 до 10 тонн
				</td>
				<td class="price-table__sub-th">
					 от 10 до 20 тонн
				</td>
				<td class="price-table__sub-th">
					 больше 20 тонн
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
 <strong style="white-space: nowrap">1,25 - 2,5</strong><br>
				</td>
				<td colspan="5">
					 договорная
				</td>
			</tr>
			<tr>
				<td style="white-space: nowrap">
 <strong>0,63 - 1,5</strong><br>
				</td>
				<td colspan="5">
					договорная
				</td>
			</tr>
			</tbody>
			</table>
    </div>
 <a href="/tseny/">Все цены</a>
		</div>
		<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width3 block-wrap__item_s-width6">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_availability-instock.php"
	)
);?>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_calc-anchor.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="fasovka">Фасовка</h2>
	<div class="block-wrap block-wrap_wrap ">
		<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
          "AREA_FILE_SHOW" => "file",
          "COMPONENT_TEMPLATE" => ".default",
          "EDIT_TEMPLATE" => "",
          "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-navalom.php"
        )
      );?>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
          "AREA_FILE_SHOW" => "file",
          "COMPONENT_TEMPLATE" => ".default",
          "EDIT_TEMPLATE" => "",
          "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-big-bag.php"
        )
      );?>
    </div>
    <div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
      <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        ".default",
        Array(
          "AREA_FILE_SHOW" => "file",
          "COMPONENT_TEMPLATE" => ".default",
          "EDIT_TEMPLATE" => "",
          "PATH" => "/kvartsevyy-pesok-v-big-begah/sect_fasovka-meshok.php"
        )
      );?>
    </div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/tsvetnoy-kvartsevyy-pesok/sect_advantages.php"
	)
);?> <section class="container page-section">
<div class="content">
	<h2 id="primery">Примеры фракций</h2>
	<div class="flexslider-container">
		<div class="flexslider">
			<ul class="slides">
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0 - 0,63" src="/images/fract-pesok/pesok-063-25-8736-2014.gif" class="figure-mark__image" title="0 - 0,63" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					0,63 - 1,25
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"><img alt="0,4 - 0,63" src="/images/fract-pesok/pesok-125-25-8736-2017.gif" class="figure-mark__image" title="0,4 - 0,63" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					1,25 - 2,5
				</div>
				<div class="figure-mark__desc" itemprop="description">
					ГОСТ 8736
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/fract-pesok/pesok-03-06-2138-91.gif" alt="" class="figure-mark__image" title="" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0-0,63&nbsp;мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/fract-pesok/pesok-03-063-2138-91.gif" alt="" class="figure-mark__image" title="" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0,315-0,63&nbsp;мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
				<li> <figure class="figure-mark" itemscope="" itemtype="http://schema.org/ImageObject"> <img src="/images/fract-pesok/pesok-01-03-2138-91.gif" alt="" class="figure-mark__image" title="" itemprop="contentUrl"> <figcaption class="figure-mark__caption" itemprop="caption">
				<div class="figure-mark__title" itemprop="name">
					 0-0,315&nbsp;мм
				</div>
				<div class="figure-mark__desc" itemprop="description">
					 ГОСТ 2138
				</div>
 </figcaption> </figure> </li>
			</ul>
		</div>
	</div>
</div>
 </section> <section class="container page-section">
<div class="content">
	<p id="primenenie">
		Наливные полы создают в торговых центрах, паркингах, гаражах и общественных учреждениях. Крайне редко их делают в загородных домах. Кварцевый песок для пола преимущественно используют в качестве главного компонента, а связующим ингредиентом бывает полиуретан либо эпоксидная смола.
	</p>
	<p>
		Поскольку в состав напольного покрытия может входить песок любого размера, а также красители самого разнообразного цвета, существенно повышаются эстетические функции материала. Например, с помощью него можно создавать специализированную разметку или узоры.
	</p>
	<h3>Преимущества наливных полов</h3>
	<ol class="list-marked">
		<li>Отсутствие швов. Нанесение финишного слоя позволяет добиться полной герметичности.</li>
		<li>Экологичность. Напольные покрытия не становятся источником вредоносных веществ при повышении температурного режима или внешнем воздействии.</li>
		<li>Стойкость к химическому воздействию. Кварцевый песок для пола не вступает во взаимодействие с моющими растворами.</li>
		<li>Безопасность. Напольные покрытия этого вида не воспламеняются.</li>
		<li>Антискользящая поверхность. Даже мельчайшая фракция песка способствует формированию шероховатой поверхности, препятствующей скольжению, повышая сцепление.</li>
		<li>Высокая стойкость к износу. В обычных условиях кварц очень прочен, а в сочетании с полимерами этот критерий возрастает в разы. </li>
	</ol>
	<p>
		Если в точности следовать всем правилам технологии укладки, качественно подготовить основание, то напольные покрытия с кварцевым песком прослужат от одного до двух десятилетий. Срок долговечности определяется интенсивностью использования.
	</p>
	<h2 id="kak-kupit">Как купить?</h2>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "how-to-buy",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/kvartsevyy-pesok-v-big-begah/sect_how-to-buy.php"
	)
);?>
	<h2>Вопросы и ответы</h2>
	<h3 class="map_toggle toggle-by faq-item-title">Что такое кварцевый песок для пола и каковы его основные свойства?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок для пола — это специальный материал, который используется в наливных полах. Он обеспечивает прочность и долговечность покрытия благодаря высокому содержанию кремния и оксида железа в составе. Песок имеет округлую форму зерен (окатанный), что способствует лучшему сцеплению с полимерными связующими, такими как полиуретан или эпоксидная смола. Это делает полы более устойчивыми к механическим повреждениям и химическим веществам, включая соль и другие агрессивные химикаты.</p>

	<h3 class="map_toggle toggle-by faq-item-title">В каких сферах используется кварцевый песок для пола?</h3>
	<p class="map toggle-me faq-item-text">Кварцевый песок применяется в различных сферах, включая торговые центры, паркинги и промышленные объекты. Он является основным компонентом наливных полов, которые часто используются в общественных учреждениях и коммерческих помещениях. Благодаря своим свойствам, кварцевый песок также может использоваться в качестве наполнителя для других строительных смесей, таких как керамзитобетон и различные типы растворов.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какова цена на кварцевый песок для пола?</h3>
	<p class="map toggle-me faq-item-text">Цена на кварцевый песок зависит от фракции и объема заказа. Мы предлагаем конкурентоспособные цены на весь ассортимент нашей продукции, включая карьерный и речной песок для различных нужд. Для получения точной информации о стоимости и возможных скидках при больших объемах заказа, пожалуйста, свяжитесь с нашим менеджером.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как осуществляется доставка кварцевого песка в Санкт-Петербурге и области?</h3>
	<p class="map toggle-me faq-item-text">Мы осуществляем доставку кварцевого песка по Санкт-Петербургу и Ленинградской области на собственном транспорте. Доставка производится быстро и удобно, что позволяет вам получить материал в кратчайшие сроки после оформления заказа. Также возможен самовывоз из нашего склада, который находится всего в 15 минутах от КАД.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие преимущества имеют наливные полы с использованием кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Наливные полы с кварцевым песком обладают рядом преимуществ: отсутствием швов, высокой стойкостью к износу и химическому воздействию. Они также экологичны и безопасны, так как не выделяют вредных веществ при повышении температуры. Кроме того, антискользящая поверхность обеспечивает безопасность при эксплуатации.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какой размер зерна у вашего кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Мы предлагаем различные фракции кварцевого песка, включая мелкие (например, 0-0,63 мм) и крупные зерна (до 2 мм). Размер зерна влияет на свойства наливного пола: мелкие фракции обеспечивают гладкость поверхности, а крупные — прочность и удобное использование в различных строительных проектах.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как правильно укладывать полы с использованием кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">Правильная укладка полимерных полов с добавлением кварцевого песка требует тщательной подготовки основания и соблюдения всех правил технологии укладки. Если следовать всем рекомендациям, такие полы могут прослужить от одного до двух лет в зависимости от интенсивности использования.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какие статьи можно почитать о применении кварцевого песка?</h3>
	<p class="map toggle-me faq-item-text">На нашем сайте вы можете найти статьи о различных способах применения кварцевого песка в строительстве и ремонте, а также о его свойствах при использовании в наливных полах и других строительных смесях.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Какой грунт лучше использовать под наливной пол?</h3>
	<p class="map toggle-me faq-item-text">Для обеспечения прочности наливного пола рекомендуется использовать ровный грунт без трещин и неровностей. Это может быть бетонное основание или специальная стяжка. Правильная подготовка основания значительно увеличивает срок службы покрытия.</p>

	<h3 class="map_toggle toggle-by faq-item-title">Как оформить заказ на кварцевый песок для пола?</h3>
	<p class="map toggle-me faq-item-text">Чтобы купить кварцевый песок для пола оптом в биг-бэгах, вы можете отправить заявку через наш сайт или позвонить по указанным телефонам. Мы предоставим вам всю необходимую информацию о наличии продукции и сроках поставки. Также вы можете скачать образец заказа и отправить его на нашу почту для быстрого оформления.</p>

</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
