<div class="container home-section home-section_home-manufacturing">
  <div class="content">
    <div class="block-wrap home-manufacturing block-wrap_nopadding block-wrap_wrap ">
      <div class="block-wrap__item block-wrap__item_xl-width7 block-wrap__item_l-width7 block-wrap__item_m-width7 block-wrap__item_s-width6">
        <h2>Собственное производство по&nbsp;переработке сырья</h2>
        <p>ООО «Юником» реализует песок с&nbsp;2012&nbsp;года.</p>
        <p>Специализируемся на&nbsp;переработке различного вида песка, гравия. Контролируем и&nbsp;ведем всю технологическую цепочку: сушка, грубое просеивание, охлаждение, мелкое просеивание (фракционирование), фасовка.</p>
        <p>В&nbsp;2020 году компания запустила завод по&nbsp;производству сухих строительных смесей на&nbsp;современном технологичном оборудовании.</p>
        <p>Все процессы производства контролируются нашей лабораторией!</p>
      </div>
    </div>
  </div>
</div>
