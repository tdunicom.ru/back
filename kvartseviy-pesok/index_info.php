
<div class="container home-section home-section_home-info">
	<div class="content">
		<div class="block-wrap  block-wrap_wrap ">
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Как выбрать кварцевый песок?</h2>
        <p>Различные виды кварцевого песка обладают своими особенностями.</p>
        <p>Для того чтобы его использование было максимально эффективны, при выборе необходимо учитывать следующие факторы.</p>
        <h3>Выбрать ГОСТ</h3>
        <p>ГОСТ 2138 (формовочный песок) или ГОСТ 8736 (строительный песок)</p>
        <h3>Выбрать применение: строительный или формовочный</h3>
        <p>В строительном песке кварца около 60-70% (ГОСТ 8736: строительный песок), в формовочном и стекольном до 99% (ГОСТ 2138: формовочный песок).</p>
        <h3>Выбрать фракцию</h3>
        <p>Кварцевый песок при помощи виброустановок в&nbsp;зависимости от&nbsp;размеров частичек разделяют на&nbsp;фракции. Каждая фракция имеет свою сферу применения.</p>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6">
				<h2>Как рассчитать нужный объем?</h2>
				<h3>Для пескоструя</h3>
				<p>Средний расход для очистки 1&nbsp;м<sup>2</sup> – 60-110&nbsp;кг.</p>
				<h3>Для фильтров бассейнов</h3>
				<p>для бассейна средних размеров (вместимость чаши около 50&nbsp;м<sup>3</sup>) требуется 50-100 &nbsp;г песка</p>
				<h3>Для спортивных покрытий</h3>
				<p>Для поля с размером в 800&nbsp;м<sup>2</sup> потребуется 12&nbsp;тонн песка фракции 0,1 - 0,63&nbsp;мм</p>
        <h3>Для наливных полов</h3>
        <p>Расход на 1&nbsp;м<sup>2</sup> при толщине 1&nbsp;мм составляет 1,5&nbsp;кг.</p>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "callback",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => ""
	)
);?>

<div class="container home-section home-section_home-how-to-buy">
	<div class="content">
		<h2>Как купить кварцевый песок?</h2>

		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			".default",
			Array(
				"AREA_FILE_RECURSIVE" => "Y",
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "how-to-buy",
				"COMPONENT_TEMPLATE" => ".default",
				"EDIT_TEMPLATE" => ""
			)
		);?>
	</div>
</div>
<div class="container home-section home-section_home-info">
	<div class="content">
		<h2>Свойства кварцевого песка</h2>
		<p>Кварцевый песок — это однородный материал природного происхождения, полученный из кварца. Основным компонентом химического состава является кремнезем, или диоксид кремния (SiO<sub>2</sub>).</p>
		<p>Среди преимуществ, которые имеет кварцевый песок — цена.</p>
		<ul class="list-marked list-marked_full">
			<li><strong>Мономинеральность</strong> – однородный и беспримесный химический состав</li>
			<li><strong>Высокая пористость</strong> – может впитывать большую часть примесей из жидкости</li>
			<li><strong>Хорошие сорбционные свойства</strong> – способность впитывать большинство примесей, содержащихся в жидкости;</li>
			<li><strong>Химическая пассивность</strong> — не взаимодействует с другими химическими материалами</li>
			<li><strong>Хорошая сыпучесть и высокая насыпная плотность</strong> – удобно использовать и перевозить такой материал</li>
			<li><strong>Высокая твердость и долговечность</strong> — частицы почти не истираются</li>
			<li><strong>Радиационная безопасность</strong> – кварцевый песок не абсорбирует излучение</li>
			<li><strong>Возможность окраски</strong> – можно легко окрасить в любой цвет</li>
		</ul>
	</div>
</div>
