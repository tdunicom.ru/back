<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Цены");
?><div class="container">
  <div class="content">
    <div class="block-wrap block-wrap_wrap">
      <div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width12 block-wrap__item_s-width6">
        <p>
           Осуществляем доставку по Северо-Западному региону, Санкт-Петербургу и Ленинградской области.
        </p>
        <ul class="list-marked">
          <li>От производителя без наценки посредников.</li>
          <li>Работаем по безналичному расчёту с НДС и за наличный расчёт.</li>
          <li>Рассрочка оплаты для постоянных заказчиков</li>
        </ul>
 <p>Цены узнавайте у наших менеджеров.</p>
        <? /* <table class="price-table">
          <tbody>
            <tr>
              <th>
              </th>
              <th colspan="3">
                 Цена, руб.
              </th>
            </tr>
            <tr>
              <th>
                 Фракция / фасовка
              </th>
              <td class="price-table__sub-th">
                 Навалом, тонна
              </td>
              <td class="price-table__sub-th">
                 Биг-бэги (за штуку, тонна)
              </td>
              <td class="price-table__sub-th">
                 Мешки&nbsp;25&nbsp;кг, 50&nbsp;кг; тонна
              </td>
            </tr>
            <tr>
              <td><strong>0-0,63</strong><br>ГОСТ 8736-2014</td>
              <td>1550</td>
              <td>1700</td>
              <td>3000</td>
            </tr>
            <tr>
              <td><strong>0,63-2,5</strong><br>ГОСТ 8736-2014</td>
              <td>1800</td>
              <td>1950<br></td>
              <td>3000<br></td>
            </tr>
            <tr>
              <td><strong>0-2,5</strong><br>ГОСТ 8736-2014</td>
              <td>1750</td>
              <td>1900</td>
              <td>3000<br></td>
            </tr>
            <tr>
              <td><strong>0,2-1,25</strong><br>ГОСТ 8736-2014</td>
              <td>1950</td>
              <td>2100</td>
              <td>3000</td>
            </tr>
            <tr>
              <td><strong>1,25-2,5</strong><br>ГОСТ 8736-2014</td>
              <td>1850</td>
              <td>2000</td>
              <td>3000</td>
            </tr>
            <tr>
              <td><strong>2,5-5</strong><br>ГОСТ 8736-2014</td>
              <td>1850</td>
              <td>2000</td>
              <td>3000</td>
            </tr>
            <tr>
              <td><strong>песок сухой кварцевый </strong><br>ГОСТ 2138-91</td>
              <td>2100</td>
              <td>2350</td>
              <td>3500</td>
            </tr>
            <tr>
              <td colspan="4">
                 все цены указаны с учетом НДС 20%
              </td>
            </tr>
          </tbody>
        </table> */ ?>
        <h2>Фасовка</h2>
        <ul class="nice">
          <li>Навалом (минимальная заказ 20 тонн)</li>
          <li>В биг-бегах (мешок весом - 1 тонна)</li>
          <li>В мешках (по 25 кг, 50 кг)</li>
        </ul>
        <p>
           Поможем <strong>выбрать</strong> подходящие фракции под ваши потребности, а также <strong>рассчитаем стоимость</strong> <a href="/dostavka/">доставки</a> до вашего объекта.
        </p>
        <p>
           Поставляем кварцевый песок различных фракций. Под заказ возможно производство <strong>определенной фракции</strong>.
        </p>
      </div>
    </div>
  </div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
