<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Проекты");
$APPLICATION->SetTitle("Проекты");
?>

<div class="container">
  <div class="content">
    <div class="block-wrap block-wrap_wrap">
      <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6 ">
        <img src="/images/kompaniya/proekty/damba.jpg" alt="Ремонт дамбы">
        <p>Поставка сухого песка для ремонта дамбы в&nbsp;<span style='white-space: nowrap;'>Санкт-Петербурге</span></p>
      </div>
    </div>
    <br>
    <div class="block-wrap block-wrap_wrap">
      <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width6 block-wrap__item_s-width6 ">
        <img src="/images/kompaniya/proekty/petropavlovskaya-krepost.jpg" alt="Петропавловская крепость">
        <p>Поставка сухого песка для пескоструйных работ при чистке фасадов Петропавловской&nbsp;крепости</p>
      </div>
    </div>
    <br>
    <div class="block-wrap block-wrap_wrap">
      <div class="block-wrap__item block-wrap__item_xl-width8 block-wrap__item_l-width8 block-wrap__item_m-width10 block-wrap__item_s-width6 ">
        <img src="/images/kompaniya/proekty/novaya-gollandiya.jpg" alt="Новая Голландия">
        <p>Поставка сухого песка для пескоструйных работ при чистке фасадов в&nbsp;Новой&nbsp;Голландии</p>
      </div>
    </div>
  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
