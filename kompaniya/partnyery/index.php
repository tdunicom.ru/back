<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Партнёры");
$APPLICATION->SetTitle("Партнёры");
?><div class="container">
	<div class="content">
		<div class="block-wrap block-wrap_wrap ">
			<div class="block-wrap__item block-wrap__item_xl-width9 block-wrap__item_l-width9 block-wrap__item_m-width8 block-wrap__item_s-width6">
				<div class="block-wrap block-wrap_wrap ">
					<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure>
						<div class="logo-text">
							<div class="logo-text__inner">
								 Лужский ГОК
							</div>
						</div>
 <figcaption>Лужский горно-обогатительный комбинат</figcaption> </figure>
					</div>
					<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure>
						<div class="logo-text">
							<div class="logo-text__inner">
								 ООО "КВАРЦ"
							</div>
						</div>
 <figcaption>Карьеры в Кировском районе Ленинградской области</figcaption> </figure>
					</div>
					<div class="block-wrap__item block-wrap__item_xl-width4 block-wrap__item_l-width4 block-wrap__item_m-width4 block-wrap__item_s-width6">
 <figure>
						<div class="logo-text">
							<div class="logo-text__inner">
								 АТП-78
							</div>
						</div>
 <figcaption>Перевозка грузов различных габаритов</figcaption> </figure>
					</div>
				</div>
			</div>
			<div class="block-wrap__item block-wrap__item_xl-width3 block-wrap__item_l-width3 block-wrap__item_m-width4 block-wrap__item_s-width6">
				<p>
 <img alt="Казаков Александр Владимирович" src="/images/kompaniya/kazakov.jpg">
				</p>
				<p>
					 Здравствуйте, меня зовут<br>
					 Волков Александр Сергеевич,<br>
					 директор ООО "Юником"
				</p>
				<p>
					 Если у вас есть предложения по сотрудничеству буду рад их рассмотреть. Присылайте на почту:<br>
 <a href="mailto:info@tdunicom.ru" title="Написать письмо">info@tdunicom.ru</a>
				</p>
			</div>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
