<div class="container home-section home-section_home-get-price">
  <div class="content">
    <div class="block-wrap home-get-price block-wrap_wrap ">
      <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-get-price__title">
        <span>У вас большие объемы?</span>
        <span>Вам большие скидки!</span>
      </div>
      <div class="block-wrap__item block-wrap__item_xl-width6 block-wrap__item_l-width6 block-wrap__item_m-width6 block-wrap__item_s-width6 home-get-price__form">
        <div class="home-get-price__form-title">Отправьте заявку и получите доступ к оптовым ценам</div>
        <form class="get-price-form form validated-form">

          <div class="form__input">
            <input type="text" placeholder="Ваше имя" name="name" class="validated required">
            <div class="validation-msg">Обязательное поле</div>
          </div>

          <div class="form__input">
            <input type="email" placeholder="Ваш E-mail" name="email" class="validated required">
            <div class="validation-msg">Обязательное поле</div>
          </div>

          <div class="form__input">
            <input type="tel" placeholder="Ваш телефон" name="tel" class="validated required">
            <div class="validation-msg">Обязательное поле</div>
          </div>

          <div class="form__input">
            <textarea placeholder="Объем, фракция, фасовка, ГОСТ (если знаете)" name="message" class="validated required"></textarea>
            <div class="validation-msg">Обязательное поле</div>
          </div>

          <input class="btn" type="submit" value="Получить оптовый прайс">

        </form>
      </div>
    </div>
  </div>
</div>
