<div class="container home-section">
  <div class="content">
    <div class="banner-home banner-home_big-bag">
      <div class="banner-home__inner">
        <div class="banner-home__main-text">
          <div class="banner-home__action">Акция</div><span>Песок для пескоструя</span><span>за 1600 за тонну в биг-бегах</span>
          <div class="banner-home__mark">* При оплате по безналичному расчету с НДС.</div>
        </div>
        <div class="banner-home__countdown">
          <div class="banner-home__countdown-title">До конца акции осталось:</div>
          <div class="banner-home-countdown"></div>
        </div>
      </div>
    </div>
  </div>
</div>
